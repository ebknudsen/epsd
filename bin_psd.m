function [D]=bin_psd(a,pw,ph,xw,yh,Emin,Emax,de)

nx=round(xw/pw);
ny=round(yh/ph);

[nx,ny]

D=zeros((Emax-Emin)/de*nx*ny,4);
size(D)
xmask=a(:,2)>-xw/2 & a(:,2)<xw/2;
ymask=a(:,3)>-yh/2 & a(:,3)<yh/2;
emask=a(:,4)>Emin & a(:,4)<Emax;

aa=a((xmask & ymask & emask),:);

for i=1:size(aa,1)
  ei=floor((aa(i,4) - Emin)/de);
  xi=floor((aa(i,2) +xw/2)/pw);
  yi=floor((aa(i,3) +yh/2)/ph);
  
  D(ei*nx*ny+yi*nx+xi,1)=D(ei*nx*ny+yi*nx+xi,1)+aa(i,1);
%  D(ei*nx*ny+yi*nx+xi,4)=Emin+de*ei+.5*de;
%  D(ei*nx*ny+yi*nx+xi,2)=-xw/2+pw*xi+.5*pw;
%  D(ei*nx*ny+yi*nx+xi,3)=-yh/2+ph*yi+.5*ph;


end

for i=1:size(a,1)
  ei=floor((a(i,4) - Emin)/de);
  xi=floor((a(i,2) +xw/2)/pw);
  yi=floor((a(i,3) +yh/2)/ph);
  D(ei*nx*ny+yi*nx+xi,4)=Emin+de*ei+.5*de;
  D(ei*nx*ny+yi*nx+xi,2)=-xw/2+pw*xi+.5*pw;
  D(ei*nx*ny+yi*nx+xi,3)=-yh/2+ph*yi+.5*ph;

end
