#!/usr/bni/env ruby -w

nx=500
dpw=100e-6
de=0.01
emin=11.75
emax=13.25

nE=((emax-emin)/de).to_i
puts nE 

width=nx*dpw
pps=Array.new(nx)do |i|
  -width/2 + dpw/2 + dpw*i
end


d=Array.new(nE*nx*nx,0.to_f)

lines=IO.readlines('gala/maxlab_sx_epsd/long/event_mon_1319070576_list.p.x.y.Ebak')
lines.each do |l|
  f=l.split
  if  f[0] !="#" && f[0]!=nil && f[0] !="%"
    xx=f[1].to_f
    yy=f[2].to_f
    ee=f[3].to_f
    if (xx>-width/2 && xx<width/2 && yy>-width/2 && yy<width/2 && ee >emin && ee<emax)
#      puts f 
      xi=( (f[1].to_f+width/2)/dpw).to_i
      yi=( (f[2].to_f+width/2)/dpw).to_i
      ei=( (f[3].to_f - emin)/de).to_i
#      puts xi,yi,ei
#      puts ei*nx*nx+yi*nx+xi
#      puts d[ei*nx*nx+yi*nx+xi]
      d[ei*nx*nx+yi*nx+xi]=d[ei*nx*nx+yi*nx+xi] + f[0].to_f
    end
  end
end

energies=Array.new(nE) do |i|
  emin+de*i
end

energies.each do |e|
  ei=energies.rindex(e)
  pps.each do |x|
    xi=pps.rindex(x)
    pps.each do |y|
      yi=pps.rindex(y)
      pp=d[ei*nx*nx+yi*nx+xi]
      if pp>0
        puts "%g %g %g %g" % [x,y,e,d[ei*nx*nx+yi*nx+xi]]
      end
    end
  end
end

