/* Automatically generated file. Do not edit. 
 * Format:     ANSI C source code
 * Creator:    McXtrace <http://www.mcxtrace.org>
 * Instrument: Test_src_gaussian.instr (Test_src_gaussian)
 * Date:       Tue Feb  5 17:47:21 2013
 * File:       Test_src_gaussian.c
 */


#define MCCODE_STRING "McXtrace x.y.z - Jan. 22, 2013"
#define FLAVOR "mcxtrace"
#define FLAVOR_UPPER "MCXTRACE"
#define MC_USE_DEFAULT_MAIN
#define MC_TRACE_ENABLED
#define MC_EMBEDDED_RUNTIME

#line 1 "mccode-r.h"
/*******************************************************************************
*
* McCode, neutron/xray ray-tracing package
*         Copyright (C) 1997-2009, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* Runtime: share/mcstas-r.h
*
* %Identification
* Written by: KN
* Date:    Aug 29, 1997
* Release: McXtrace x.y.z
* Version: $Revision: 1.112 $
*
* Runtime system header for McStas.
*
* In order to use this library as an external library, the following variables
* and macros must be declared (see details in the code)
*
*   struct mcinputtable_struct mcinputtable[];
*   int mcnumipar;
*   char mcinstrument_name[], mcinstrument_source[];
*   int mctraceenabled, mcdefaultmain;
*   extern MCNUM  mccomp_storein[];
*   extern MCNUM  mcAbsorbProp[];
*   extern MCNUM  mcScattered;
*   #define MCCODE_STRING "the McStas/McXtrace version"
*
* Usage: Automatically embbeded in the c code.
*
* $Id: mcstas-r.h,v 1.112 2009/08/13 14:52:01 farhi Exp $
*
*******************************************************************************/

#ifndef MCCODE_R_H
#define MCCODE_R_H "$Revision: 1.0 $"

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <limits.h>
#include <errno.h>
#include <time.h>
#include <float.h>

/* If the runtime is embedded in the simulation program, some definitions can
   be made static. */

#ifdef MC_EMBEDDED_RUNTIME
#define mcstatic static
#else
#define mcstatic
#endif

#ifdef __dest_os
#if (__dest_os == __mac_os)
#define MAC
#endif
#endif

#ifdef __FreeBSD__
#define NEED_STAT_H
#endif

#if defined(__APPLE__) && defined(__GNUC__)
#define NEED_STAT_H
#endif

#ifdef NEED_STAT_H
#include <sys/stat.h>
#endif

#ifndef MC_PATHSEP_C
#ifdef WIN32
#define MC_PATHSEP_C '\\'
#define MC_PATHSEP_S "\\"
#define random rand
#define srandom srand
#else  /* !WIN32 */
#ifdef MAC
#define MC_PATHSEP_C ':'
#define MC_PATHSEP_S ":"
#else  /* !MAC */
#define MC_PATHSEP_C '/'
#define MC_PATHSEP_S "/"
#endif /* !MAC */
#endif /* !WIN32 */
#endif /* MC_PATHSEP_C */

/* the version string is replaced when building distribution with mkdist */
#ifndef MCCODE_STRING
#define MCCODE_STRING "McXtrace x.y.z - Jan. 22, 2013"
#endif

#ifndef MCCODE_DATE
#define MCCODE_DATE "Jan. 22, 2013"
#endif

#ifndef MCCODE_VERSION
#define MCCODE_VERSION "x.y.z"
#endif

#ifndef MCCODE_NAME
#define MCCODE_NAME "McXtrace"
#endif

#ifndef MCCODE_PARTICULE
#define MCCODE_PARTICULE "xray"
#endif

#ifndef MCCODE_LIBENV
#define MCCODE_LIBENV "MCXTRACE"
#endif

#ifndef FLAVOR_UPPER
#define FLAVOR_UPPER MCCODE_NAME
#endif

#ifdef MC_PORTABLE
#ifndef NOSIGNALS
#define NOSIGNALS 1
#endif
#endif

#ifdef MAC
#ifndef NOSIGNALS
#define NOSIGNALS 1
#endif
#endif

#if (USE_MPI == 0)
#undef USE_MPI
#endif

#ifdef USE_MPI  /* default is to disable signals with MPI, as MPICH uses them to communicate */
#ifndef NOSIGNALS
#define NOSIGNALS 1
#endif
#endif

#if (NOSIGNALS == 0)
#undef NOSIGNALS
#endif

#if (USE_NEXUS == 0)
#undef USE_NEXUS
#endif

/* I/O section part ========================================================= */

/* Note: the enum instr_formal_types definition MUST be kept
   synchronized with the one in mcstas.h and with the
   instr_formal_type_names array in cogen.c. */
enum instr_formal_types
  {
    instr_type_double, instr_type_int, instr_type_string
  };
struct mcinputtable_struct { /* defines instrument parameters */
  char *name; /* name of parameter */
  void *par;  /* pointer to instrument parameter (variable) */
  enum instr_formal_types type;
  char *val;  /* default value */
};

typedef double MCNUM;
typedef struct {MCNUM x, y, z;} Coords;
typedef MCNUM Rotation[3][3];

/* the following variables are defined in the McStas generated C code
   but should be defined externally in case of independent library usage */
#ifndef DANSE
extern struct mcinputtable_struct mcinputtable[]; /* list of instrument parameters */
extern int    mcnumipar;                          /* number of instrument parameters */
extern char   mcinstrument_name[], mcinstrument_source[]; /* instrument name and filename */
extern MCNUM  mccomp_storein[]; /* 11 coords * number of components in instrument */
extern MCNUM  mcAbsorbProp[];
extern MCNUM  mcScattered;      /* number of SCATTER calls in current component */
#ifndef MC_ANCIENT_COMPATIBILITY
extern int mctraceenabled, mcdefaultmain;
#endif
#endif

/* file I/O definitions and function prototypes */

struct mcformats_struct {
  char *Name;       /* format name: may also specify: append, partial(hidden), binary */
  char *Extension;  /* file name extension set if not specified */
  char *Header;     /* to be written as header(begin) in output file */
  char *Footer;     /* to be written as foorter(end) in output file */
  char *BeginSection; /* defines how to start a new section */
  char *EndSection;   /* defines how to end a new section */
  char *AssignTag;    /* defines how to write e.g. 'par=value' */
  char *BeginData;    /* defines how to start a data block */
  char *EndData;      /* defines how to end a data block */
  char *BeginErrors;  /* defines how to start an error(statistical) block */
  char *EndErrors;    /* defines how to end an error(statistical) block */
  char *BeginNcount;  /* defines how to start an event (number of) block */
  char *EndNcount;    /* defines how to end an event (number of) block */
  };

#ifndef MC_EMBEDDED_RUNTIME /* the mcstatic variables (from mcstas-r.c) */
extern FILE * mcsiminfo_file;     /* handle to the output siminfo file */
extern int    mcgravitation;      /* flag to enable gravitation */
extern int    mcdotrace;          /* flag to print MCDISPLAY messages */
extern struct mcformats_struct mcformats[]; /* array of all possible formats */
extern struct mcformats_struct mcformat;    /* selected format to be used */
extern struct mcformats_struct mcformat_data; /* same as above, but may be different for data blocks */
#else
mcstatic FILE *mcsiminfo_file        = NULL;
#endif

/* Useful macros ============================================================ */

/* MPI stuff */

#ifdef USE_MPI
#include "mpi.h"

#ifdef OMPI_MPI_H  /* openmpi does not use signals: we may install our sighandler */
#undef NOSIGNALS
#endif

/*
 * MPI_MASTER(i):
 * execution of i only on master node
 */
#define MPI_MASTER(statement) { \
  if(mpi_node_rank == mpi_node_root)\
  { statement; } \
}

#ifndef MPI_REDUCE_BLOCKSIZE
#define MPI_REDUCE_BLOCKSIZE 1000
#endif

int mc_MPI_Sum(double* buf, long count);
int mc_MPI_Send(void *sbuf, long count, MPI_Datatype dtype, int dest);
int mc_MPI_Recv(void *rbuf, long count, MPI_Datatype dtype, int source);

/* MPI_Finalize exits gracefully and should be preferred to MPI_Abort */
#define exit(code) do {                                   \
    MPI_Finalize();                                       \
    exit(code);                                           \
  } while(0)

#else /* !USE_MPI */
#define MPI_MASTER(instr) instr
#endif /* USE_MPI */

#ifdef USE_MPI
static int mpi_node_count;
#endif

#ifdef USE_THREADS  /* user want threads */
#error Threading (USE_THREADS) support has been removed for very poor efficiency. Use MPI/SSH grid instead.
#endif

/* I/O function prototypes ================================================== */

#ifndef CHAR_BUF_LENGTH
#define CHAR_BUF_LENGTH 1024
#endif

/* main DETECTOR structure which stores most information to write to data files */
struct mcdetector_struct {
  char   prefix[CHAR_BUF_LENGTH];     /* prefix to prepend to format specifications */
  char   filename[CHAR_BUF_LENGTH];   /* file name of monitor */
  char   position[CHAR_BUF_LENGTH];   /* position of detector component */
  char   component[CHAR_BUF_LENGTH];  /* component instance name */
  char   instrument[CHAR_BUF_LENGTH]; /* instrument name */
  char   simulation[CHAR_BUF_LENGTH]; /* simulation name, e.g. directory */
  char   type[CHAR_BUF_LENGTH];       /* data type, e.g. 0d, 1d, 2d, 3d */
  char   user[CHAR_BUF_LENGTH];       /* user name, e.g. HOME */
  char   date[CHAR_BUF_LENGTH];       /* date of simulation end/write time */
  char   title[CHAR_BUF_LENGTH];      /* title of detector */
  char   xlabel[CHAR_BUF_LENGTH];     /* X axis label */
  char   ylabel[CHAR_BUF_LENGTH];     /* Y axis label */
  char   zlabel[CHAR_BUF_LENGTH];     /* Z axis label */
  char   xvar[CHAR_BUF_LENGTH];       /* X variable name */
  char   yvar[CHAR_BUF_LENGTH];       /* Y variable name */
  char   zvar[CHAR_BUF_LENGTH];       /* Z variable name */
  char   ncount[CHAR_BUF_LENGTH];     /* number of events initially generated */
  char   limits[CHAR_BUF_LENGTH];     /* X Y Z limits, e.g. [xmin xmax ymin ymax zmin zmax] */
  char   variables[CHAR_BUF_LENGTH];  /* variables written into data block */
  char   statistics[CHAR_BUF_LENGTH]; /* center, mean and half width along axis */
  char   signal[CHAR_BUF_LENGTH];     /* min max and mean of signal (data block) */
  char   values[CHAR_BUF_LENGTH];     /* integrated values e.g. [I I_err N] */
  double xmin,xmax;                   /* min max of axes */
  double ymin,ymax;
  double zmin,zmax;
  double intensity;                   /* integrated values for data block */
  double error;
  double events;
  double min;                         /* statistics for data block */
  double max;
  double mean;
  double centerX;                     /* statistics for axes */
  double halfwidthX;
  double centerY;
  double halfwidthY;
  int    rank;                        /* dimensionaly of monitor, e.g. 0 1 2 3 */
  char   istransposed;                /* flag to transpose matrix for some formats */

  long   m,n,p;                       /* dimensions of data block and along axes */
  long   date_l;                      /* same as date, but in sec since 1970 */

  double *p0, *p1, *p2;               /* pointers to saved data, NULL when freed */
  double *p0_orig,*p1_orig,*p2_orig;  /* initial pointer in case some post processing is done
                                         with e.g. lists and MPI handling */

  FILE   *file_handle;                /* file handle for detector file */
  struct mcformats_struct format;     /* format used for that detector */
};

typedef struct mcdetector_struct MCDETECTOR;

void   mcset_ncount(unsigned long long count);    /* wrapper to get mcncount */
unsigned long long int mcget_ncount(void);            /* wrapper to set mcncount */
unsigned long long mcget_run_num(void);           /* wrapper to get mcrun_num=0:mcncount */
char *mcfull_file(char *name, char *ext); /* builds "name+ext" */

/* Needed function signatures */
MCDETECTOR mcdetector_write_data(MCDETECTOR detector);

/* output functions */
MCDETECTOR mcdetector_out_0D(char *t, double p0, double p1, double p2, char *c, Coords pos);
MCDETECTOR mcdetector_out_1D(char *t, char *xl, char *yl,
                  char *xvar, double x1, double x2, int n,
                  double *p0, double *p1, double *p2, char *f, char *c, Coords pos);
MCDETECTOR mcdetector_out_2D(char *t, char *xl, char *yl,
                  double x1, double x2, double y1, double y2, int m,
                  int n, double *p0, double *p1, double *p2, char *f,
                  char *c, Coords pos);
MCDETECTOR mcdetector_out_3D(char *t, char *xl, char *yl, char *zl,
      char *xvar, char *yvar, char *zvar,
                  double x1, double x2, double y1, double y2, double z1, double z2, int m,
                  int n, int p, double *p0, double *p1, double *p2, char *f,
                  char *c, Coords pos);
/* wrappers to output functions, that automatically set NAME and POSITION */
#define DETECTOR_OUT(p0,p1,p2) mcdetector_out_0D(NAME_CURRENT_COMP,p0,p1,p2,NAME_CURRENT_COMP,POS_A_CURRENT_COMP)
#define DETECTOR_OUT_0D(t,p0,p1,p2) mcdetector_out_0D(t,p0,p1,p2,NAME_CURRENT_COMP,POS_A_CURRENT_COMP)
#define DETECTOR_OUT_1D(t,xl,yl,xvar,x1,x2,n,p0,p1,p2,f) \
     mcdetector_out_1D(t,xl,yl,xvar,x1,x2,n,p0,p1,p2,f,NAME_CURRENT_COMP,POS_A_CURRENT_COMP)
#define DETECTOR_OUT_2D(t,xl,yl,x1,x2,y1,y2,m,n,p0,p1,p2,f) \
     mcdetector_out_2D(t,xl,yl,x1,x2,y1,y2,m,n,p0,p1,p2,f,NAME_CURRENT_COMP,POS_A_CURRENT_COMP)
#define DETECTOR_OUT_3D(t,xl,yl,zl,xv,yv,zv,x1,x2,y1,y2,z1,z2,m,n,p,p0,p1,p2,f) \
     mcdetector_out_3D(t,xl,yl,zl,xv,yv,zv,x1,x2,y1,y2,z1,z2,m,n,p,p0,p1,p2,f,NAME_CURRENT_COMP,POS_A_CURRENT_COMP)
#define DETECTOR_CUSTOM_HEADER(t)  if (t && strlen(t)) { \
     mcDetectorCustomHeader=malloc(strlen(t)); \
     if (mcDetectorCustomHeader) strcpy(mcDetectorCustomHeader, t); }

/* Following part is only embedded when not redundent with mcstas.h ========= */

#ifndef MCCODE_H

#ifndef NOSIGNALS
#include <signal.h>
#define SIG_MESSAGE(msg) strcpy(mcsig_message, msg);
#else
#define SIG_MESSAGE(msg)
#endif /* !NOSIGNALS */



/* Useful macros and constants ============================================== */

#ifndef FLT_MAX
#define FLT_MAX         3.40282347E+38F /* max decimal value of a "float" */
#endif

#ifndef MIN
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#endif
#ifndef SQR
#define SQR(x) ( (x) * (x) )
#endif
#ifndef SIGN
#define SIGN(x) (((x)>0.0)?(1):(-1))
#endif

#define RAD2MIN  ((180*60)/PI)
#define MIN2RAD  (PI/(180*60))
#define DEG2RAD  (PI/180)
#define RAD2DEG  (180/PI)
#define FWHM2RMS 0.424660900144    /* Convert between full-width-half-max and */
#define RMS2FWHM 2.35482004503     /* root-mean-square (standard deviation) */
#define HBAR     1.05457168e-34    /* [Js] h bar Planck constant CODATA 2002 */
#define MNEUTRON 1.67492728e-27    /* [kg] mass of neutron CODATA 2002 */
#define GRAVITY  9.81              /* [m/s^2] gravitational acceleration */
#define NA       6.02214179e23     /* [#atoms/g .mole] Avogadro's number*/

#ifndef PI
# ifdef M_PI
#  define PI M_PI
# else
#  define PI 3.14159265358979323846
# endif
#endif

/* wrapper to get absolute and relative position of comp */
/* mccomp_posa and mccomp_posr are defined in McStas generated C code */
#define POS_A_COMP_INDEX(index) \
    (mccomp_posa[index])
#define POS_R_COMP_INDEX(index) \
    (mccomp_posr[index])
/* number of SCATTER calls in current comp: mcScattered defined in McStas generated C code */
#define SCATTERED mcScattered

/* Retrieve component information from the kernel */
/* Name, position and orientation (both absolute and relative)  */
/* Any component: For "redundancy", see comment by KN */
#define tmp_name_comp(comp) #comp
#define NAME_COMP(comp) tmp_name_comp(comp)
#define tmp_pos_a_comp(comp) (mcposa ## comp)
#define POS_A_COMP(comp) tmp_pos_a_comp(comp)
#define tmp_pos_r_comp(comp) (mcposr ## comp)
#define POS_R_COMP(comp) tmp_pos_r_comp(comp)
#define tmp_rot_a_comp(comp) (mcrota ## comp)
#define ROT_A_COMP(comp) tmp_rot_a_comp(comp)
#define tmp_rot_r_comp(comp) (mcrotr ## comp)
#define ROT_R_COMP(comp) tmp_rot_r_comp(comp)

/* Current component name, index, position and orientation */
#define NAME_CURRENT_COMP  NAME_COMP(mccompcurname)
#define INDEX_CURRENT_COMP mccompcurindex
#define POS_A_CURRENT_COMP POS_A_COMP(mccompcurname)
#define POS_R_CURRENT_COMP POS_R_COMP(mccompcurname)
#define ROT_A_CURRENT_COMP ROT_A_COMP(mccompcurname)
#define ROT_R_CURRENT_COMP ROT_R_COMP(mccompcurname)

/* Note: The two-stage approach to MC_GETPAR is NOT redundant; without it,
* after #define C sample, MC_GETPAR(C,x) would refer to component C, not to
* component sample. Such are the joys of ANSI C.

* Anyway the usage of MCGETPAR requires that we use sometimes bare names...
*/
#define MC_GETPAR2(comp, par) (mcc ## comp ## _ ## par)
#define MC_GETPAR(comp, par) MC_GETPAR2(comp,par)

/* MCDISPLAY/trace and debugging message sent to stdout */
#ifdef MC_TRACE_ENABLED
#define DEBUG
#endif

#ifdef DEBUG
#define mcDEBUG_INSTR() if(!mcdotrace); else { printf("INSTRUMENT:\n"); printf("Instrument '%s' (%s)\n", mcinstrument_name, mcinstrument_source); }
#define mcDEBUG_COMPONENT(name,c,t) if(!mcdotrace); else {\
  printf("COMPONENT: \"%s\"\n" \
         "POS: %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g\n", \
         name, c.x, c.y, c.z, t[0][0], t[0][1], t[0][2], \
         t[1][0], t[1][1], t[1][2], t[2][0], t[2][1], t[2][2]); \
  printf("Component %30s AT (%g,%g,%g)\n", name, c.x, c.y, c.z); \
  }
#define mcDEBUG_INSTR_END() if(!mcdotrace); else printf("INSTRUMENT END:\n");
#define mcDEBUG_ENTER() if(!mcdotrace); else printf("ENTER:\n");
#define mcDEBUG_COMP(c) if(!mcdotrace); else printf("COMP: \"%s\"\n", c);
#define mcDEBUG_LEAVE() if(!mcdotrace); else printf("LEAVE:\n");
#define mcDEBUG_ABSORB() if(!mcdotrace); else printf("ABSORB:\n");
#else
#define mcDEBUG_INSTR()
#define mcDEBUG_COMPONENT(name,c,t)
#define mcDEBUG_INSTR_END()
#define mcDEBUG_ENTER()
#define mcDEBUG_COMP(c)
#define mcDEBUG_LEAVE()
#define mcDEBUG_ABSORB()
#endif

// mcDEBUG_STATE and mcDEBUG_SCATTER are defined by mcstas-r.h and mcxtrace-r.h



#ifdef TEST
#define test_printf printf
#else
#define test_printf while(0) printf
#endif

/* send MCDISPLAY message to stdout to show gemoetry */
void mcdis_magnify(char *what);
void mcdis_line(double x1, double y1, double z1,
                double x2, double y2, double z2);
void mcdis_dashed_linemcdis_dashed_line(double x1, double y1, double z1,
		       double x2, double y2, double z2, int n);
void mcdis_multiline(int count, ...);
void mcdis_rectangle(char* plane, double x, double y, double z,
		     double width, double height);
void mcdis_box(double x, double y, double z,
	       double width, double height, double length);
void mcdis_circle(char *plane, double x, double y, double z, double r);

/* selection of random number generator. default is MT */
#ifndef MC_RAND_ALG
#define MC_RAND_ALG 1
#endif

#if MC_RAND_ALG == 0
   /* Use system random() (not recommended). */
#  define MC_RAND_MAX RAND_MAX
#elif MC_RAND_ALG == 1
   /* "Mersenne Twister", by Makoto Matsumoto and Takuji Nishimura. */
#  define MC_RAND_MAX ((unsigned long)0xffffffff)
#  define random mt_random
#  define srandom mt_srandom
#elif MC_RAND_ALG == 2
   /* Algorithm used in McStas CVS-080208 and earlier (not recommended). */
#  define MC_RAND_MAX 0x7fffffff
#  define random mc_random
#  define srandom mc_srandom
#else
#  error "Bad value for random number generator choice."
#endif

typedef int mc_int32_t;
mc_int32_t mc_random(void);
void mc_srandom (unsigned int x);
unsigned long mt_random(void);
void mt_srandom (unsigned long x);

double rand01();
double randpm1();
double rand0max(double max);
double randminmax(double min, double max);

double randnorm(void);
double randtriangle(void);

#ifndef DANSE
void mcinit(void);
void mcraytrace(void);
void mcsave(FILE *);
void mcfinally(void);
void mcdisplay(void);
#endif

/* simple vector algebra ==================================================== */
#define vec_prod(x, y, z, x1, y1, z1, x2, y2, z2) \
	vec_prod_func(&x, &y, &z, x1, y1, z1, x2, y2, z2)
mcstatic inline void vec_prod_func(double *x, double *y, double *z,
		double x1, double y1, double z1, double x2, double y2, double z2);

mcstatic inline double scalar_prod(
		double x1, double y1, double z1, double x2, double y2, double z2);

#define NORM(x,y,z) \
	norm_func(&x, &y, &z)
mcstatic inline void norm_func(double *x, double *y, double *z) {
	double temp = (*x * *x) + (*y * *y) + (*z * *z);
	if (temp != 0) {
		temp = sqrt(temp);
		*x /= temp;
		*y /= temp;
		*z /= temp;
	}
}

void normal_vec(double *nx, double *ny, double *nz,
    double x, double y, double z);

/**
 * Rotate the given xyz in a certain way. Used in pol-lib
 */
#define rotate(x, y, z, vx, vy, vz, phi, ax, ay, az) \
  do { \
    double mcrt_tmpx = (ax), mcrt_tmpy = (ay), mcrt_tmpz = (az); \
    double mcrt_vp, mcrt_vpx, mcrt_vpy, mcrt_vpz; \
    double mcrt_vnx, mcrt_vny, mcrt_vnz, mcrt_vn1x, mcrt_vn1y, mcrt_vn1z; \
    double mcrt_bx, mcrt_by, mcrt_bz; \
    double mcrt_cos, mcrt_sin; \
    NORM(mcrt_tmpx, mcrt_tmpy, mcrt_tmpz); \
    mcrt_vp = scalar_prod((vx), (vy), (vz), mcrt_tmpx, mcrt_tmpy, mcrt_tmpz); \
    mcrt_vpx = mcrt_vp*mcrt_tmpx; \
    mcrt_vpy = mcrt_vp*mcrt_tmpy; \
    mcrt_vpz = mcrt_vp*mcrt_tmpz; \
    mcrt_vnx = (vx) - mcrt_vpx; \
    mcrt_vny = (vy) - mcrt_vpy; \
    mcrt_vnz = (vz) - mcrt_vpz; \
    vec_prod(mcrt_bx, mcrt_by, mcrt_bz, \
             mcrt_tmpx, mcrt_tmpy, mcrt_tmpz, mcrt_vnx, mcrt_vny, mcrt_vnz); \
    mcrt_cos = cos((phi)); mcrt_sin = sin((phi)); \
    mcrt_vn1x = mcrt_vnx*mcrt_cos + mcrt_bx*mcrt_sin; \
    mcrt_vn1y = mcrt_vny*mcrt_cos + mcrt_by*mcrt_sin; \
    mcrt_vn1z = mcrt_vnz*mcrt_cos + mcrt_bz*mcrt_sin; \
    (x) = mcrt_vpx + mcrt_vn1x; \
    (y) = mcrt_vpy + mcrt_vn1y; \
    (z) = mcrt_vpz + mcrt_vn1z; \
  } while(0)

/**
 * Mirror (xyz) in the plane given by the point (rx,ry,rz) and normal (nx,ny,nz)
 *
 * TODO: This define is seemingly never used...
 */
#define mirror(x,y,z,rx,ry,rz,nx,ny,nz) \
  do { \
    double mcrt_tmpx= (nx), mcrt_tmpy = (ny), mcrt_tmpz = (nz); \
    double mcrt_tmpt; \
    NORM(mcrt_tmpx, mcrt_tmpy, mcrt_tmpz); \
    mcrt_tmpt=scalar_prod((rx),(ry),(rz),mcrt_tmpx,mcrt_tmpy,mcrt_tmpz); \
    (x) = rx -2 * mcrt_tmpt*mcrt_rmpx; \
    (y) = ry -2 * mcrt_tmpt*mcrt_rmpy; \
    (z) = rz -2 * mcrt_tmpt*mcrt_rmpz; \
  } while (0)

Coords coords_set(MCNUM x, MCNUM y, MCNUM z);
Coords coords_get(Coords a, MCNUM *x, MCNUM *y, MCNUM *z);
Coords coords_add(Coords a, Coords b);
Coords coords_sub(Coords a, Coords b);
Coords coords_neg(Coords a);
Coords coords_scale(Coords b, double scale);
double coords_sp(Coords a, Coords b);
Coords coords_xp(Coords b, Coords c);
void   coords_print(Coords a);
mcstatic inline void coords_norm(Coords* c);

void rot_set_rotation(Rotation t, double phx, double phy, double phz);
int  rot_test_identity(Rotation t);
void rot_mul(Rotation t1, Rotation t2, Rotation t3);
void rot_copy(Rotation dest, Rotation src);
void rot_transpose(Rotation src, Rotation dst);
Coords rot_apply(Rotation t, Coords a);

void mccoordschange(Coords a, Rotation t, double *x, double *y, double *z,
    double *vx, double *vy, double *vz, double *sx, double *sy, double *sz);
void
mccoordschange_polarisation(Rotation t, double *sx, double *sy, double *sz);

double mcestimate_error(double N, double p1, double p2);
void mcreadparams(void);

/* this is now in mcstas-r.h and mcxtrace-r.h as the number of state parameters is no longer equal*/
/* void mcsetstate(double x, double y, double z, double vx, double vy, double vz,
                double t, double sx, double sy, double sz, double p);
*/
void mcgenstate(void);

/* trajectory/shape intersection routines */
int inside_rectangle(double, double, double, double);
int box_intersect(double *dt_in, double *dt_out, double x, double y, double z,
    double vx, double vy, double vz, double dx, double dy, double dz);
int cylinder_intersect(double *t0, double *t1, double x, double y, double z,
    double vx, double vy, double vz, double r, double h);
int sphere_intersect(double *t0, double *t1, double x, double y, double z,
                 double vx, double vy, double vz, double r);
/* second order equation roots */
int solve_2nd_order(double *t1, double *t2,
    double A,  double B,  double C);

/* random vector generation to shape */
void randvec_target_circle(double *xo, double *yo, double *zo,
    double *solid_angle, double xi, double yi, double zi, double radius);
#define randvec_target_sphere randvec_target_circle
void randvec_target_rect_angular(double *xo, double *yo, double *zo,
    double *solid_angle,
               double xi, double yi, double zi, double height, double width, Rotation A);
#define randvec_target_rect(p0,p1,p2,p3,p4,p5,p6,p7,p8,p9)  randvec_target_rect_real(p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,0,0,0,1)
void randvec_target_rect_real(double *xo, double *yo, double *zo,
    double *solid_angle,
	       double xi, double yi, double zi, double height, double width, Rotation A,
			 double lx, double ly, double lz, int order);

/* this is the main() */
int mccode_main(int argc, char *argv[]);


#endif /* !MCCODE_H */

#endif /* MCCODE_R_H */
/* End of file "mccode-r.h". */

#line 694 "Test_src_gaussian.c"

#line 1 "mcxtrace-r.h"
/*******************************************************************************
*
* McXtrace, X-ray ray-tracing package
*         Copyright (C) 1997-2009, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* Runtime: share/mcxtrace-r.h
*
* %Identification
* Written by: KN
* Date:    Aug 29, 1997
* Release: McXtrace X.Y
* Version: $Revision: 1.109 $
*
* Runtime system header for McXtrace.
*
* In order to use this library as an external library, the following variables
* and macros must be declared (see details in the code)
*
*   struct mcinputtable_struct mcinputtable[];
*   int mcnumipar;
*   char mcinstrument_name[], mcinstrument_source[];
*   int mctraceenabled, mcdefaultmain;
*   extern MCNUM  mccomp_storein[];
*   extern MCNUM  mcAbsorbProp[];
*   extern MCNUM  mcScattered;
*   #define MCCODE_STRING "the McXtrace version"
*
* Usage: Automatically embbeded in the c code.
*
*******************************************************************************/

#ifndef MCXTRACE_R_H
#define MCXTRACE_R_H "$Revision: $"

/* Following part is only embedded when not redundant with mcstas.h ========= */

#ifndef MCCODE_H

#define CELE     1.602176487e-19   /* [C] Elementary charge CODATA 2006*/
#define M_C      299792458         /* [m/s] speed of light CODATA 2006*/
#define E2K      0.506773091264796 /* Convert k[1/AA] to E [keV] (CELE/(HBAR*M_C)*1e-10)*1e3 */
#define K2E      1.97326972808327  /*Convert E[keV] to k[1/AA] (1e10*M_C*HBAR/CELE)/1e3 */
#define RE       2.8179402894e-5   /*[AA] Thomson scattering length*/

#define SCATTER do {mcDEBUG_SCATTER(mcnlx, mcnly, mcnlz, mcnlkx, mcnlky, mcnlkz, \
    mcnlphi, mcnlt, mcnlEx,mcnlEy,mcnlEz, mcnlp); mcScattered++;} while(0)
#define ABSORB do {mcDEBUG_STATE(mcnlx, mcnly, mcnlz, mcnlkx, mcnlky, mcnlkz, \
    mcnlphi, mcnlt, mcnlEx,mcnlEy,mcnlEz, mcnlp); mcDEBUG_ABSORB(); goto mcabsorb;} while(0)

#define STORE_XRAY(index, x,y,z, kx,ky,kz, phi, t, Ex,Ey,Ez, p) \
  mcstore_xray(mccomp_storein,index, x,y,z, kx,ky,kz, phi, t, Ex,Ey,Ez, p);
#define RESTORE_XRAY(index, x,y,z, kx,ky,kz, phi, t, Ex,Ey,Ez, p) \
  mcrestore_xray(mccomp_storein,index, &x,&y,&z, &kx,&ky,&kz, &phi, &t, &Ex,&Ey,&Ez, &p);

/*magnet stuff is probably redundant*/
#define MAGNET_ON \
  do { \
    mcMagnet = 1; \
  } while(0)

#define MAGNET_OFF \
  do { \
    mcMagnet = 0; \
  } while(0)

#define ALLOW_BACKPROP \
  do { \
    mcallowbackprop = 1; \
  } while(0)

#define DISALLOW_BACKPROP \
  do { \
    mcallowbackprop = 0; \
  } while(0)

#define PROP_MAGNET(dt) \
  do { \
    /* change coordinates from local system to magnet system */ \
    Rotation rotLM, rotTemp; \
    Coords   posLM = coords_sub(POS_A_CURRENT_COMP, mcMagnetPos); \
    rot_transpose(ROT_A_CURRENT_COMP, rotTemp); \
    rot_mul(rotTemp, mcMagnetRot, rotLM); \
    mcMagnetPrecession(mcnlx, mcnly, mcnlz, mcnlt, mcnlvx, mcnlvy, mcnlvz, \
	   	       &mcnlsx, &mcnlsy, &mcnlsz, dt, posLM, rotLM); \
  } while(0)

#define mcPROP_DT(dt) \
  do { \
    if (mcMagnet && dt > 0) PROP_MAGNET(dt);\
    mcnlx += mcnlvx*(dt); \
    mcnly += mcnlvy*(dt); \
    mcnlz += mcnlvz*(dt); \
    mcnlt += (dt); \
    if (isnan(p) || isinf(p)) { mcAbsorbProp[INDEX_CURRENT_COMP]++; ABSORB; }\
  } while(0)

/*An interrupt a'la mcMagnet should be inserted below if there's non-zero permeability*/
/*perhaps some kind of PROP_POL*/

#define mcPROP_DL(dl) \
  do { \
    MCNUM k=sqrt( scalar_prod(mcnlkx,mcnlky,mcnlkz,mcnlkx,mcnlky,mcnlkz));\
    mcnlx += (dl)*mcnlkx/k;\
    mcnly += (dl)*mcnlky/k;\
    mcnlz += (dl)*mcnlkz/k;\
    mcnlphi += 1e10*k*(dl);\
    mcnlt += (dl)/((double)M_C);\
  }while (0)

/*gravity not an issue with x-rays*/
/* ADD: E. Farhi, Aug 6th, 2001 PROP_GRAV_DT propagation with acceleration. */
#define PROP_GRAV_DT(dt, Ax, Ay, Az) \
  do { \
    if(dt < 0 && mcallowbackprop == 0) { mcAbsorbProp[INDEX_CURRENT_COMP]++; ABSORB; }\
    if (mcMagnet) printf("Spin precession gravity\n"); \
    mcnlx  += mcnlvx*(dt) + (Ax)*(dt)*(dt)/2; \
    mcnly  += mcnlvy*(dt) + (Ay)*(dt)*(dt)/2; \
    mcnlz  += mcnlvz*(dt) + (Az)*(dt)*(dt)/2; \
    mcnlvx += (Ax)*(dt); \
    mcnlvy += (Ay)*(dt); \
    mcnlvz += (Az)*(dt); \
    mcnlt  += (dt); \
    DISALLOW_BACKPROP;\
  } while(0)

/*adapted from PROP_DT(dt)*//*{{{*/
#define PROP_DL(dl) \
  do{ \
    if( dl <0 && mcallowbackprop == 0) { (mcAbsorbProp[INDEX_CURRENT_COMP])++; ABSORB; }; \
    mcPROP_DL(dl); \
    DISALLOW_BACKPROP;\
  } while (0)

#define PROP_DT(dt) \
  do { \
    if(dt < 0 && mcallowbackprop == 0) { mcAbsorbProp[INDEX_CURRENT_COMP]++; ABSORB; }; \
    if (mcgravitation) { Coords mcLocG; double mc_gx, mc_gy, mc_gz; \
    mcLocG = rot_apply(ROT_A_CURRENT_COMP, coords_set(0,-GRAVITY,0)); \
    coords_get(mcLocG, &mc_gx, &mc_gy, &mc_gz); \
    PROP_GRAV_DT(dt, mc_gx, mc_gy, mc_gz); } \
    else mcPROP_DT(dt); \
    DISALLOW_BACKPROP;\
  } while(0)/*}}}*/

#define PROP_Z0 \
  mcPROP_P0(z)

#define PROP_X0 \
  mcPROP_P0(x)

#define PROP_Y0 \
  mcPROP_P0(y)

#define mcPROP_P0(P) \
  do { \
    MCNUM mc_dl,mc_k; \
    if(mcnlk ## P == 0) { mcAbsorbProp[INDEX_CURRENT_COMP]++; ABSORB; }; \
    mc_k=sqrt(scalar_prod(mcnlkx,mcnlky,mcnlkz,mcnlkx,mcnlky,mcnlkz));\
    mc_dl= -mcnl ## P * mc_k / mcnlk ## P;\
    if(mc_dl<0 && mcallowbackprop==0) { mcAbsorbProp[INDEX_CURRENT_COMP]++; ABSORB; };\
    PROP_DL(mc_dl);\
  } while(0)

void mcsetstate(double x, double y, double z, double kx, double ky, double kz,
    double phi, double t, double Ex, double Ey, double Ez, double p);


#endif /* !MCCODE_H */


#ifdef DEBUG

#define mcDEBUG_STATE(x,y,z,kx,ky,kz,phi,t,Ex,Ey,Ez,p) if(!mcdotrace); else \
  printf("STATE: %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g\n", \
      x,y,z,kx,ky,kz,phi,t,Ex,Ey,Ez,p);
#define mcDEBUG_SCATTER(x,y,z,kx,ky,kz,phi,t,Ex,Ey,Ez,p) if(!mcdotrace); else \
  printf("SCATTER: %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g\n", \
      x,y,z,kx,ky,kz,phi,t,Ex,Ey,Ez,p);

#else

#define mcDEBUG_STATE(x,y,z,kx,ky,kz,phi,t,Ex,Ey,Ez,p)
#define mcDEBUG_SCATTER(x,y,z,kx,ky,kz,phi,t,Ex,Ey,Ez,p)

#endif


#endif /* MCXTRACE_R_H */
/* End of file "mcxtrace-r.h". */

#line 889 "Test_src_gaussian.c"

#line 1 "nexus-lib.h"
/*******************************************************************************
*
* McStas, neutron ray-tracing package
*         Copyright (C) 1997-2008, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* Runtime: share/nexus-lib.h
*
* %Identification
* Written by: EF
* Date:    Jan 17, 2007
* Release: McStas CVS-080208
* Version: $Revision: 1.8 $
*
* NeXus Runtime system header for McStas.
* Overrides default mcstas runtime functions.
* Embedded within instrument in runtime mode.
*
* Usage: Automatically embbeded in the c code whenever required.
*
*******************************************************************************/

#ifdef USE_NEXUS

#include "napi.h"
#include <sys/stat.h>

#include <sys/types.h>
#include <ctype.h>

/* NeXus variables to be used in functions */
NXhandle mcnxHandle;
char    *mcnxFilename=NULL;
char     mcnxversion[128];       /* init in cogen_init: 4,5 xml and compress */

/* NeXus output functions that replace calls to pfprintf in mcstas-r */
int mcnxfile_init(char *name, char *ext, char *mode, NXhandle *nxhandle);
int mcnxfile_close(NXhandle *nxHandle);

/* header/footer. f=mcsiminfo_file, datafile */
/* creates Entry=valid_parent+file+timestamp */
int mcnxinfo_header(NXhandle nxhandle, char *part,
    char *pre,                  /* %1$s  PRE  */
    char *instrname,            /* %2$s  SRC  */
    char *file,                 /* %3$s  FIL  */
    char *format_name,          /* %4$s  FMT  */
    char *date,                 /* %5$s  DAT  */
    char *user,                 /* %6$s  USR  */
    char *valid_parent,         /* %7$s  PAR = file */
    long  date_l);               /* %8$li DATL */

/* tag=value */
int mcnxinfo_tag(NXhandle nxhandle,
    char *pre,          /* %1$s PRE */
    char *valid_section,/* %2$s SEC */
    char *name,         /* %3$s NAM */
    char *value);        /* %4$s VAL */

/* output instrument/description */
int mcnxfile_instrcode(NXhandle nxhandle, 
    char *name,
    char *parent);

/* begin/end section */
int mcnxfile_section(NXhandle nxhandle, char *part,
    char *pre,          /* %1$s  PRE  */
    char *type,         /* %2$s  TYP  */
    char *name,         /* %3$s  NAM  */
    char *valid_name,   /* %4$s  VNA  */
    char *parent,       /* %5$s  PAR  */
    char *valid_parent, /* %6$s  VPA  */
    int   level);        /* %7$i  LVL */

/* data block begin/end */
int mcnxfile_data(NXhandle nxhandle, MCDETECTOR detector, char *part,
  char *valid_parent, char *valid_xlabel, char *valid_ylabel, char *valid_zlabel);

/* Prototype for function in mccode-r.c only included when USE_NEXUS */
int mcnxfile_parameters(NXhandle nxhandle);

#endif
/* End of file "nexus-lib.h". */

#line 976 "Test_src_gaussian.c"

#line 1 "nexus-lib.c"
/*******************************************************************************
*
* McStas, neutron ray-tracing package
*         Copyright (C) 1997-2006, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* Runtime: share/nexus-lib.c
*
* %Identification
* Written by: KN
* Date:    Jan 17, 2007
* Release: McStas 1.10
* Version: $Revision: 1.14 $
*
* NeXus Runtime output functions for McStas.
* Overrides default mcstas runtime functions.
* Embedded within instrument in runtime mode.
*
* Usage: Automatically embbeded in the C code whenever required.
*
*******************************************************************************/

#ifdef USE_NEXUS

/*******************************************************************************
* mcnxfile_init: Initialize NeXus file (open it). handles NeXus 4/5 and compression
* Returns: NX_ERROR or NX_OK
*******************************************************************************/
int mcnxfile_init(char *name, char *ext, char *mode, NXhandle *nxhandle)
{
  int mcnxMode=NXACC_CREATE5;
  char mcnxExt[CHAR_BUF_LENGTH];
  strcpy(mcnxExt, ext);
  char nxversion[CHAR_BUF_LENGTH];
  int i;
  if (!mcnxversion || !strlen(mcnxversion)) strcpy(nxversion, "5 zip");
  else for (i=0; i< strlen(mcnxversion) && i < 128; nxversion[i]=tolower(mcnxversion[i++]));

  if    (strstr(nxversion,"xml")) { mcnxMode =NXACC_CREATEXML; strcpy(mcnxExt, "xml"); }
  else if (strstr(nxversion,"4")) { mcnxMode =NXACC_CREATE;    strcpy(mcnxExt, "h4"); }
  else if (strstr(nxversion,"5")) { mcnxMode =NXACC_CREATE5;   strcpy(mcnxExt, "h5"); }

  if (!strcmp(mode, "a"))    mcnxMode |= NXACC_RDWR;
  mcnxFilename = mcfull_file(name, mcnxExt);
  if (NXopen(mcnxFilename, mcnxMode, nxhandle) == NX_ERROR) {
    fprintf(stderr, "Warning: NeXus: could not open file %s\n", mcnxFilename);
    mcsiminfo_file = NULL;
  } else { mcsiminfo_file=(FILE*)mcnxFilename; }
  return(mcsiminfo_file != NULL);
}

/*******************************************************************************
* mcnxfile_close: Close NeXus file
* Returns: NX_ERROR or NX_OK
*******************************************************************************/
int mcnxfile_close(NXhandle *nxHandle)
{
  return(NXclose(nxHandle));
}

/*******************************************************************************
* mcnxinfo_header: Write general header/footer information tags (header/footer)
*                  into e.g. mcsiminfo_file, datafile
*                  Group and DataSet (information) must have been opened
* Returns: NX_ERROR or NX_OK
*******************************************************************************/
int mcnxinfo_header(NXhandle nxhandle, char *part,
    char *pre,                  /* %1$s  PRE  */
    char *instrname,            /* %2$s  SRC  */
    char *file,                 /* %3$s  FIL  */
    char *format_name,          /* %4$s  FMT  */
    char *date,                 /* %5$s  DAT  */
    char *user,                 /* %6$s  USR  */
    char *valid_parent,         /* %7$s  PAR = file */
    long  date_l)               /* %8$li DATL */
{
  if (!strcmp(part, "header")) {
    if (NXputattr(nxhandle, "user_name", user, strlen(user), NX_CHAR) == NX_ERROR) {
      fprintf(stderr, "Warning: NeXus: could not write header information in /%s/%s/%s\n", 
        file, instrname, valid_parent);
      return(NX_ERROR);
    }
    char creator[CHAR_BUF_LENGTH];
    sprintf(creator, "%s gerenated with " MCCODE_STRING " (" MCCODE_DATE ") [www.mccode.org]", instrname);
    NXputattr(nxhandle, "creator", creator, strlen(creator), NX_CHAR);
    NXputattr(nxhandle, "simulation_begin", date, strlen(date), NX_CHAR);
    char *url="http://www.nexusformat.org/";
    NXputattr(nxhandle, "URL", url, strlen(url), NX_CHAR);
    char *browser="hdfview or NXbrowse or HDFExplorer";
    NXputattr(nxhandle, "Browser", browser, strlen(browser), NX_CHAR);
#if defined (USE_MPI) 
    NXputattr (nxhandle, "number_of_nodes", &mpi_node_count, 1, NX_INT32);
#endif
    return(NXputattr(nxhandle, "Format", format_name, strlen(format_name), NX_CHAR));
  } else
    return(NXputattr(nxhandle, "simulation_end", date, strlen(date), NX_CHAR));
  
} /* mcnxinfo_header */

/*******************************************************************************
* mcnxinfo_tag: write a single tag
*               Group and DataSet (information) must have been opened
* Returns: NX_ERROR or NX_OK
*******************************************************************************/
int mcnxinfo_tag(NXhandle nxhandle,
    char *pre,          /* %1$s PRE */
    char *valid_section,/* %2$s SEC */
    char *name,         /* %3$s NAM */
    char *value)        /* %4$s VAL */
{
  return(NXputattr(nxhandle, name, value, strlen(value), NX_CHAR));
} /* mcnxinfo_tag */

/*******************************************************************************
* mcnxfile_instrcode: writes the instrument description file
*                   open/close a new 'description' Data Set in the current opened Group
* Returns: NX_ERROR or NX_OK
*******************************************************************************/
int mcnxfile_instrcode(NXhandle nxhandle, 
    char *name,
    char *parent)
{
  FILE *f;
  char *instr_code=NULL;
  char nxname[CHAR_BUF_LENGTH];
  int length;
  
  struct stat stfile;
  if (stat(name,&stfile) != 0) {
    instr_code = (char*)malloc(CHAR_BUF_LENGTH);
    if (instr_code) 
      sprintf(instr_code, "File %s not found (instrument description %s is missing)", 
        name, parent);
  } else {
    long filesize = stfile.st_size;
    f=fopen(name, "r");
    instr_code = (char*)malloc(filesize);
    if (instr_code && f) fread(instr_code, 1, filesize, f);
    if (f) fclose(f);
  }
  length = instr_code ? strlen(instr_code) : 0;
  if (length) {
    time_t t;
    NXmakedata(nxhandle, "description", NX_CHAR, 1, &length);
    if (NXopendata(nxhandle, "description") == NX_ERROR) {
      fprintf(stderr, "Warning: NeXus: could not write instrument description %s (%s)\n", 
        name, parent);
      free(instr_code);
      return(NX_ERROR);
    }
    NXputdata (nxhandle, instr_code);
    free(instr_code);
    NXputattr (nxhandle, "file_name", name, strlen(name), NX_CHAR);
    NXputattr (nxhandle, "file_size", &length, 1, NX_INT32);
    t=stfile.st_mtime; strncpy(nxname, ctime(&t), CHAR_BUF_LENGTH);
    NXputattr (nxhandle, "file_date", nxname, strlen(nxname), NX_CHAR);
    NXputattr (nxhandle, "MCCODE_STRING", MCCODE_STRING, strlen(MCCODE_STRING), NX_CHAR);
    NXputattr (nxhandle, "name", parent, strlen(parent), NX_CHAR);
    
    return(NXclosedata(nxhandle));
  } else
  return(NX_ERROR);
} /* mcnxfile_instrcode */

/*******************************************************************************
* mcnxfile_section: begin/end a section
*                   open a new 'section' Group and an 'information' Data Set
*                   close the current Group when part="end"
*                   the Data Set 'information' is not closed here
* Returns: NX_ERROR or NX_OK
*******************************************************************************/
int mcnxfile_section(NXhandle nxhandle, char *part,
    char *pre,          /* %1$s  PRE  */
    char *type,         /* %2$s  TYP  */
    char *name,         /* %3$s  NAM  */
    char *valid_name,   /* %4$s  VNA  */
    char *parent,       /* %5$s  PAR  */
    char *valid_parent, /* %6$s  VPA  */
    int   level)        /* %7$i  LVL */
{
  if (!strcmp(part, "end")) {
    int ret;
    ret = NXclosegroup(nxhandle);
    if (ret == NX_ERROR)
      fprintf(stderr, "Warning: NeXus: could not close group %s (%s)\n", 
          valid_name, type);
    return(ret);
  }
  
  if (!strcmp(part, "begin")) {
    int length;
    char nxtype[CHAR_BUF_LENGTH];
    char nxname[CHAR_BUF_LENGTH];
    
    if (!strcmp(type, "instrument"))      strcpy(nxname, "instrument");
    else if (!strcmp(type, "simulation")) strcpy(nxname, "simulation");
    else strcpy(nxname, valid_name);
    snprintf(nxtype, CHAR_BUF_LENGTH, "NX%s", type);
    
    NXMDisableErrorReporting(); /* unactivate NeXus error messages */
    NXmakegroup(nxhandle, nxname, nxtype);
    NXMEnableErrorReporting();  /* enable NeXus error messages */
    
    if (NXopengroup(nxhandle, nxname, nxtype) == NX_ERROR) {
      fprintf(stderr, "Warning: NeXus: could not open group %s (%s) to store 'information'\n", 
          nxname, nxtype);
      return(NX_ERROR);
    }
    /* open a SDS to store attributes */
    snprintf(nxname, CHAR_BUF_LENGTH, "Information about %s of type %s is stored in attributes", name, nxtype);
    length = strlen(nxname);
    
    NXMDisableErrorReporting(); /* unactivate NeXus error messages */
    NXmakedata(nxhandle, "information", NX_CHAR, 1, &length);
    NXMEnableErrorReporting();  /* enable NeXus error messages */
    if (NXopendata(nxhandle, "information") == NX_ERROR) {
      fprintf(stderr, "Warning: NeXus: could not open 'information' for %s (%s)\n", 
          name, nxtype);
      return(NX_ERROR);
    }
    NXputdata (nxhandle, nxname);
    NXputattr(nxhandle, "name", name, strlen(name), NX_CHAR);
    NXputattr(nxhandle, "parent", parent, strlen(parent), NX_CHAR);

  }
  return(NX_OK);
} /* mcnxfile_section */

/*******************************************************************************
* mcnxfile_section: begin/end a data block (data/errors/events)
*                   open/close a 'part' Data Set
*                   open/close Axes (except for lists)
*                   handles compressed Data Set
* Returns: NX_ERROR or NX_OK
*******************************************************************************/
/* mcnxfile_datablock: data block begin/end. Returns: NX_ERROR or NX_OK */
int mcnxfile_data(NXhandle nxhandle, MCDETECTOR detector, char *part,
  char *valid_parent, char *valid_xlabel, char *valid_ylabel, char *valid_zlabel)
{
  /* write axes, only for data */
  if (strstr(part, "data")) {
    int i;
    if (!strstr(detector.format.Name, "list")) {
    
      if (detector.m > 1) {       /* X axis */
        double axis[detector.m];
        int dim=(int)detector.m;
        for(i = 0; i < detector.m; i++)
          axis[i] = detector.xmin+(detector.xmax-detector.xmin)*(i+0.5)/detector.m;
        if (strstr(mcnxversion,"compress") || strstr(mcnxversion,"zip"))
          NXcompmakedata(nxhandle, valid_xlabel, NX_FLOAT64, 1, &dim, NX_COMP_LZW, &dim);
        else
          NXmakedata(nxhandle, valid_xlabel, NX_FLOAT64, 1, &dim);

        if (NXopendata(nxhandle, valid_xlabel) == NX_ERROR) {
          fprintf(stderr, "Warning: could not open X axis %s in %s\n",
            valid_xlabel, detector.filename);
          return(NX_ERROR);
        }
        NXputdata (nxhandle, axis);
        NXputattr (nxhandle, "long_name", detector.xlabel, strlen(detector.xlabel), NX_CHAR);
        NXputattr (nxhandle, "short_name", detector.xvar, strlen(detector.xvar), NX_CHAR);
        int naxis=1;
        NXputattr (nxhandle, "axis", &naxis, 1, NX_INT32);
        NXputattr (nxhandle, "units", detector.xvar, strlen(detector.xvar), NX_CHAR);
        int nprimary=1;
        NXputattr (nxhandle, "primary", &nprimary, 1, NX_INT32);
        NXclosedata(nxhandle);
      }
      
      if (detector.n >= 1) {      /* Y axis */
        double axis[detector.n];
        int dim=(int)detector.n;
        for(i = 0; i < detector.n; i++)
          axis[i] = detector.ymin+(detector.ymax-detector.ymin)*(i+0.5)/detector.n;
        if (strstr(mcnxversion,"compress") || strstr(mcnxversion,"zip"))
          NXcompmakedata(nxhandle, valid_ylabel, NX_FLOAT64, 1, &dim, NX_COMP_LZW, &dim);
        else
          NXmakedata(nxhandle, valid_ylabel, NX_FLOAT64, 1, &dim);

        if (NXopendata(nxhandle, valid_ylabel) == NX_ERROR) {
          fprintf(stderr, "Warning: could not open Y axis %s in %s\n",
            valid_ylabel, detector.filename);
          return(NX_ERROR);
        }
        NXputdata (nxhandle, axis);
        NXputattr (nxhandle, "long_name", detector.ylabel, strlen(detector.ylabel), NX_CHAR);
        NXputattr (nxhandle, "short_name", detector.yvar, strlen(detector.yvar), NX_CHAR);
        int naxis=2;
        NXputattr (nxhandle, "axis", &naxis, 1, NX_INT32);
        NXputattr (nxhandle, "units", detector.yvar, strlen(detector.yvar), NX_CHAR);
        int nprimary=1;
        NXputattr (nxhandle, "primary", &nprimary, 1, NX_INT32);
        NXclosedata(nxhandle);
      }
      
      if (detector.p > 1) {     /* Z axis */
        double axis[detector.p];
        int dim=(int)detector.p;
        for(i = 0; i < detector.p; i++)
          axis[i] = detector.zmin+(detector.zmax-detector.zmin)*(i+0.5)/detector.p;
        if (strstr(mcnxversion,"compress") || strstr(mcnxversion,"zip"))
          NXcompmakedata(nxhandle, valid_zlabel, NX_FLOAT64, 1, &dim, NX_COMP_LZW, &dim);
        else
          NXmakedata(nxhandle, valid_zlabel, NX_FLOAT64, 1, &dim);

        if (NXopendata(nxhandle, valid_zlabel) == NX_ERROR) {
          fprintf(stderr, "Warning: could not open Z axis %s in %s\n",
            valid_ylabel, detector.filename);
          return(NX_ERROR);
        }
        NXputdata (nxhandle, axis);
        NXputattr (nxhandle, "long_name", detector.zlabel, strlen(detector.zlabel), NX_CHAR);
        NXputattr (nxhandle, "short_name", detector.zvar, strlen(detector.zvar), NX_CHAR);
        int naxis=3;
        NXputattr (nxhandle, "axis", &naxis, 1, NX_INT32);
        NXputattr (nxhandle, "units", detector.zvar, strlen(detector.zvar), NX_CHAR);
        int nprimary=1;
        NXputattr (nxhandle, "primary", &nprimary, 1, NX_INT32);
        NXclosedata(nxhandle);
      }
    } /* !list */
  } /* end format != list for data */
  
  /* write data */
  int dims[3]={detector.m,detector.n,detector.p};  /* number of elements to write */
  char *nxname=part;
  double *data;
  if (strstr(part,"data"))         { data=detector.p1; }
  else if (strstr(part,"errors"))  { data=detector.p2; }
  else if (strstr(part,"ncount"))  { data=detector.p0; }
  /* ignore errors for making/opening data (in case this has already been done */
  if (strstr(mcnxversion,"compress") || strstr(mcnxversion,"zip"))
    NXmakedata(nxhandle, nxname, NX_FLOAT64, detector.rank, dims);
  else
    NXcompmakedata(nxhandle, nxname, NX_FLOAT64, detector.rank, dims, NX_COMP_LZW, dims);

  if (NXopendata(nxhandle, nxname) == NX_ERROR) {
        fprintf(stderr, "Warning: could not open DataSet '%s' in %s\n",
          nxname, detector.filename);
        return(NX_ERROR);
      }
  NXputdata (nxhandle, data);
  NXputattr(nxhandle, "parent", valid_parent, strlen(valid_parent), NX_CHAR);
  int signal=1;
  if (strstr(part,"data")) {
    NXputattr(nxhandle, "signal", &signal, 1, NX_INT32);
    NXputattr(nxhandle, "short_name", detector.filename, strlen(detector.filename), NX_CHAR);
  }
  char nxtitle[CHAR_BUF_LENGTH];
  sprintf(nxtitle, "%s '%s'", nxname, detector.title);
  NXputattr(nxhandle, "long_name", nxtitle, strlen(nxtitle), NX_CHAR);
  /* first write attributes */
  char creator[CHAR_BUF_LENGTH];
  sprintf(creator, "%s/%s", mcinstrument_name, valid_parent);
  NXputattr(nxhandle, "creator", creator, strlen(creator), NX_CHAR);
  return(NXclosedata(nxhandle));
} /* mcnxfile_datablock */


#endif /* USE_NEXUS */
/* End of file "nexus-lib.c". */

#line 1343 "Test_src_gaussian.c"

#line 1 "mccode-r.c"
/*******************************************************************************
*
* McCode, neutron/xray ray-tracing package
*         Copyright (C) 1997-2009, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* Runtime: share/mcstas-r.c
*
* %Identification
* Written by: KN
* Date:    Aug 29, 1997
* Release: McStas X.Y/McXtrace X.Y
* Version: $Revision: 1.229 $
*
* Runtime system for McStas and McXtrace.
* Embedded within instrument in runtime mode.
* Contains SECTIONS:
*   MPI handling (sum, send, recv)
*   format definitions
*   I/O
*   mcdisplay support
*   random numbers
*   coordinates handling
*   vectors math (solve 2nd order, normals, randvec...)
*   parameter handling
*   signal and main handlers
*
* Usage: Automatically embbeded in the c code whenever required.
*
* $Id: mcstas-r.c,v 1.229 2009/08/13 14:52:01 farhi Exp $
*
*******************************************************************************/

/*******************************************************************************
* The I/O format definitions and functions
*******************************************************************************/


/** Include header files to avoid implicit declarations (not allowed on LLVM) */
#include <ctype.h>
#include <sys/types.h>

// UNIX specific headers (non-Windows)
#if defined(__unix__) || defined(__APPLE__)
#include <unistd.h>
#include <sys/stat.h>
#endif


#ifndef DANSE
#ifdef MC_ANCIENT_COMPATIBILITY
int mctraceenabled = 0;
int mcdefaultmain  = 0;
#endif
/* else defined directly in the McStas generated C code */

static   long mcseed                 = 0; /* seed for random generator */
static   int  mcascii_only           = 0; /* flag for no header */
static   int  mcsingle_file          = 0; /* flag for storing all detectors into a single file */
static   long mcstartdate            = 0; /* start simulation time */
static   int  mcdisable_output_files = 0; /* --no-output-files */
mcstatic int  mcgravitation          = 0; /* use gravir=tation flag, for PROP macros */
int      mcMagnet                    = 0; /* megnet stack flag */
mcstatic int  mcdotrace              = 0; /* flag for --trace and messages for DISPLAY */
/* mcstatic FILE *mcsiminfo_file        = NULL; */
static   char *mcdirname             = NULL;      /* name of output directory */
static   char *mcsiminfo_name        = "mcstas";  /* default output sim file name */
int      mcallowbackprop             = 0;         /* flag to enable negative/backprop */
char*    mcDetectorCustomHeader      = NULL;      /* additional user output Tag in data files */
char*    mcopenedfiles               = "";        /* list of opened files (for append) */
long     mcopenedfiles_size          = 0;         /* size of that opened files list */
MCDETECTOR* mcDetectorArray          = NULL;      /* array of all opened detectors */
long     mcDetectorArray_size        = 0;         /* allocated detector array size */
long     mcDetectorArray_index       = 0;         /* current detector length (number of detectors so far) */

/* Number of particule histories to simulate. */
#ifdef NEUTRONICS
mcstatic unsigned long long int mcncount             = 1;
mcstatic unsigned long long int mcrun_num            = 0;
#else
mcstatic unsigned long long int mcncount             = 1e6;
mcstatic unsigned long long int mcrun_num            = 0;
#endif /* NEUTRONICS */

/* I/O structures */
mcstatic struct mcformats_struct mcformat;
mcstatic struct mcformats_struct mcformat_data;
#else
#include "mcstas-globals.h"
#endif /* !DANSE */

#ifndef MCSTAS_FORMAT
#define MCSTAS_FORMAT "McStas"  /* default format */
#endif

/* SECTION: parameters handling ============================================= */

/* Instrument input parameter type handling. */
/*******************************************************************************
* mcparm_double: extract double value from 's' into 'vptr'
*******************************************************************************/
static int
mcparm_double(char *s, void *vptr)
{
  char *p;
  double *v = (double *)vptr;

  if (!s) { *v = 0; return(1); }
  *v = strtod(s, &p);
  if(*s == '\0' || (p != NULL && *p != '\0') || errno == ERANGE)
    return 0;                        /* Failed */
  else
    return 1;                        /* Success */
}

/*******************************************************************************
* mcparminfo_double: display parameter type double
*******************************************************************************/
static char *
mcparminfo_double(char *parmname)
{
  return "double";
}

/*******************************************************************************
* mcparmerror_double: display error message when failed extract double
*******************************************************************************/
static void
mcparmerror_double(char *parm, char *val)
{
  fprintf(stderr, "Error: Invalid value '%s' for floating point parameter %s (mcparmerror_double)\n",
          val, parm);
}

/*******************************************************************************
* mcparmprinter_double: convert double to string
*******************************************************************************/
static void
mcparmprinter_double(char *f, void *vptr)
{
  double *v = (double *)vptr;
  sprintf(f, "%g", *v);
}

/*******************************************************************************
* mcparm_int: extract int value from 's' into 'vptr'
*******************************************************************************/
static int
mcparm_int(char *s, void *vptr)
{
  char *p;
  int *v = (int *)vptr;
  long x;

  if (!s) { *v = 0; return(1); }
  *v = 0;
  x = strtol(s, &p, 10);
  if(x < INT_MIN || x > INT_MAX)
    return 0;                        /* Under/overflow */
  *v = x;
  if(*s == '\0' || (p != NULL && *p != '\0') || errno == ERANGE)
    return 0;                        /* Failed */
  else
    return 1;                        /* Success */
}

/*******************************************************************************
* mcparminfo_int: display parameter type int
*******************************************************************************/
static char *
mcparminfo_int(char *parmname)
{
  return "int";
}

/*******************************************************************************
* mcparmerror_int: display error message when failed extract int
*******************************************************************************/
static void
mcparmerror_int(char *parm, char *val)
{
  fprintf(stderr, "Error: Invalid value '%s' for integer parameter %s (mcparmerror_int)\n",
          val, parm);
}

/*******************************************************************************
* mcparmprinter_int: convert int to string
*******************************************************************************/
static void
mcparmprinter_int(char *f, void *vptr)
{
  int *v = (int *)vptr;
  sprintf(f, "%d", *v);
}

/*******************************************************************************
* mcparm_string: extract char* value from 's' into 'vptr' (copy)
*******************************************************************************/
static int
mcparm_string(char *s, void *vptr)
{
  char **v = (char **)vptr;
  if (!s) { *v = NULL; return(1); }
  *v = (char *)malloc(strlen(s) + 1);
  if(*v == NULL)
  {
    exit(-fprintf(stderr, "Error: Out of memory %li (mcparm_string).\n", (long)strlen(s) + 1));
  }
  strcpy(*v, s);
  return 1;                        /* Success */
}

/*******************************************************************************
* mcparminfo_string: display parameter type string
*******************************************************************************/
static char *
mcparminfo_string(char *parmname)
{
  return "string";
}

/*******************************************************************************
* mcparmerror_string: display error message when failed extract string
*******************************************************************************/
static void
mcparmerror_string(char *parm, char *val)
{
  fprintf(stderr, "Error: Invalid value '%s' for string parameter %s (mcparmerror_string)\n",
          val, parm);
}

/*******************************************************************************
* mcparmprinter_string: convert string to string (including esc chars)
*******************************************************************************/
static void
mcparmprinter_string(char *f, void *vptr)
{
  char **v = (char **)vptr;
  char *p;

  if (!*v) { *f='\0'; return; }
  strcpy(f, "");
  for(p = *v; *p != '\0'; p++)
  {
    switch(*p)
    {
      case '\n':
        strcat(f, "\\n");
        break;
      case '\r':
        strcat(f, "\\r");
        break;
      case '"':
        strcat(f, "\\\"");
        break;
      case '\\':
        strcat(f, "\\\\");
        break;
      default:
        strncat(f, p, 1);
    }
  }
  /* strcat(f, "\""); */
} /* mcparmprinter_string */

/* now we may define the parameter structure, using previous functions */
static struct
  {
    int (*getparm)(char *, void *);
    char * (*parminfo)(char *);
    void (*error)(char *, char *);
    void (*printer)(char *, void *);
} mcinputtypes[] = {
  {
    mcparm_double, mcparminfo_double, mcparmerror_double,
    mcparmprinter_double
  }, {
    mcparm_int, mcparminfo_int, mcparmerror_int,
    mcparmprinter_int
  }, {
    mcparm_string, mcparminfo_string, mcparmerror_string,
    mcparmprinter_string
  }
};

/*******************************************************************************
* mcestimate_error: compute sigma from N,p,p2 in Gaussian large numbers approx
*******************************************************************************/
double mcestimate_error(double N, double p1, double p2)
{
  double pmean, n1;
  if(N <= 1)
    return p1;
  pmean = p1 / N;
  n1 = N - 1;
  /* Note: underflow may cause p2 to become zero; the fabs() below guards
     against this. */
  return sqrt((N/n1)*fabs(p2 - pmean*pmean));
}

double (*mcestimate_error_p)
  (double V2, double psum, double p2sum)=mcestimate_error;

/* SECTION: MPI handling ==================================================== */

#ifdef USE_MPI
/* MPI rank */
static int mpi_node_rank;
static int mpi_node_root = 0;


/*******************************************************************************
* mc_MPI_Reduce: Gathers arrays from MPI nodes using Reduce function.
*******************************************************************************/
int mc_MPI_Sum(double *sbuf, long count)
{
  if (!sbuf || count <= 0) return(MPI_ERR_COUNT);
  else {
    /* we must cut the buffer into blocks not exceeding the MPI max buffer size of 32000 */
    long   offset=0;
    double *rbuf=NULL;
    int    length=MPI_REDUCE_BLOCKSIZE; /* defined in mcstas.h */
    int    i=0;
    rbuf = calloc(count, sizeof(double));
    if (!rbuf)
      exit(-fprintf(stderr, "Error: Out of memory %li (mc_MPI_Sum)\n", count*sizeof(double)));
    while (offset < count) {
      if (!length || offset+length > count-1) length=count-offset;
      else length=MPI_REDUCE_BLOCKSIZE;
      MPI_Allreduce((double*)(sbuf+offset), (double*)(rbuf+offset),
              length, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      offset += length;
    }

    for (i=0; i<count; i++) sbuf[i] = rbuf[i];
    free(rbuf);
  }
  return MPI_SUCCESS;
} /* mc_MPI_Sum */

/*******************************************************************************
* mc_MPI_Send: Send array to MPI node by blocks to avoid buffer limit
*******************************************************************************/
int mc_MPI_Send(void *sbuf,
                  long count, MPI_Datatype dtype,
                  int dest)
{
  int dsize;
  long offset=0;
  int  tag=1;
  int  length=MPI_REDUCE_BLOCKSIZE; /* defined in mcstas.h */

  if (!sbuf || count <= 0) return(MPI_ERR_COUNT);
  MPI_Type_size(dtype, &dsize);

  while (offset < count) {
    if (offset+length > count-1) length=count-offset;
    else length=MPI_REDUCE_BLOCKSIZE;
    MPI_Send((void*)(sbuf+offset*dsize), length, dtype, dest, tag++, MPI_COMM_WORLD);
    offset += length;
  }

  return MPI_SUCCESS;
} /* mc_MPI_Send */

/*******************************************************************************
* mc_MPI_Recv: Receives arrays from MPI nodes by blocks to avoid buffer limit
*             the buffer must have been allocated previously.
*******************************************************************************/
int mc_MPI_Recv(void *sbuf,
                  long count, MPI_Datatype dtype,
                  int source)
{
  int dsize;
  long offset=0;
  int  tag=1;
  int  length=MPI_REDUCE_BLOCKSIZE; /* defined in mcstas.h */

  if (!sbuf || count <= 0) return(MPI_ERR_COUNT);
  MPI_Type_size(dtype, &dsize);

  while (offset < count) {
    if (offset+length > count-1) length=count-offset;
    else length=MPI_REDUCE_BLOCKSIZE;
    MPI_Recv((void*)(sbuf+offset*dsize), length, dtype, source, tag++,
            MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    offset += length;
  }

  return MPI_SUCCESS;
} /* mc_MPI_Recv */

#endif /* USE_MPI */

/* SECTION: file i/o handling ================================================ */

/* Multiple output format support. ========================================== */
#ifdef USE_NEXUS
#define mcNUMFORMATS 10
#else
#define mcNUMFORMATS 9
#endif

/*******************************************************************************
* Definition of output formats. structure defined in mcstas-r.h
* Name aliases are defined in mcuse_format_* functions (below)
*******************************************************************************/

/* {Name, Extension, Header, Footer,
    BeginSection, EndSection, AssignTag;
    BeginData, EndData;
    BeginErrors, EndErrors;
    BeginNcount, EndNcount};
 */

mcstatic struct mcformats_struct mcformats[mcNUMFORMATS] = {
  { "McStas", "sim",
    "%PREFormat: %FMT file. Use mcplot/PGPLOT to view.\n"
      "%PREURL:    http://www.mccode.org/\n"
      "%PREEditor: %USR\n"
      "%PRECreator:%SRC simulation (" MCCODE_STRING ")\n"
      "%PREDate:   Simulation started (%DATL) %DAT\n"
      "%PREFile:   %FIL\n",
    "%PREEndDate:%DAT\n",
    "%PREbegin %TYP\n",
    "%PREend %TYP\n",
    "%PRE%NAM: %VAL\n",
    "%PREData [%PAR/%FIL] I: \n", "",
    "%PREErrors [%PAR/%FIL] E: \n", "",
    "%PREEvents [%PAR/%FIL] N: \n", "" },
  { "Scilab", "sci",
    "function mc_%VPA = get_%VPA(p)\n"
      "// %FMT function generated by McStas on %DAT\n"
      "// McStas simulation %SRC: %FIL %FMT\n"
      "// Import data using scilab> exec('%VPA.sci',-1); s=get_%VPA(); and s=get_%VPA('plot'); to plot\n\n"
      "mode(-1); //silent execution\n"
      "if argn(2) > 0, p=1; else p=0; end\n"
      "mc_%VPA = struct();\n"
      "mc_%VPA.Format ='%FMT';\n"
      "mc_%VPA.URL    ='http://www.mccode.org';\n"
      "mc_%VPA.Editor ='%USR';\n"
      "mc_%VPA.Creator='%SRC " MCCODE_STRING " simulation';\n"
      "mc_%VPA.Date   =%DATL; // for getdate\n"
      "mc_%VPA.File   ='%FIL';\n",
    "mc_%VPA.EndDate=%DATL; // for getdate\nendfunction\n"
    "function d=mcload_inline(d)\n"
      "// local inline func to load data\n"
      "execstr(['S=['+part(d.type,10:(length(d.type)-1))+'];']);\n"
      "if ~length(d.data)\n"
      " if ~length(strindex(d.format, 'binary'))\n"
      "  exec(d.filename,-1);p=d.parent;\n"
      "  if ~execstr('d2='+d.func+'();','errcatch'),d=d2; d.parent=p;end\n"
      " else\n"
      "  if length(strindex(d.format, 'float')), t='f';\n"
      "  elseif length(strindex(d.format, 'double')), t='d';\n"
      "  else return; end\n"
      "  fid=mopen(d.filename, 'rb');\n"
      "  pS = prod(S);\n"
      "  x = mget(3*pS, t, fid);\n"
      "  d.data  =matrix(x(1:pS), S);\n"
      "  if length(x) >= 3*pS,\n"
      "  d.errors=matrix(x((pS+1):(2*pS)), S);\n"
      "  d.events=matrix(x((2*pS+1):(3*pS)), S);end\n"
      "  mclose(fid);\n"
      "  return\n"
      " end\n"
      "end\n"
      "endfunction\n"
      "function d=mcplot_inline(d,p)\n"
      "// local inline func to plot data\n"
      "if ~length(strindex(d.type,'0d')), d=mcload_inline(d); end\n"
      "if ~p, return; end;\n"
      "execstr(['l=[',d.xylimits,'];']); S=size(d.data);\n"
      "t1=['['+d.parent+'] '+d.filename+': '+d.title];t = [t1;['  '+d.variables+'=['+d.values+']'];['  '+d.signal];['  '+d.statistics]];\n"
      "mprintf('%s\\n',t(:));\n"
      "if length(strindex(d.type,'0d')),return; end\n"
      "w=winsid();if length(w),w=w($)+1; else w=0; end\n"
      "xbasr(w); xset('window',w);\n"
      "if length(strindex(d.type,'2d'))\n"
      " if S(2) > 1, d.stepx=abs(l(1)-l(2))/(S(2)-1); else d.stepx=0; end\n"
      " if S(1) > 1, d.stepy=abs(l(3)-l(4))/(S(1)-1); else d.stepy=0; end\n"
      " d.x=linspace(l(1)+d.stepx/2,l(2)-d.stepx/2,S(2));\n"
      " d.y=linspace(l(3)+d.stepy/2,l(4)-d.stepy/2,S(1)); z=d.data;\n"
      " xlab=d.xlabel; ylab=d.ylabel; x=d.x; y=d.y;\n"
      " fz=max(abs(z));fx=max(abs(d.x));fy=max(abs(d.y));\n"
      " if fx>0,fx=round(log10(fx)); x=x/10^fx; xlab=xlab+' [*10^'+string(fx)+']'; end\n"
      " if fy>0,fy=round(log10(fy)); y=y/10^fy; ylab=ylab+' [*10^'+string(fy)+']'; end\n"
      " if fz>0,fz=round(log10(fz)); z=z/10^fz; t1=t1+' [*10^'+string(fz)+']'; end\n"
      " xset('colormap',hotcolormap(64));\n"
      " plot3d1(x,y,z',90,0,xlab+'@'+ylab+'@'+d.zlabel,[-1,2,4]); xtitle(t);\n"
      "else\n"
      " if max(S) > 1, d.stepx=abs(l(1)-l(2))/(max(S)-1); else d.stepx=0; end\n"
      " d.x=linspace(l(1)+d.stepx/2,l(2)-d.stepx/2,max(S));\n"
      " plot2d(d.x,d.data);xtitle(t,d.xlabel,d.ylabel);\n"
      "end\n"
      "xname(t1);\nendfunction\n"
    "mc_%VPA=get_%VPA();\n",
    "// Section %TYP [%NAM] (level %LVL)\n"
      "%PREt=[]; execstr('t=mc_%VNA.class','errcatch'); if ~length(t), mc_%VNA = struct(); end; mc_%VNA.class = '%TYP';",
    "%PREmc_%VPA.mc_%VNA = 0; mc_%VPA.mc_%VNA = mc_%VNA;\n",
    "%PREmc_%SEC.%NAM = '%VAL';\n",
    "%PREmc_%VPA.func='get_%VPA';\n%PREmc_%VPA.data = [ \n",
    " ]; // end of data\n%PREif length(mc_%VPA.data) == 0, single_file=0; else single_file=1; end\n%PREmc_%VPA=mcplot_inline(mc_%VPA,p);\n",
    "%PREerrors = [ \n",
    " ]; // end of errors\n%PREif single_file == 1, mc_%VPA.errors=errors; end\n",
    "%PREevents = [ \n",
    " ]; // end of events\n%PREif single_file == 1, mc_%VPA.events=events; end\n"},
  { "Matlab", "m",
    "function mc_%VPA = get_%VPA(p)\n"
      "%% %FMT function generated by McStas on %DAT\n"
      "%% McStas simulation %SRC: %FIL\n"
      "%% Import data using matlab> s=%VPA; and s=%VPA('plot'); to plot\n\n"
      "if nargout == 0 | nargin > 0, p=1; else p=0; end\n"
      "mc_%VPA.Format ='%FMT';\n"
      "mc_%VPA.URL    ='http://www.mccode.org';\n"
      "mc_%VPA.Editor ='%USR';\n"
      "mc_%VPA.Creator='%SRC " MCCODE_STRING " simulation';\n"
      "mc_%VPA.Date   =%DATL; %% for datestr\n"
      "mc_%VPA.File   ='%FIL';\n",
    "mc_%VPA.EndDate=%DATL; %% for datestr\n"
      "function d=mcload_inline(d)\n"
      "%% local inline function to load data\n"
      "S=d.type; eval(['S=[ ' S(10:(length(S)-1)) ' ];']);\n"
      "if isempty(d.data)\n"
      " if ~length(findstr(d.format, 'binary'))\n"
      "  if ~strcmp(d.filename,[d.func,'.m']) copyfile(d.filename,[d.func,'.m']); end\n"
      "  p=d.parent;path(path);\n"
      "  eval(['d=',d.func,';']);d.parent=p;\n"
      "  if ~strcmp(d.filename,[d.func,'.m']) delete([d.func,'.m']); end\n"
      " else\n"
      "  if length(findstr(d.format, 'float')), t='single';\n"
      "  elseif length(findstr(d.format, 'double')), t='double';\n"
      "  else return; end\n"
      "  if length(S) == 1, S=[S 1]; end\n"
      "  fid=fopen(d.filename, 'r');\n"
      "  pS = prod(S);\n"
      "  x = fread(fid, 3*pS, t);\n"
      "  d.data  =reshape(x(1:pS), S);\n"
      "  if prod(size(x)) >= 3*pS,\n"
      "  d.errors=reshape(x((pS+1):(2*pS)), S);\n"
      "  d.events=reshape(x((2*pS+1):(3*pS)), S);end\n"
      "  fclose(fid);\n"
      "  return\n"
      " end\n"
      "end\n"
      "return;\n"
      "function d=mcplot_inline(d,p)\n"
      "%% local inline function to plot data\n"
      "if isempty(findstr(d.type,'0d')), d=mcload_inline(d); end\nif ~p, return; end;\n"
      "eval(['l=[',d.xylimits,'];']); S=size(d.data);\n"
      "t1=['[',d.parent,'] ',d.filename,': ',d.title];t = strvcat(t1,['  ',d.variables,'=[',d.values,']'],['  ',d.signal],['  ',d.statistics]);\n"
      "disp(t);\n"
      "if ~isempty(findstr(d.type,'0d')), return; end\n"
      "figure; if ~isempty(findstr(d.type,'2d'))\n"
      " if S(2) > 1, d.stepx=abs(l(1)-l(2))/(S(2)-1); else d.stepx=0; end\n"
      " if S(1) > 1, d.stepy=abs(l(3)-l(4))/(S(1)-1); else d.stepy=0; end\n"
      " d.x=linspace(l(1)+d.stepx/2,l(2)-d.stepx/2,S(2));\n"
      " d.y=linspace(l(3)+d.stepy/2,l(4)-d.stepy/2,S(1));\n"
      " surface(d.x,d.y,d.data); xlim([l(1) l(2)]); ylim([l(3) l(4)]); shading flat;\n"
      "else\n"
      " if max(S) > 1, d.stepx=abs(l(1)-l(2))/(max(S)-1); else d.stepx=0; end\n"
      " d.x=linspace(l(1)+d.stepx/2,l(2)-d.stepx/2,max(S));\n"
      " plot(d.x,d.data); xlim([l(1) l(2)]);\n"
      "end\n"
      "xlabel(d.xlabel); ylabel(d.ylabel); title(t); \n"
      "set(gca,'position',[.18,.18,.7,.65]); set(gcf,'name',t1);grid on;\n"
      "if ~isempty(findstr(d.type,'2d')), colorbar; end\n",
    "%% Section %TYP [%NAM] (level %LVL)\n"
      "%PREmc_%VNA.class = '%TYP';",
    "mc_%VPA.mc_%VNA = mc_%VNA;\n",
    "%PREmc_%SEC.%NAM = '%VAL';\n",
    "%PREmc_%VPA.func='%VPA';\n%PREmc_%VPA.data = [ \n",
    " ]; %% end of data\nif length(mc_%VPA.data) == 0, single_file=0; else single_file=1; end\nmc_%VPA=mcplot_inline(mc_%VPA,p);\n",
    "%PREerrors = [ \n",
    " ]; %% end of errors\nif single_file, mc_%VPA.errors=errors; end\n",
    "%PREevents = [ \n",
    " ]; %% end of events\nif single_file, mc_%VPA.events=events; end\n"},
  { "IDL", "pro",
    "; %FMT function generated by McStas on %DAT\n"
      "; McStas simulation %SRC: %FIL\n"
      "; import using idl> s=%VPA() and s=%VPA(/plot) to plot\n\n"
      "function mcload_inline,d\n"
      "; local inline function to load external data\n"
      "S=d.type & a=execute('S=long(['+strmid(S,9,strlen(S)-10)+'])')\n"
      "if strpos(d.format, 'binary') lt 0 then begin\n"
      " p=d.parent\n"
      " x=read_binary(d.filename)\n"
      " get_lun, lun\n"
      " openw,lun,d.func+'.pro'\n"
      " writeu, lun,x\n"
      " free_lun,lun\n"
      " resolve_routine, d.func, /is_func, /no\n"
      " d=call_function(d.func)\n"
      "endif else begin\n"
      " if strpos(d.format, 'float') ge 0 then t=4 $\n"
      " else if strpos(d.format, 'double') ge 0 then t=5 $\n"
      " else return,d\n"
      " x=read_binary(d.filename, data_type=t)\n"
      " pS=n_elements(S)\nif pS eq 1 then pS=long(S) $\n"
      " else if pS eq 2 then pS=long(S(0)*S(1)) $\n"
      " else pS=long(S(0)*S(1)*S(2))\n"
      " pS=pS(0)\nstv,d,'data',reform(x(0:(pS-1)),S)\n"
      " d.data=transpose(d.data)\n"
      " if n_elements(x) ge long(3*pS) then begin\n"
      "  stv,d,'errors',reform(x(pS:(2*pS-1)),S)\n"
      "  stv,d,'events',reform(x((2*pS):(3*pS-1)),S)\n"
      "  d.errors=transpose(d.errors)\n"
      "  d.events=transpose(d.events)\n"
      " endif\n"
      "endelse\n"
      "return,d\nend ; FUN load\n"
    "function mcplot_inline,d,p\n"
      "; local inline function to plot data\n"
      "if size(d.data,/typ) eq 7 and strpos(d.type,'0d') lt 0 then d=mcload_inline(d)\n"
      "if p eq 0 or strpos(d.type,'0d') ge 0 then return, d\n"
      "S=d.type & a=execute('S=long(['+strmid(S,9,strlen(S)-10)+'])')\n"
      "stv,d,'data',reform(d.data,S,/over)\n"
      "if total(strpos(tag_names(d),'ERRORS')+1) gt 0 then begin\n"
      " stv,d,'errors',reform(d.errors,S,/over)\n"
      " stv,d,'events',reform(d.events,S,/over)\n"
      "endif\n"
      "d.xylimits=strjoin(strsplit(d.xylimits,' ',/extract),',') & a=execute('l=['+d.xylimits+']')\n"
      "t1='['+d.parent+'] '+d.filename+': '+d.title\n"
      "t=[t1,'  '+d.variables+'=['+d.values+']','  '+d.signal,'  '+d.statistics]\n"
      "print,t\n"
      "if strpos(d.type,'0d') ge 0 then return,d\n"
      "d.xlabel=strjoin(strsplit(d.xlabel,'`!\"^&*()-+=|\\,.<>/?@''~#{[}]',/extract),'_')\n"
      "d.ylabel=strjoin(strsplit(d.ylabel,'`!\"^&*()-+=|\\,.<>/?@''~#{[}]',/extract),'_')\n"
      "stv,d,'x',l(0)+indgen(S(0))*(l(1)-l(0))/S(0)\n"
      "if strpos(d.type,'2d') ge 0 then begin\n"
      "  name={DATA:d.func,IX:d.xlabel,IY:d.ylabel}\n"
      "  stv,d,'y',l(2)+indgen(S(1))*(l(3)-l(2))/S(1)\n"
      "  live_surface,d.data,xindependent=d.x,yindependent=d.y,name=name,reference_out=Win\n"
      "endif else begin\n"
      "  name={DATA:d.func,I:d.xlabel}\n"
      "  live_plot,d.data,independent=d.x,name=name,reference_out=Win\n"
      "endelse\n"
      "live_text,t,Window_In=Win.Win,location=[0.3,0.9]\n"
      "return,d\nend ; FUN plot\n"
    "pro stv,S,T,V\n"
      "; procedure set-tag-value that does S.T=V\n"
      "sv=size(V)\n"
      "T=strupcase(T)\n"
      "TL=strupcase(tag_names(S))\n"
      "id=where(TL eq T)\n"
      "sz=[0,0,0]\n"
      "vd=n_elements(sv)-2\n"
      "type=sv[vd]\n"
      "if id(0) ge 0 then d=execute('sz=SIZE(S.'+T+')')\n"
      "if (sz(sz(0)+1) ne sv(sv(0)+1)) or (sz(0) ne sv(0)) $\n"
      "  or (sz(sz(0)+2) ne sv(sv(0)+2)) $\n"
      "  or type eq 8 then begin\n"
      " ES = ''\n"
      " for k=0,n_elements(TL)-1 do begin\n"
      "  case TL(k) of\n"
      "   T:\n"
      "   else: ES=ES+','+TL(k)+':S.'+TL(k)\n"
      "  endcase\n"
      " endfor\n"
      " d=execute('S={'+T+':V'+ES+'}')\n"
      "endif else d=execute('S.'+T+'=V')\n"
      "end ; PRO stv\n"
    "function %VPA,plot=plot\n"
      "; %FMT function generated by McStas on %DAT\n"
      "; McStas simulation %SRC: %FIL\n"
      "; import using s=%VPA() and s=%VPA(/plot) to plot\n"
      "if keyword_set(plot) then p=1 else p=0\n"
      "%7$s={Format:'%FMT',URL:'http://www.mccode.org',"
      "Editor:'%USR',$\n"
      "Creator:'%SRC " MCCODE_STRING " simulation',$\n"
      "Date:%DATL,"
      "File:'%FIL'}\n",
    "stv,%VPA,'EndDate',%DATL ; for systime\nreturn, %VPA\nend\n",
    "; Section %TYP [%NAM] (level %LVL)\n"
      "%PRE%VNA={class:'%TYP'}\n",
    "%PREstv,%VPA,'%VNA',%VNA\n",
    "%PREstv,%SEC,'%NAM','%VAL'\n",
    "%PREstv,%VPA,'func','%VPA' & data=[ $\n",
    " ]\n%PREif size(data,/type) eq 7 then single_file=0 else single_file=1\n"
    "%PREstv,%VPA,'data',data & data=0 & %VPA=mcplot_inline(%VPA,p)\n",
    "%PREif single_file ne 0 then begin errors=[ ",
    " ]\n%PREstv,%VPA,'errors',reform(errors,%MDIM,%NDIM,/over) & errors=0\n%PREendif\n",
    "%PREif single_file ne 0 then begin events=[ ",
    " ]\n%PREstv,%VPA,'events',reform(events,%MDIM,%NDIM,/over) & events=0\n%PREendif\n\n"},
  { "XML", "xml",
    "<?xml version=\"1.0\" ?>\n<!--\n"
      "URL:    http://www.nexusformat.org/\n"
      "Editor: %USR\n"
      "Creator:%SRC " MCCODE_STRING " [www.mccode.org].\n"
      "Date:   Simulation started (%DATL) %DAT\n"
      "File:   %FIL\n"
      "View with Mozilla, InternetExplorer, gxmlviewer, kxmleditor\n-->\n"
      "<NX%PAR file_name=\"%FIL\" file_time=\"%DAT\" user=\"%USR\">\n"
        "<NXentry name=\"" MCCODE_STRING "\"><start_time>%DAT</start_time>\n",
    "<end_time>%DAT</end_time></NXentry></NX%PAR>\n<!-- EndDate:%DAT -->\n",
    "%PRE<NX%TYP name=\"%NAM\">\n",
    "%PRE</NX%TYP>\n",
    "%PRE<%NAM>%VAL</%NAM>\n",
    "%PRE<%XVL long_name=\"%XLA\" axis=\"1\" primary=\"1\" min=\"%XMIN\""
        " max=\"%XMAX\" dims=\"%MDIM\" range=\"1\"></%XVL>\n"
      "%PRE<%YVL long_name=\"%YLA\" axis=\"2\" primary=\"1\" min=\"%YMIN\""
        " max=\"%YMAX\" dims=\"%NDIM\" range=\"1\"></%YVL>\n"
      "%PRE<%ZVL long_name=\"%ZLA\" axis=\"3\" primary=\"1\" min=\"%ZMIN\""
        " max=\"%ZMAX\" dims=\"%PDIM\" range=\"1\"></%ZVL>\n"
      "%PRE<data long_name=\"%TITL\" signal=\"1\"  axis=\"[%XVL,%YVL,%ZVL]\" file_name=\"%FIL\">\n",
    "%PRE</data>\n",
    "%PRE<errors>\n", "%PRE</errors>\n",
    "%PRE<monitor>\n", "%PRE</monitor>\n"},
  { "HTML", "html",
    "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD %DAT//EN\"\n"
      "\"http://www.w3.org/TR/html4/strict.dtd\">\n"
      "<HTML><HEAD><META name=\"Author\" content=\"%PAR\">\n"
      "<META name=\"Creator\" content=\"%PAR (%SRC) " MCCODE_STRING " [www.mccode.org] simulation\">\n"
      "<META name=\"Date\" content=\"%DAT\">\n"
      "<TITLE>[McStas %PAR (%SRC)]%FIL</TITLE></HEAD>\n"
      "<BODY><center><h1><a name=\"%PAR\">"
        "McStas simulation %SRC (%SRC): Result file %FIL.html</a></h1></center><br>\n"
        "This simulation report was automatically created by"
        " <a href=\"http://www.mccode.org/\"><i>" MCCODE_STRING "</i></a><br>\n"
        "<pre>User:   %USR<br>\n"
        "%PRECreator: <a href=\"%SRC\">%SRC</a> %PAR McStas simulation<br>\n"
        "%PREFormat:  %FMT<br>\n"
        "%PREDate:    (%DATL) %DAT<br></pre>\n"
        "VRML viewers may be obtained at <a href=\"http://cic.nist.gov/vrml/vbdetect.html\">http://cic.nist.gov/vrml/vbdetect.html</a>\n",
    "<b>EndDate: </b>(%DATL) %DAT<br></BODY></HTML>\n",
    "%PRE<h%LVL><a name=\"%NAM\">%TYP %NAM</a></h%LVL> "
      "[child of <a href=\"#%PAR\">%PAR</a>]<br>\n",
    "[end of <a href=\"#%NAM\">%TYP %NAM</a>]<br>\n",
    "%PRE<b>%NAM: </b>%VAL<br>\n",
    "%PRE<b>DATA</b><br><center><embed src=\"%FIL\" type=\"model/vrml\" width=\"75%%\" height=\"50%%\"></embed><br>File <a href=\"%FIL\">%FIL [VRML format]</a></center><br>\n", "%PREEnd of DATA<br>\n",
    "%PRE<b>ERRORS</b><br>\n","%PREEnd of ERRORS<br>\n",
    "%PRE<b>EVENTS</b><br>\n", "%PREEnd of EVENTS<br>\n"},
  { "Octave", "m",
    "function mc_%VPA = get_%VPA(p)\n"
      "%% %FMT function generated by McStas on %DAT\n"
      "%% McStas simulation %SRC: %FIL\n"
      "%% Import data using octave> s=%VPA(); and plot with s=%VPA('plot');\n"
      "if nargin > 0, p=1; else p=0; end\n"
      "mc_%VPA.Format ='%FMT';\n"
      "mc_%VPA.URL    ='http://www.mccode.org';\n"
      "mc_%VPA.Editor ='%USR';\n"
      "mc_%VPA.Creator='%SRC " MCCODE_STRING " simulation';\n"
      "mc_%VPA.Date   =%DATL; %% for datestr\n"
      "mc_%VPA.File   ='%FIL';\n",
    "mc_%VPA.EndDate=%DATL; %% for datestr\nendfunction\n"
      "if exist('mcload_inline'), return; end\n"
      "function d=mcload_inline(d)\n"
      "%% local inline function to load data\n"
      "S=d.type; eval(['S=[ ' S(10:(length(S)-1)) ' ];']);\n"
      "if isempty(d.data)\n"
      " if ~length(findstr(d.format, 'binary'))\n"
      "  source(d.filename);p=d.parent;\n"
      "  eval(['d=get_',d.func,';']);d.parent=p;\n"
      " else\n"
      "  if length(findstr(d.format, 'float')), t='float';\n"
      "  elseif length(findstr(d.format, 'double')), t='double';\n"
      "  else return; end\n"
      "  if length(S) == 1, S=[S 1]; end\n"
      "  fid=fopen(d.filename, 'r');\n"
      "  pS = prod(S);\n"
      "  x = fread(fid, 3*pS, t);\n"
      "  d.data  =reshape(x(1:pS), S);\n"
      "  if prod(size(x)) >= 3*pS,\n"
      "  d.errors=reshape(x((pS+1):(2*pS)), S);\n"
      "  d.events=reshape(x((2*pS+1):(3*pS)), S);end\n"
      "  fclose(fid);\n"
      "  return\n"
      " end\n"
      "end\n"
      "return;\nendfunction\n\n"
      "function d=mcplot_inline(d,p)\n"
      "%% local inline function to plot data\n"
      "if isempty(findstr(d.type,'0d')), d=mcload_inline(d); end\nif ~p, return; end;\n"
      "eval(['l=[',d.xylimits,'];']); S=size(d.data);\n"
      "t1=['[',d.parent,'] ',d.filename,': ',d.title];t = strcat(t1,['  ',d.variables,'=[',d.values,']'],['  ',d.signal],['  ',d.statistics]);\n"
      "disp(t);\n"
      "if ~isempty(findstr(d.type,'0d')), return; end\n"
      "xlabel(d.xlabel); ylabel(d.ylabel); title(t);"
      "figure; if ~isempty(findstr(d.type,'2d'))\n"
      " if S(2) > 1, d.stepx=abs(l(1)-l(2))/(S(2)-1); else d.stepx=0; end\n"
      " if S(1) > 1, d.stepy=abs(l(3)-l(4))/(S(1)-1); else d.stepy=0; end\n"
      " d.x=linspace(l(1)+d.stepx/2,l(2)-d.stepx/2,S(2));\n"
      " d.y=linspace(l(3)+d.stepy/2,l(4)-d.stepy/2,S(1));\n"
      " mesh(d.x,d.y,d.data);\n"
      "else\n"
      " if max(S) > 1, d.stepx=abs(l(1)-l(2))/(max(S)-1); else d.stepx=0; end\n"
      " d.x=linspace(l(1)+d.stepx/2,l(2)-d.stepx/2,max(S));\n"
      " plot(d.x,d.data);\n"
      "end\nendfunction\n",
    "%% Section %TYP [%NAM] (level %LVL)\n"
      "mc_%VNA.class = '%TYP';",
    "mc_%VPA.mc_%VNA = mc_%VNA;\n",
    "mc_%SEC.%NAM = '%VAL';\n",
    "mc_%VPA.func='%VPA';\n%PREmc_%VPA.data = [ \n",
    " ]; %% end of data\nif length(mc_%VPA.data) == 0, single_file=0; else single_file=1; end\nmc_%VPA=mcplot_inline(mc_%VPA,p);\n",
    "errors = [ \n",
    " ]; %% end of errors\nif single_file, mc_%VPA.errors=errors; end\n",
    "events = [ \n",
    " ]; %% end of events\nif single_file, mc_%VPA.events=events; end\n"},
  { "VRML", "wrl",
    "#VRML V2.0 utf8\n# Format: %FMT file\n"
      "use freeWRL, openvrml, vrmlview, CosmoPlayer, Cortona, Octaga... to view file\n"
      "WorldInfo {\n"
      "title \"%SRC/%FIL simulation Data\"\n"
      "info [ \"URL:    http://www.mccode.org/\"\n"
      "       \"Editor: %USR\"\n"
      "       \"Creator:%SRC simulation (" MCCODE_STRING ")\"\n"
      "       \"Date:   Simulation started (%DATL) %DAT\"\n"
      "       \"File:   %FIL\" ]\n}\n"
      "Background { skyAngle [ 1.57 1.57] skyColor [0 0 1, 1 1 1, 0.1 0 0] }\n",
    "# EndDate:%DAT\n",
    "# begin %TYP %PAR\n",
    "# end %TYP %PAR\n",
    "%PRE%SEC.%NAM= '%VAL'\n",
    "# The Proto that contains data values and objects to plot these\n"
      "PROTO I_ERR_N_%VPA [\n"
      "# the PROTO parameters\n"
      "  field MFFloat Data [ ]\n"
      "  field MFFloat Errors [ ]\n"
      "  field MFFloat Ncounts [ ]\n"
      "] { # The plotting objects/methods in the Proto\n"
      "  # draw a small sphere at the origin\n"
      "  DEF Data_%VPA Group {\n"
      "  children [\n"
      "    DEF CoordinateOrigin Group {\n"
      "      children [\n"
      "        Transform { translation  0 0 0 }\n"
      "        Shape { \n"
      "          appearance Appearance { \n"
      "            material Material {\n"
      "              diffuseColor 1.0 1.0 0.0\n"
      "              transparency 0.5 } }\n"
      "          geometry Sphere { radius .01 } \n"
      "    } ] }\n"
      "    # defintion of the arrow allong Y axis\n"
      "    DEF ARROW Group {\n"
      "      children [\n"
      "        Transform {\n"
      "          translation 0 0.5 0\n"
      "          children [\n"
      "            Shape {\n"
      "              appearance DEF ARROW_APPEARANCE Appearance {\n"
      "                material Material {\n"
      "                  diffuseColor .3 .3 1\n"
      "                  emissiveColor .1 .1 .33\n"
      "                }\n"
      "              }\n"
      "              geometry Cylinder {\n"
      "                bottom FALSE\n"
      "                radius .005\n"
      "                height 1\n"
      "                top FALSE\n"
      "        } } ] }\n"
      "        Transform {\n"
      "          translation 0 1 0\n"
      "          children [\n"
      "            DEF ARROW_POINTER Shape {\n"
      "              geometry Cone {\n"
      "                bottomRadius .05\n"
      "                height .1\n"
      "              }\n"
      "              appearance USE ARROW_APPEARANCE\n"
      "    } ] } ] }\n"
      "    # the arrow along X axis\n"
      "    Transform {\n"
      "      translation 0 0 0\n"
      "      rotation 1 0 0 1.57\n"
      "      children [\n"
      "        Group {\n"
      "          children [ \n"
      "            USE ARROW\n"
      "    ] } ] }\n"
      "    # the arrow along Z axis\n"
      "    Transform {\n"
      "      translation 0 0 0\n"
      "      rotation 0 0 1 -1.57\n"
      "      children [\n"
      "        Group {\n"
      "          children [ \n"
      "            USE ARROW\n"
      "    ] } ] }\n"
      "    # the Y label (which is vertical)\n"
      "    DEF Y_Label Group {\n"
      "      children [\n"
      "        Transform {\n"
      "          translation 0 1 0\n"
      "          children [\n"
      "            Billboard {\n"
      "              children [\n"
      "                Shape {\n"
      "                  appearance DEF LABEL_APPEARANCE Appearance {\n"
      "                    material Material {\n"
      "                      diffuseColor 1 1 .3\n"
      "                      emissiveColor .33 .33 .1\n"
      "                    } }\n"
      "                  geometry Text { \n"
      "                    string [ \"%ZVAR: %ZLA\", \"%ZMIN:%ZMAX - %PDIM points\" ] \n"
      "                    fontStyle FontStyle {  size .2 }\n"
      "    } } ] } ] } ] }\n"
      "    # the X label\n"
      "    DEF X_Label Group {\n"
      "      children [\n"
      "        Transform {\n"
      "          translation 1 0 0\n"
      "          children [\n"
      "            Billboard {\n"
      "              children [\n"
      "                Shape {\n"
      "                  appearance DEF LABEL_APPEARANCE Appearance {\n"
      "                    material Material {\n"
      "                      diffuseColor 1 1 .3\n"
      "                      emissiveColor .33 .33 .1\n"
      "                    } }\n"
      "                  geometry Text { \n"
      "                    string [ \"%XVAR: %XLA\", \"%XMIN:%XMAX - %MDIM points\" ] \n"
      "                    fontStyle FontStyle {  size .2 }\n"
      "    } } ] } ] } ] }\n"
      "    # the Z label\n"
      "    DEF Z_Label Group {\n"
      "      children [\n"
      "        Transform {\n"
      "          translation 0 0 1\n"
      "          children [\n"
      "            Billboard {\n"
      "              children [\n"
      "                Shape {\n"
      "                  appearance DEF LABEL_APPEARANCE Appearance {\n"
      "                    material Material {\n"
      "                      diffuseColor 1 1 .3\n"
      "                      emissiveColor .33 .33 .1\n"
      "                    } }\n"
      "                  geometry Text { \n"
      "                    string [ \"%YVAR: %YLA\", \"%YMIN:%YMAX - %NDIM points\" ] \n"
      "                    fontStyle FontStyle {  size .2 }\n"
      "    } } ] } ] } ] }\n"
      "    # The text information (header data )\n"
      "    DEF Header Group {\n"
      "      children [\n"
      "        Transform {\n"
      "          translation 0 2 0\n"
      "          children [\n"
      "            Billboard {\n"
      "              children [\n"
      "                Shape {\n"
      "                  appearance Appearance {\n"
      "                    material Material { \n"
      "                      diffuseColor .9 0 0\n"
      "                      emissiveColor .9 0 0 }\n"
      "                  }\n"
      "                  geometry Text {\n"
      "                    string [ \"%PAR/%FIL\",\"%TITL\" ]\n"
      "                    fontStyle FontStyle {\n"
      "                        style \"BOLD\"\n"
      "                        size .2\n"
      "    } } } ] } ] } ] }\n"
      "    # The Data plot\n"
      "    DEF MonitorData Group {\n"
      "      children [\n"
      "        DEF TransformData Transform {\n"
      "          children [\n"
      "            Shape {\n"
      "              appearance Appearance {\n"
      "                material Material { emissiveColor 0 0.2 0 }\n"
      "              }\n"
      "              geometry ElevationGrid {\n"
      "                xDimension  %MDIM\n"
      "                zDimension  %NDIM\n"
      "                xSpacing    1\n"
      "                zSpacing    1\n"
      "                solid       FALSE\n"
      "                height IS Data\n"
      "    } } ] } ] }\n"
      "    # The VRMLScript that redimension x and z axis within 0:1\n"
      "    # and re-scale data within 0:1\n"
      "    DEF GetScale Script {\n"
      "      eventOut SFVec3f scale_vect\n"
      "      url \"javascript: \n"
      "        function initialize( ) {\n"
      "          scale_vect = new SFVec3f(1.0/%MDIM, 1.0/Math.abs(%ZMAX-%ZMIN), 1.0/%NDIM); }\" }\n"
      "  ] }\n"
      "ROUTE GetScale.scale_vect TO TransformData.scale\n} # end of PROTO\n"
      "# now we call the proto with Data values\n"
      "I_ERR_N_%VPA {\nData [\n",
    "] # End of Data\n",
    "Errors [\n",
    "] # End of Errors\n",
    "Ncounts [\n",
    "] # End of Ncounts\n}" },
    { "Python", "py",
    "from numpy import array\ndef %VPA(p):\n"
      "  '''\n  %PAR (%FIL) procedure generated from McStas on %DAT\n\n"
      "  McStas simulation %SRC: %FIL\n"
      "  import data using python> s=%VPA(p); with p=0/1 for no plot/plot\n  '''\n"
      "  %VPA={'Format':'%FMT','URL':'http://www.mccode.org',\\\n"
      "  'Editor':'%USR',\\\n"
      "  'Creator':'%SRC " MCCODE_STRING " [www.mccode.org]',\\\n"
      "  'Date':%DATL,\\\n"
      "  'File':'%FIL'}\n",
    "  %VPA['EndDate']=%DATL\nreturn %VPA\n",
    "  # Section Section %TYP [%NAM] (level %LVL)\n"
      "  %VNA['class'] = '%TYP'\n",
    "  %VPA['%VNA'] = %VNA\n  del %VNA\n",
    "  %SEC['%NAM'] = '%VAL'\n",
    "  %VPA['func'] ='%VPA'\n  %VPA['data'] = [ ",
    "  ] # end of data\n",
    "  %VPA['errors'] = [ ",
    "  ] # end of errors\n",
    "  %VPA['ncount'] = [ ",
    "  ] # end of ncount\n",
    }
#ifdef USE_NEXUS
    ,
    { "NeXus", "nxs",
    "%PREFormat: %FMT file. Use hdfview to view.\n"
      "%PREURL:    http://www.mccode.org/\n"
      "%PREEditor: %USR\n"
      "%PRECreator:%SRC simulation (" MCCODE_STRING ")\n"
      "%PREDate:   Simulation started (%DATL) %DAT\n"
      "%PREFile:   %FIL\n",
    "%PREEndDate:%DAT\n",
    "%PREbegin %TYP\n",
    "%PREend %TYP\n",
    "%PRE%NAM: %VAL\n",
    "", "",
    "%PREErrors [%PAR/%FIL]: \n", "",
    "%PREEvents [%PAR/%FIL]: \n", "" }
#endif
};

/*******************************************************************************
* mcfull_file: allocates a full file name=mcdirname+file. Catenate extension if missing.
*******************************************************************************/
char *mcfull_file(char *name, char *ext)
{
  int   dirlen=0;
  char *mem   =NULL;

  dirlen = mcdirname ? strlen(mcdirname) : 0;
  mem = malloc(dirlen + strlen(name) + CHAR_BUF_LENGTH);
  if(!mem) {
    exit(-fprintf(stderr, "Error: Out of memory %li (mcfull_file)\n", (long)(dirlen + strlen(name) + 256)));
  }
  strcpy(mem, "");

  /* prepend directory name to path if name does not contain a path */
  if (dirlen > 0 && !strchr(name, MC_PATHSEP_C)) {
    strcat(mem, mcdirname);
    strcat(mem, MC_PATHSEP_S);
  } /* dirlen */

  strcat(mem, name);
  if (!strchr(name, '.') && ext && strlen(ext))
  { /* add extension if not in file name already */
    strcat(mem, ".");
    strcat(mem, ext);
  }
  return(mem);
} /* mcfull_file */

/*******************************************************************************
* mcnew_file: opens a new file within mcdirname if non NULL
*             if mode is non 0, then mode is used, else mode is 'w'
*******************************************************************************/
FILE *mcnew_file(char *name, char *ext, char *mode)
{
  char *mem;
  FILE *file;

  if (!name || strlen(name) == 0 || mcdisable_output_files) return(NULL);

  mem = mcfull_file(name, ext);
  file = fopen(mem, (mode ? mode : "w"));
  if(!file)
    fprintf(stderr, "Warning: could not open output file '%s' in mode '%s' (mcnew_file)\n", mem, mode);
  else {
    if (!mcopenedfiles ||
        (mcopenedfiles && mcopenedfiles_size <= strlen(mcopenedfiles)+strlen(mem))) {
      mcopenedfiles_size+=CHAR_BUF_LENGTH;
      if (!mcopenedfiles || !strlen(mcopenedfiles))
        mcopenedfiles = calloc(1, mcopenedfiles_size);
      else
        mcopenedfiles = realloc(mcopenedfiles, mcopenedfiles_size);
    }
    strcat(mcopenedfiles, " ");
    strcat(mcopenedfiles, mem);
  }
  free(mem);

  return file;
} /* mcnew_file */

/*******************************************************************************
* str_rep: Replaces a token by an other (of SAME length) in a string
* This function modifies 'string'
*******************************************************************************/
char *str_rep(char *string, char *from, char *to)
{
  char *p;

  if (!string || !strlen(string)) return(string);
  if (strlen(from) != strlen(to)) return(string);

  p   = string;

  while (( p = strstr(p, from) ) != NULL) {
    long index;
    for (index=0; index<strlen(to); index++) p[index]=to[index];
  }
  return(string);
} /* str_rep */

#define VALID_NAME_LENGTH 64
/*******************************************************************************
* mcvalid_name: makes a valid string for variable names.
*   copy 'original' into 'valid', replacing invalid characters by '_'
*   char arrays must be pre-allocated. n can be 0, or the maximum number of
*   chars to be copied/checked
*******************************************************************************/
static char *mcvalid_name(char *valid, char *original, int n)
{
  long i;


  if (original == NULL || strlen(original) == 0)
  { strcpy(valid, "noname"); return(valid); }
  if (n <= 0) n = strlen(valid);

  if (n > strlen(original)) n = strlen(original);
  else original += strlen(original)-n;
  strncpy(valid, original, n);

  for (i=0; i < n; i++)
  {
    if ( (valid[i] > 122)
      || (valid[i] < 32)
      || (strchr("!\"#$%&'()*+,-.:;<=>?@[\\]^`/ \n\r\t", valid[i]) != NULL) )
    {
      if (i) valid[i] = '_'; else valid[i] = 'm';
    }
  }
  valid[i] = '\0';

  return(valid);
} /* mcvalid_name */

/*******************************************************************************
* pfprintf: just as fprintf with positional arguments %N$t,
*   but with (char *)fmt_args being the list of arg type 't'.
*   Needed as the vfprintf is not correctly handled on some platforms.
*   1- look for the maximum %d$ field in fmt
*   2- look for all %d$ fields up to max in fmt and set their type (next alpha)
*   3- retrieve va_arg up to max, and save pointer to arg in local arg array
*   4- use strchr to split around '%' chars, until all pieces are written
*   returns number of arguments written.
* Warning: this function is restricted to only handles types t=s,g,i,li
*          without additional field formating, e.g. %N$t
*******************************************************************************/
static int pfprintf(FILE *f, char *fmt, char *fmt_args, ...)
{
  #define MyNL_ARGMAX 50
  char  *fmt_pos;

  char *arg_char[MyNL_ARGMAX];
  int   arg_int[MyNL_ARGMAX];
  long  arg_long[MyNL_ARGMAX];
  double arg_double[MyNL_ARGMAX];

  char *arg_posB[MyNL_ARGMAX];  /* position of '%' */
  char *arg_posE[MyNL_ARGMAX];  /* position of '$' */
  char *arg_posT[MyNL_ARGMAX];  /* position of type */

  int   arg_num[MyNL_ARGMAX];   /* number of argument (between % and $) */
  int   this_arg=0;
  int   arg_max=0;
  va_list ap;

  if (!f || !fmt_args || !fmt) return(-1);
  for (this_arg=0; this_arg<MyNL_ARGMAX;  arg_num[this_arg++] =0); this_arg = 0;
  fmt_pos = fmt;
  while(1)  /* analyse the format string 'fmt' */
  {
    char *tmp;

    arg_posB[this_arg] = (char *)strchr(fmt_pos, '%');
    tmp = arg_posB[this_arg];
    if (tmp)	/* found a percent */
    {
      char  printf_formats[]="dliouxXeEfgGcs\0";
      arg_posE[this_arg] = (char *)strchr(tmp, '$');
      if (arg_posE[this_arg] && isdigit(tmp[1]))
      { /* found a dollar following a percent  and a digit after percent */
        char  this_arg_chr[10];


        /* extract positional argument index %*$ in fmt */
        strncpy(this_arg_chr, arg_posB[this_arg]+1, arg_posE[this_arg]-arg_posB[this_arg]-1 < 10 ? arg_posE[this_arg]-arg_posB[this_arg]-1 : 9);
        this_arg_chr[arg_posE[this_arg]-arg_posB[this_arg]-1] = '\0';
        arg_num[this_arg] = atoi(this_arg_chr);
        if (arg_num[this_arg] <=0 || arg_num[this_arg] >= MyNL_ARGMAX)
          return(-fprintf(stderr,"pfprintf: invalid positional argument number (%i is <=0 or >=%i) from '%s'.\n", arg_num[this_arg], MyNL_ARGMAX, this_arg_chr));
        /* get type of positional argument: follows '%' -> arg_posE[this_arg]+1 */
        fmt_pos = arg_posE[this_arg]+1;
        fmt_pos[0] = tolower(fmt_pos[0]);
        if (!strchr(printf_formats, fmt_pos[0]))
          return(-fprintf(stderr,"pfprintf: invalid positional argument type (%c != expected %c).\n", fmt_pos[0], fmt_args[arg_num[this_arg]-1]));
        if (fmt_pos[0] == 'l' && (fmt_pos[1] == 'i' || fmt_pos[1] == 'd')) fmt_pos++;
        arg_posT[this_arg] = fmt_pos;
        /* get next argument... */
        this_arg++;
      }
      else
      { /* no dollar or no digit */
        if  (tmp[1] == '%') {
          fmt_pos = arg_posB[this_arg]+2;  /* found %% */
        } else if (strchr(printf_formats,tmp[1])) {
          fmt_pos = arg_posB[this_arg]+1;  /* found %s */
        } else {
          return(-fprintf(stderr,"pfprintf: must use only positional arguments (%s).\n", arg_posB[this_arg]));
        }
      }
    } else
      break;  /* no more % argument */
  }
  arg_max = this_arg;
  /* get arguments from va_arg list, according to their type */
  va_start(ap, fmt_args);
  for (this_arg=0; this_arg<strlen(fmt_args); this_arg++)
  {

    switch(tolower(fmt_args[this_arg]))
    {
      case 's':                       /* string */
              arg_char[this_arg] = va_arg(ap, char *);
              break;
      case 'd':
      case 'i':
      case 'c':                      /* int */
              arg_int[this_arg] = va_arg(ap, int);
              break;
      case 'l':                       /* long int */
              arg_long[this_arg] = va_arg(ap, long int);
              break;
      case 'f':
      case 'g':
      case 'e':                      /* double */
              arg_double[this_arg] = va_arg(ap, double);
              break;
      default: fprintf(stderr,"pfprintf: argument type is not implemented (arg %%%i$ type %c).\n", this_arg+1, fmt_args[this_arg]);
    }
  }
  va_end(ap);
  /* split fmt string into bits containing only 1 argument */
  fmt_pos = fmt;
  for (this_arg=0; this_arg<arg_max; this_arg++)
  {
    char *fmt_bit;
    int   arg_n;

    if (arg_posB[this_arg]-fmt_pos>0)
    {
      fmt_bit = (char*)malloc(arg_posB[this_arg]-fmt_pos+10);
      if (!fmt_bit) return(-fprintf(stderr,"pfprintf: not enough memory.\n"));
      strncpy(fmt_bit, fmt_pos, arg_posB[this_arg]-fmt_pos);
      fmt_bit[arg_posB[this_arg]-fmt_pos] = '\0';
      fprintf(f, "%s", fmt_bit); /* fmt part without argument */
    } else
    {
      fmt_bit = (char*)malloc(10);
      if (!fmt_bit) return(-fprintf(stderr,"pfprintf: not enough memory.\n"));
    }
    arg_n = arg_num[this_arg]-1; /* must be >= 0 */
    strcpy(fmt_bit, "%");
    strncat(fmt_bit, arg_posE[this_arg]+1, arg_posT[this_arg]-arg_posE[this_arg]);
    fmt_bit[arg_posT[this_arg]-arg_posE[this_arg]+1] = '\0';

    switch(tolower(fmt_args[arg_n]))
    {
      case 's': fprintf(f, fmt_bit, arg_char[arg_n]);
                break;
      case 'd':
      case 'i':
      case 'c':                      /* int */
              fprintf(f, fmt_bit, arg_int[arg_n]);
              break;
      case 'l':                       /* long */
              fprintf(f, fmt_bit, arg_long[arg_n]);
              break;
      case 'f':
      case 'g':
      case 'e':                       /* double */
              fprintf(f, fmt_bit, arg_double[arg_n]);
              break;
    }
    fmt_pos = arg_posT[this_arg]+1;
    if (this_arg == arg_max-1)
    { /* add eventual leading characters for last parameter */
      if (fmt_pos < fmt+strlen(fmt))
        fprintf(f, "%s", fmt_pos);
    }
    if (fmt_bit) free(fmt_bit);

  }
  return(this_arg);
} /* pfprintf */

/*******************************************************************************
* mcinfo_header: output header/footer tags/info using specific file format.
*   the 'part' may be 'header' or 'footer' depending on part to write.
* RETURN: negative==error, positive=all went fine
* Used by: mcsiminfo_init, mcsiminfo_close
*******************************************************************************/
static int mcinfo_header(MCDETECTOR detector, char *part)
{
  char*HeadFoot;              /* format string */
  long date_l;                /* date as a long number */
  char date[CHAR_BUF_LENGTH]; /* date as a string */
  char valid_parent[VALID_NAME_LENGTH];     /* who generates that: the simulation or mcstas */

  if (mcdisable_output_files) return(-1);
  if (!detector.file_handle && !strstr(detector.format.Name,"NeXus")) return(-1);
  if (strcmp(part,"header") && strstr(detector.format.Name, "no header")) return (-2);
  if (strcmp(part,"footer") && strstr(detector.format.Name, "no footer")) return (-3);

  /* initiate date and format string ======================================== */

  date_l = detector.date_l;   /* use current write time (from import) by default */
  strncpy(date, detector.date, CHAR_BUF_LENGTH);

  if (part && !strcmp(part,"footer"))
    HeadFoot = detector.format.Footer; /* footer has always detector (current) write time */
  else {
    HeadFoot = detector.format.Header; /* SIM header has simulation start time */
    if (detector.file_handle == mcsiminfo_file
        && !strcmp(detector.filename, mcsiminfo_name)) {
      date_l = mcstartdate;
      time_t t = (time_t)date_l;
      strncpy(date, ctime(&t), CHAR_BUF_LENGTH);
      if (strlen(date)) date[strlen(date)-1] = '\0';
    }
  }

  mcvalid_name(valid_parent,
    strlen(detector.filename) && detector.file_handle!=mcsiminfo_file ?
      detector.filename : detector.simulation, VALID_NAME_LENGTH);

  /* output header ========================================================== */

#ifdef USE_NEXUS
  if (strstr(detector.format.Name, "NeXus")) {
    if(mcnxinfo_header(mcnxHandle, part,
      detector.prefix,                          /* %1$s  PRE  */
      detector.instrument,                      /* %2$s  SRC  */
      strlen(detector.filename) ?
        detector.filename: detector.simulation, /* %3$s  FIL  */
      detector.format.Name,                     /* %4$s  FMT  */
      detector.date,                            /* %5$s  DAT  */
      detector.user,                            /* %6$s  USR  */
      valid_parent,                             /* %7$s  PAR  */
      detector.date_l) == NX_ERROR) {           /* %8$li DATL */
        fprintf(stderr, "Error: writing NeXus header file %s (mcinfo_header)\n",
          strlen(detector.filename) ?
            detector.filename: detector.simulation);
        /* close information data set opened by mcnxfile_section */
      return(-1);
    }
    else return(1);
  }
  else
#endif
  return(pfprintf(detector.file_handle, HeadFoot, "sssssssl",
    detector.prefix,                          /* %1$s  PRE  */
    detector.instrument,                      /* %2$s  SRC  */
    strlen(detector.filename) ?
      detector.filename: detector.simulation, /* %3$s  FIL  */
    detector.format.Name,                     /* %4$s  FMT  */
    detector.date,                            /* %5$s  DAT  */
    detector.user,                            /* %6$s  USR  */
    valid_parent,                             /* %7$s  PAR  */
    detector.date_l));                        /* %8$li DATL */
} /* mcinfo_header */

/*******************************************************************************
* mcinfo_tag: output tag/value using specific file format.
*   outputs a tag/value pair e.g. section.name=value
* RETURN: negative==error, positive=all went fine
* Used by: mcfile_section, mcinfo_instrument, mcinfo_simulation, mcinfo_data
*******************************************************************************/
static int mcinfo_tag(MCDETECTOR detector, char *section, char *name, char *value)
{
  char valid_section[VALID_NAME_LENGTH];
  char valid_name[VALID_NAME_LENGTH];
  int i;

  if (!detector.file_handle && !strstr(detector.format.Name,"NeXus")) return(-1);
  if (!strlen(detector.format.AssignTag)
   || !name || !strlen(name)
   || mcdisable_output_files) return(-1);

  mcvalid_name(valid_section, section, VALID_NAME_LENGTH);
  mcvalid_name(valid_name,    name,    VALID_NAME_LENGTH);

  /* remove quote chars in values =========================================== */
  if (strstr(detector.format.Name, "Scilab")
   || strstr(detector.format.Name, "Matlab")
   || strstr(detector.format.Name, "IDL"))
    for(i = 0; i < strlen(value); i++) {
      if (value[i] == '"' || value[i] == '\'')   value[i] = ' ';
      if (value[i] == '\n'  || value[i] == '\r') value[i] = ';';
    }

  /* output tag ============================================================= */
#ifdef USE_NEXUS
  if (strstr(detector.format.Name, "NeXus")) {
    if(mcnxinfo_tag(mcnxHandle, detector.prefix, valid_section, name, value) == NX_ERROR) {
      fprintf(stderr, "Error: writing NeXus tag file %s/%s=%s (mcinfo_tag)\n", section, name, value);
      return(-1);
    } else return(1); }
  else
#endif
  return(pfprintf(detector.file_handle, detector.format.AssignTag, "ssss",
    detector.prefix,  /* %1$s PRE */
    valid_section,    /* %2$s SEC */
    valid_name,       /* %3$s NAM */
    value));          /* %4$s VAL */
} /* mcinfo_tag */

/*******************************************************************************
* mcfile_section: output section start/end using specific file format.
*   'part' may be 'begin' or 'end' depending on section part to write
*   'type' may be e.g. 'instrument','simulation','component','data'
*   the prefix is automatically indented/un-indented
* RETURN: detector structure with updated prefix (indentation)
* Used by: mcsiminfo_init, mcsiminfo_close, mcinfo_simulation, mcdetector_write_sim
*******************************************************************************/
MCDETECTOR mcfile_section(MCDETECTOR detector, char *part, char *parent, char *section, char *type, int level)
{
  char *Section;
  char valid_section[VALID_NAME_LENGTH];
  char valid_parent[VALID_NAME_LENGTH];

  if((!detector.file_handle && !strstr(detector.format.Name,"NeXus"))
   || !section || !strlen(section)
   || mcdisable_output_files) return(detector);
  if (strcmp(part,"begin") && strstr(detector.format.Name, "no header")) return (detector);
  if (strcmp(part,"end")   && strstr(detector.format.Name, "no footer")) return (detector);

  /* initiate format string and handle prefix-- ============================= */

  if (part && !strcmp(part,"end")) Section = detector.format.EndSection;
  else                             Section = detector.format.BeginSection;

  if (!strlen(Section)) return (detector);

  mcvalid_name(valid_section, section, VALID_NAME_LENGTH);
  if (parent && strlen(parent)) mcvalid_name(valid_parent, parent, VALID_NAME_LENGTH);
  else                                strcpy(valid_parent, "root");

  if (!strcmp(part,"end") && strlen(detector.prefix) >= 2)
    detector.prefix[strlen(detector.prefix)-2]='\0'; /* un-indent prefix */

  /* output section ========================================================= */

#ifdef USE_NEXUS
  if (strstr(detector.format.Name, "NeXus")) {
    if (mcnxfile_section(mcnxHandle,part,
      detector.prefix, type, section, valid_section, parent, valid_parent, level) == NX_ERROR) {
      fprintf(stderr, "Error: writing NeXus section %s/%s=NX%s (mcfile_section)\n", parent, section, type);
      return(detector);
    }
  }
  else
#endif
  pfprintf(detector.file_handle, Section, "ssssssi",
    detector.prefix,/* %1$s  PRE  */
    type,           /* %2$s  TYP  */
    section,        /* %3$s  NAM  */
    valid_section,  /* %4$s  VNA  */
    parent,         /* %5$s  PAR  */
    valid_parent,   /* %6$s  VPA  */
    level);         /* %7$i  LVL */

  /* handle prefix++ and write section start ID ============================= */

  if (!strcmp(part,"begin"))
  {
    if (strlen(detector.prefix) < CHAR_BUF_LENGTH-2) strcat(detector.prefix,"  ");  /* indent prefix */
    if (section && strlen(section))
      mcinfo_tag(detector, section, "name",   section);
    if (parent && strlen(parent))
      mcinfo_tag(detector, section, "parent", parent);
  }

  return(detector);
} /* mcfile_section */

/*******************************************************************************
* mcinfo_instrument: output instrument tags/info. Only written to SIM file
* RETURN: negative==error, positive=all went fine
* Used by: mcsiminfo_init, mcdetector_write_sim
*******************************************************************************/
static int mcinfo_instrument(MCDETECTOR detector, char *name)
{
  char Parameters[CHAR_BUF_LENGTH] = "";
  int  i;

  if (!detector.file_handle && !strstr(detector.format.Name,"NeXus")) return(-1);
  if (!name || !strlen(name)
   || mcdisable_output_files) return(-1);

  /* create parameter string ================================================ */

  for(i = 0; i < mcnumipar; i++)
  {
    char ThisParam[VALID_NAME_LENGTH];
    if (strlen(mcinputtable[i].name) > VALID_NAME_LENGTH) break;
    snprintf(ThisParam, VALID_NAME_LENGTH, " %s(%s)", mcinputtable[i].name,
            (*mcinputtypes[mcinputtable[i].type].parminfo)
                (mcinputtable[i].name));
    strcat(Parameters, ThisParam);
    if (strlen(Parameters) >= CHAR_BUF_LENGTH-VALID_NAME_LENGTH) break;
  }

  /* output data ============================================================ */
  mcinfo_tag(detector, name, "Parameters",    Parameters);
  mcinfo_tag(detector, name, "Source",        mcinstrument_source);
  mcinfo_tag(detector, name, "Trace_enabled", mctraceenabled ? "yes" : "no");
  mcinfo_tag(detector, name, "Default_main",  mcdefaultmain ? "yes" : "no");
  mcinfo_tag(detector, name, "Embedded_runtime",
#ifdef MC_EMBEDDED_RUNTIME
         "yes"
#else
         "no"
#endif
         );

  fflush(NULL);

  return(1);
} /* mcinfo_instrument */

/*******************************************************************************
* mcinfo_simulation: output simulation tags/info
* RETURN: detector structure with updated prefix (indentation)
* Used by: mcsiminfo_init, mcdetector_write_sim
*******************************************************************************/
MCDETECTOR mcinfo_simulation(MCDETECTOR detector, char *instr)
{
  int i;
  char Parameters[CHAR_BUF_LENGTH];

  if (!detector.file_handle && !strstr(detector.format.Name,"NeXus")) return(detector);
  if (!instr || !strlen(instr)
   || mcdisable_output_files) return(detector);

  mcinfo_tag(detector, instr, "Ncount",      detector.ncount);
  mcinfo_tag(detector, instr, "Trace",       mcdotrace ? "yes" : "no");
  mcinfo_tag(detector, instr, "Gravitation", mcgravitation ? "yes" : "no");
  snprintf(Parameters, CHAR_BUF_LENGTH, "%ld", mcseed);
  mcinfo_tag(detector, instr, "Seed", Parameters);

  /* output parameter string ================================================ */
  if (!strstr(detector.format.Name, "McStas")) {
#ifdef USE_NEXUS
    /* close simulation/information */
    if (strstr(detector.format.Name, "NeXus"))
    if (NXclosedata(mcnxHandle) == NX_ERROR)
      fprintf(stderr, "Warning: NeXus: could not close data set simulation/information in %s\n", detector.filename);
#endif
    detector = mcfile_section(detector, "begin", instr, "parameters", "parameters", 3);
  }

  for(i = 0; i < mcnumipar; i++) {
    if (mcget_run_num() || (mcinputtable[i].val && strlen(mcinputtable[i].val))) {
      if (mcinputtable[i].par == NULL)
        strncpy(Parameters, (mcinputtable[i].val ? mcinputtable[i].val : ""), CHAR_BUF_LENGTH);
      else
        (*mcinputtypes[mcinputtable[i].type].printer)(Parameters, mcinputtable[i].par);
      if (strstr(detector.format.Name, "McStas"))
        fprintf(detector.file_handle, "%sParam: %s=%s\n", detector.prefix, mcinputtable[i].name, Parameters);
      else
        mcinfo_tag(detector, "parameters", mcinputtable[i].name, Parameters);
    }
  }
#ifdef USE_NEXUS
  if (strstr(mcformat.Name, "NeXus")) {
    /* close information SDS opened with mcfile_section(detector, "parameters") */
    if (NXclosedata(mcnxHandle) == NX_ERROR)
      fprintf(stderr, "Warning: NeXus: could not close data set simulation/parameters/information in %s\n", detector.filename);
    /* in the parameter group, create one SDS per parameter */
    mcnxfile_parameters(mcnxHandle);

    /* this can not be included in nexus-lib as it requires mcinputtypes which
      is defined only in mccode-r.c (points to function that are defined here) */


    /* re-open the simulation/information dataset */
    if (NXopendata(mcnxHandle, "information") == NX_ERROR) {
      fprintf(stderr, "Warning: NeXus: could not re-open 'information' for simulation in %s\n", detector.filename);
    }
  }
#endif
  if (!strstr(detector.format.Name, "McStas"))
    detector = mcfile_section(detector, "end", instr, "parameters", "parameters", 3);

  fflush(NULL);

  return(detector);
} /* mcinfo_simulation */

/*******************************************************************************
* mcinfo_data: output data tags/info
*              mcinfo_data(detector, NULL)        writes data info to SIM file
*              mcinfo_data(detector, "data file") writes info to data file
* Used by: mcdetector_write_sim, mcdetector_write_data
*******************************************************************************/
void mcinfo_data(MCDETECTOR detector, char *filename)
{
  char parent[CHAR_BUF_LENGTH];

  if (!detector.m || mcdisable_output_files) return;

  /* access either SIM or Data file */
  if (!filename) {
    /* use SIM file which has already be opened with instrument/simulation info */
    detector.file_handle = mcsiminfo_file;
    if (strstr(detector.format.Name, "McStas") && strlen(detector.prefix)>1) detector.prefix[0]=' ';
  } else {
    if (!detector.file_handle) return;
  }

  /* parent must be a valid name */
  mcvalid_name(parent, detector.filename, VALID_NAME_LENGTH);

  /* output data ============================================================ */
  mcinfo_tag(detector, parent, "type",       detector.type);
  mcinfo_tag(detector, parent, "Source",     mcinstrument_source);
  mcinfo_tag(detector, parent, (strstr(detector.format.Name,"McStas") ?
        "component" : "parent"),             detector.component);
  mcinfo_tag(detector, parent, "position",   detector.position);

  mcinfo_tag(detector, parent, "title",      detector.title);
  mcinfo_tag(detector, parent, !mcget_run_num() || mcget_run_num() >= mcget_ncount() ?
      "Ncount" : "ratio",      detector.ncount);

  if (strlen(detector.filename)) {
    mcinfo_tag(detector, parent, "filename", detector.filename);
    mcinfo_tag(detector, parent, "format",   detector.format.Name);
  }

  mcinfo_tag(detector, parent, "statistics", detector.statistics);
  mcinfo_tag(detector, parent, strstr(detector.format.Name, "NeXus") ?
       "Signal" : "signal",                  detector.signal);
  mcinfo_tag(detector, parent, "values",     detector.values);

  if (detector.rank >= 1 || detector.rank == -1)
  {
    mcinfo_tag(detector, parent, (strstr(detector.format.Name," scan ") ?
          "xvars" : "xvar"),                 detector.xvar);
    mcinfo_tag(detector, parent, (strstr(detector.format.Name," scan ") ?
          "yvars" : "yvar"),                 detector.yvar);
    mcinfo_tag(detector, parent, "xlabel",   detector.xlabel);
    mcinfo_tag(detector, parent, "ylabel",   detector.ylabel);
    if (detector.rank > 1 || detector.rank == -1) {
      mcinfo_tag(detector, parent, "zvar",   detector.zvar);
      mcinfo_tag(detector, parent, "zlabel", detector.zlabel);
    }
  }

  mcinfo_tag(detector, parent, abs(detector.rank)==1 && strstr(detector.format.Name, "McStas") ?
                    "xlimits" : "xylimits", detector.limits);
  mcinfo_tag(detector, parent, "variables", detector.rank == -1 ?
                                            detector.zvar : detector.variables);

   if (mcDetectorCustomHeader && strlen(mcDetectorCustomHeader)) {
     mcinfo_tag(detector, parent, "custom",  mcDetectorCustomHeader);
   }

   fflush(NULL);
} /* mcinfo_data */

/*******************************************************************************
* mcdetector_import: build detector structure, merge non-lists from MPI
*                    compute basic stat, write "Detector:" line
*                    mcdetector_import(... filename=NULL ...) sets siminfo data
* RETURN:            detector structure. Invalid data if detector.p1 == NULL
*                    Invalid detector sets m=0 and filename=""
*                    Simulation data  sets m=0 and filename=mcsiminfo_name
* Used by: mcdetector_out_nD public functions, mcdetector_import_sim
*******************************************************************************/
MCDETECTOR mcdetector_import(struct mcformats_struct format,
  char *component, char *title,
  long m, long n,  long p,
  char *xlabel, char *ylabel, char *zlabel,
  char *xvar, char *yvar, char *zvar,
  double x1, double x2, double y1, double y2, double z1, double z2,
  char *filename,
  double *p0, double *p1, double *p2,
  Coords position)
{
  time_t t;       /* for detector.date */
  long   date_l;  /* date as a long number */
  char   istransposed=0;
  char   c[CHAR_BUF_LENGTH]; /* temp var for signal label */

  MCDETECTOR detector;

  /* build MCDETECTOR structure ============================================= */
  /* make sure we do not have NULL for char fields */

  /* these also apply to simfile */
  strncpy (detector.filename,  filename ? filename : "",    CHAR_BUF_LENGTH);
  /* add extension if missing */
  if (strlen(detector.filename) && !strchr(detector.filename, '.') && format.Extension)
  { /* add extension if not in file name already */
    strcat(detector.filename, ".");
    strcat(detector.filename, format.Extension);
  }
  strncpy (detector.component, component ? component : "McStas component", CHAR_BUF_LENGTH);
  snprintf(detector.simulation, CHAR_BUF_LENGTH, "%s%s%s",
      mcdirname && strlen(mcdirname) ? mcdirname : ".", MC_PATHSEP_S, mcsiminfo_name);
  detector.format=format;

  /* set prefix: # for McStas data files, VRML and Python */
  strcpy(detector.prefix,
      (strstr(format.Name, "McStas") && (strlen(detector.filename)) ?
      "# " : ""));

  snprintf(detector.instrument, CHAR_BUF_LENGTH, "%s (%s)", mcinstrument_name, mcinstrument_source);
  snprintf(detector.user, CHAR_BUF_LENGTH,      "%s on %s",
        getenv("USER") ? getenv("USER") : "mcstas",
        getenv("HOST") ? getenv("HOST") : "localhost");
  time(&t);         /* get current write time */
  date_l = (long)t; /* same but as a long */
  snprintf(detector.date, CHAR_BUF_LENGTH, "%s", ctime(&t));
  if (strlen(detector.date))   detector.date[strlen(detector.date)-1] = '\0'; /* remove last \n in date */
  detector.date_l = date_l;

  if (!mcget_run_num() || mcget_run_num() >= mcget_ncount())
    snprintf(detector.ncount, CHAR_BUF_LENGTH, "%g", (double)mcget_ncount());
  else
    snprintf(detector.ncount, CHAR_BUF_LENGTH, "%g/%g", (double)mcget_run_num(), (double)mcget_ncount());

  detector.p0         = p0; detector.p0_orig=p0;
  detector.p1         = p1; detector.p1_orig=p1;
  detector.p2         = p2; detector.p2_orig=p2;
  detector.file_handle= filename ? NULL : mcsiminfo_file;

  /* handle transposition */
  if (!strstr(format.Name, "NeXus")) {
    if (m<0 || n<0 || p<0)                istransposed = !istransposed;
    if (strstr(format.Name, "binary"))    istransposed = !istransposed;
    if (strstr(format.Name, "transpose")) istransposed = !istransposed;
    if (istransposed) { /* do the swap once for all */
      long i=m; m=abs(n); n=abs(i); p=abs(p);
    }
  }
  m=abs(m); n=abs(n); p=abs(p); /* make sure dimensions are positive */
  detector.istransposed = istransposed;

  /* determine detector rank (dimensionality) */
  if (!m || !n || !p || !p1) detector.rank = 4; /* invalid: exit with m=0 filename="" */
  else if (strstr(format.Name," scan ")) detector.rank=-1;  /* 1D scan: multiarray */
  else if (m*n*p == 1)       detector.rank = 0; /* 0D */
  else if (n == 1 || m == 1) detector.rank = 1; /* 1D */
  else if (p == 1)           detector.rank = 2; /* 2D */
  else                       detector.rank = 3; /* 3D */

  /* from rank, set type */
  switch (detector.rank) {
    case -1: sprintf(detector.type, "multiarray_1d(%ld)", n); p=1; break;
    case 0:  strcpy(detector.type,  "array_0d"); m=n=p=1; break;
    case 1:  sprintf(detector.type, "array_1d(%ld)", m*n*p); m *= n*p; n=p=1; break;
    case 2:  sprintf(detector.type, "array_2d(%ld, %ld)", m, n*p); n *= p; p=1; break;
    case 3:  sprintf(detector.type, "array_3d(%ld, %ld, %ld)", m, n, p); break;
    default: m=0; strcpy(detector.type, ""); strcpy(detector.filename, "");/* invalid */
  }

  detector.m    = m;
  detector.n    = n;
  detector.p    = p;

/* init default values for statistics */
  detector.intensity  = 0;
  detector.error      = 0;
  detector.events     = 0;
  detector.min        = 0;
  detector.max        = 0;
  detector.mean       = 0;
  detector.centerX    = 0;
  detector.halfwidthX = 0;
  detector.centerY    = 0;
  detector.halfwidthY = 0;

  /* these only apply to detector files ===================================== */

  snprintf(detector.position, CHAR_BUF_LENGTH, "%g %g %g", position.x, position.y, position.z);
  /* may also store actual detector orientation in the future */

  strncpy(detector.title,      title && strlen(title) ? title : component,       CHAR_BUF_LENGTH);
  strncpy(detector.xlabel,     xlabel && strlen(xlabel) ? xlabel : "X", CHAR_BUF_LENGTH); /* axis labels */
  strncpy(detector.ylabel,     ylabel && strlen(ylabel) ? ylabel : "Y", CHAR_BUF_LENGTH);
  strncpy(detector.zlabel,     zlabel && strlen(zlabel) ? zlabel : "Z", CHAR_BUF_LENGTH);
  strncpy(detector.xvar,       xvar && strlen(xvar) ? xvar :       "x", CHAR_BUF_LENGTH); /* axis variables */
  strncpy(detector.yvar,       yvar && strlen(yvar) ? yvar :       detector.xvar, CHAR_BUF_LENGTH);
  strncpy(detector.zvar,       zvar && strlen(zvar) ? zvar :       detector.yvar, CHAR_BUF_LENGTH);

  /* set "variables" as e.g. "I I_err N" */
  strcpy(c, "I ");
  if (strlen(detector.zvar))      strncpy(c, detector.zvar,32);
  else if (strlen(detector.yvar)) strncpy(c, detector.yvar,32);
  else if (strlen(detector.xvar)) strncpy(c, detector.xvar,32);

  if (detector.rank == 1)
    snprintf(detector.variables, CHAR_BUF_LENGTH, "%s %s %s_err N", detector.xvar, c, c);
  else
    snprintf(detector.variables, CHAR_BUF_LENGTH, "%s %s_err N", c, c);

  /* limits */
  detector.xmin = x1;
  detector.xmax = x2;
  detector.ymin = y1;
  detector.ymax = y2;
  detector.zmin = z1;
  detector.zmax = z2;
  if (abs(detector.rank) == 1 && strstr(format.Name, "McStas"))
    snprintf(detector.limits, CHAR_BUF_LENGTH, "%g %g", x1, x2);
  else if (detector.rank == 2)
    snprintf(detector.limits, CHAR_BUF_LENGTH, "%g %g %g %g", x1, x2, y1, y2);
  else
    snprintf(detector.limits, CHAR_BUF_LENGTH, "%g %g %g %g %g %g", x1, x2, y1, y2, z1, z2);

  if (!m || !n || !p) {
    return(detector);
  }

  /* if MPI and nodes_nb > 1: reduce data sets when using MPI =============== */
  /* not for scan steps/multiarray (only processed by root */
#ifdef USE_MPI
  if (!strstr(format.Name," list ") && mpi_node_count > 1 && detector.rank != -1) {
    /* we save additive data: reduce everything into mpi_node_root */
    if (p0) mc_MPI_Sum(p0, m*n*p);
    if (p1) mc_MPI_Sum(p1, m*n*p);
    if (p2) mc_MPI_Sum(p2, m*n*p);
    if (!p0) {  /* additive signal must be then divided by the number of nodes */
      int i;
      for (i=0; i<m*n*p; i++) {
        p1[i] /= mpi_node_count;
        if (p2) p2[i] /= mpi_node_count;
      }
    }
  }
#endif /* USE_MPI */

  /* compute statistics and update MCDETECTOR structure ===================== */
  double sum_z   = 0;
  double min_z   = 0;
  double max_z   = 0;
  double fmon_x=0, smon_x=0, fmon_y=0, smon_y=0, mean_z=0;
  double Nsum=0;
  double P2sum=0;

  double sum_xz  = 0;
  double sum_yz  = 0;
  double sum_x   = 0;
  double sum_y   = 0;
  double sum_x2z = 0;
  double sum_y2z = 0;
  int    i,j;
  char   hasnan=0;
  char   hasinf=0;
  char   israw = (strstr(detector.format.Name," raw") != NULL);
  double *this_p1=NULL; /* new 1D McStas array [x I E N]. Freed after writing data */

  /* if McStas/PGPLOT and rank==1 we create a new m*4 data block=[x I E N] */
  if (detector.rank == 1 && strstr(detector.format.Name,"McStas")) {
    this_p1 = (double *)calloc(detector.m*detector.n*detector.p*4, sizeof(double));
    if (!this_p1)
      exit(-fprintf(stderr, "Error: Out of memory creating %li 1D McStas data set for file '%s' (mcdetector_import)\n", detector.m*detector.n*detector.p*4*sizeof(double*), detector.filename));
  }

  max_z = min_z = p1[0];

  for(j = 0; j < n*p; j++)
  {
    for(i = 0; i < m; i++)
    {
      double x,y,z;
      double N, E;
      long   index= !detector.istransposed ? i*n*p + j : i+j*m;

      if (m) x = x1 + (i + 0.5)/m*(x2 - x1); else x = 0;
      if (n && p) y = y1 + (j + 0.5)/n/p*(y2 - y1); else y = 0;
      z = p1[index];
      N = p0 ? p0[index] : 1;
      E = p2 ? p2[index] : 0;
      if (p2 && !israw) p2[index] = (*mcestimate_error_p)(p0[i],p1[i],p2[i]); /* set sigma */

      /* compute stats integrals */
      sum_xz += x*z;
      sum_yz += y*z;
      sum_x += x;
      sum_y += y;
      sum_z += z;
      sum_x2z += x*x*z;
      sum_y2z += y*y*z;
      if (z > max_z) max_z = z;
      if (z < min_z) min_z = z;

      Nsum += N;
      P2sum += E;

      if (isnan(z) || isnan(E) || isnan(N)) hasnan=1;
      if (isinf(z) || isinf(E) || isinf(N)) hasinf=1;

      if (detector.rank == 1 && this_p1 && strstr(detector.format.Name,"McStas")) {
        /* fill-in 1D McStas array [x I E N] */
        this_p1[index*4]   = x;
        this_p1[index*4+1] = z;
        this_p1[index*4+2] = p2 ? p2[index] : 0;
        this_p1[index*4+3] = N;
      }
    }
  } /* for j */

  /* compute 1st and 2nd moments */
  if (sum_z && n*m*p)
  {
    fmon_x = sum_xz/sum_z;
    fmon_y = sum_yz/sum_z;
    smon_x = sum_x2z/sum_z-fmon_x*fmon_x; smon_x = smon_x > 0 ? sqrt(smon_x) : 0;
    smon_y = sum_y2z/sum_z-fmon_y*fmon_y; smon_y = smon_y > 0 ? sqrt(smon_y) : 0;
    mean_z = sum_z/n/m/p;
  }
  /* store statistics into detector */
  detector.intensity = sum_z;
  detector.error     = (*mcestimate_error_p)(Nsum, sum_z, P2sum);
  detector.events    = Nsum;
  detector.min       = min_z;
  detector.max       = max_z;
  detector.mean      = mean_z;
  detector.centerX   = fmon_x;
  detector.halfwidthX= smon_x;
  detector.centerY   = fmon_y;
  detector.halfwidthY= smon_y;

  /* if McStas/PGPLOT and rank==1 replace p1 with new m*4 1D McStas and clear others */
  if (detector.rank == 1 && this_p1 && strstr(detector.format.Name,"McStas")) {
    detector.p1 = this_p1;
    detector.n=detector.m; detector.m  = 4;
    detector.p0 = NULL;
    detector.p2 = NULL;
    detector.istransposed = 1;
  }

  if (n*m*p > 1)
    sprintf(detector.signal, "Min=%g; Max=%g; Mean=%g;", detector.min, detector.max, detector.mean);
  else
    strcpy(detector.signal, "");
  sprintf(detector.values, "%g %g %g", detector.intensity, detector.error, detector.events);

  switch (detector.rank) {
    case 0:  strcpy(detector.statistics, ""); break;
    case 1:  snprintf(detector.statistics, CHAR_BUF_LENGTH, "X0=%g; dX=%g;",
      detector.centerX, detector.halfwidthX); break;
    case 2:
    case 3:  snprintf(detector.statistics, CHAR_BUF_LENGTH, "X0=%g; dX=%g; Y0=%g; dY=%g;",
      detector.centerX, detector.halfwidthX, detector.centerY, detector.halfwidthY);
      break;
    default: strcpy(detector.statistics, "");
  }

#ifdef USE_MPI
  /* slaves are done */
  if(mpi_node_rank != mpi_node_root) {
    return detector;
  }
#endif
  /* output "Detector:" line ================================================ */
  /* when this is a detector written by a component (not the SAVE from instrument),
     not a multiarray nor event lists */
  if (detector.rank == -1 || strstr(format.Name," list ")) return(detector);

  if (!strcmp(detector.component, mcinstrument_name)) {
    if (strlen(detector.filename))  /* we name it from its filename, or from its title */
      mcvalid_name(c, detector.filename, CHAR_BUF_LENGTH);
    else
      mcvalid_name(c, detector.title, CHAR_BUF_LENGTH);
  } else
    strncpy(c, detector.component, CHAR_BUF_LENGTH);  /* usual detectors written by components */

  printf("Detector: %s_I=%g %s_ERR=%g %s_N=%g",
         c, detector.intensity,
         c, detector.error,
         c, detector.events);
  printf(" \"%s\"\n", strlen(detector.filename) ? detector.filename : detector.component);

  if (hasnan)
    printf("WARNING: Nan detected in component %s\n", detector.component);
  if (hasinf)
    printf("WARNING: Inf detected in component %s\n", detector.component);

  /* add warning in case of low statistics or large number of bins in text format mode */
  if (detector.error > detector.intensity/4)
    printf("WARNING: file '%s': Low Statistics\n",    detector.filename);
  else if (strlen(detector.filename)) {
    if (m*n*p > 1000 && Nsum < m*n*p && Nsum)
       printf(
        "WARNING: file '%s': Low Statistics (%g events in %ldx%ldx%ld bins).\n",
        detector.filename, Nsum, m,n,p);
    if ( !strstr(format.Name, "binary")
      && (strstr(format.Name, "Scilab") || strstr(format.Name, "Matlab"))
      && (n*m*p > 10000 || m > 1000) ) printf(
        "WARNING: file '%s' (%s format)\n"
        "         Large matrices (%ldx%ldx%ld) in text mode may be\n"
        "         slow or fail at import. Prefer binary mode e.g. --format='Matlab_binary'.\n",
        detector.filename, format.Name, m,n,p);
  }

  if (mcDetectorCustomHeader && strlen(mcDetectorCustomHeader)) {
   if (strstr(detector.format.Name, "Octave") || strstr(detector.format.Name, "Matlab"))
     str_rep(mcDetectorCustomHeader, "%PRE", "%   ");
   else if (strstr(detector.format.Name, "IDL"))    str_rep(mcDetectorCustomHeader, "%PRE", ";   ");
   else if (strstr(detector.format.Name, "Scilab")) str_rep(mcDetectorCustomHeader, "%PRE", "//  ");
   else if (strstr(detector.format.Name, "McStas")) str_rep(mcDetectorCustomHeader, "%PRE", "#   ");
   else str_rep(mcDetectorCustomHeader, "%PRE", "    ");
  }

  return(detector);
} /* mcdetector_import */

/*******************************************************************************
* mcdetector_register: adds detector to the detector array, for scans and sim abstract
* RETURN:            detector array pointer
* Used by: mcdetector_out_xD
*******************************************************************************/
MCDETECTOR* mcdetector_register(MCDETECTOR detector)
{
#ifdef USE_MPI
  /* only for Master */
  if(mpi_node_rank != mpi_node_root)                      return(NULL);
#endif

  if (detector.m) {
    /* add detector entry in the mcDetectorArray */
    if (!mcDetectorArray ||
        (mcDetectorArray && mcDetectorArray_size <= mcDetectorArray_index)) {
      mcDetectorArray_size+=CHAR_BUF_LENGTH;
      if (!mcDetectorArray || !mcDetectorArray_index)
        mcDetectorArray = (MCDETECTOR*)calloc(mcDetectorArray_size, sizeof(MCDETECTOR));
      else
        mcDetectorArray = (MCDETECTOR*)realloc(mcDetectorArray, mcDetectorArray_size*sizeof(MCDETECTOR));
    }
    mcDetectorArray[mcDetectorArray_index++]=detector;
  }

  return(mcDetectorArray);

} /* mcdetector_register */

MCDETECTOR mcdetector_init(void) {
  Coords zero={0.0,0.0,0.0};
  MCDETECTOR detector=mcdetector_import(mcformat, "mcstas", NULL,
    0,0,0,            /* mnp */
    NULL, NULL, NULL, /* labels */
    NULL, NULL, NULL, /* vars */
    0,0,0,0,0,0,      /* limits */
    NULL,             /* filename */
    NULL, NULL, NULL, /* p012 */
    zero);
  strncpy(detector.filename, mcsiminfo_name, CHAR_BUF_LENGTH);
  return(detector);
}

/*******************************************************************************
* mcdetector_import_sim: build detector structure as SIM data
* RETURN:            detector structure for SIM (m=0 and filename=mcsiminfo_name).
* Used by: mcsiminfo_init, mcsiminfo_close, mcdetector_write_sim
*******************************************************************************/
MCDETECTOR mcdetector_import_sim(void) {
  MCDETECTOR detector  = mcdetector_init();
  detector.file_handle = mcsiminfo_file;
  return(detector);
}

/*******************************************************************************
* mcsiminfo_init: writes simulation structured description file (mcstas.sim)
*                 f may be NULL or e.g. stdout
*                 opens NX file if this is NeXus format
* Used by: mcsave/mcfinally from code generation (cogen), mcinfo, mcstas_main
*******************************************************************************/
static int mcsiminfo_init(FILE *f)
{

#ifdef USE_MPI
  /* only for Master */
  if(mpi_node_rank != mpi_node_root)                      return(-1);
#endif
  if (mcdisable_output_files)                             return(-2);
  if (!f && (!mcsiminfo_name || !strlen(mcsiminfo_name))) return(-3);

  /* clear list of opened files to start new save session */
  if (mcopenedfiles && strlen(mcopenedfiles) > 0) strcpy(mcopenedfiles, "");

  /* open sim file ========================================================== */

#ifdef USE_NEXUS
  /* NeXus sim info is the NeXus root file. */
  if(strstr(mcformat.Name, "NeXus")) {
    mcsiminfo_file = NULL;
    if (mcnxfile_init(mcsiminfo_name, mcformat.Extension,
      strstr(mcformat.Name, "append") || strstr(mcformat.Name, "catenate") ? "a":"w",
      &mcnxHandle) == NX_ERROR) {
      return(-fprintf(stderr, "Error: opening NeXus file %s (mcsiminfo_init)\n", mcsiminfo_name));
    } else {
      mcsiminfo_file = (FILE*)mcnxHandle; /* make it non NULL */
    }
  } else /* not NX - includes next if+else */
#endif
  {
    if (!f || (!mcsiminfo_file && f != stdout))
      mcsiminfo_file = mcnew_file(mcsiminfo_name, mcformat.Extension, "w");
    else mcsiminfo_file = f;
  }

  if(!mcsiminfo_file)
    return(-fprintf(stderr,
            "Warning: could not open simulation description file '%s' (mcsiminfo_init)\n",
            mcsiminfo_name));

  /* initialize sim file information, sets detector.file_handle=mcsiminfo_file */
  MCDETECTOR mcsiminfo = mcdetector_import_sim();

  /* flag true for McStas or NX data format */
  int  ismcstas_nx= (strstr(mcformat.Name, "McStas") || strstr(mcformat.Name, "NeXus"));

  /* start to write meta data =============================================== */

#ifdef USE_NEXUS
  if (strstr(mcformat.Name, "NeXus")) {
    /* NXentry class */
    char file_time[CHAR_BUF_LENGTH];
    snprintf(file_time, CHAR_BUF_LENGTH, "%s_%li", mcinstrument_name, mcstartdate);
    mcsiminfo = mcfile_section(mcsiminfo, "begin", "mcstas", file_time, "entry", 1);
  }
#endif
  mcinfo_header(mcsiminfo, "header");
#ifdef USE_NEXUS
  /* close information SDS opened with mcfile_section(siminfo, "mcstas (header)") */
  if (strstr(mcformat.Name, "NeXus")) {
    if (NXclosedata(mcnxHandle) == NX_ERROR)
      fprintf(stderr, "Warning: NeXus: could not close data set mcstas/information (header)\n");
  }
#endif

  /* begin-end instrument =================================================== */
  mcsiminfo = mcfile_section(mcsiminfo, "begin", mcsiminfo.simulation, mcinstrument_name, "instrument", 1);
  mcinfo_instrument(mcsiminfo, mcinstrument_name);

#ifdef USE_NEXUS
  if (strstr(mcformat.Name, "NeXus")) {
    /* close information SDS opened with mcfile_section(siminfo, "instrument") */
    if (NXclosedata(mcnxHandle) == NX_ERROR)
      fprintf(stderr, "Warning: NeXus: could not close data set instrument/information\n");
    /* insert instrument source code */
    mcnxfile_instrcode(mcnxHandle, mcinstrument_source, mcinstrument_name);
  }
#endif
  if (ismcstas_nx)
    mcsiminfo = mcfile_section(mcsiminfo, "end", mcsiminfo.simulation, mcinstrument_name, "instrument", 1);

  /* begin-end simulation =================================================== */
  mcsiminfo = mcfile_section(mcsiminfo, "begin", mcsiminfo.simulation, mcsiminfo_name, "simulation", 2);
  mcsiminfo = mcinfo_simulation(mcsiminfo, mcsiminfo_name);

#ifdef USE_NEXUS
  /* close information SDS opened with mcfile_section(siminfo,"simulation") */
  if (strstr(mcformat.Name, "NeXus")) {
    if (NXclosedata(mcnxHandle) == NX_ERROR)
      fprintf(stderr, "Warning: NeXus: could not close data set simulation/information\n");
  }
#endif
  if (ismcstas_nx)
    mcsiminfo = mcfile_section(mcsiminfo, "end", mcsiminfo.simulation, mcsiminfo_name, "simulation", 2);

  return(1);
} /* mcsiminfo_init */

/*******************************************************************************
* mcdetector_write_content: write mcstas.dat, which has integrated intensities for all monitors
* Used by: mcsiminfo_close
*******************************************************************************/
int mcdetector_write_content(MCDETECTOR *DetectorArray, long DetectorArray_index)
{
#ifdef USE_MPI
  /* only for Master */
  if(mpi_node_rank != mpi_node_root)                      return(-1);
#endif
  if (mcdisable_output_files)                             return(-2);
  if (!mcsiminfo_name || !strlen(mcsiminfo_name))         return(-3);
  if (!DetectorArray_index)                               return(-4);

  /* build p1 array from all detector integrated counts */
  double *this_p1 = (double *)calloc(DetectorArray_index*2, sizeof(double));
  int i;

  if (this_p1 && DetectorArray) {
    char   *labels=NULL;
    long    labels_size=0;
    long    index=0;
    for (i=0; i < DetectorArray_index; i++) {
        char mem[CHAR_BUF_LENGTH];
        char valid[CHAR_BUF_LENGTH];
        /* store I I_err */
        this_p1[index++] = DetectorArray[i].intensity;
        this_p1[index++] = DetectorArray[i].error;
        /* add corresponding label */
        mcvalid_name(valid, DetectorArray[i].filename, CHAR_BUF_LENGTH);
        sprintf(mem, "%s_I %s_Err ", valid, valid);
        if (!labels ||
          (labels && labels_size <= strlen(labels)+strlen(mem))) {
          labels_size+=CHAR_BUF_LENGTH;
          if (!labels || !strlen(labels))
            labels = calloc(1, labels_size);
          else
            labels = realloc(labels, labels_size);
        }
        strcat(labels, " ");
        strcat(labels, mem);
    } /* for */

    struct mcformats_struct format=mcformat;
    strcat(format.Name, " scan step");
    Coords zero={0.0,0.0,0.0};

    /* now create detector and write 'abstract' file */
    MCDETECTOR detector = mcdetector_import(format,
      mcinstrument_name, "Monitor integrated counts",
      index, 1, 1,
      "Monitors", "I", "Integrated Signal",
      "Index", labels, "I",
      0, index-1, 0, 0, 0, 0, "content",  /* use name from OpenOffice content.xml description file */
      NULL, this_p1, NULL, zero);

    mcdetector_write_data(detector);

    free(labels); labels=NULL; labels_size=0;

    /* free DETECTOR array */
    free(this_p1); this_p1=NULL;
    free(mcDetectorArray); mcDetectorArray=NULL;
  } /* if this_p1 */

  return 0;

} /* mcdetector_write_content */


/*******************************************************************************
* mcsiminfo_close: close simulation file (mcstas.sim) and write mcstas.dat
* Used by: mcsave/mcfinally from code generation (cogen), mcinfo, mcstas_main
*******************************************************************************/
void mcsiminfo_close(void)
{
  int i;
#ifdef USE_MPI
  if(mpi_node_rank != mpi_node_root) return;
#endif
  if (mcdisable_output_files || !mcsiminfo_file) return;

  int  ismcstas_nx  = (strstr(mcformat.Name, "McStas") || strstr(mcformat.Name, "NeXus"));

  mcdetector_write_content(mcDetectorArray, mcDetectorArray_index);

  /* initialize sim file information, sets detector.file_handle=mcsiminfo_file */
  MCDETECTOR mcsiminfo = mcdetector_import_sim();

  /* close those sections which were opened in mcsiminfo_init =============== */
  if (!ismcstas_nx) {
    mcfile_section(mcsiminfo, "end", mcsiminfo.simulation, mcsiminfo_name, "simulation", 2);
    mcfile_section(mcsiminfo, "end", mcsiminfo.simulation, mcinstrument_name, "instrument", 1);
  }
#ifdef USE_NEXUS
  if (strstr(mcformat.Name, "NeXus")) {
    /* re-open the simulation/information dataset */
    if (NXopendata(mcnxHandle, "information") == NX_ERROR)
      fprintf(stderr, "Warning: NeXus: could not re-open 'information' for mcstas NXentry\n");
  }
#endif
  mcinfo_header(mcsiminfo, "footer");
#ifdef USE_NEXUS
  /* close information SDS opened with mcfile_section(siminfo, "mcstas(footer)" */
  if (strstr(mcformat.Name, "NeXus")) {
    if (NXclosedata(mcnxHandle) == NX_ERROR)
      fprintf(stderr, "Warning: NeXus: could not close data set mcstas/information (footer)\n");
    mcfile_section(mcsiminfo, "end", "mcstas", mcinstrument_name, "entry", 1);
  }
#endif

  /* close sim file ========================================================= */
#ifdef USE_NEXUS
  if (strstr(mcformat.Name, "NeXus")) mcnxfile_close(&mcnxHandle);
  else
#endif
  if (mcsiminfo_file != stdout && mcsiminfo_file) {
    fclose(mcsiminfo_file);
  }
  mcsiminfo_file = NULL;

} /* mcsiminfo_close */

/*******************************************************************************
* mcfile_data: output a single data block using specific file format, e.g. "part=[ array ]"
*   'part' can be 'data','errors','ncount'
* Used by: mcdetector_write_sim, mcdetector_write_data
*******************************************************************************/
int mcfile_data(MCDETECTOR detector, char *part)
{
  double *this_p1=NULL;
  char   *Begin  =NULL;
  char   *End    =NULL;
  char    valid_xlabel[VALID_NAME_LENGTH];
  char    valid_ylabel[VALID_NAME_LENGTH];
  char    valid_zlabel[VALID_NAME_LENGTH];
  char    valid_parent[VALID_NAME_LENGTH];

  if (strstr(part,"data"))
  { this_p1=detector.p1; Begin = detector.format.BeginData; End = detector.format.EndData; }
  if (strstr(part,"errors"))
  { this_p1=detector.p2;Begin = detector.format.BeginErrors; End = detector.format.EndErrors; }
  if (strstr(part,"ncount") || strstr(part,"events"))
  { this_p1=detector.p0;Begin = detector.format.BeginNcount; End = detector.format.EndNcount; }

  if (!this_p1 || mcdisable_output_files) return(-1);
  if (!detector.file_handle && !strstr(detector.format.Name,"NeXus")) return(-1);
  if (!detector.rank) return(-1);


  mcvalid_name(valid_xlabel, detector.xlabel, VALID_NAME_LENGTH);
  mcvalid_name(valid_ylabel, detector.ylabel, VALID_NAME_LENGTH);
  mcvalid_name(valid_zlabel, detector.zlabel, VALID_NAME_LENGTH);
  mcvalid_name(valid_parent, detector.filename, VALID_NAME_LENGTH);

  /* output Begin =========================================================== */
  if (!strstr(detector.format.Name,"NeXus")
   && (!strstr(detector.format.Name,"binary") || strstr(part,"empty array"))
   && !strstr(detector.format.Name,"no header"))
  pfprintf(detector.file_handle, Begin, "sssssssssssssiiigggggg",
      detector.prefix,       /* %1$s   PRE  */
      valid_parent,          /* %2$s   PAR  */
      detector.filename,     /* %3$s   FIL  */
      detector.xlabel,       /* %4$s   XLA  */
      valid_xlabel,          /* %5$s   XVL  */
      detector.ylabel,       /* %6$s   YLA  */
      valid_ylabel,          /* %7$s   YVL  */
      detector.zlabel,       /* %8$s   ZLA  */
      valid_zlabel,          /* %9$s   ZVL  */
      detector.title,        /* %10$s  TITL */
      detector.xvar,         /* %11$s  XVAR */
      detector.yvar,         /* %12$s  YVAR */
      detector.zvar,         /* %13$s  ZVAR */
      detector.m,            /* %14$i  MDIM */
      detector.n,            /* %15$i  NDIM */
      detector.p,            /* %16$i  PDIM */
      detector.xmin,         /* %17$g  XMIN */
      detector.xmax,         /* %18$g  XMAX */
      detector.ymin,         /* %19$g  YMIN */
      detector.ymax,         /* %20$g  YMAX */
      detector.zmin,         /* %21$g  ZMIN */
      detector.zmax);        /* %22$g  ZMAX */

  /* output array */
  if (strstr(part,"empty array")) {
    /* skip array output: set as empty */

    /* special case of IDL: can not have empty vectors. Init to 'external' */
    if (strstr(detector.format.Name, "IDL")) fprintf(detector.file_handle, "'external'");
  } else {
#ifdef USE_NEXUS
    /* array NeXus ========================================================== */
    if (strstr(detector.format.Name,"NeXus")) {
      if(mcnxfile_data(mcnxHandle, detector, part,
        valid_parent, valid_xlabel, valid_ylabel, valid_zlabel) == NX_ERROR) {
        fprintf(stderr, "Error: writing NeXus data %s/%s (mcfile_data)\n", valid_parent, detector.filename);
        return(-1);
      }
      return(1);
    } /* end if NeXus */
#endif
    /* array non binary (text) ============================================== */
    if (!strstr(detector.format.Name, "binary")
     && !strstr(detector.format.Name, "float") && !strstr(detector.format.Name, "double"))
    {
      char eol_char[3];
      int isIDL    = (strstr(detector.format.Name, "IDL") != NULL);
      int isPython = (strstr(detector.format.Name, "Python") != NULL);
      int i,j;
      if (isIDL) strcpy(eol_char,"$\n");
      else if (isPython) strcpy(eol_char,"\\\n");
      else strcpy(eol_char,"\n");

      for(j = 0; j < detector.n*detector.p; j++)  /* loop on rows (y) */
      {
        for(i = 0; i < detector.m; i++)  /* write all columns (x) */
        {
          long index   = !detector.istransposed ? i*detector.n*detector.p + j : i+j*detector.m;
          double value = this_p1[index];
          fprintf(detector.file_handle, "%.12g", value);
          fprintf(detector.file_handle, "%s",
            (isIDL || isPython) && ((i+1)*(j+1) < detector.m*detector.n*detector.p) ?
            "," : " ");
        }
        fprintf(detector.file_handle, "%s", eol_char);
      } /* end 2 loops if not Binary */
    } /* end if !Binary */

    /* array binary double =================================================== */
    if (strstr(detector.format.Name, "double"))
    {
      long count=0;
      count = fwrite(this_p1, sizeof(double), detector.m*detector.n*detector.p, detector.file_handle);
      if (count != detector.m*detector.n*detector.p)
        fprintf(stderr, "Error: writing double binary file '%s' (%li instead of %li, mcfile_data)\n", detector.filename,count, detector.m*detector.n*detector.p);
    } /* end if Binary double */

    /* array binary float =================================================== */
    if (strstr(detector.format.Name, "binary") || strstr(detector.format.Name, "float"))
    {
      float *s;
      s = (float*)malloc(detector.m*detector.n*detector.p*sizeof(float));
      if (s)
      {
        long    i, count=0;
        for (i=0; i<detector.m*detector.n*detector.p; i++) {
          s[i] = (float)this_p1[i]; }
        count = fwrite(s, sizeof(float), detector.m*detector.n*detector.p, detector.file_handle);
        if (count != detector.m*detector.n*detector.p)
          fprintf(stderr, "Error writing float binary file '%s' (%li instead of %li, mcfile_data)\n",
            detector.filename,count, detector.m*detector.n*detector.p);
        free(s);
      } else
      fprintf(stderr, "Error: Out of memory for writing %li float binary file '%s' (mcfile_data)\n",
        detector.m*detector.n*detector.p*sizeof(float), detector.filename);
    } /* end if Binary float */
  } /* end if not empty array */

  /* output End ============================================================= */
  if (!strstr(detector.format.Name,"NeXus")
   && (!strstr(detector.format.Name,"binary") || strstr(part,"empty array"))
   && !strstr(detector.format.Name,"no footer"))
  pfprintf(detector.file_handle, End, "sssssssssssssiiigggggg",
      detector.prefix,       /* %1$s   PRE  */
      valid_parent,          /* %2$s   PAR  */
      detector.filename,     /* %3$s   FIL  */
      detector.xlabel,       /* %4$s   XLA  */
      valid_xlabel,          /* %5$s   XVL  */
      detector.ylabel,       /* %6$s   YLA  */
      valid_ylabel,          /* %7$s   YVL  */
      detector.zlabel,       /* %8$s   ZLA  */
      valid_zlabel,          /* %9$s   ZVL  */
      detector.title,        /* %10$s  TITL */
      detector.xvar,         /* %11$s  XVAR */
      detector.yvar,         /* %12$s  YVAR */
      detector.zvar,         /* %13$s  ZVAR */
      detector.m,            /* %14$i  MDIM */
      detector.n,            /* %15$i  NDIM */
      detector.p,            /* %16$i  PDIM */
      detector.xmin,         /* %17$g  XMIN */
      detector.xmax,         /* %18$g  XMAX */
      detector.ymin,         /* %19$g  YMIN */
      detector.ymax,         /* %20$g  YMAX */
      detector.zmin,         /* %21$g  ZMIN */
      detector.zmax);        /* %22$g  ZMAX */

  return(2);
} /* mcfile_data */

/*******************************************************************************
* mcdetector_write_sim: write information to sim file
*******************************************************************************/
MCDETECTOR mcdetector_write_sim(MCDETECTOR detector)
{
  /* MPI: only write sim by Master ========================================== */
#ifdef USE_MPI
  if(mpi_node_rank != mpi_node_root) return(detector);
#endif
  /* skip invalid detectors */
  if (!detector.m || mcdisable_output_files) return(detector);

  /* sim file has been initialized when starting simulation and when calling
   * mcsave ; this defines mcsiminfo_file as the SIM file handle
   * and calls:
   *    mcinfo_header
   *    mcfile_section begin instrument
   *    mcinfo_instrument
   *    mcfile_section end instrument
   *    mcfile_section begin simulation
   *    mcinfo_simulation
   *    mcfile_section end simulation
   *    sets "mcopenedfiles" list to empty string
   * When using NeXus format, this information is stored into mcnxHandle NX
   *
   * The sim file (and mcnxHandle) is closed when ending mcsave
   */

  MCDETECTOR simfile = mcdetector_import_sim(); /* store reference structure to SIM file */

  /* add component and data section to SIM file */
  if (!strstr(detector.format.Name,"NeXus"))
    simfile = mcfile_section(simfile, "begin", detector.simulation, detector.component, "component", 3);
  simfile = mcfile_section(simfile, "begin", detector.component, detector.filename, "data", 4);

  /* handle indentation for simfile */
  char prefix[CHAR_BUF_LENGTH];
  strncpy(prefix, detector.prefix, CHAR_BUF_LENGTH);
  strncpy(detector.prefix, simfile.prefix, CHAR_BUF_LENGTH);

  /* WRITE detector information to sim file ================================= */
  mcinfo_data(detector, NULL);
#ifdef USE_NEXUS
  /* close information SDS opened with mcfile_section(siminfo,"data") */
  if (strstr(mcformat.Name, "NeXus"))
    if (NXclosedata(mcnxHandle) == NX_ERROR)
      fprintf(stderr, "Warning: NeXus: could not close data set %s/information\n",
        detector.filename);
#endif
  /* WRITE data link to SIM file */
  if (!strstr(detector.format.Name,"McStas") && !mcsingle_file) {
    FILE *file=detector.file_handle;
    detector.file_handle = mcsiminfo_file;
    mcfile_data(detector, "data (empty array)");
    if (detector.p2) mcfile_data(detector, "errors (empty array)");
    if (detector.p0) mcfile_data(detector, "events (empty array)");
    detector.file_handle = file;
  }

  /* close component and data sections in SIM file */
  simfile = mcfile_section(simfile, "end", detector.component, detector.filename, "data", 4);
  if (!strstr(detector.format.Name,"NeXus"))
    simfile = mcfile_section(simfile, "end", detector.simulation, detector.component, "component", 3);

  strncpy(detector.prefix, prefix, CHAR_BUF_LENGTH);

  return(detector);
} /* mcdetector_write_sim */

/*******************************************************************************
* mcdetector_write_data: write information to data file and catenate lists
*                        this is where we open/close data files
*                        also free detector.p1=this_p1 field which may hold
*                        1D McStas [x I Ierr N] array which is set in mcdetector_import
*******************************************************************************/
MCDETECTOR mcdetector_write_data(MCDETECTOR detector)
{
  /* skip if 0D or no filename or no data (only stored in sim file) */
  if (!detector.rank || !strlen(detector.filename)
   || !detector.m) return(detector);

#ifdef USE_MPI
  /* only by MASTER for non lists (MPI reduce has been done in detector_import) */
  if (!strstr(detector.format.Name," list ") && mpi_node_rank != mpi_node_root) {
    if (detector.rank == 1 && detector.p1 && strstr(detector.format.Name,"McStas")) {
      free(detector.p1);  /* 'this_p1' allocated in mcdetector_import for 1D McStas data sets [x I E N] */
      detector.p0 = detector.p0_orig;
      detector.p1 = detector.p1_orig;
      detector.p2 = detector.p2_orig;
      detector.m = detector.n; detector.n=1; detector.istransposed=0;
    }
    return(detector);
  }
#endif

  /* OPEN data file (possibly appending if already opened) ================== */
  if (mcformat_data.Name && strlen(mcformat_data.Name) && !mcsingle_file)
    detector.format = mcformat_data;

  if (!strstr(detector.format.Name, "NeXus")) {
    /* explicitely open data file if not NeXus format */
    char mode[10];

    /* open or re-open file for write/append */
    strcpy(mode,
           (strstr(detector.format.Name, "no header")
            || strstr(detector.format.Name, "append") || strstr(detector.format.Name, "catenate")
            || strstr(mcopenedfiles, detector.filename) ?
           "a" : "w"));
    if (strstr(detector.format.Name, "binary")) strcat(mode, "b");
    detector.file_handle = mcnew_file(!mcsingle_file ?
      detector.filename : mcsiminfo_name,
      detector.format.Extension, mode);
  } else {
    /* NeXus file mcnxHandle has been opened in mcsiminfo_init */
    /* but we must open the proper Data Set group and write information */
    mcfile_section(detector, "begin", detector.component, detector.filename, "data", 4);
  }

  /* WRITE data header (except when in 'no header') */
  if (!strstr(detector.format.Name, "no header")) {
    if (!strstr(detector.format.Name, "binary") && !mcascii_only)  {
      /* skip in data-only mode or binary */
      mcinfo_header(detector, "header");
      mcinfo_simulation(detector, mcsiminfo_name);
      mcinfo_data(detector, detector.filename);
      /* mcinfo_simulation(detector, detector.filename); */
    }
  }
#ifdef USE_NEXUS
  /* close information SDS opened with mcfile_section(detector,"data") */
  if (strstr(mcformat.Name, "NeXus")) {
    mcinfo_header(detector, "footer");
    if (NXclosedata(mcnxHandle) == NX_ERROR)
      fprintf(stderr, "Warning: NeXus: could not close data set %s/information (footer)\n",
        detector.filename);
  }
  /* group remains opened for data to be written */
#endif

  /* case: list of events =================================================== */
  if (strstr(detector.format.Name," list ")) {

#ifdef USE_MPI
    if (mpi_node_rank != mpi_node_root) {
      /* MPI slave: all slaves send their data to master */
      /* m, n, p must be sent too, since all slaves do not have the same number of events */
      int mnp[3]={detector.m,detector.n,detector.p};

      if (mc_MPI_Send(mnp, 3, MPI_INT, mpi_node_root)!= MPI_SUCCESS)
        fprintf(stderr, "Warning: proc %i to master: MPI_Send mnp list error (mcdetector_write_data)", mpi_node_rank);
      if (!detector.p1
       || mc_MPI_Send(detector.p1, abs(mnp[0]*mnp[1]*mnp[2]), MPI_DOUBLE, mpi_node_root) != MPI_SUCCESS)
        fprintf(stderr, "Warning: proc %i to master: MPI_Send p1 list error (mcdetector_write_data)", mpi_node_rank);
      /* slaves are done */
      return (detector);
    }
#endif

    /* master node or non-MPI list: store data block */
    int     master_mnp[3]={detector.m,detector.n,detector.p};

#ifdef USE_MPI
    int     node_i=0;
    /* MPI master: receive data from slaves */
    for(node_i=0; node_i<mpi_node_count; node_i++) {
      double *this_p1=NULL;                               /* buffer to hold the list to save */
      int     mnp[3]={detector.m,detector.m,detector.p};  /* size of this buffer */
      if (node_i != mpi_node_root) { /* get data from slaves */
        if (mc_MPI_Recv(mnp, 3, MPI_INT, node_i) != MPI_SUCCESS)
          fprintf(stderr, "Warning: master from proc %i: "
            "MPI_Recv mnp list error (mcdetector_write_data)", node_i);
        this_p1 = (double *)calloc(abs(mnp[0]*mnp[1]*mnp[2]), sizeof(double));
        if (!this_p1 || mc_MPI_Recv(this_p1, abs(mnp[0]*mnp[1]*mnp[2]), MPI_DOUBLE, node_i)!= MPI_SUCCESS)
          fprintf(stderr, "Warning: master from proc %i: "
            "MPI_Recv p1 list error (mcdetector_write_data)", node_i);
        detector.p1 = this_p1;
        detector.m  = mnp[0]; detector.n  = mnp[1]; detector.p  = mnp[2];
      } else
#endif
      { /* MASTER/single node use its own detector structure */
        detector.p1 = detector.p1_orig;
        detector.m  = master_mnp[0]; detector.n  = master_mnp[1]; detector.p  = master_mnp[2];
      }

      /* WRITE list data (will be catenated in case of MPI as file is already opened) */
      mcfile_data(detector, "data");

#ifdef USE_MPI
      /* free temporary MPI block used for Recv from slaves */
      if (this_p1 && node_i != mpi_node_root)
        free(this_p1);
      detector.p0 = detector.p0_orig;
      detector.p1 = detector.p1_orig;
      detector.p2 = detector.p2_orig;
      detector.m  = master_mnp[0]; detector.n  = master_mnp[1]; detector.p  = master_mnp[2];

    } /* for node_i: end loop on nodes */
#endif

  } /* end list case */
  else
  {
  /* case: normal detector ================================================== */

    /* WRITE data */
    mcfile_data(detector, "data");

    /* write errors (not for lists) */
    if (detector.p2) mcfile_data(detector, "errors");

    /* write events (not for lists) */
    if (detector.p0) mcfile_data(detector, "events");

    if (detector.rank == 1 && detector.p1 && strstr(detector.format.Name,"McStas")) {
      free(detector.p1);  /* 'this_p1' allocated in mcdetector_write_sim for 1D McStas data sets [x I E N] */
      detector.p0 = detector.p0_orig;
      detector.p1 = detector.p1_orig;
      detector.p2 = detector.p2_orig;
      detector.m = detector.n; detector.n=1; detector.istransposed=0;
    }
  } /* end normal detector case */

  /* WRITE data footer (except when 'no footer') */
  if (!strstr(detector.format.Name, "no footer") && !strstr(mcformat.Name, "NeXus")) {
    /* skip in data-only mode or binary */
    if (!strstr(detector.format.Name, "binary") && !mcascii_only)
      mcinfo_header(detector, "footer");
  }

  /* close data file and free memory */
  if (mcDetectorCustomHeader && strlen(mcDetectorCustomHeader)) {
    free(mcDetectorCustomHeader); mcDetectorCustomHeader=NULL; }

  if (!strstr(mcformat.Name, "NeXus") && detector.file_handle != mcsiminfo_file
   && !mcdisable_output_files && detector.file_handle)
    fclose(detector.file_handle);
  else if (strstr(mcformat.Name, "NeXus")) {
    /* close data set group */
    mcfile_section(detector, "end", detector.component, detector.filename, "data", 4);
  }

  return(detector);
} /* mcdetector_write_data */

/*******************************************************************************
* mcdetector_out_0D: wrapper for 0D (single value).
*******************************************************************************/
MCDETECTOR mcdetector_out_0D(char *t, double p0, double p1, double p2,
                         char *c, Coords posa)
{
  /* import and perform basic detector analysis (and handle MPI reduce except for lists) */
  MCDETECTOR detector = mcdetector_import(mcformat,
    c, (t ? t : "McStas data"),
    1, 1, 1,
    "I", "", "",
    "I", "", "",
    0, 0, 0, 0, 0, 0, "",
    &p0, &p1, &p2, posa); /* write Detector: line */

  /* write detector to simulation file (incl custom header if any) */
  detector = mcdetector_write_sim(detector);
  mcdetector_register(detector);
  return(detector);
}

/*******************************************************************************
* mcdetector_out_1D: wrapper for 1D.
*******************************************************************************/
MCDETECTOR mcdetector_out_1D(char *t, char *xl, char *yl,
        char *xvar, double x1, double x2,
        int n,
        double *p0, double *p1, double *p2, char *f,
        char *c, Coords posa)
{
  /* import and perform basic detector analysis (and handle MPI_Reduce except for lists) */
  MCDETECTOR detector = mcdetector_import(mcformat,
    c, (t ? t : "McStas 1D data"),
    n, 1, 1,
    xl, yl, (n > 1 ? "Signal per bin" : " Signal"),
    xvar, "(I,I_err)", "I",
    x1, x2, 0, 0, 0, 0, f,
    p0, p1, p2, posa); /* write Detector: line */

  /* write detector to simulation and data file (incl custom header if any) */
  detector = mcdetector_write_sim(detector);
  detector = mcdetector_write_data(detector);  /* will also merge lists */
  mcdetector_register(detector);
  return(detector);
}

/*******************************************************************************
* mcdetector_out_2D: wrapper for 2D.
*******************************************************************************/
MCDETECTOR mcdetector_out_2D(char *t, char *xl, char *yl,
                  double x1, double x2, double y1, double y2,
                  int m, int n,
                  double *p0, double *p1, double *p2, char *f,
                  char *c, Coords posa)
{
  char xvar[CHAR_BUF_LENGTH];
  char yvar[CHAR_BUF_LENGTH];

  if (xl && strlen(xl)) { strncpy(xvar, xl, CHAR_BUF_LENGTH); xvar[2]='\0'; }
  else strcpy(xvar, "x");
  if (yl && strlen(yl)) { strncpy(yvar, yl, CHAR_BUF_LENGTH); yvar[2]='\0'; }
  else strcpy(yvar, "y");

  /* is it in fact a 1D call ? */
  if (m == 1)      return(mcdetector_out_1D(
                    t, yl, "I", yvar, y1, y2, n, p0, p1, p2, f, c, posa));
  else if (n == 1) return(mcdetector_out_1D(
                    t, xl, "I", xvar, x1, x2, m, p0, p1, p2, f, c, posa));

  /* import and perform basic detector analysis (and handle MPI_Reduce except for lists) */
  MCDETECTOR detector = mcdetector_import(mcformat,
    c, (t ? t : "McStas 2D data"),
    m, n, 1,
    xl, yl, "Signal per bin",
    xvar, yvar, "I",
    x1, x2, y1, y2, 0, 0, f,
    p0, p1, p2, posa); /* write Detector: line */

  /* write detector to simulation and data file (incl custom header if any) */
  detector = mcdetector_write_sim(detector);
  detector = mcdetector_write_data(detector); /* will also merge lists */
  mcdetector_register(detector);
  return(detector);
}

/*******************************************************************************
* mcdetector_out_3D: wrapper for 3D.
*   exported as a large 2D array, but the 3 dims are given in the header
*******************************************************************************/
MCDETECTOR mcdetector_out_3D(char *t, char *xl, char *yl, char *zl,
      char *xvar, char *yvar, char *zvar,
      double x1, double x2, double y1, double y2, double z1, double z2,
      int m, int n, int p,
      double *p0, double *p1, double *p2, char *f,
      char *c, Coords posa)
{
  /* import and perform basic detector analysis (and handle MPI_Reduce except for lists) */
  MCDETECTOR detector = mcdetector_import(mcformat,
    c, (t ? t : "McStas 3D data"),
    m, n, p,
    xl, yl, zl,
    xvar, yvar, zvar,
    x1, x2, y1, y2, z1, z2, f,
    p0, p1, p2, posa); /* write Detector: line */

  /* write detector to simulation and data file (incl custom header if any) */
  detector = mcdetector_write_sim(detector);
  detector = mcdetector_write_data(detector); /* will also merge lists */
  mcdetector_register(detector);
  return(detector);
}

/*******************************************************************************
* mcuse_format_header: Replaces aliases names in format fields (header part)
*******************************************************************************/
char *mcuse_format_header(char *format_const)
{ /* Header Footer */
  char *format=NULL;

  if (!format_const) return(NULL);
  format = (char *)malloc(strlen(format_const)+1);
  if (!format) exit(fprintf(stderr, "Error: insufficient memory (mcuse_format_header)\n"));
  strcpy(format, format_const);
  str_rep(format, "%PRE", "%1$s"); /* prefix */
  str_rep(format, "%SRC", "%2$s"); /* name of instrument source file */
  str_rep(format, "%FIL", "%3$s"); /* output file name (data) */
  str_rep(format, "%FMT", "%4$s"); /* format name */
  str_rep(format, "%DATL","%8$li");/* Time elapsed since Jan 1st 1970, in seconds */
  str_rep(format, "%DAT", "%5$s"); /* Date as a string */
  str_rep(format, "%USR", "%6$s"); /* User/machine name */
  str_rep(format, "%PAR", "%7$s"); /* Parent name (root/mcstas) valid_parent */
  str_rep(format, "%VPA", "%7$s");
  return(format);
}

/*******************************************************************************
* mcuse_format_tag: Replaces aliases names in format fields (tag part)
*******************************************************************************/
char *mcuse_format_tag(char *format_const)
{ /* AssignTag */
  char *format=NULL;

  if (!format_const) return(NULL);
  format = (char *)malloc(strlen(format_const)+1);
  if (!format) exit(fprintf(stderr, "Error: insufficient memory (mcuse_format_tag)\n"));
  strcpy(format, format_const);
  str_rep(format, "%PRE", "%1$s"); /* prefix */
  str_rep(format, "%SEC", "%2$s"); /* section/parent name valid_parent/section */
  str_rep(format, "%PAR", "%2$s");
  str_rep(format, "%VPA", "%2$s");
  str_rep(format, "%NAM", "%3$s"); /* name of field valid_name */
  str_rep(format, "%VNA", "%3$s");
  str_rep(format, "%VAL", "%4$s"); /* value of field */
  return(format);
}

/*******************************************************************************
* mcuse_format_section: Replaces aliases names in format fields (section part)
*******************************************************************************/
char *mcuse_format_section(char *format_const)
{ /* BeginSection EndSection */
  char *format=NULL;

  if (!format_const) return(NULL);
  format = (char *)malloc(strlen(format_const)+1);
  if (!format) exit(fprintf(stderr, "Error: insufficient memory (mcuse_format_section)\n"));
  strcpy(format, format_const);
  str_rep(format, "%PRE", "%1$s"); /* prefix */
  str_rep(format, "%TYP", "%2$s"); /* type/class of section */
  str_rep(format, "%NAM", "%3$s"); /* name of section */
  str_rep(format, "%SEC", "%3$s");
  str_rep(format, "%VNA", "%4$s"); /* valid name (letters/digits) of section */
  str_rep(format, "%PAR", "%5$s"); /* parent name (simulation) */
  str_rep(format, "%VPA", "%6$s"); /* valid parent name (letters/digits) */
  str_rep(format, "%LVL", "%7$i"); /* level index */
  return(format);
}

/*******************************************************************************
* mcuse_format_data: Replaces aliases names in format fields (data part)
*******************************************************************************/
char *mcuse_format_data(char *format_const)
{ /* BeginData EndData BeginErrors EndErrors BeginNcount EndNcount */
  char *format=NULL;

  if (!format_const) return(NULL);
  format = (char *)malloc(strlen(format_const)+1);
  if (!format) exit(fprintf(stderr, "Error: insufficient memory (mcuse_format_data)\n"));
  strcpy(format, format_const);
  str_rep(format, "%PRE", "%1$s"); /* prefix */
  str_rep(format, "%PAR", "%2$s"); /* parent name (component instance name) valid_parent */
  str_rep(format, "%VPA", "%2$s");
  str_rep(format, "%FIL", "%3$s"); /* output file name (data) */
  str_rep(format, "%XLA", "%4$s"); /* x axis label */
  str_rep(format, "%XVL", "%5$s"); /* x axis valid label (letters/digits) */
  str_rep(format, "%YLA", "%6$s"); /* y axis label */
  str_rep(format, "%YVL", "%7$s"); /* y axis valid label (letters/digits) */
  str_rep(format, "%ZLA", "%8$s"); /* z axis label */
  str_rep(format, "%ZVL", "%9$s"); /* z axis valid label (letters/digits) */
  str_rep(format, "%TITL", "%10$s"); /* data title */
  str_rep(format, "%XVAR", "%11$s"); /* x variables */
  str_rep(format, "%YVAR", "%12$s"); /* y variables */
  str_rep(format, "%ZVAR", "%13$s"); /* z variables */
  str_rep(format, "%MDIM", "%14$i"); /* m dimension of x axis */
  str_rep(format, "%NDIM", "%15$i"); /* n dimension of y axis */
  str_rep(format, "%PDIM", "%16$i"); /* p dimension of z axis */
  str_rep(format, "%XMIN", "%17$g"); /* x min axis value (m bins) */
  str_rep(format, "%XMAX", "%18$g"); /* x max axis value (m bins) */
  str_rep(format, "%YMIN", "%19$g"); /* y min axis value (n bins) */
  str_rep(format, "%YMAX", "%20$g"); /* y max axis value (n bins) */
  str_rep(format, "%ZMIN", "%21$g"); /* z min axis value (usually min of signal, p bins) */
  str_rep(format, "%ZMAX", "%22$g"); /* z max axis value (usually max of signal, p bins) */
  return(format);
}

/*******************************************************************************
* mcuse_format: selects an output format for sim and data files. returns format
*******************************************************************************/
struct mcformats_struct mcuse_format(char *format)
{
  int i,j;
  int i_format=-1;
  char *tmp;
  char low_format[256];
  struct mcformats_struct usedformat;

  usedformat.Name = NULL;
  /* get the format to lower case */
  if (!format) exit(fprintf(stderr, "Error: Invalid NULL format. Exiting (mcuse_format)\n"));
  strcpy(low_format, format);
  for (i=0; i<strlen(low_format); i++) low_format[i]=tolower(format[i]);
  /* handle format aliases */
  if (!strcmp(low_format, "pgplot")) strcpy(low_format, "mcstas");
  if (!strcmp(low_format, "hdf"))    strcpy(low_format, "nexus");
#ifndef USE_NEXUS
  if (!strcmp(low_format, "nexus"))
    fprintf(stderr, "WARNING: to enable NeXus format you must have the NeXus libs installed.\n"
                    "         You should then re-compile with the -DUSE_NEXUS define.\n");
#endif

  tmp = (char *)malloc(256);
  if(!tmp) exit(fprintf(stderr, "Error: insufficient memory (mcuse_format)\n"));

  /* look for a specific format in mcformats.Name table */
  for (i=0; i < mcNUMFORMATS; i++)
  {
    strcpy(tmp, mcformats[i].Name);
    for (j=0; j<strlen(tmp); j++) tmp[j] = tolower(tmp[j]);
    if (strstr(low_format, tmp))  i_format = i;
  }
  if (i_format < 0)
  {
    i_format = 0; /* default format is #0 McStas */
    fprintf(stderr, "Warning: unknown output format '%s'. Using default (%s, mcuse_format).\n", format, mcformats[i_format].Name);
  }

  usedformat = mcformats[i_format];
  strcpy(tmp, usedformat.Name);
  usedformat.Name = tmp;
  if (strstr(low_format,"raw")) strcat(usedformat.Name," raw");
  if (strstr(low_format,"binary"))
  {
    if (strstr(low_format,"double")) strcat(usedformat.Name," binary double data");
    else strcat(usedformat.Name," binary float data");
    mcascii_only = 1;
  }

  /* Replaces vfprintf parameter name aliases */
  /* Header Footer */
  usedformat.Header       = mcuse_format_header(usedformat.Header);
  usedformat.Footer       = mcuse_format_header(usedformat.Footer);
  /* AssignTag */
  usedformat.AssignTag    = mcuse_format_tag(usedformat.AssignTag);
  /* BeginSection EndSection */
  usedformat.BeginSection = mcuse_format_section(usedformat.BeginSection);
  usedformat.EndSection   = mcuse_format_section(usedformat.EndSection);
  /*  BeginData  EndData  BeginErrors  EndErrors  BeginNcount  EndNcount  */
  usedformat.BeginData    = mcuse_format_data(usedformat.BeginData  );
  usedformat.EndData      = mcuse_format_data(usedformat.EndData    );
  usedformat.BeginErrors  = mcuse_format_data(usedformat.BeginErrors);
  usedformat.EndErrors    = mcuse_format_data(usedformat.EndErrors  );
  usedformat.BeginNcount  = mcuse_format_data(usedformat.BeginNcount);
  usedformat.EndNcount    = mcuse_format_data(usedformat.EndNcount  );

  return(usedformat);
} /* mcuse_format */

/*******************************************************************************
* mcclear_format: free format structure
*******************************************************************************/
void mcclear_format(struct mcformats_struct usedformat)
{
/* free format specification strings */
  if (usedformat.Name        ) free(usedformat.Name        );
  else return;
  if (usedformat.Header      ) free(usedformat.Header      );
  if (usedformat.Footer      ) free(usedformat.Footer      );
  if (usedformat.AssignTag   ) free(usedformat.AssignTag   );
  if (usedformat.BeginSection) free(usedformat.BeginSection);
  if (usedformat.EndSection  ) free(usedformat.EndSection  );
  if (usedformat.BeginData   ) free(usedformat.BeginData   );
  if (usedformat.EndData     ) free(usedformat.EndData     );
  if (usedformat.BeginErrors ) free(usedformat.BeginErrors );
  if (usedformat.EndErrors   ) free(usedformat.EndErrors   );
  if (usedformat.BeginNcount ) free(usedformat.BeginNcount );
  if (usedformat.EndNcount   ) free(usedformat.EndNcount   );
} /* mcclear_format */

/*******************************************************************************
* mcuse_file: will save data/sim files
*******************************************************************************/
static void mcuse_file(char *file)
{
  if (file && strcmp(file, "NULL"))
    mcsiminfo_name = file;
  else {
    char *filename=(char*)malloc(CHAR_BUF_LENGTH);
    sprintf(filename, "%s_%li", mcinstrument_name, mcstartdate);
    mcsiminfo_name = filename;
  }
  mcsingle_file  = 1;
}

/*******************************************************************************
* mcset_ncount: set total number of rays to generate
*******************************************************************************/
void mcset_ncount(unsigned long long int count)
{
  mcncount = count;
}

/* mcget_ncount: get total number of rays to generate */
unsigned long long int mcget_ncount(void)
{
  return mcncount;
}

/* mcget_run_num: get curent number of rays in TRACE */
unsigned long long int mcget_run_num(void)
{
  return mcrun_num;
}

/* mcsetn_arg: get ncount from a string argument */
static void
mcsetn_arg(char *arg)
{
  mcset_ncount((long long int) strtod(arg, NULL));
}

/* mcsetseed: set the random generator seed from a string argument */
static void
mcsetseed(char *arg)
{
  mcseed = atol(arg);
  if(mcseed) {
#ifdef USE_MPI
    if (mpi_node_count > 1) srandom(mcseed+mpi_node_rank);
    else
#endif
    srandom(mcseed);
  } else {
    fprintf(stderr, "Error: seed must not be zero (mcsetseed)\n");
    exit(1);
  }
}

/* Following part is only embedded when not redundent with mcstas.h ========= */

#ifndef MCCODE_H

/* SECTION: MCDISPLAY support. =============================================== */

/*******************************************************************************
* Just output MCDISPLAY keywords to be caught by an external plotter client.
*******************************************************************************/

void mcdis_magnify(char *what){
  printf("MCDISPLAY: magnify('%s')\n", what);
}

void mcdis_line(double x1, double y1, double z1,
                double x2, double y2, double z2){
  printf("MCDISPLAY: multiline(2,%g,%g,%g,%g,%g,%g)\n",
         x1,y1,z1,x2,y2,z2);
}

void mcdis_dashed_line(double x1, double y1, double z1,
		       double x2, double y2, double z2, int n){
  int i;
  const double dx = (x2-x1)/(2*n+1);
  const double dy = (y2-y1)/(2*n+1);
  const double dz = (z2-z1)/(2*n+1);

  for(i = 0; i < n+1; i++)
    mcdis_line(x1 + 2*i*dx,     y1 + 2*i*dy,     z1 + 2*i*dz,
	       x1 + (2*i+1)*dx, y1 + (2*i+1)*dy, z1 + (2*i+1)*dz);
}

void mcdis_multiline(int count, ...){
  va_list ap;
  double x,y,z;

  printf("MCDISPLAY: multiline(%d", count);
  va_start(ap, count);
  while(count--)
    {
    x = va_arg(ap, double);
    y = va_arg(ap, double);
    z = va_arg(ap, double);
    printf(",%g,%g,%g", x, y, z);
    }
  va_end(ap);
  printf(")\n");
}

void mcdis_rectangle(char* plane, double x, double y, double z,
		     double width, double height){
  /* draws a rectangle in the plane           */
  /* x is ALWAYS width and y is ALWAYS height */
  if (strcmp("xy", plane)==0) {
    mcdis_multiline(5,
		    x - width/2, y - height/2, z,
		    x + width/2, y - height/2, z,
		    x + width/2, y + height/2, z,
		    x - width/2, y + height/2, z,
		    x - width/2, y - height/2, z);
  } else if (strcmp("xz", plane)==0) {
    mcdis_multiline(5,
		    x - width/2, y, z - height/2,
		    x + width/2, y, z - height/2,
		    x + width/2, y, z + height/2,
		    x - width/2, y, z + height/2,
		    x - width/2, y, z - height/2);
  } else if (strcmp("yz", plane)==0) {
    mcdis_multiline(5,
		    x, y - height/2, z - width/2,
		    x, y - height/2, z + width/2,
		    x, y + height/2, z + width/2,
		    x, y + height/2, z - width/2,
		    x, y - height/2, z - width/2);
  } else {

    fprintf(stderr, "Error: Definition of plane %s unknown\n", plane);
    exit(1);
  }
}

/*  draws a box with center at (x, y, z) and
    width (deltax), height (deltay), length (deltaz) */
void mcdis_box(double x, double y, double z,
	       double width, double height, double length){

  mcdis_rectangle("xy", x, y, z-length/2, width, height);
  mcdis_rectangle("xy", x, y, z+length/2, width, height);
  mcdis_line(x-width/2, y-height/2, z-length/2,
	     x-width/2, y-height/2, z+length/2);
  mcdis_line(x-width/2, y+height/2, z-length/2,
	     x-width/2, y+height/2, z+length/2);
  mcdis_line(x+width/2, y-height/2, z-length/2,
	     x+width/2, y-height/2, z+length/2);
  mcdis_line(x+width/2, y+height/2, z-length/2,
	     x+width/2, y+height/2, z+length/2);
}

void mcdis_circle(char *plane, double x, double y, double z, double r){
  printf("MCDISPLAY: circle('%s',%g,%g,%g,%g)\n", plane, x, y, z, r);
}

/* SECTION: coordinates handling ============================================ */

/*******************************************************************************
* Since we use a lot of geometric calculations using Cartesian coordinates,
* we collect some useful routines here. However, it is also permissible to
* work directly on the underlying struct coords whenever that is most
* convenient (that is, the type Coords is not abstract).
*
* Coordinates are also used to store rotation angles around x/y/z axis.
*
* Since coordinates are used much like a basic type (such as double), the
* structure itself is passed and returned, rather than a pointer.
*
* At compile-time, the values of the coordinates may be unknown (for example
* a motor position). Hence coordinates are general expressions and not simple
* numbers. For this we used the type Coords_exp which has three CExp
* fields. For runtime (or calculations possible at compile time), we use
* Coords which contains three double fields.
*******************************************************************************/

/* coords_set: Assign coordinates. */
Coords
coords_set(MCNUM x, MCNUM y, MCNUM z)
{
  Coords a;

  a.x = x;
  a.y = y;
  a.z = z;
  return a;
}

/* coords_get: get coordinates. Required when 'x','y','z' are #defined as ray pars */
Coords
coords_get(Coords a, MCNUM *x, MCNUM *y, MCNUM *z)
{
  *x = a.x;
  *y = a.y;
  *z = a.z;
  return a;
}

/* coords_add: Add two coordinates. */
Coords
coords_add(Coords a, Coords b)
{
  Coords c;

  c.x = a.x + b.x;
  c.y = a.y + b.y;
  c.z = a.z + b.z;
  if (fabs(c.z) < 1e-14) c.z=0.0;
  return c;
}

/* coords_sub: Subtract two coordinates. */
Coords
coords_sub(Coords a, Coords b)
{
  Coords c;

  c.x = a.x - b.x;
  c.y = a.y - b.y;
  c.z = a.z - b.z;
  if (fabs(c.z) < 1e-14) c.z=0.0;
  return c;
}

/* coords_neg: Negate coordinates. */
Coords
coords_neg(Coords a)
{
  Coords b;

  b.x = -a.x;
  b.y = -a.y;
  b.z = -a.z;
  return b;
}

/* coords_scale: Scale a vector. */
Coords coords_scale(Coords b, double scale) {
  Coords a;

  a.x = b.x*scale;
  a.y = b.y*scale;
  a.z = b.z*scale;
  return a;
}

/* coords_sp: Scalar product: a . b */
double coords_sp(Coords a, Coords b) {
  double value;

  value = a.x*b.x + a.y*b.y + a.z*b.z;
  return value;
}

/* coords_xp: Cross product: a = b x c. */
Coords coords_xp(Coords b, Coords c) {
  Coords a;

  a.x = b.y*c.z - c.y*b.z;
  a.y = b.z*c.x - c.z*b.x;
  a.z = b.x*c.y - c.x*b.y;
  return a;
}

/* coords_mirror: Mirror a in plane (through the origin) defined by normal n*/
Coords coords_mirror(Coords a, Coords n) {
  double t = scalar_prod(n.x, n.y, n.z, n.x, n.y, n.z);
  Coords b;
  if (t!=1) {
    t = sqrt(t);
    n.x /= t;
    n.y /= t;
    n.z /= t;
  }
  t=scalar_prod(a.x, a.y, a.z, n.x, n.y, n.z);
  b.x = a.x-2*t*n.x;
  b.y = a.y-2*t*n.y;
  b.z = a.z-2*t*n.z;
  return b;
}

/* coords_print: Print out vector values. */
void coords_print(Coords a) {

  fprintf(stdout, "(%f, %f, %f)\n", a.x, a.y, a.z);
  return;
}

mcstatic inline void coords_norm(Coords* c) {
	double temp = coords_sp(*c,*c);

	// Skip if we will end dividing by zero
	if (temp == 0) return;

	temp = sqrt(temp);

	c->x /= temp;
	c->y /= temp;
	c->z /= temp;
}

/*******************************************************************************
* The Rotation type implements a rotation transformation of a coordinate
* system in the form of a double[3][3] matrix.
*
* Contrary to the Coords type in coords.c, rotations are passed by
* reference. Functions that yield new rotations do so by writing to an
* explicit result parameter; rotations are not returned from functions. The
* reason for this is that arrays cannot by returned from functions (though
* structures can; thus an alternative would have been to wrap the
* double[3][3] array up in a struct). Such are the ways of C programming.
*
* A rotation represents the tranformation of the coordinates of a vector when
* changing between coordinate systems that are rotated with respect to each
* other. For example, suppose that coordinate system Q is rotated 45 degrees
* around the Z axis with respect to coordinate system P. Let T be the
* rotation transformation representing a 45 degree rotation around Z. Then to
* get the coordinates of a vector r in system Q, apply T to the coordinates
* of r in P. If r=(1,0,0) in P, it will be (sqrt(1/2),-sqrt(1/2),0) in
* Q. Thus we should be careful when interpreting the sign of rotation angles:
* they represent the rotation of the coordinate systems, not of the
* coordinates (which has opposite sign).
*******************************************************************************/

/*******************************************************************************
* rot_set_rotation: Get transformation for rotation first phx around x axis,
* then phy around y, then phz around z.
*******************************************************************************/
void
rot_set_rotation(Rotation t, double phx, double phy, double phz)
{
  if ((phx == 0) && (phy == 0) && (phz == 0)) {
    t[0][0] = 1.0;
    t[0][1] = 0.0;
    t[0][2] = 0.0;
    t[1][0] = 0.0;
    t[1][1] = 1.0;
    t[1][2] = 0.0;
    t[2][0] = 0.0;
    t[2][1] = 0.0;
    t[2][2] = 1.0;
  } else {
    double cx = cos(phx);
    double sx = sin(phx);
    double cy = cos(phy);
    double sy = sin(phy);
    double cz = cos(phz);
    double sz = sin(phz);

    t[0][0] = cy*cz;
    t[0][1] = sx*sy*cz + cx*sz;
    t[0][2] = sx*sz - cx*sy*cz;
    t[1][0] = -cy*sz;
    t[1][1] = cx*cz - sx*sy*sz;
    t[1][2] = sx*cz + cx*sy*sz;
    t[2][0] = sy;
    t[2][1] = -sx*cy;
    t[2][2] = cx*cy;
  }
}

/*******************************************************************************
* rot_test_identity: Test if rotation is identity
*******************************************************************************/
int
rot_test_identity(Rotation t)
{
  return (t[0][0] + t[1][1] + t[2][2] == 3);
}

/*******************************************************************************
* rot_mul: Matrix multiplication of transformations (this corresponds to
* combining transformations). After rot_mul(T1, T2, T3), doing T3 is
* equal to doing first T2, then T1.
* Note that T3 must not alias (use the same array as) T1 or T2.
*******************************************************************************/
void
rot_mul(Rotation t1, Rotation t2, Rotation t3)
{
  if (rot_test_identity(t1)) {
    rot_copy(t3, t2);
  } else if (rot_test_identity(t2)) {
    rot_copy(t3, t1);
  } else {
    int i,j;
    for(i = 0; i < 3; i++)
      for(j = 0; j < 3; j++)
	t3[i][j] = t1[i][0]*t2[0][j] + t1[i][1]*t2[1][j] + t1[i][2]*t2[2][j];
  }
}

/*******************************************************************************
* rot_copy: Copy a rotation transformation (arrays cannot be assigned in C).
*******************************************************************************/
void
rot_copy(Rotation dest, Rotation src)
{
  int i,j;
  for(i = 0; i < 3; i++)
    for(j = 0; j < 3; j++)
      dest[i][j] = src[i][j];
}

/*******************************************************************************
* rot_transpose: Matrix transposition, which is inversion for Rotation matrices
*******************************************************************************/
void
rot_transpose(Rotation src, Rotation dst)
{
  dst[0][0] = src[0][0];
  dst[0][1] = src[1][0];
  dst[0][2] = src[2][0];
  dst[1][0] = src[0][1];
  dst[1][1] = src[1][1];
  dst[1][2] = src[2][1];
  dst[2][0] = src[0][2];
  dst[2][1] = src[1][2];
  dst[2][2] = src[2][2];
}

/*******************************************************************************
* rot_apply: returns t*a
*******************************************************************************/
Coords
rot_apply(Rotation t, Coords a)
{
  Coords b;
  if (rot_test_identity(t)) {
    return a;
  } else {
    b.x = t[0][0]*a.x + t[0][1]*a.y + t[0][2]*a.z;
    b.y = t[1][0]*a.x + t[1][1]*a.y + t[1][2]*a.z;
    b.z = t[2][0]*a.x + t[2][1]*a.y + t[2][2]*a.z;
    return b;
  }
}

/**
 * Pretty-printing of rotation matrices.
 */
void rot_print(Rotation rot) {
	printf("[ %4.2f %4.2f %4.2f ]\n",
			rot[0][0], rot[0][1], rot[0][2]);
	printf("[ %4.2f %4.2f %4.2f ]\n",
			rot[1][0], rot[1][1], rot[1][2]);
	printf("[ %4.2f %4.2f %4.2f ]\n\n",
			rot[2][0], rot[2][1], rot[2][2]);
}

/**
 * Vector product: used by vec_prod (mccode-r.h). Use coords_xp for Coords.
 */
mcstatic inline void vec_prod_func(double *x, double *y, double *z,
		double x1, double y1, double z1,
		double x2, double y2, double z2) {
    *x = (y1)*(z2) - (y2)*(z1);
    *y = (z1)*(x2) - (z2)*(x1);
    *z = (x1)*(y2) - (x2)*(y1);
}

/**
 * Scalar product: use coords_sp for Coords.
 */
mcstatic inline double scalar_prod(
		double x1, double y1, double z1,
		double x2, double y2, double z2) {
	return ((x1 * x2) + (y1 * y2) + (z1 * z2));
}

/*******************************************************************************
* mccoordschange: applies rotation to (x y z) and (vx vy vz) and Spin (sx,sy,sz)
*******************************************************************************/
void
mccoordschange(Coords a, Rotation t, double *x, double *y, double *z,
               double *vx, double *vy, double *vz, double *sx, double *sy, double *sz)
{
  Coords b, c;

  if (x != NULL && y != NULL && z != NULL) {
    b.x = *x;
    b.y = *y;
    b.z = *z;
    c = rot_apply(t, b);
    b = coords_add(c, a);
    *x = b.x;
    *y = b.y;
    *z = b.z;
  }

  if (vx != NULL && vy != NULL && vz != NULL) mccoordschange_polarisation(t, vx, vy, vz);

  if (sx != NULL && sy != NULL && sz != NULL) mccoordschange_polarisation(t, sx, sy, sz);

}

/*******************************************************************************
* mccoordschange_polarisation: applies rotation to vector (sx sy sz)
*******************************************************************************/
void
mccoordschange_polarisation(Rotation t, double *sx, double *sy, double *sz)
{
  Coords b, c;

  b.x = *sx;
  b.y = *sy;
  b.z = *sz;
  c = rot_apply(t, b);
  *sx = c.x;
  *sy = c.y;
  *sz = c.z;
}

/* SECTION: vector math  ==================================================== */

/* normal_vec: Compute normal vector to (x,y,z). */
void normal_vec(double *nx, double *ny, double *nz,
                double x, double y, double z)
{
  double ax = fabs(x);
  double ay = fabs(y);
  double az = fabs(z);
  double l;
  if(x == 0 && y == 0 && z == 0)
  {
    *nx = 0;
    *ny = 0;
    *nz = 0;
    return;
  }
  if(ax < ay)
  {
    if(ax < az)
    {                           /* Use X axis */
      l = sqrt(z*z + y*y);
      *nx = 0;
      *ny = z/l;
      *nz = -y/l;
      return;
    }
  }
  else
  {
    if(ay < az)
    {                           /* Use Y axis */
      l = sqrt(z*z + x*x);
      *nx = z/l;
      *ny = 0;
      *nz = -x/l;
      return;
    }
  }
  /* Use Z axis */
  l = sqrt(y*y + x*x);
  *nx = y/l;
  *ny = -x/l;
  *nz = 0;
} /* normal_vec */

/*******************************************************************************
 * solve_2nd_order: second order equation solve: A*t^2 + B*t + C = 0
 * solve_2nd_order(&t1, NULL, A,B,C)
 *   returns 0 if no solution was found, or set 't1' to the smallest positive
 *   solution.
 * solve_2nd_order(&t1, &t2, A,B,C)
 *   same as with &t2=NULL, but also returns the second solution.
 * EXAMPLE usage for intersection of a trajectory with a plane in gravitation
 * field (gx,gy,gz):
 * The neutron starts at point r=(x,y,z) with velocityv=(vx vy vz). The plane
 * has a normal vector n=(nx,ny,nz) and contains the point W=(wx,wy,wz).
 * The problem consists in solving the 2nd order equation:
 *      1/2.n.g.t^2 + n.v.t + n.(r-W) = 0
 * so that A = 0.5 n.g; B = n.v; C = n.(r-W);
 * Without acceleration, t=-n.(r-W)/n.v
 ******************************************************************************/
int solve_2nd_order(double *t1, double *t2,
                  double A,  double B,  double C)
{
  int ret=0;

  if (!t1) return 0;
  *t1 = 0;
  if (t2) *t2=0;

  if (fabs(A) < 1E-10) /* approximate to linear equation: A ~ 0 */
  {
    if (B) {  *t1 = -C/B; ret=1; if (t2) *t2=*t1; }
    /* else no intersection: A=B=0 ret=0 */
  }
  else
  {
    double D;
    D = B*B - 4*A*C;
    if (D >= 0) /* Delta > 0: two solutions */
    {
      double sD, dt1, dt2;
      sD = sqrt(D);
      dt1 = (-B + sD)/2/A;
      dt2 = (-B - sD)/2/A;
      /* we identify very small values with zero */
      if (fabs(dt1) < 1e-10) dt1=0.0;
      if (fabs(dt2) < 1e-10) dt2=0.0;

      /* now we choose the smallest positive solution */
      if      (dt1<=0.0 && dt2>0.0) ret=2; /* dt2 positive */
      else if (dt2<=0.0 && dt1>0.0) ret=1; /* dt1 positive */
      else if (dt1> 0.0 && dt2>0.0)
      {  if (dt1 < dt2) ret=1; else ret=2; } /* all positive: min(dt1,dt2) */
      /* else two solutions are negative. ret=-1 */
      if (ret==1) { *t1 = dt1;  if (t2) *t2=dt2; }
      else        { *t1 = dt2;  if (t2) *t2=dt1; }
      ret=2;  /* found 2 solutions and t1 is the positive one */
    } /* else Delta <0: no intersection. ret=0 */
  }
  return(ret);
} /* solve_2nd_order */

/*******************************************************************************
 * randvec_target_circle: Choose random direction towards target at (x,y,z)
 * with given radius.
 * If radius is zero, choose random direction in full 4PI, no target.
 ******************************************************************************/
void
randvec_target_circle(double *xo, double *yo, double *zo, double *solid_angle,
               double xi, double yi, double zi, double radius)
{
  double l2, phi, theta, nx, ny, nz, xt, yt, zt, xu, yu, zu;

  if(radius == 0.0)
  {
    /* No target, choose uniformly a direction in full 4PI solid angle. */
    theta = acos (1 - rand0max(2));
    phi = rand0max(2 * PI);
    if(solid_angle)
      *solid_angle = 4*PI;
    nx = 1;
    ny = 0;
    nz = 0;
    yi = sqrt(xi*xi+yi*yi+zi*zi);
    zi = 0;
    xi = 0;
  }
  else
  {
    double costheta0;
    l2 = xi*xi + yi*yi + zi*zi; /* sqr Distance to target. */
    costheta0 = sqrt(l2/(radius*radius+l2));
    if (radius < 0) costheta0 *= -1;
    if(solid_angle)
    {
      /* Compute solid angle of target as seen from origin. */
        *solid_angle = 2*PI*(1 - costheta0);
    }

    /* Now choose point uniformly on circle surface within angle theta0 */
    theta = acos (1 - rand0max(1 - costheta0)); /* radius on circle */
    phi = rand0max(2 * PI); /* rotation on circle at given radius */
    /* Now, to obtain the desired vector rotate (xi,yi,zi) angle theta around a
       perpendicular axis u=i x n and then angle phi around i. */
    if(xi == 0 && zi == 0)
    {
      nx = 1;
      ny = 0;
      nz = 0;
    }
    else
    {
      nx = -zi;
      nz = xi;
      ny = 0;
    }
  }

  /* [xyz]u = [xyz]i x n[xyz] (usually vertical) */
  vec_prod(xu,  yu,  zu, xi, yi, zi,        nx, ny, nz);
  /* [xyz]t = [xyz]i rotated theta around [xyz]u */
  rotate  (xt,  yt,  zt, xi, yi, zi, theta, xu, yu, zu);
  /* [xyz]o = [xyz]t rotated phi around n[xyz] */
  rotate (*xo, *yo, *zo, xt, yt, zt, phi, xi, yi, zi);
} /* randvec_target_circle */

/*******************************************************************************
 * randvec_target_rect_angular: Choose random direction towards target at
 * (xi,yi,zi) with given ANGULAR dimension height x width. height=phi_x,
 * width=phi_y (radians)
 * If height or width is zero, choose random direction in full 4PI, no target.
 *******************************************************************************/
void
randvec_target_rect_angular(double *xo, double *yo, double *zo, double *solid_angle,
               double xi, double yi, double zi, double width, double height, Rotation A)
{
  double theta, phi, nx, ny, nz, xt, yt, zt, xu, yu, zu;
  Coords tmp;
  Rotation Ainverse;

  rot_transpose(A, Ainverse);

  if(height == 0.0 || width == 0.0)
  {
    randvec_target_circle(xo, yo, zo, solid_angle,
               xi, yi, zi, 0);
    return;
  }
  else
  {
    if(solid_angle)
    {
      /* Compute solid angle of target as seen from origin. */
      *solid_angle = 2*fabs(width*sin(height/2));
    }

    /* Go to global coordinate system */

    tmp = coords_set(xi, yi, zi);
    tmp = rot_apply(Ainverse, tmp);
    coords_get(tmp, &xi, &yi, &zi);

    /* Now choose point uniformly on quadrant within angle theta0/phi0 */
    theta = width*randpm1()/2.0;
    phi   = height*randpm1()/2.0;
    /* Now, to obtain the desired vector rotate (xi,yi,zi) angle phi around
       n, and then theta around u. */
    if(xi == 0 && zi == 0)
    {
      nx = 1;
      ny = 0;
      nz = 0;
    }
    else
    {
      nx = -zi;
      nz = xi;
      ny = 0;
    }
  }

  /* [xyz]u = [xyz]i x n[xyz] (usually vertical) */
  vec_prod(xu,  yu,  zu, xi, yi, zi,        nx, ny, nz);
  /* [xyz]t = [xyz]i rotated theta around [xyz]u */
  rotate  (xt,  yt,  zt, xi, yi, zi, phi, nx, ny, nz);
  /* [xyz]o = [xyz]t rotated phi around n[xyz] */
  rotate (*xo, *yo, *zo, xt, yt, zt, theta, xu,  yu,  zu);

  /* Go back to local coordinate system */
  tmp = coords_set(*xo, *yo, *zo);
  tmp = rot_apply(A, tmp);
  coords_get(tmp, &*xo, &*yo, &*zo);

} /* randvec_target_rect_angular */

/*******************************************************************************
 * randvec_target_rect_real: Choose random direction towards target at (xi,yi,zi)
 * with given dimension height x width (in meters !).
 *
 * Local emission coordinate is taken into account and corrected for 'order' times.
 * (See remarks posted to mcstas-users by George Apostolopoulus <gapost@ipta.demokritos.gr>)
 *
 * If height or width is zero, choose random direction in full 4PI, no target.
 *
 * Traditionally, this routine had the name randvec_target_rect - this is now a
 * a define (see mcstas-r.h) pointing here. If you use the old rouine, you are NOT
 * taking the local emmission coordinate into account.
*******************************************************************************/

void
randvec_target_rect_real(double *xo, double *yo, double *zo, double *solid_angle,
               double xi, double yi, double zi,
               double width, double height, Rotation A,
               double lx, double ly, double lz, int order)
{
  double dx, dy, dist, dist_p, nx, ny, nz, mx, my, mz, n_norm, m_norm;
  double cos_theta;
  Coords tmp;
  Rotation Ainverse;

  rot_transpose(A, Ainverse);

  if(height == 0.0 || width == 0.0)
  {
    randvec_target_circle(xo, yo, zo, solid_angle,
               xi, yi, zi, 0);
    return;
  }
  else
  {

    /* Now choose point uniformly on rectangle within width x height */
    dx = width*randpm1()/2.0;
    dy = height*randpm1()/2.0;

    /* Determine distance to target plane*/
    dist = sqrt(xi*xi + yi*yi + zi*zi);
    /* Go to global coordinate system */

    tmp = coords_set(xi, yi, zi);
    tmp = rot_apply(Ainverse, tmp);
    coords_get(tmp, &xi, &yi, &zi);

    /* Determine vector normal to trajectory axis (z) and gravity [0 1 0] */
    vec_prod(nx, ny, nz, xi, yi, zi, 0, 1, 0);

    /* This now defines the x-axis, normalize: */
    n_norm=sqrt(nx*nx + ny*ny + nz*nz);
    nx = nx/n_norm;
    ny = ny/n_norm;
    nz = nz/n_norm;

    /* Now, determine our y-axis (vertical in many cases...) */
    vec_prod(mx, my, mz, xi, yi, zi, nx, ny, nz);
    m_norm=sqrt(mx*mx + my*my + mz*mz);
    mx = mx/m_norm;
    my = my/m_norm;
    mz = mz/m_norm;

    /* Our output, random vector can now be defined by linear combination: */

    *xo = xi + dx * nx + dy * mx;
    *yo = yi + dx * ny + dy * my;
    *zo = zi + dx * nz + dy * mz;

    /* Go back to local coordinate system */
    tmp = coords_set(*xo, *yo, *zo);
    tmp = rot_apply(A, tmp);
    coords_get(tmp, &*xo, &*yo, &*zo);

    /* Go back to local coordinate system */
    tmp = coords_set(xi, yi, zi);
    tmp = rot_apply(A, tmp);
    coords_get(tmp, &xi, &yi, &zi);

    if (solid_angle) {
      /* Calculate vector from local point to remote random point */
      lx = *xo - lx;
      ly = *yo - ly;
      lz = *zo - lz;
      dist_p = sqrt(lx*lx + ly*ly + lz*lz);

      /* Adjust the 'solid angle' */
      /* 1/r^2 to the chosen point times cos(\theta) between the normal */
      /* vector of the target rectangle and direction vector of the chosen point. */
      cos_theta = (xi * lx + yi * ly + zi * lz) / (dist * dist_p);
      *solid_angle = width * height / (dist_p * dist_p);
      int counter;
      for (counter = 0; counter < order; counter++) {
	*solid_angle = *solid_angle * cos_theta;
      }
    }
  }
} /* randvec_target_rect_real */

/* SECTION: random numbers ================================================== */

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Berkeley.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * This is derived from the Berkeley source:
 *        @(#)random.c        5.5 (Berkeley) 7/6/88
 * It was reworked for the GNU C Library by Roland McGrath.
 * Rewritten to use reentrant functions by Ulrich Drepper, 1995.
 */

/*******************************************************************************
* Modified for McStas from glibc 2.0.7pre1 stdlib/random.c and
* stdlib/random_r.c.
*
* This way random() is more than four times faster compared to calling
* standard glibc random() on ix86 Linux, probably due to multithread support,
* ELF shared library overhead, etc. It also makes McStas generated
* simulations more portable (more likely to behave identically across
* platforms, important for parrallel computations).
*******************************************************************************/


#define        TYPE_3                3
#define        BREAK_3                128
#define        DEG_3                31
#define        SEP_3                3

static mc_int32_t randtbl[DEG_3 + 1] =
  {
    TYPE_3,

    -1726662223, 379960547, 1735697613, 1040273694, 1313901226,
    1627687941, -179304937, -2073333483, 1780058412, -1989503057,
    -615974602, 344556628, 939512070, -1249116260, 1507946756,
    -812545463, 154635395, 1388815473, -1926676823, 525320961,
    -1009028674, 968117788, -123449607, 1284210865, 435012392,
    -2017506339, -911064859, -370259173, 1132637927, 1398500161,
    -205601318,
  };

static mc_int32_t *fptr = &randtbl[SEP_3 + 1];
static mc_int32_t *rptr = &randtbl[1];
static mc_int32_t *state = &randtbl[1];
#define rand_deg DEG_3
#define rand_sep SEP_3
static mc_int32_t *end_ptr = &randtbl[sizeof (randtbl) / sizeof (randtbl[0])];

mc_int32_t
mc_random (void)
{
  mc_int32_t result;

  *fptr += *rptr;
  /* Chucking least random bit.  */
  result = (*fptr >> 1) & 0x7fffffff;
  ++fptr;
  if (fptr >= end_ptr)
  {
    fptr = state;
    ++rptr;
  }
  else
  {
    ++rptr;
    if (rptr >= end_ptr)
      rptr = state;
  }
  return result;
}

void
mc_srandom (unsigned int x)
{
  /* We must make sure the seed is not 0.  Take arbitrarily 1 in this case.  */
  state[0] = x ? x : 1;
  {
    long int i;
    for (i = 1; i < rand_deg; ++i)
    {
      /* This does:
         state[i] = (16807 * state[i - 1]) % 2147483647;
         but avoids overflowing 31 bits.  */
      long int hi = state[i - 1] / 127773;
      long int lo = state[i - 1] % 127773;
      long int test = 16807 * lo - 2836 * hi;
      state[i] = test + (test < 0 ? 2147483647 : 0);
    }
    fptr = &state[rand_sep];
    rptr = &state[0];
    for (i = 0; i < 10 * rand_deg; ++i)
      random ();
  }
}

/* "Mersenne Twister", by Makoto Matsumoto and Takuji Nishimura. */
/* See http://www.math.keio.ac.jp/~matumoto/emt.html for original source. */


/*
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using mt_srandom(seed)
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote
        products derived from this software without specific prior written
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.keio.ac.jp/matumoto/emt.html
   email: matumoto@math.keio.ac.jp
*/

#include <stdio.h>

/* Period parameters */
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

static unsigned long mt[N]; /* the array for the state vector  */
static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

/* initializes mt[N] with a seed */
void mt_srandom(unsigned long s)
{
    mt[0]= s & 0xffffffffUL;
    for (mti=1; mti<N; mti++) {
        mt[mti] =
            (1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti);
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        mt[mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
void init_by_array(init_key, key_length)
unsigned long init_key[], key_length;
{
    int i, j, k;
    mt_srandom(19650218UL);
    i=1; j=0;
    k = (N>key_length ? N : key_length);
    for (; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525UL))
          + init_key[j] + j; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++; j++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
        if (j>=key_length) j=0;
    }
    for (k=N-1; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941UL))
          - i; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
    }

    mt[0] = 0x80000000UL; /* MSB is 1; assuring non-zero initial array */
}

/* generates a random number on [0,0xffffffff]-interval */
unsigned long mt_random(void)
{
    unsigned long y;
    static unsigned long mag01[2]={0x0UL, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
        int kk;

        if (mti == N+1)   /* if mt_srandom() has not been called, */
            mt_srandom(5489UL); /* a default initial seed is used */

        for (kk=0;kk<N-M;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        for (;kk<N-1;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        mti = 0;
    }

    y = mt[mti++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}

#undef N
#undef M
#undef MATRIX_A
#undef UPPER_MASK
#undef LOWER_MASK

/* End of "Mersenne Twister". */

/* End of McStas random number routine. */

/* randnorm: generate a random number from normal law */
double
randnorm(void)
{
  static double v1, v2, s;
  static int phase = 0;
  double X, u1, u2;

  if(phase == 0)
  {
    do
    {
      u1 = rand01();
      u2 = rand01();
      v1 = 2*u1 - 1;
      v2 = 2*u2 - 1;
      s = v1*v1 + v2*v2;
    } while(s >= 1 || s == 0);

    X = v1*sqrt(-2*log(s)/s);
  }
  else
  {
    X = v2*sqrt(-2*log(s)/s);
  }

  phase = 1 - phase;
  return X;
}

/**
 * Generate a random number from -1 to 1 with triangle distribution
 */
double randtriangle(void) {
	double randnum = rand01();
	if (randnum>0.5) return(1-sqrt(2*(randnum-0.5)));
	else return(sqrt(2*randnum)-1);
}

/**
 * Random number between 0.0 and 1.0 (including?)
 */
double rand01() {
	double rand;
	rand = (double) random();
	rand /= (double) MC_RAND_MAX + 1;
	return rand;
}

/**
 * Return a random number between 1 and -1
 */
double randpm1() {
	double rand;
	rand = (double) random();
	rand /= ((double) MC_RAND_MAX + 1) / 2;
	rand -= 1;
	return rand;
}

/**
 * Return a random number between 0 and max.
 */
double rand0max(double max) {
	double rand;
	rand = (double) random();
	rand /= ((double) MC_RAND_MAX + 1) / max;
	return rand;
}

/**
 * Return a random number between min and max.
 */
double randminmax(double min, double max) {
	return rand0max(max - min) + max;
}

/* SECTION: main and signal handlers ======================================== */

/*******************************************************************************
* mchelp: displays instrument executable help with possible options
*******************************************************************************/
static void
mchelp(char *pgmname)
{
  int i;

  fprintf(stderr, "%s (%s) instrument simulation, generated with " MCCODE_STRING " (" MCCODE_DATE ")\n", mcinstrument_name, mcinstrument_source);
  fprintf(stderr, "Usage: %s [options] [parm=value ...]\n", pgmname);
  fprintf(stderr,
"Options are:\n"
"  -s SEED   --seed=SEED      Set random seed (must be != 0)\n"
"  -n COUNT  --ncount=COUNT   Set number of @MCCODE_PARTICULE@s to simulate.\n"
"  -d DIR    --dir=DIR        Put all data files in directory DIR.\n"
"  -f FILE   --file=FILE      Put all data in a single file.\n"
"  -t        --trace          Enable trace of @MCCODE_PARTICULE@s through instrument.\n"
"  -g        --gravitation    Enable gravitation for all trajectories.\n"
"  -a        --data-only      Do not put any headers in the data files.\n"
"  --no-output-files          Do not write any data files.\n"
"  -h        --help           Show this help message.\n"
"  -i        --info           Detailed instrument information.\n"
"  --format=FORMAT            Output data files using format FORMAT\n"
"                             (use option +a to include text header in files\n"
#ifdef USE_MPI
"This instrument has been compiled with MPI support. Use 'mpirun'.\n"
#endif
"\n"
);
  if(mcnumipar > 0)
  {
    fprintf(stderr, "Instrument parameters are:\n");
    for(i = 0; i < mcnumipar; i++)
      if (mcinputtable[i].val && strlen(mcinputtable[i].val))
        fprintf(stderr, "  %-16s(%s) [default='%s']\n", mcinputtable[i].name,
        (*mcinputtypes[mcinputtable[i].type].parminfo)(mcinputtable[i].name),
        mcinputtable[i].val);
      else
        fprintf(stderr, "  %-16s(%s)\n", mcinputtable[i].name,
        (*mcinputtypes[mcinputtable[i].type].parminfo)(mcinputtable[i].name));
  }
  fprintf(stderr, "Available output formats are (default is %s):\n  ", mcformat.Name);
  for (i=0; i < mcNUMFORMATS; fprintf(stderr,"\"%s\" " , mcformats[i++].Name) );
  fprintf(stderr, "\n  Format modifiers: FORMAT may be followed by 'binary float' or \n");
  fprintf(stderr, "  'binary double' to save data blocks as binary. This removes text headers.\n");
  fprintf(stderr, "  The " FLAVOR_UPPER "_FORMAT environment variable may set the default FORMAT to use.\n");
#ifndef NOSIGNALS
  fprintf(stderr, "Known signals are: "
#ifdef SIGUSR1
  "USR1 (status) "
#endif
#ifdef SIGUSR2
  "USR2 (save) "
#endif
#ifdef SIGBREAK
  "BREAK (save) "
#endif
#ifdef SIGTERM
  "TERM (save and exit)"
#endif
  "\n");
#endif /* !NOSIGNALS */
} /* mchelp */


/* mcshowhelp: show help and exit with 0 */
static void
mcshowhelp(char *pgmname)
{
  mchelp(pgmname);
  exit(0);
}

/* mcusage: display usage when error in input arguments and exit with 1 */
static void
mcusage(char *pgmname)
{
  fprintf(stderr, "Error: incorrect command line arguments\n");
  mchelp(pgmname);
  exit(1);
}

/* mcenabletrace: enable trace/mcdisplay or error if requires recompile */
static void
mcenabletrace(void)
{
 if(mctraceenabled)
  mcdotrace = 1;
 else
 {
   fprintf(stderr,
           "Error: trace not enabled (mcenabletrace)\n"
           "Please re-run the McStas compiler "
                   "with the --trace option, or rerun the\n"
           "C compiler with the MC_TRACE_ENABLED macro defined.\n");
   exit(1);
 }
}

/*******************************************************************************
 * mcuse_dir: set data/sim storage directory and create it,
 * or exit with error if exists
 ******************************************************************************/
static void
mcuse_dir(char *dir)
{
  if (!dir || !strlen(dir)) return;
#ifdef MC_PORTABLE
  fprintf(stderr, "Error: "
          "Directory output cannot be used with portable simulation (mcuse_dir)\n");
  exit(1);
#else  /* !MC_PORTABLE */
#ifdef USE_MPI
  if(mpi_node_rank == mpi_node_root)
  {
#endif
    if(mkdir(dir, 0777)) {
#ifndef DANSE
      fprintf(stderr, "Error: unable to create directory '%s' (mcuse_dir)\n", dir);
      fprintf(stderr, "(Maybe the directory already exists?)\n");
      exit(1);
#endif
    }
#ifdef USE_MPI
  }
#endif
  /* handle file://directory URL type */
  if (strncmp(dir, "file:/", strlen("file:/")))
    mcdirname = dir;
  else
    mcdirname = dir+strlen("file:/");

  /* remove trailing PATHSEP (if any) */
  while (strlen(mcdirname) && mcdirname[strlen(mcdirname) - 1] == MC_PATHSEP_C)
    mcdirname[strlen(mcdirname) - 1]='\0';
#endif /* !MC_PORTABLE */
} /* mcuse_dir */

/*******************************************************************************
* mcinfo: display instrument simulation info to stdout and exit
*******************************************************************************/
static void
mcinfo(void)
{
  if(strstr(mcformat.Name,"NeXus"))
    exit(fprintf(stderr,"Error: Can not display instrument information in NeXus binary format\n"));
  mcsiminfo_init(stdout);
  mcsiminfo_close();
  exit(0); /* includes MPI_Finalize in MPI mode */
} /* mcinfo */

/*******************************************************************************
* mcreadparams: request parameters from the prompt (or use default)
*******************************************************************************/
void
mcreadparams(void)
{
  int i,j,status;
  static char buf[CHAR_BUF_LENGTH];
  char *p;
  int len;

  MPI_MASTER(printf("Instrument parameters for %s (%s)\n",
                    mcinstrument_name, mcinstrument_source));

  for(i = 0; mcinputtable[i].name != 0; i++)
  {
    do
    {
      MPI_MASTER(
                 if (mcinputtable[i].val && strlen(mcinputtable[i].val))
                   printf("Set value of instrument parameter %s (%s) [default='%s']:\n",
                          mcinputtable[i].name,
                          (*mcinputtypes[mcinputtable[i].type].parminfo)
                          (mcinputtable[i].name), mcinputtable[i].val);
                 else
                   printf("Set value of instrument parameter %s (%s):\n",
                          mcinputtable[i].name,
                          (*mcinputtypes[mcinputtable[i].type].parminfo)
                          (mcinputtable[i].name));
                 fflush(stdout);
                 );
#ifdef USE_MPI
      if(mpi_node_rank == mpi_node_root)
        {
          p = fgets(buf, CHAR_BUF_LENGTH, stdin);
          if(p == NULL)
            {
              fprintf(stderr, "Error: empty input for paramater %s (mcreadparams)\n", mcinputtable[i].name);
              exit(1);
            }
        }
      else
        p = buf;
      MPI_Bcast(buf, CHAR_BUF_LENGTH, MPI_CHAR, mpi_node_root, MPI_COMM_WORLD);
#else /* !USE_MPI */
      p = fgets(buf, CHAR_BUF_LENGTH, stdin);
      if(p == NULL)
        {
          fprintf(stderr, "Error: empty input for paramater %s (mcreadparams)\n", mcinputtable[i].name);
          exit(1);
        }
#endif /* USE_MPI */
      len = strlen(buf);
      if (!len || (len == 1 && (buf[0] == '\n' || buf[0] == '\r')))
      {
        if (mcinputtable[i].val && strlen(mcinputtable[i].val)) {
          strncpy(buf, mcinputtable[i].val, CHAR_BUF_LENGTH);  /* use default value */
          len = strlen(buf);
        }
      }
      for(j = 0; j < 2; j++)
      {
        if(len > 0 && (buf[len - 1] == '\n' || buf[len - 1] == '\r'))
        {
          len--;
          buf[len] = '\0';
        }
      }

      status = (*mcinputtypes[mcinputtable[i].type].getparm)
                   (buf, mcinputtable[i].par);
      if(!status)
      {
        (*mcinputtypes[mcinputtable[i].type].error)(mcinputtable[i].name, buf);
        if (!mcinputtable[i].val || strlen(mcinputtable[i].val)) {
          fprintf(stderr, "       Change %s default value in instrument definition.\n", mcinputtable[i].name);
          exit(1);
        }
      }
    } while(!status);
  }
} /* mcreadparams */

/*******************************************************************************
* mcparseoptions: parse command line arguments (options, parameters)
*******************************************************************************/
void
mcparseoptions(int argc, char *argv[])
{
  int i, j;
  char *p;
  int paramset = 0, *paramsetarray;
  char *usedir=NULL;

  /* Add one to mcnumipar to avoid allocating zero size memory block. */
  paramsetarray = malloc((mcnumipar + 1)*sizeof(*paramsetarray));
  if(paramsetarray == NULL)
  {
    fprintf(stderr, "Error: insufficient memory (mcparseoptions)\n");
    exit(1);
  }
  for(j = 0; j < mcnumipar; j++)
    {
      paramsetarray[j] = 0;
      if (mcinputtable[j].val != NULL && strlen(mcinputtable[j].val))
      {
        int  status;
        char buf[CHAR_BUF_LENGTH];
        strncpy(buf, mcinputtable[j].val, CHAR_BUF_LENGTH);
        status = (*mcinputtypes[mcinputtable[j].type].getparm)
                   (buf, mcinputtable[j].par);
        if(!status) fprintf(stderr, "Invalid '%s' default value %s in instrument definition (mcparseoptions)\n", mcinputtable[j].name, buf);
        else paramsetarray[j] = 1;
      } else {
        (*mcinputtypes[mcinputtable[j].type].getparm)
          (NULL, mcinputtable[j].par);
        paramsetarray[j] = 0;
      }
    }
  for(i = 1; i < argc; i++)
  {
    if(!strcmp("-s", argv[i]) && (i + 1) < argc)
      mcsetseed(argv[++i]);
    else if(!strncmp("-s", argv[i], 2))
      mcsetseed(&argv[i][2]);
    else if(!strcmp("--seed", argv[i]) && (i + 1) < argc)
      mcsetseed(argv[++i]);
    else if(!strncmp("--seed=", argv[i], 7))
      mcsetseed(&argv[i][7]);
    else if(!strcmp("-n", argv[i]) && (i + 1) < argc)
      mcsetn_arg(argv[++i]);
    else if(!strncmp("-n", argv[i], 2))
      mcsetn_arg(&argv[i][2]);
    else if(!strcmp("--ncount", argv[i]) && (i + 1) < argc)
      mcsetn_arg(argv[++i]);
    else if(!strncmp("--ncount=", argv[i], 9))
      mcsetn_arg(&argv[i][9]);
    else if(!strcmp("-d", argv[i]) && (i + 1) < argc)
      usedir=argv[++i];  /* will create directory after parsing all arguments (end of this function) */
    else if(!strncmp("-d", argv[i], 2))
      usedir=&argv[i][2];
    else if(!strcmp("--dir", argv[i]) && (i + 1) < argc)
      usedir=argv[++i];
    else if(!strncmp("--dir=", argv[i], 6))
      usedir=&argv[i][6];
    else if(!strcmp("-f", argv[i]) && (i + 1) < argc)
      mcuse_file(argv[++i]);
    else if(!strncmp("-f", argv[i], 2))
      mcuse_file(&argv[i][2]);
    else if(!strcmp("--file", argv[i]) && (i + 1) < argc)
      mcuse_file(argv[++i]);
    else if(!strncmp("--file=", argv[i], 7))
      mcuse_file(&argv[i][7]);
    else if(!strcmp("-h", argv[i]))
      mcshowhelp(argv[0]);
    else if(!strcmp("--help", argv[i]))
      mcshowhelp(argv[0]);
    else if(!strcmp("-i", argv[i])) {
      mcformat=mcuse_format(MCSTAS_FORMAT);
      mcinfo();
    }
    else if(!strcmp("--info", argv[i]))
      mcinfo();
    else if(!strcmp("-t", argv[i]))
      mcenabletrace();
    else if(!strcmp("--trace", argv[i]))
      mcenabletrace();
    else if(!strcmp("-a", argv[i]))
      mcascii_only = 1;
    else if(!strcmp("+a", argv[i]))
      mcascii_only = 0;
    else if(!strcmp("--data-only", argv[i]))
      mcascii_only = 1;
    else if(!strcmp("--gravitation", argv[i]))
      mcgravitation = 1;
    else if(!strcmp("-g", argv[i]))
      mcgravitation = 1;
    else if(!strncmp("--format=", argv[i], 9)) {
      mcascii_only = 0;
      mcformat=mcuse_format(&argv[i][9]);
    }
    else if(!strcmp("--format", argv[i]) && (i + 1) < argc) {
      mcascii_only = 0;
      mcformat=mcuse_format(argv[++i]);
    }
    else if(!strncmp("--format_data=", argv[i], 14) || !strncmp("--format-data=", argv[i], 14)) {
      mcascii_only = 0;
      mcformat_data=mcuse_format(&argv[i][14]);
    }
    else if((!strcmp("--format_data", argv[i]) || !strcmp("--format-data", argv[i])) && (i + 1) < argc) {
      mcascii_only = 0;
      mcformat_data=mcuse_format(argv[++i]);
    }
    else if(!strcmp("--no-output-files", argv[i]))
      mcdisable_output_files = 1;
    else if(argv[i][0] != '-' && (p = strchr(argv[i], '=')) != NULL)
    {
      *p++ = '\0';

      for(j = 0; j < mcnumipar; j++)
        if(!strcmp(mcinputtable[j].name, argv[i]))
        {
          int status;
          status = (*mcinputtypes[mcinputtable[j].type].getparm)(p,
                        mcinputtable[j].par);
          if(!status || !strlen(p))
          {
            (*mcinputtypes[mcinputtable[j].type].error)
              (mcinputtable[j].name, p);
            exit(1);
          }
          paramsetarray[j] = 1;
          paramset = 1;
          break;
        }
      if(j == mcnumipar)
      {                                /* Unrecognized parameter name */
        fprintf(stderr, "Error: unrecognized parameter %s (mcparseoptions)\n", argv[i]);
        exit(1);
      }
    }
    else if(argv[i][0] == '-') {
      fprintf(stderr, "Error: unrecognized option argument %s (mcparseoptions). Ignored.\n", argv[i++]);
    }
    else {
      fprintf(stderr, "Error: unrecognized argument %s (mcparseoptions). Aborting.\n", argv[i]);
      mcusage(argv[0]);
    }
  }
  if (!mcascii_only) {
    if (strstr(mcformat.Name,"binary")) fprintf(stderr, "Warning: %s files will contain text headers.\n         Use -a option to clean up.\n", mcformat.Name);
    strcat(mcformat.Name, " with text headers");
  }
  if(!paramset)
    mcreadparams();                /* Prompt for parameters if not specified. */
  else
  {
    for(j = 0; j < mcnumipar; j++)
      if(!paramsetarray[j])
      {
        fprintf(stderr, "Error: Instrument parameter %s left unset (mcparseoptions)\n",
                mcinputtable[j].name);
        exit(1);
      }
  }
  free(paramsetarray);
#ifdef USE_MPI
  if (mcdotrace) mpi_node_count=1; /* disable threading when in trace mode */
#endif
  if (usedir && strlen(usedir)) mcuse_dir(usedir);
} /* mcparseoptions */

#ifndef NOSIGNALS
mcstatic char  mcsig_message[256];


/*******************************************************************************
* sighandler: signal handler that makes simulation stop, and save results
*******************************************************************************/
void sighandler(int sig)
{
  /* MOD: E. Farhi, Sep 20th 2001: give more info */
  time_t t1, t0;
#define SIG_SAVE 0
#define SIG_TERM 1
#define SIG_STAT 2
#define SIG_ABRT 3

  printf("\n# McStas: [pid %i] Signal %i detected", getpid(), sig);
#ifdef USE_MPI
  printf("[proc %i]", mpi_node_rank);
#endif
#if defined(SIGUSR1) && defined(SIGUSR2) && defined(SIGKILL)
  if (!strcmp(mcsig_message, "sighandler") && (sig != SIGUSR1) && (sig != SIGUSR2))
  {
    printf("\n# Fatal : unrecoverable loop ! Suicide (naughty boy).\n");
    kill(0, SIGKILL); /* kill myself if error occurs within sighandler: loops */
  }
#endif
  switch (sig) {
#ifdef SIGINT
    case SIGINT : printf(" SIGINT (interrupt from terminal, Ctrl-C)"); sig = SIG_TERM; break;
#endif
#ifdef SIGILL
    case SIGILL  : printf(" SIGILL (Illegal instruction)"); sig = SIG_ABRT; break;
#endif
#ifdef SIGFPE
    case SIGFPE  : printf(" SIGFPE (Math Error)"); sig = SIG_ABRT; break;
#endif
#ifdef SIGSEGV
    case SIGSEGV : printf(" SIGSEGV (Mem Error)"); sig = SIG_ABRT; break;
#endif
#ifdef SIGTERM
    case SIGTERM : printf(" SIGTERM (Termination)"); sig = SIG_TERM; break;
#endif
#ifdef SIGABRT
    case SIGABRT : printf(" SIGABRT (Abort)"); sig = SIG_ABRT; break;
#endif
#ifdef SIGQUIT
    case SIGQUIT : printf(" SIGQUIT (Quit from terminal)"); sig = SIG_TERM; break;
#endif
#ifdef SIGTRAP
    case SIGTRAP : printf(" SIGTRAP (Trace trap)"); sig = SIG_ABRT; break;
#endif
#ifdef SIGPIPE
    case SIGPIPE : printf(" SIGPIPE (Broken pipe)"); sig = SIG_ABRT; break;
#endif
#ifdef SIGUSR1
    case SIGUSR1 : printf(" SIGUSR1 (Display info)"); sig = SIG_STAT; break;
#endif
#ifdef SIGUSR2
    case SIGUSR2 : printf(" SIGUSR2 (Save simulation)"); sig = SIG_SAVE; break;
#endif
#ifdef SIGHUP
    case SIGHUP  : printf(" SIGHUP (Hangup/update)"); sig = SIG_SAVE; break;
#endif
#ifdef SIGBUS
    case SIGBUS  : printf(" SIGBUS (Bus error)"); sig = SIG_ABRT; break;
#endif
#ifdef SIGURG
    case SIGURG  : printf(" SIGURG (Urgent socket condition)"); sig = SIG_ABRT; break;
#endif
#ifdef SIGBREAK
    case SIGBREAK: printf(" SIGBREAK (Break signal, Ctrl-Break)"); sig = SIG_SAVE; break;
#endif
    default : printf(" (look at signal list for signification)"); sig = SIG_ABRT; break;
  }
  printf("\n");
  printf("# Simulation: %s (%s) \n", mcinstrument_name, mcinstrument_source);
  printf("# Breakpoint: %s ", mcsig_message);
  if (strstr(mcsig_message, "Save") && (sig == SIG_SAVE))
    sig = SIG_STAT;
  SIG_MESSAGE("sighandler");
  if (mcget_ncount() == 0)
    printf("(0 %%)\n" );
  else
  {
    printf("%.2f %% (%10.1f/%10.1f)\n", 100.0*mcget_run_num()/mcget_ncount(), 1.0*mcget_run_num(), 1.0*mcget_ncount());
  }
  t0 = (time_t)mcstartdate;
  t1 = time(NULL);
  printf("# Date:      %s", ctime(&t1));
  printf("# Started:   %s", ctime(&t0));

  if (sig == SIG_STAT)
  {
    printf("# McStas: Resuming simulation (continue)\n");
    fflush(stdout);
    return;
  }
  else
  if (sig == SIG_SAVE)
  {
    printf("# McStas: Saving data and resume simulation (continue)\n");
    mcsave(NULL);
    fflush(stdout);
    return;
  }
  else
  if (sig == SIG_TERM)
  {
    printf("# McStas: Finishing simulation (save results and exit)\n");
    mcfinally();
    exit(0);
  }
  else
  {
    fflush(stdout);
    perror("# Last I/O Error");
    printf("# McStas: Simulation stop (abort)\n");
    exit(-1);
  }
#undef SIG_SAVE
#undef SIG_TERM
#undef SIG_STAT
#undef SIG_ABRT

} /* sighandler */
#endif /* !NOSIGNALS */

/*******************************************************************************
* mccode_main: McCode main() function.
*******************************************************************************/
int mccode_main(int argc, char *argv[])
{
/*  double run_num = 0; */
  time_t t;
#ifdef USE_MPI
  char mpi_node_name[MPI_MAX_PROCESSOR_NAME];
  int  mpi_node_name_len;
#endif /* USE_MPI */

#ifdef MAC
  argc = ccommand(&argv);
#endif

#ifdef USE_MPI
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_node_count); /* get number of nodes */
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_node_rank);
  MPI_Get_processor_name(mpi_node_name, &mpi_node_name_len);
#endif /* USE_MPI */

#ifdef USE_MPI
/* *** print number of nodes *********************************************** */
  if (mpi_node_count > 1) {
    MPI_MASTER(
    printf("Simulation '%s' (%s): running on %i nodes (master is '%s', MPI version %i.%i).\n",
      mcinstrument_name, mcinstrument_source, mpi_node_count, mpi_node_name, MPI_VERSION, MPI_SUBVERSION);
    );
    /* adapt random seed for each node */
    mcseed=(long)(time(&t) + mpi_node_rank);
    srandom(mcseed);
    t += mpi_node_rank;
  } else
#else /* !USE_MPI */
  mcseed=(long)time(&t);
  srandom(mcseed);
#endif /* USE_MPI */
  mcstartdate = (long)t;  /* set start date before parsing options and creating sim file */

/* *** parse options ******************************************************* */
  SIG_MESSAGE("main (Start)");
  mcformat=mcuse_format(getenv(FLAVOR_UPPER "_FORMAT") ?
                        getenv(FLAVOR_UPPER "_FORMAT") : MCSTAS_FORMAT);
  /* default is to output as McStas format */
  mcformat_data.Name=NULL;
  /* read simulation parameters and options */
  mcparseoptions(argc, argv); /* sets output dir and format */
  if (strstr(mcformat.Name, "NeXus")) {
    if (mcformat_data.Name) mcclear_format(mcformat_data);
    mcformat_data.Name=NULL;
  }
  if (!mcformat_data.Name && strstr(mcformat.Name, "HTML"))
    mcformat_data = mcuse_format("VRML");

/* *** install sig handler, but only once !! after parameters parsing ******* */
#ifndef NOSIGNALS
#ifdef SIGQUIT
  if (signal( SIGQUIT ,sighandler) == SIG_IGN)
    signal( SIGQUIT,SIG_IGN);   /* quit (ASCII FS) */
#endif
#ifdef SIGABRT
  if (signal( SIGABRT ,sighandler) == SIG_IGN)
    signal( SIGABRT,SIG_IGN);   /* used by abort, replace SIGIOT in the future */
#endif
#ifdef SIGTERM
  if (signal( SIGTERM ,sighandler) == SIG_IGN)
    signal( SIGTERM,SIG_IGN);   /* software termination signal from kill */
#endif
#ifdef SIGUSR1
  if (signal( SIGUSR1 ,sighandler) == SIG_IGN)
    signal( SIGUSR1,SIG_IGN);   /* display simulation status */
#endif
#ifdef SIGUSR2
  if (signal( SIGUSR2 ,sighandler) == SIG_IGN)
    signal( SIGUSR2,SIG_IGN);
#endif
#ifdef SIGHUP
  if (signal( SIGHUP ,sighandler) == SIG_IGN)
    signal( SIGHUP,SIG_IGN);
#endif
#ifdef SIGILL
  if (signal( SIGILL ,sighandler) == SIG_IGN)
    signal( SIGILL,SIG_IGN);    /* illegal instruction (not reset when caught) */
#endif
#ifdef SIGFPE
  if (signal( SIGFPE ,sighandler) == SIG_IGN)
    signal( SIGSEGV,SIG_IGN);    /* floating point exception */
#endif
#ifdef SIGBUS
  if (signal( SIGBUS ,sighandler) == SIG_IGN)
    signal( SIGSEGV,SIG_IGN);    /* bus error */
#endif
#ifdef SIGSEGV
  if (signal( SIGSEGV ,sighandler) == SIG_IGN)
    signal( SIGSEGV,SIG_IGN);   /* segmentation violation */
#endif
#endif /* !NOSIGNALS */
  if (!strstr(mcformat.Name,"NeXus")) {
    mcsiminfo_init(NULL); mcsiminfo_close();  /* makes sure we can do that */
  }
  SIG_MESSAGE("main (Init)");
  mcinit();
#ifndef NOSIGNALS
#ifdef SIGINT
  if (signal( SIGINT ,sighandler) == SIG_IGN)
    signal( SIGINT,SIG_IGN);    /* interrupt (rubout) only after INIT */
#endif
#endif /* !NOSIGNALS */

/* ================ main particule generation/propagation loop ================ */
#if defined (USE_MPI)
  /* sliced Ncount on each MPI node */
  mcncount = mpi_node_count > 1 ?
    floor(mcncount / mpi_node_count) :
    mcncount; /* number of rays per node */
#endif

/* main particule event loop */
while(mcrun_num < mcncount || mcrun_num < mcget_ncount())
  {
#ifndef NEUTRONICS
    mcgenstate();
#endif
    /* old init: mcsetstate(0, 0, 0, 0, 0, 1, 0, sx=0, sy=1, sz=0, 1); */
    mcraytrace();
    mcrun_num++;
  }

#ifdef USE_MPI
 /* merge run_num from MPI nodes */
  if (mpi_node_count > 1) {
  double mcrun_num_double = (double)mcrun_num;
  mc_MPI_Sum(&mcrun_num_double, 1);
  mcrun_num = (unsigned long long)mcrun_num_double;
  }
#endif

/* save/finally executed by master node/thread */
  mcfinally();
  mcclear_format(mcformat);
  if (mcformat_data.Name) mcclear_format(mcformat_data);

#ifdef USE_MPI
  MPI_Finalize();
#endif /* USE_MPI */

  return 0;
} /* mccode_main */

#ifdef NEUTRONICS
/*Main neutronics function steers the McStas calls, initializes parameters etc */
/* Only called in case NEUTRONICS = TRUE */
void neutronics_main_(float *inx, float *iny, float *inz, float *invx, float *invy, float *invz, float *intime, float *insx, float *insy, float *insz, float *inw, float *outx, float *outy, float *outz, float *outvx, float *outvy, float *outvz, float *outtime, float *outsx, float *outsy, float *outsz, float *outwgt)
{

  extern double mcnx, mcny, mcnz, mcnvx, mcnvy, mcnvz;
  extern double mcnt, mcnsx, mcnsy, mcnsz, mcnp;

  /* External code governs iteration - McStas is iterated once per call to neutronics_main. I.e. below counter must be initiancated for each call to neutronics_main*/
  mcrun_num=0;

  time_t t;
  t = (time_t)mcstartdate;
  mcstartdate = t;  /* set start date before parsing options and creating sim file */
  mcinit();

  /* *** parse options *** */
  SIG_MESSAGE("main (Start)");
  mcformat=mcuse_format(getenv(FLAVOR_UPPER "_FORMAT") ?
                        getenv(FLAVOR_UPPER "_FORMAT") : MCSTAS_FORMAT);
  /* default is to output as McStas format */
  mcformat_data.Name=NULL;
  /* read simulation parameters and options */
  if (strstr(mcformat.Name, "NeXus")) {
    if (mcformat_data.Name) mcclear_format(mcformat_data);
    mcformat_data.Name=NULL;
  }
  if (!mcformat_data.Name && strstr(mcformat.Name, "HTML"))
    mcformat_data = mcuse_format("VRML");

  /* Set neutron state based on input from neutronics code */
  mcsetstate(*inx,*iny,*inz,*invx,*invy,*invz,*intime,*insx,*insy,*insz,*inw);

  /* main neutron event loop - runs only one iteration */

  //mcstas_raytrace(&mcncount); /* prior to McStas 1.12 */

  mcallowbackprop = 1; //avoid absorbtion from negative dt
  int argc=1;
  char *argv[0];
  int dummy = mccode_main(argc, argv);

  /*  save/finally executed by master node/thread. Needed for McStas 1.12 and older */
  /*  mcfinally(); */
  /*  mcclear_format(mcformat); */

  if (mcformat_data.Name) mcclear_format(mcformat_data);

  *outx =  mcnx;
  *outy =  mcny;
  *outz =  mcnz;
  *outvx =  mcnvx;
  *outvy =  mcnvy;
  *outvz =  mcnvz;
  *outtime =  mcnt;
  *outsx =  mcnsx;
  *outsy =  mcnsy;
  *outsz =  mcnsz;
  *outwgt =  mcnp;

  return;
} /* neutronics_main */

#endif /*NEUTRONICS*/

#ifdef USE_NEXUS
/*******************************************************************************
* mcnxfile_parameters: writes the simulation parameters
*                   open/close a new Data Set per parameter in the current simulation Group
* NOTE: this function can not be included in nexus-lib as it depends on mcinputtypes
*       and mcinputtable are defined at compile time in here.
* Returns: NX_OK
*******************************************************************************/
int mcnxfile_parameters(NXhandle nxhandle)
{
  int i;
  char Parameters[CHAR_BUF_LENGTH];
  /* in the parameter group, create one SDS per parameter */
  for(i = 0; i < mcnumipar; i++) {
    if (mcget_run_num() || (mcinputtable[i].val && strlen(mcinputtable[i].val))) {
      char nxname[CHAR_BUF_LENGTH];
      int length;
      if (mcinputtable[i].par == NULL)
        strncpy(Parameters, (mcinputtable[i].val ? mcinputtable[i].val : ""), CHAR_BUF_LENGTH);
      else
        (*mcinputtypes[mcinputtable[i].type].printer)(Parameters, mcinputtable[i].par);
      sprintf(nxname, "%s", Parameters);
      length = strlen(nxname);
      NXmakedata(mcnxHandle, mcinputtable[i].name, NX_CHAR, 1, &length);
      NXopendata(mcnxHandle, mcinputtable[i].name);
      NXputdata (mcnxHandle, nxname);
      strcpy(nxname, (*mcinputtypes[mcinputtable[i].type].parminfo)(mcinputtable[i].name));
      NXputattr (mcnxHandle, "type", nxname, strlen(nxname), NX_CHAR);
      strcpy(nxname, mcinputtable[i].val ? mcinputtable[i].val : "");
      NXputattr (mcnxHandle, "default_value", nxname, strlen(nxname), NX_CHAR);
      NXputattr (mcnxHandle, "name", mcinputtable[i].name, strlen(mcinputtable[i].name), NX_CHAR);
      NXclosedata(mcnxHandle);
    }
  }
  return(NX_OK);
} /* mcnxfile_parameters */
#endif /* USE_NEXUS */

#endif /* !MCCODE_H */
/* End of file "mccode-r.c". */
/* End of file "mccode-r.c". */

#line 6361 "Test_src_gaussian.c"

#line 1 "mcxtrace-r.c"
/*******************************************************************************
*
* McXtrace, X-ray tracing package
*           Copyright (C) 1997-2009, All rights reserved
*           Risoe National Laboratory, Roskilde, Denmark
*           Institut Laue Langevin, Grenoble, France
*
* Runtime: share/mcxtrace-r.c
*
* %Identification
* Edited by: EK
* Date:    May 29, 2009
* Release: McXtrace X.Y
* Version: $Revision: $
*
* Runtime system for McXtrace.
* Embedded within instrument in runtime mode.
*
* Usage: Automatically embedded in the c code whenever required.
*
*******************************************************************************/

#ifndef MCXTRACE_H

/*******************************************************************************
* mcstore_xray: stores neutron coodinates into global array (per component)
*******************************************************************************/
void
mcstore_xray(MCNUM *s, int index, double x, double y, double z,
               double kx, double ky, double kz, double phi, double t,
               double Ex, double Ey, double Ez, double p)
{
    double *dptr = &s[11*index];
    *dptr++  = x;
    *dptr++  = y ;
    *dptr++  = z ;
    *dptr++  = kx;
    *dptr++  = ky;
    *dptr++  = kz;
    *dptr++  = phi;
    *dptr++  = t;
    *dptr++  = Ex;
    *dptr++  = Ey;
    *dptr++  = Ez;
    *dptr    = p ;
}

/*******************************************************************************
* mcrestore_xray: restores neutron coodinates from global array
*******************************************************************************/
void
mcrestore_xray(MCNUM *s, int index, double *x, double *y, double *z,
               double *kx, double *ky, double *kz, double *phi, double *t,
               double *Ex, double *Ey, double *Ez, double *p)
{
    double *dptr = &s[11*index];
    *x  =  *dptr++;
    *y  =  *dptr++;
    *z  =  *dptr++;
    *kx =  *dptr++;
    *ky =  *dptr++;
    *kz =  *dptr++;
    *phi=  *dptr++;
    *t  =  *dptr++;
    *Ex =  *dptr++;
    *Ey =  *dptr++;
    *Ez =  *dptr++;
    *p  =  *dptr;
} /* mcrestore_xray */

/*******************************************************************************
* mcsetstate: transfer parameters into global McXtrace variables 
*******************************************************************************/
void
mcsetstate(double x, double y, double z, double kx, double ky, double kz,
           double phi, double t, double Ex, double Ey, double Ez, double p)
{
  extern double mcnx, mcny, mcnz, mcnkx, mcnky, mcnkz;
  extern double mcnphi, mcnt, mcnEx, mcnEy, mcnEz, mcnp;

  mcnx = x;
  mcny = y;
  mcnz = z;
  mcnkx = kx;
  mcnky = ky;
  mcnkz = kz;
  mcnphi = phi;
  mcnt = t;
  mcnEx = Ex;
  mcnEy = Ey;
  mcnEz = Ez;
  mcnp = p;
} /* mcsetstate */

/*******************************************************************************
* mcgenstate: set default xray parameters 
*******************************************************************************/
void
mcgenstate(void)
{
  mcsetstate(0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1);
  /* old initialisation: mcsetstate(0, 0, 0, 0, 0, 1, 0, sx=0, sy=1, sz=0, 1); */
}

/* intersection routines ==================================================== */

/*******************************************************************************
* inside_rectangle: Check if (x,y) is inside rectangle (xwidth, yheight) 
* return 0 if outside and 1 if inside 
*******************************************************************************/
int inside_rectangle(double x, double y, double xwidth, double yheight)
{
  if (x>-xwidth/2 && x<xwidth/2 && y>-yheight/2 && y<yheight/2)
    return 1;
  else
    return 0;
}

/*******************************************************************************
 * box_intersect: compute length intersection with a box
 * returns 0 when no intersection is found
 *      or 1 in case of intersection with resulting travelling lengths dl_in and dl_out
*******************************************************************************/
int box_intersect(double *dl_in, double *dl_out,
                  double x, double y, double z,
                  double kx, double ky, double kz,
                  double dx, double dy, double dz)
{

  double k, l,xf,yf,zf, l_[6],dx_2,dy_2,dz_2;
  double ab[2];
  unsigned int count=0;
  k=sqrt(scalar_prod(kx,ky,kz,kx,ky,kz));
  dx_2=dx/2.0;dy_2=dy/2.0;dz_2=dz/2.0; 
  /*we really don't need to store the 6 intersects as only two are possible. i.e. should remove that.*/
  if (kx) {
    l=(-dx_2-x)/kx*k;
    yf=l*ky/k+y;zf=l*kz/k+z;
    if(yf > -dy_2 && yf<dy_2 && zf > -dz_2 && zf<dz_2){
      l_[0]=l;
      ab[count++]=l_[0];
    }else{
      l_[0]=0;
    }
    l=(dx_2-x)/kx*k;
    yf=l*ky/k+y;zf=l*kz/k+z;
    if(yf > -dy_2 && yf<dy_2 && zf > -dz_2 && zf<dz_2){
      l_[1]=l;
      ab[count++]=l_[1];
    }else{
      l_[1]=0;
    }
  }
  if (ky) {
    l=(-dy_2-y)/ky*k;
    xf=l*kx/k+x;zf=l*kz/k+z;
    if(xf > -dx_2 && xf<dx_2 && zf > -dz_2 && zf<dz_2){
      l_[2]=l;
      ab[count++]=l_[2];
    }else{
      l_[2]=0;
    } 
    l=(dy_2-y)/ky*k;
    xf=l*kx/k+x;zf=l*kz/k+z;
    if(xf > -dx_2 && xf<dx_2 && zf > -dz_2 && zf<dz_2){
      l_[3]=l;
      ab[count++]=l_[3];
    }else{
      l_[3]=0;
    }
  }
  if (kz) {
    l=(-dz_2-z)/kz*k;
    xf=l*kx/k+x; yf=l*ky/k+y;
    if(xf > -dx_2 && xf<dx_2 && yf > -dy_2 && yf<dy_2){
      l_[4]=l;
      ab[count++]=l_[4];
    }else{
      l_[4]=0;
    }
    l=(dz_2-z)/kz*k;
    xf=l*kx/k+x; yf=l*ky/k+y;
    if(xf > -dx_2 && xf<dx_2 && yf > -dy_2 && yf<dy_2){
      l_[5]=l;
      ab[count++]=l_[5];
    }else{
      l_[5]=0;
    }
  }
  /*check validity of intersects*/
  if (count>2){
    fprintf(stderr,"box_instersect: xray hitting box more than twice\n");
  }
  if (!count){
    *dl_in=0;*dl_out=0;
    return 0;
  }

  if (ab[0]<ab[1]){
    *dl_in=ab[0];*dl_out=ab[1];
    return 1;
  }else{
    *dl_in=ab[1];*dl_out=ab[0];
    return 1;
  }
} /* box_intersect */

/*******************************************************************************
 * cylinder_intersect: compute intersection with a cylinder
 * returns 0 when no intersection is found
 *      or 1/2/4/8/16 bits depending on intersection,
 *     and resulting times l0 and l1
 * Written by: EK 11.6.09 
 *******************************************************************************/
int
cylinder_intersect(double *l0, double *l1, double x, double y, double z,
                   double kx, double ky, double kz, double r, double h)
{
  double A,B,C,D,k2,k;
  double dl1p=0,dl0p=0,dl1c=0,dl0c=0,y0,y1;
  int ret=1,stat=0,plane_stat=0;
  enum {HIT_CYL=01,ENTER_TOP=02,ENTER_BOT=04,EXIT_TOP=010,EXIT_BOT=020,ENTER_MASK=060,EXIT_MASK=030};
  k2=(kx*kx + ky*ky + kz*kz);
  k=sqrt(k2);

  /*check for prop. vector 0*/
  if(!k2) return 0;

  A= (k2 - ky*ky);
  B= 2*(x*kx + z*kz);
  C=(x*x + z*z - r*r);
  D=B*B-4*A*C;
  if(D>=0){
    if (kx || kz){
      stat|=HIT_CYL;
    /*propagation not parallel to y-axis*/
    /*hit infinitely high cylinder?*/
      D=sqrt(D);
      dl0c=k*(-B-D)/(2*A);
      dl1c=k*(-B+D)/(2*A);
      y0=dl0c*ky/k+y;
      y1=dl1c*ky/k+y;
      if ( (y0<-h/2 && y1<-h/2) || (y0>h/2 && y1>h/2) ){
        /*ray passes above or below cylinder*/
        return 0;
      }
    }
    /*now check top and bottom planes*/
    if (ky){
      dl0p = k*(-h/2-y)/ky;
      dl1p = k*(h/2-y)/ky;
      /*switch solutions?*/
      if (dl0p<dl1p){
        plane_stat|=(ENTER_BOT|EXIT_TOP);
      }else{
        double tmp=dl1p;
        dl1p=dl0p;dl0p=tmp;
        plane_stat|=(ENTER_TOP|EXIT_BOT);
      }
    }
  }
  if (stat & HIT_CYL){
    if (ky && dl0p>dl0c){
      *l0=dl0p;/*1st top/bottom plane intersection happens after 1st cylinder intersect*/
      stat|= plane_stat & ENTER_MASK;
    } else
      *l0=dl0c;
    if(ky && dl1p<dl1c){
      *l1=dl1p;/*2nd top/bottom plane intersection happens before 2nd cylinder intersect*/
      stat|= plane_stat & EXIT_MASK;
    }else
      *l1=dl1c;
  }
  return stat;
} /* cylinder_intersect */

/*******************************************************************************
 * sphere_intersect: Calculate intersection between a line and a sphere.
 * returns 0 when no intersection is found
 *      or 1 in case of intersection with resulting lengths l0 and l1 
 *******************************************************************************/
int
sphere_intersect(double *l0, double *l1, double x, double y, double z,
                 double kx, double ky, double kz, double r)
{
  double B, C, D, k;

  k = kx*kx + ky*ky + kz*kz;
  B = (x*kx + y*ky + z*kz);
  C = x*x + y*y + z*z - r*r;
  D = B*B - k*C;
  if(D < 0)
    return 0;
  D = sqrt(D);
  *l0 = (-B - D) / sqrt(k);
  *l1 = (-B + D) / sqrt(k);
  return 1;
} /* sphere_intersect */

/******************************************************************************
 * elliosoid_intersect: Calculate intersection between a line and an ellipsoid.
 * They ellisoid is fixed by a set of half-axis (a,b,c) and a matrix Q, with the
 * columns of Q being the (orthogonal) vectors along which the half-axis lie.
 * This allows for complete freedom in orienting th eellipsoid.
 * returns 0 when no intersection is found
 *      or 1 when they are found with resulting lemngths l0 and l1.
 *****************************************************************************/
int
ellipsoid_intersect(double *l0, double *l1, double x, double y, double z,
    double kx, double ky, double kz, double a, double b, double c,
    Rotation Q)
{
  Rotation A,Gamma,Q_t,Tmp;
  double u,v,w;

  Gamma[0][0]=Gamma[0][1]=Gamma[0][2]=0;
  Gamma[1][1]=Gamma[1][0]=Gamma[1][2]=0;
  Gamma[2][2]=Gamma[2][0]=Gamma[2][1]=0;
  /*now set diagonal to ellipsoid half axis if non-zero.
   * This way a zero value mean the sllipsoid extends infinitely along that axis,
   * which is useful for objects only curved in one direction*/ 
  if (a!=0){
    Gamma[0][0]=1/(a*a);
  }
  if (b!=0){
    Gamma[1][1]=1/(b*b);
  }
  if (c!=0){
    Gamma[2][2]=1/(c*c);
  }

  if (Q!=NULL){
    rot_transpose(Q,Q_t);
    rot_mul(Gamma,Q_t,Tmp);
    rot_mul(Q,Tmp,A);
  }else{
    rot_copy(A,Gamma);
  }

  /*to get the solutions as lengths in m use unit vector along k*/
  double ex,ey,ez,k;
  k=sqrt(kx*kx+ky*ky+kz*kz);
  ex=kx/k;
  ey=ky/k;
  ez=kz/k;

  u=ex*(A[0][0]*ex + A[1][0]*ey + A[2][0]*ez) + ey*( A[0][1]*ex + A[1][1]*ey + A[2][1]*ez) + ez*(A[0][2]*ex + A[1][2]*ey + A[2][2]*ez);
  v=x *(A[0][0]*ex + A[1][0]*ey + A[2][0]*ez) + ex*(A[0][0]*x + A[1][0]*y + A[2][0]*z) +
    y *(A[0][1]*ex + A[1][1]*ey + A[2][1]*ez) + ey*(A[0][1]*x + A[1][1]*y + A[2][1]*z) +
    z *(A[0][2]*ex + A[1][2]*ey + A[2][2]*ez) + ez*(A[0][2]*x + A[1][2]*y + A[2][2]*z);
  w=x*(A[0][0]*x + A[1][0]*y + A[2][0]*z) + y*(A[0][1]*x + A[1][1]*y + A[2][1]*z) + z*(A[0][2]*x + A[1][2]*y + A[2][2]*z);

  double D=v*v-4*u*w+4*u;
  if (D<0) return 0;

  D=sqrt(D);

  *l0=(-v-D) / (2*u);
  *l1=(-v+D) / (2*u);
  return 1;
}


/*******************************************************************************
 * plane_intersect: Calculate intersection between a plane (with normal n including the point w)
 * and a line thourhg x along the direction k.
 * returns 0 when no intersection is found (i.e. line is parallel to the plane)
 * returns 1 or -1 when intersection length is positive and negative, respectively
 *******************************************************************************/
int
plane_intersect(double *l, double x, double y, double z,
                 double kx, double ky, double kz, double nx, double ny, double nz, double wx, double wy, double wz)
{
  double s,k2;
  k2=scalar_prod(kx,ky,kz,kx,ky,kz);
  s=scalar_prod(kx,ky,kz,nx,ny,nz);
  if (k2<FLT_EPSILON || fabs(s)<FLT_EPSILON) return 0;
  *l = - sqrt(k2)*scalar_prod(nx,ny,nz,x-wx,y-wy,z-wz)/s;
  if (*l<0) return -1;
  else return 1;
} /* plane_intersect */

#endif /* !MCXTRACE_H */
/* End of file "mcxtrace-r.c". */

#line 6749 "Test_src_gaussian.c"
#ifdef MC_TRACE_ENABLED
int mctraceenabled = 1;
#else
int mctraceenabled = 0;
#endif
#define MCSTAS "/usr/local/lib/mcxtrace-x.y.z/"
int mcdefaultmain = 1;
char mcinstrument_name[] = "Test_src_gaussian";
char mcinstrument_source[] = "Test_src_gaussian.instr";
int main(int argc, char *argv[]){return mccode_main(argc, argv);}
void mcinit(void);
void mcraytrace(void);
void mcsave(FILE *);
void mcfinally(void);
void mcdisplay(void);

/* Shared user declarations for all components 'Source_gaussian'. */
#line 52 "/usr/local/lib/mcxtrace-x.y.z/sources/Source_gaussian.comp"
/*******************************************************************************
*
* McStas, neutron ray-tracing package
*         Copyright 1997-2002, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* Library: share/read_table-lib.h
*
* %Identification
* Written by: EF
* Date: Aug 28, 2002
* Origin: ILL
* Release: McStas 1.6
* Version: $Revision: 1.21 $
*
* This file is to be imported by components that may read data from table files
* It handles some shared functions.
*
* This library may be used directly as an external library. It has no dependency
*
* Usage: within SHARE
* %include "read_table-lib"
*
*******************************************************************************/

#ifndef READ_TABLE_LIB_H
#define READ_TABLE_LIB_H "$Revision: 1.21 $"

#define READ_TABLE_STEPTOL  0.04 /* tolerancy for constant step approx */

#ifndef MC_PATHSEP_C
#ifdef WIN32
#define MC_PATHSEP_C '\\'
#define MC_PATHSEP_S "\\"
#else  /* !WIN32 */
#ifdef MAC
#define MC_PATHSEP_C ':'
#define MC_PATHSEP_S ":"
#else  /* !MAC */
#define MC_PATHSEP_C '/'
#define MC_PATHSEP_S "/"
#endif /* !MAC */
#endif /* !WIN32 */
#endif /* !MC_PATHSEP_C */

#ifndef MCSTAS
#ifdef WIN32
#define MCSTAS "C:\\mcstas\\lib"
#else  /* !WIN32 */
#ifdef MAC
#define MCSTAS ":mcstas:lib" /* ToDo: What to put here? */
#else  /* !MAC */
#define MCSTAS "/usr/local/lib/mcstas"
#endif /* !MAC */
#endif /* !WIN32 */
#endif /* !MCSTAS */

#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>

  typedef struct struct_table
  {
    char    filename[256];
    long    filesize;
    char   *header;  /* text header, e.g. comments */
    double *data;    /* vector { x[0], y[0], ... x[n-1], y[n-1]... } */
    double  min_x;   /* min value of first column */
    double  max_x;   /* max value of first column */
    double  step_x;  /* minimal step value of first column */
    long    rows;    /* number of rows in matrix block */
    long    columns; /* number of columns in matrix block */

    long    begin;   /* start fseek index of block */
    long    end;     /* stop  fseek index of block */
    long    block_number;  /* block index. 0 is catenation of all */
    long    array_length;  /* number of elements in the t_Table array */
    char    monotonic;     /* true when 1st column/vector data is monotonic */
    char    constantstep;  /* true when 1st column/vector data has constant step */
    char    method[32];    /* interpolation method: nearest, linear */
  } t_Table;

/* read_table-lib function prototypes */
/* ========================================================================= */

/* 'public' functions */
long     Table_Read              (t_Table *Table, char *File, long block_number);
long     Table_Read_Offset       (t_Table *Table, char *File, long block_number,
                                  long *offset, long max_lines);
long     Table_Read_Offset_Binary(t_Table *Table, char *File, char *Type,
                                  long *Offset, long Rows, long Columns);
long     Table_Rebin(t_Table *Table); /* rebin table with regular 1st column and interpolate all columns 2:end */
long     Table_Info (t_Table Table);
double   Table_Index(t_Table Table,   long i, long j); /* get indexed value */
double   Table_Value(t_Table Table, double X, long j); /* search X in 1st column and return interpolated value in j-column */
t_Table *Table_Read_Array(char *File, long *blocks);
void     Table_Free_Array(t_Table *Table);
long     Table_Info_Array(t_Table *Table);
int      Table_SetElement(t_Table *Table, long i, long j, double value);
long     Table_Init(t_Table *Table, long rows, long columns); /* create a Table */
double   Table_Value2d(t_Table Table, double X, double Y);    /* same as Table_Index with non-integer indices and 2d interpolation */
MCDETECTOR Table_Write(t_Table Table, char*file, char*xl, char*yl, 
           double x1, double x2, double y1, double y2); /* write Table to disk */

#define Table_ParseHeader(header, ...) \
  Table_ParseHeader_backend(header,__VA_ARGS__,NULL);

char **Table_ParseHeader_backend(char *header, ...);

/* private functions */
void Table_Free(t_Table *Table);
long Table_Read_Handle(t_Table *Table, FILE *fid, long block_number, long max_lines, char *name);
static void Table_Stat(t_Table *Table);
double Table_Interp1d(double x, double x1, double y1, double x2, double y2);
double Table_Interp1d_nearest(double x, double x1, double y1, double x2, double y2);
double Table_Interp2d(double x, double y, double x1, double y1, double x2, double y2,
  double z11, double z12, double z21, double z22);

#endif

/* end of read_table-lib.h */
/*******************************************************************************
*
* McStas, neutron ray-tracing package
*         Copyright (C) 1997-2009, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* Library: share/read_table-lib.c
*
* %Identification
* Written by: EF
* Date: Aug 28, 2002
* Origin: ILL
* Release: McStas CVS_090504
* Version: $Revision: 1.42 $
*
* This file is to be imported by components that may read data from table files
* It handles some shared functions. Embedded within instrument in runtime mode.
*
* Usage: within SHARE
* %include "read_table-lib"
*
*******************************************************************************/

#ifndef READ_TABLE_LIB_H
#include "read_table-lib.h"
#endif

/*******************************************************************************
* long Read_Table(t_Table *Table, char *name, int block_number)
*   ACTION: read a single Table from a text file
*   input   Table: pointer to a t_Table structure
*           name:  file name from which table should be extracted
*           block_number: if the file does contain more than one
*                 data block, then indicates which one to get (from index 1)
*                 a 0 value means append/catenate all
*   return  initialized single Table t_Table structure containing data, header, ...
*           number of read elements (-1: error, 0:header only)
* The routine stores any line starting with '#', '%' and ';' into the header
* File is opened, read and closed
* Other lines are interpreted as numerical data, and stored.
* Data block should be a rectangular matrix or vector.
* Data block may be rebined with Table_Rebin (also sort in ascending order)
*******************************************************************************/
  long Table_Read(t_Table *Table, char *File, long block_number)
  { /* reads all or a single data block from 'file' and returns a Table structure  */
    return(Table_Read_Offset(Table, File, block_number, NULL, 0));
  } /* end Table_Read */

/*******************************************************************************
* long Table_Read_Offset(t_Table *Table, char *name, int block_number, long *offset
*                        long max_rows)
*   ACTION: read a single Table from a text file, starting at offset
*     Same as Table_Read(..) except:
*   input   offset:    pointer to an offset (*offset should be 0 at start)
*           max_rows: max number of data rows to read from file (0 means all)
*   return  initialized single Table t_Table structure containing data, header, ...
*           number of read elements (-1: error, 0:header only)
*           updated *offset position (where end of reading occured)
*******************************************************************************/
  long Table_Read_Offset(t_Table *Table, char *File,
                         long block_number, long *offset,
                         long max_rows)
  { /* reads all/a data block in 'file' and returns a Table structure  */
    FILE *hfile;
    long  nelements;
    long  begin;
    long  filesize=0;
    char  name[256];
    char  path[1024];
    struct stat stfile;

    if (!Table) return(-1);
    Table_Init(Table, 0, 0);
    if (!File || File[0]=='\0')  return(-1);
    if (!strcmp(File,"NULL") || !strcmp(File,"0"))  return(-1);
    strncpy(path, File, 1024);
    hfile = fopen(path, "r");
    if(!hfile)
    {
      char dir[1024];

      if (!hfile) /* search in instrument location */
      {
        char *path_pos   = NULL;
        /* extract path: searches for last file separator */
        path_pos    = strrchr(mcinstrument_source, MC_PATHSEP_C);  /* last PATHSEP */
        if (path_pos) {
          long path_length = path_pos +1 - mcinstrument_source;  /* from start to path+sep */
          if (path_length) {
            strncpy(dir, mcinstrument_source, path_length);
            sprintf(path, "%s%c%s", dir, MC_PATHSEP_C, File);
            hfile = fopen(path, "r");
          }
        }
      }
      if (!hfile) /* search in HOME */
      {
        strcpy(dir, getenv("HOME") ? getenv("HOME") : ".");
        sprintf(path, "%s%c%s", dir, MC_PATHSEP_C, File);
        hfile = fopen(path, "r");
      }
      if (!hfile) /* search in MCSTAS data */
      {
        strcpy(dir, getenv(FLAVOR_UPPER) ? getenv(FLAVOR_UPPER) : MCSTAS);
        sprintf(path, "%s%c%s%c%s", dir, MC_PATHSEP_C, "data", MC_PATHSEP_C, File);
        hfile = fopen(path, "r");
      }
      if (!hfile) /* search in MVCSTAS/contrib */
      {
        strcpy(dir, getenv(FLAVOR_UPPER) ? getenv(FLAVOR_UPPER) : MCSTAS);
        sprintf(path, "%s%c%s%c%s", dir, MC_PATHSEP_C, "contrib", MC_PATHSEP_C, File);
        hfile = fopen(path, "r");
      }
      if(!hfile)
      {
        fprintf(stderr, "Error: Could not open input file '%s' (Table_Read_Offset_Binary)\n", File);
        return (-1);
      } else if (!offset || (offset && !*offset))
        printf("Opening input file '%s' (Table_Read)\n", path);
    }
    stat(path,&stfile); filesize = stfile.st_size;
    if (offset && *offset) fseek(hfile, *offset, SEEK_SET);
    begin     = ftell(hfile);
    if (offset && *offset) sprintf(name, "%s@%li", File, *offset);
    else                   strncpy(name, File, 128);
    nelements = Table_Read_Handle(Table, hfile, block_number, max_rows, name);
    Table->begin = begin;
    Table->end   = ftell(hfile);
    Table->filesize = (filesize>0 ? filesize : 0);
    Table_Stat(Table);
    if (offset) *offset=Table->end;
    fclose(hfile);
    return(nelements);

  } /* end Table_Read_Offset */

/*******************************************************************************
* long Table_Read_Offset_Binary(t_Table *Table, char *File, char *type,
*                               long *offset, long rows, long columns)
*   ACTION: read a single Table from a binary file, starting at offset
*     Same as Table_Read_Offset(..) except that it handles binary files.
*   input   type: may be "float"/NULL or "double"
*           offset: pointer to an offset (*offset should be 0 at start)
*           rows   : number of rows (0 means read all)
*           columns: number of columns
*   return  initialized single Table t_Table structure containing data, header, ...
*           number of read elements (-1: error, 0:header only)
*           updated *offset position (where end of reading occured)
*******************************************************************************/
  long Table_Read_Offset_Binary(t_Table *Table, char *File, char *type,
                                long *offset, long rows, long columns)
  { /* reads all/a data block in binary 'file' and returns a Table structure  */
    long    nelements, sizeofelement;
    long    filesize;
    FILE   *hfile;
    struct stat stfile;
    double *data;
    long    i;
    long    begin;

    if (!Table) return(-1);

    Table_Init(Table, 0, 0);
    if (!File || File[0]=='\0')  return(-1);
    if (!strcmp(File,"NULL") || !strcmp(File,"0"))  return(-1);

    hfile = fopen(File, "r");
    if(!hfile)
    {
      char path[1024];
      char dir[1024];

      if (!hfile) /* search in instrument location */
      {
        char *path_pos   = NULL;
        /* extract path: searches for last file separator */
        path_pos    = strrchr(mcinstrument_source, MC_PATHSEP_C);  /* last PATHSEP */
        if (path_pos) {
          long path_length = path_pos +1 - mcinstrument_source;  /* from start to path+sep */
          if (path_length) {
            strncpy(dir, mcinstrument_source, path_length);
            sprintf(path, "%s%c%s", dir, MC_PATHSEP_C, File);
            hfile = fopen(path, "r");
          }
        }
      }
      if (!hfile) /* search in HOME */
      {
        strcpy(dir, getenv("HOME") ? getenv("HOME") : ".");
        sprintf(path, "%s%c%s", dir, MC_PATHSEP_C, File);
        hfile = fopen(path, "r");
      }
      if (!hfile) /* search in MCSTAS data */
      {
        strcpy(dir, getenv(FLAVOR_UPPER) ? getenv(FLAVOR_UPPER) : MCSTAS);
        sprintf(path, "%s%c%s%c%s", dir, MC_PATHSEP_C, "data", MC_PATHSEP_C, File);
        hfile = fopen(path, "r");
      }
      if (!hfile) /* search in MVCSTAS/contrib */
      {
        strcpy(dir, getenv(FLAVOR_UPPER) ? getenv(FLAVOR_UPPER) : MCSTAS);
        sprintf(path, "%s%c%s%c%s", dir, MC_PATHSEP_C, "contrib", MC_PATHSEP_C, File);
        hfile = fopen(path, "r");
      }
      if(!hfile)
      {
        fprintf(stderr, "Error: Could not open input file '%s' (Table_Read_Offset_Binary)\n", File);
        return (-1);
      } else
        printf("Opening input file '%s' (Table_Read)\n", path);
    }
    stat(File,&stfile);
    filesize = stfile.st_size;
    Table->filesize=filesize;
    if (type && !strcmp(type,"double")) sizeofelement = sizeof(double);
    else  sizeofelement = sizeof(float);
    if (offset && *offset) fseek(hfile, *offset, SEEK_SET);
    begin     = ftell(hfile);
    if (rows && filesize > sizeofelement*columns*rows)
      nelements = columns*rows;
    else nelements = (long)(filesize/sizeofelement);
    if (!nelements || filesize <= *offset) return(0);
    data    = (double*)malloc(nelements*sizeofelement);
    if (!data) {
      fprintf(stderr,"Error: allocating %ld elements for %s file '%s'. Too big (Table_Read_Offset_Binary).\n", nelements, type, File);
      exit(-1);
    }
    nelements = fread(data, sizeofelement, nelements, hfile);

    if (!data || !nelements)
    {
      fprintf(stderr,"Error: reading %ld elements from %s file '%s' (Table_Read_Offset_Binary)\n", nelements, type, File);
      exit(-1);
    }
    Table->begin   = begin;
    Table->end     = ftell(hfile);
    if (offset) *offset=Table->end;
    fclose(hfile);
    data = (double*)realloc(data, (double)nelements*sizeofelement);
    /* copy file data into Table */
    if (type && !strcmp(type,"double")) Table->data = data;
    else {
      float  *s;
      double *dataf;
      s     = (float*)data;
      dataf = (double*)malloc(sizeof(double)*nelements);
      for (i=0; i<nelements; i++)
        dataf[i]=s[i];
      free(data);
      Table->data = dataf;
    }
    strcpy(Table->filename, File);
    Table->rows    = nelements/columns;
    Table->columns = columns;
    Table->array_length = 1;
    Table->block_number = 1;

    Table_Stat(Table);

    return(nelements);
  } /* end Table_Read_Offset_Binary */

/*******************************************************************************
* long Table_Read_Handle(t_Table *Table, FILE *fid, int block_number, long max_rows, char *name)
*   ACTION: read a single Table from a text file handle (private)
*   input   Table:pointer to a t_Table structure
*           fid:  pointer to FILE handle
*           block_number: if the file does contain more than one
*                 data block, then indicates which one to get (from index 1)
*                 a 0 value means append/catenate all
*           max_rows: if non 0, only reads that number of lines
*   return  initialized single Table t_Table structure containing data, header, ...
*           modified Table t_Table structure containing data, header, ...
*           number of read elements (-1: error, 0:header only)
* The routine stores any line starting with '#', '%' and ';' into the header
* Other lines are interpreted as numerical data, and stored.
* Data block should be a rectangular matrix or vector.
* Data block may be rebined with Table_Rebin (also sort in ascending order)
*******************************************************************************/
  long Table_Read_Handle(t_Table *Table, FILE *hfile,
                         long block_number, long max_rows, char *name)
  { /* reads all/a data block from 'file' handle and returns a Table structure  */
    double *Data;
    char *Header              = NULL;
    long  malloc_size         = CHAR_BUF_LENGTH;
    long  malloc_size_h       = 4096;
    long  Rows = 0,   Columns = 0;
    long  count_in_array      = 0;
    long  count_in_header     = 0;
    long  block_Current_index = 0;
    char  flag_End_row_loop   = 0;

    if (!Table) return(-1);
    Table_Init(Table, 0, 0);
    if (name && name[0]!='\0') strcpy(Table->filename, name);

    if(!hfile) {
       fprintf(stderr, "Error: File handle is NULL (Table_Read_Handle).\n");
       return (-1);
    }
    Header = (char*)  calloc(malloc_size_h, sizeof(char));
    Data   = (double*)calloc(malloc_size,   sizeof(double));
    if ((Header == NULL) || (Data == NULL)) {
       fprintf(stderr, "Error: Could not allocate Table and Header (Table_Read_Handle).\n");
       return (-1);
    }

    int flag_In_array = 0;
    do { /* while (!flag_End_row_loop) */
      char  line[1024*CHAR_BUF_LENGTH];
      long  back_pos=0;   /* ftell start of line */

      back_pos = ftell(hfile);
      if (fgets(line, 1024*CHAR_BUF_LENGTH, hfile) != NULL) { /* analyse line */
        /* first skip blank and tabulation characters */
        int i = strspn(line, " \t");

        /* handle comments: stored in header */
        if (NULL != strchr("#%;/", line[i]))
        { /* line is a comment */
          count_in_header += strlen(line);
          if (count_in_header >= malloc_size_h) {
            /* if succeed and in array : add (and realloc if necessary) */
            malloc_size_h = count_in_header+4096;
            Header        = (char*)realloc(Header, malloc_size_h*sizeof(char));
          }
          strncat(Header, line, 4096);
          flag_In_array=0;
          /* exit line and file if passed desired block */
          if (block_number > 0 && block_number == block_Current_index) {
            flag_End_row_loop = 1;
          }

          /* Continue with next line */
          continue;
        }

        /* get the number of columns splitting line with strtok */
        char  *lexeme;
        char  flag_End_Line = 0;
        long  block_Num_Columns = 0;
        const char seps[] = " ,;\t\n\r";

        lexeme = strtok(line, seps);
        while (!flag_End_Line) {
          if ((lexeme != NULL) && (lexeme[0] != '\0')) {
            /* reading line: the token is not empty */
            double X;
            if (sscanf(lexeme,"%lg",&X) == 1) {
              /* reading line: the token is a number in the line */
              if (!flag_In_array) {
                /* reading num: not already in a block: starts a new data block */
                block_Current_index++;
                flag_In_array    = 1;
                block_Num_Columns= 0;
                if (block_number > 0) {
                  /* initialise a new data block */
                  Rows = 0;
                  count_in_array = 0;
                } /* else append */
              }
              /* reading num: all blocks or selected block */
              if (flag_In_array && (block_number == 0 ||
                  block_number == block_Current_index)) {
                /* starting block: already the desired number of rows ? */
                if (block_Num_Columns == 0 &&
                    max_rows > 0 && Rows >= max_rows) {
                  flag_End_Line      = 1;
                  flag_End_row_loop  = 1;
                  flag_In_array      = 0;
                  /* reposition to begining of line (ignore line) */
                  fseek(hfile, back_pos, SEEK_SET);
                } else { /* store into data array */
                  if (count_in_array >= malloc_size) {
                    /* realloc data buffer if necessary */
                    malloc_size = count_in_array+CHAR_BUF_LENGTH;
                    Data = (double*) realloc(Data, malloc_size*sizeof(double));
                    if (Data == NULL) {
                      fprintf(stderr, "Error: Can not re-allocate memory %li (Table_Read_Handle).\n",
                              malloc_size*sizeof(double));
                      return (-1);
                    }
                  }
                  if (0 == block_Num_Columns) Rows++;
                  Data[count_in_array] = X;
                  count_in_array++;
                  block_Num_Columns++;
                }
              } /* reading num: end if flag_In_array */
            } /* end reading num: end if sscanf lexeme -> numerical */
            else {
              /* reading line: the token is not numerical in that line. end block */
              if (block_Current_index == block_number) {
                flag_End_Line = 1;
                flag_End_row_loop = 1;
              } else {
                flag_In_array = 0;
                flag_End_Line = 1;
              }
            }
          }
          else {
            /* no more tokens in line */
            flag_End_Line = 1;
            if (block_Num_Columns > 0) Columns = block_Num_Columns;
          }

          // parse next token
          lexeme = strtok(NULL, seps);

        } /* while (!flag_End_Line) */
      } /* end: if fgets */
      else flag_End_row_loop = 1; /* else fgets : end of file */

    } while (!flag_End_row_loop); /* end while flag_End_row_loop */

    Table->block_number = block_number;
    Table->array_length = 1;

    // shrink header to actual size (plus terminating 0-byte)
    if (count_in_header) {
      Header = (char*)realloc(Header, count_in_header*sizeof(char) + 1);
    }
    Table->header = Header;

    if (count_in_array*Rows*Columns == 0)
    {
      Table->rows         = 0;
      Table->columns      = 0;
      free(Data);
      return (0);
    }
    if (Rows * Columns != count_in_array)
    {
      fprintf(stderr, "Warning: Read_Table :%s %s Data has %li values that should be %li x %li\n",
        (Table->filename ? Table->filename : ""),
        (!block_number ? " catenated" : ""),
        count_in_array, Rows, Columns);
      Columns = count_in_array; Rows = 1;
    }
    Data     = (double*)realloc(Data, count_in_array*sizeof(double));
    Table->data         = Data;
    Table->rows         = Rows;
    Table->columns      = Columns;

    return (count_in_array);

  } /* end Table_Read_Handle */

/*******************************************************************************
* long Table_Rebin(t_Table *Table)
*   ACTION: rebin a single Table, sorting 1st column in ascending order
*   input   Table: single table containing data.
*                  The data block is reallocated in this process
*   return  updated Table with increasing, evenly spaced first column (index 0)
*           number of data elements (-1: error, 0:empty data)
*******************************************************************************/
  long Table_Rebin(t_Table *Table)
  {
    double new_step=0;
    long   i;
    /* performs linear interpolation on X axis (0-th column) */

    if (!Table) return(-1);
    if (!Table->data
    || Table->rows*Table->columns == 0 || !Table->step_x)
      return(0);
    Table_Stat(Table); /* recompute statitstics and minimal step */
    new_step = Table->step_x; /* minimal step in 1st column */

    if (!(Table->constantstep)) /* not already evenly spaced */
    {
      long Length_Table;
      double *New_Table;

      Length_Table = ceil(fabs(Table->max_x - Table->min_x)/new_step);
      New_Table    = (double*)malloc(Length_Table*Table->columns*sizeof(double));

      for (i=0; i < Length_Table; i++)
      {
        long   j;
        double X;
        X = Table->min_x + i*new_step;
        New_Table[i*Table->columns] = X;
        for (j=1; j < Table->columns; j++)
          New_Table[i*Table->columns+j]
                = Table_Value(*Table, X, j);
      } /* end for i */

      Table->rows = Length_Table;
      Table->step_x = new_step;
      free(Table->data);
      Table->data = New_Table;
    } /* end else (!constantstep) */
    return (Table->rows*Table->columns);
  } /* end Table_Rebin */

/*******************************************************************************
* double Table_Index(t_Table Table, long i, long j)
*   ACTION: read an element [i,j] of a single Table
*   input   Table: table containing data
*           i : index of row      (0:Rows-1)
*           j : index of column   (0:Columns-1)
*   return  Value = data[i][j]
* Returns Value from the i-th row, j-th column of Table
* Tests are performed on indexes i,j to avoid errors
*******************************************************************************/

#ifndef MIN
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#endif

double Table_Index(t_Table Table, long i, long j)
{
  long AbsIndex;

  if (Table.rows == 1 || Table.columns == 1) {
    /* vector */
    j = MIN(MAX(0, i+j), Table.columns*Table.rows - 1);
    i = 0;
  } else {
    /* matrix */
    i = MIN(MAX(0, i), Table.rows - 1);
    j = MIN(MAX(0, j), Table.columns - 1);
  }

  /* handle vectors specifically */
  AbsIndex = i*(Table.columns)+j;

  if (Table.data != NULL)
    return (Table.data[AbsIndex]);
  else
    return 0;
} /* end Table_Index */

/*******************************************************************************
* void Table_SetElement(t_Table *Table, long i, long j, double value)
*   ACTION: set an element [i,j] of a single Table
*   input   Table: table containing data
*           i : index of row      (0:Rows-1)
*           j : index of column   (0:Columns-1)
*           value = data[i][j]
* Returns 0 in case of error
* Tests are performed on indexes i,j to avoid errors
*******************************************************************************/
int Table_SetElement(t_Table *Table, long i, long j,
                     double value)
{
  long AbsIndex;

  if (Table->rows == 1 || Table->columns == 1) {
    /* vector */
    j = MIN(MAX(0, i+j), Table->columns*Table->rows - 1); i=0;
  } else {
    /* matrix */
    i = MIN(MAX(0, i), Table->rows - 1);
    j = MIN(MAX(0, j), Table->columns - 1);
  }

  AbsIndex = i*(Table->columns)+j;
  if (Table->data != NULL) {
    Table->data[AbsIndex] = value;
    return 1;
  }

  return 0;
} /* end Table_SetElement */

/*******************************************************************************
* double Table_Value(t_Table Table, double X, long j)
*   ACTION: read column [j] of a single Table at row which 1st column is X
*   input   Table: table containing data.
*           X : data value in the first column (index 0)
*           j : index of column from which is extracted the Value (0:Columns-1)
*   return  Value = data[index for X][j] with linear interpolation
* Returns Value from the j-th column of Table corresponding to the
* X value for the 1st column (index 0)
* Tests are performed (within Table_Index) on indexes i,j to avoid errors
* NOTE: data should rather be monotonic, and evenly sampled.
*******************************************************************************/
double Table_Value(t_Table Table, double X, long j)
{
  long   Index = -1;
  double X1=0, Y1=0, X2=0, Y2=0;
  double ret=0;

  if (X > Table.max_x) return Table_Index(Table,Table.rows-1  ,j);
  if (X < Table.min_x) return Table_Index(Table,0  ,j);

  // Use constant-time lookup when possible
  if(Table.constantstep) {
    Index = (long)floor(
              (X - Table.min_x) / (Table.max_x - Table.min_x) * Table.rows);
    X1 = Table_Index(Table,Index  ,0);
    X2 = Table_Index(Table,Index+1,0);
  }
  // Use binary search on large, monotonic tables
  else if(Table.monotonic && Table.rows > 100) {
    long left = Table.min_x;
    long right = Table.max_x;

    while (!((X1 <= X) && (X < X2)) && (right - left > 1)) {
      Index = (left + right) / 2;

      X1 = Table_Index(Table, Index-1, 0);
      X2 = Table_Index(Table, Index,   0);

      if (X < X1) {
        right = Index;
      } else {
        left  = Index;
      }
    }
  }

  // Fall back to linear search, if no-one else has set X1, X2 correctly
  if (!((X1 <= X) && (X < X2))) {
    /* look for index surrounding X in the table -> Index */
    for (Index=1; Index < Table.rows-1; Index++) {
        X1 = Table_Index(Table, Index-1,0);
        X2 = Table_Index(Table, Index  ,0);
        if ((X1 <= X) && (X < X2)) break;
      } /* end for Index */
  }

  Y1 = Table_Index(Table,Index-1,j);
  Y2 = Table_Index(Table,Index  ,j);

  if (!strcmp(Table.method,"linear")) {
    ret = Table_Interp1d(X, X1,Y1, X2,Y2);
  }
  else if (!strcmp(Table.method,"nearest")) {
    ret = Table_Interp1d_nearest(X, X1,Y1, X2,Y2);
  }

  return ret;
} /* end Table_Value */

/*******************************************************************************
* double Table_Value2d(t_Table Table, double X, double Y)
*   ACTION: read element [X,Y] of a matrix Table
*   input   Table: table containing data.
*           X : row index, may be non integer
*           Y : column index, may be non integer
*   return  Value = data[index X][index Y] with bi-linear interpolation
* Returns Value for the indices [X,Y]
* Tests are performed (within Table_Index) on indexes i,j to avoid errors
* NOTE: data should rather be monotonic, and evenly sampled.
*******************************************************************************/
  double Table_Value2d(t_Table Table, double X, double Y)
  {
    long   x1,x2,y1,y2;
    double z11,z12,z21,z22;
    double ret=0;

    x1 = (long)floor(X);
    y1 = (long)floor(Y);

    if (x1 > Table.rows-1 || x1 < 0) {
      x2 = x1;
    } else {
      x2 = x1 + 1;
    }

    if (y1 > Table.columns-1 || y1 < 0) {
      y2 = y1;
    } else {
      y2 = y1 + 1;
    }

    z11 = Table_Index(Table, x1, y1);

    if (y2 != y1) z12=Table_Index(Table, x1, y2); else z12 = z11;
    if (x2 != x1) z21=Table_Index(Table, x2, y1); else z21 = z11;
    if (y2 != y1) z22=Table_Index(Table, x2, y2); else z22 = z21;

    if (!strcmp(Table.method,"linear"))
      ret = Table_Interp2d(X,Y, x1,y1,x2,y2, z11,z12,z21,z22);
    else {
      if (fabs(X-x1) < fabs(X-x2)) {
        if (fabs(Y-y1) < fabs(Y-y2)) ret = z11; else ret = z12;
      } else {
        if (fabs(Y-y1) < fabs(Y-y2)) ret = z21; else ret = z22;
      }
    }
    return ret;
  } /* end Table_Value2d */


/*******************************************************************************
* void Table_Free(t_Table *Table)
*   ACTION: free a single Table
*   return: empty Table
*******************************************************************************/
  void Table_Free(t_Table *Table)
  {
    if (!Table) return;
    if (Table->data   != NULL) free(Table->data);
    if (Table->header != NULL) free(Table->header);
    Table->data   = NULL;
    Table->header = NULL;
  } /* end Table_Free */

/******************************************************************************
* void Table_Info(t_Table Table)
*    ACTION: print informations about a single Table
*******************************************************************************/
  long Table_Info(t_Table Table)
  {
    char buffer[256];
    long ret=0;

    if (!Table.block_number) strcpy(buffer, "catenated");
    else sprintf(buffer, "block %li", Table.block_number);
    printf("Table from file '%s' (%s)",
      Table.filename && strlen(Table.filename) ? Table.filename : "", buffer);
    if ((Table.data != NULL) && (Table.rows*Table.columns))
    {
      printf(" is %li x %li ", Table.rows, Table.columns);
      if (Table.rows*Table.columns > 1)
        printf("(x=%g:%g)", Table.min_x, Table.max_x);
      else printf("(x=%g) ", Table.min_x);
      ret = Table.rows*Table.columns;
      if (Table.monotonic)    printf(", monotonic");
      if (Table.constantstep) printf(", constant step");
      printf(". interpolation: %s\n", Table.method);
    }
    else printf(" is empty.\n");

    if (Table.header && strlen(Table.header)) {
      char *header;
      int  i;
      header = malloc(80);
      if (!header) return(ret);
      for (i=0; i<80; header[i++]=0);
      strncpy(header, Table.header, 75);
      if (strlen(Table.header) > 75) {
        strcat( header, " ...");
      }
      for (i=0; i<strlen(header); i++)
        if (header[i] == '\n' || header[i] == '\r') header[i] = ';';
      printf("  '%s'\n", header);
      free(header);
    }
    return(ret);
  } /* end Table_Info */

/******************************************************************************
* long Table_Init(t_Table *Table, m, n)
*   ACTION: initialise a Table to empty m by n table
*   return: empty Table
******************************************************************************/
long Table_Init(t_Table *Table, long rows, long columns)
{
  double *data=NULL;
  long   i;

  if (!Table) return(0);

  Table->header  = NULL;
  Table->filename[0]= '\0';
  Table->filesize= 0;
  Table->min_x   = 0;
  Table->max_x   = 0;
  Table->step_x  = 0;
  Table->block_number = 0;
  Table->array_length = 0;
  Table->monotonic    = 0;
  Table->constantstep = 0;
  Table->begin   = 0;
  Table->end     = 0;
  strcpy(Table->method,"linear");

  if (rows*columns >= 1) {
    data    = (double*)malloc(rows*columns*sizeof(double));
    if (data) for (i=0; i < rows*columns; data[i++]=0);
    else {
      fprintf(stderr,"Error: allocating %ld double elements."
                     "Too big (Table_Init).\n", rows*columns);
      rows = columns = 0;
    }
  }
  Table->rows    = (rows >= 1 ? rows : 0);
  Table->columns = (columns >= 1 ? columns : 0);
  Table->data    = data;
  return(Table->rows*Table->columns);
} /* end Table_Init */

/******************************************************************************
* long Table_Write(t_Table Table, char *file, x1,x2, y1,y2)
*   ACTION: write a Table to disk (ascii).
*     when x1=x2=0 or y1=y2=0, the table default limits are used.
*   return: 0=all is fine, non-0: error
*******************************************************************************/
MCDETECTOR Table_Write(t_Table Table, char *file, char *xl, char *yl, 
  double x1, double x2, double y1, double y2)
{
  long    i =0;
  MCDETECTOR detector;

  if ((Table.data == NULL) && (Table.rows*Table.columns)) {
    detector.m = 0;
    return(detector); /* Table is empty - nothing to do */
  }
  if (!x1 && !x2) {
    x1 = Table.min_x;
    x2 = Table.max_x;
  }
  if (!y1 && !y2) {
    y1 = 1;
    y2 = Table.columns;
  }

  /* transfer content of the Table into a 2D detector */
  Coords coords = { 0, 0, 0};

  if (Table.rows == 1 || Table.columns == 1) {
    detector = mcdetector_out_1D(Table.filename,
                      xl ? xl : "", yl ? yl : "",
                      "x", x1, x2,
                      Table.rows * Table.columns,
                      NULL, Table.data, NULL,
                      file, file, coords);
  } else {
    detector = mcdetector_out_2D(Table.filename,
                      xl ? xl : "", yl ? yl : "",
                      x1, x2, y1, y2,
                      Table.rows, Table.columns,
                      NULL, Table.data, NULL,
                      file, file, coords);
  }
  return(detector);
}

/******************************************************************************
* void Table_Stat(t_Table *Table)
*   ACTION: computes min/max/mean step of 1st column for a single table (private)
*   return: updated Table
*******************************************************************************/
  static void Table_Stat(t_Table *Table)
  {
    long   i;
    double max_x, min_x;
    double row=1;
    char   monotonic=1;
    char   constantstep=1;
    double step=0;
    double n;

    if (!Table) return;
    if (!Table->rows || !Table->columns) return;
    if (Table->rows == 1) row=0;
    max_x = -FLT_MAX;
    min_x =  FLT_MAX;
    n     = (row ? Table->rows : Table->columns);
    /* get min and max of first column/vector */
    for (i=0; i < n; i++)
    {
      double X;
      X = (row ? Table_Index(*Table,i  ,0)
                               : Table_Index(*Table,0, i));
      if (X < min_x) min_x = X;
      if (X > max_x) max_x = X;
    } /* for */
    /* test for monotonicity and constant step if the table is an XY or single vector */
    if (n > 1) {
      /* mean step */
      step = (max_x - min_x)/(n-1);
      /* now test if table is monotonic on first column, and get minimal step size */
      for (i=0; i < n-1; i++) {
        double X, diff;;
        X    = (row ? Table_Index(*Table,i  ,0)
                    : Table_Index(*Table,0,  i));
        diff = (row ? Table_Index(*Table,i+1,0)
                    : Table_Index(*Table,0,  i+1)) - X;
        if (fabs(diff) < fabs(step)) step = diff;
        /* change sign ? */
        if ((max_x - min_x)*diff < 0 && monotonic)
          monotonic = 0;
      } /* end for */
      /* now test if steps are constant within READ_TABLE_STEPTOL */
      if(!step){
        /*means there's a disconitnuity -> not constantstep*/
        constantstep=0;
      }else if (monotonic) {
        for (i=0; i < n-1; i++) {
          double X, diff;
          X    = (row ? Table_Index(*Table,i  ,0)
              : Table_Index(*Table,0,  i));
          diff = (row ? Table_Index(*Table,i+1,0)
              : Table_Index(*Table,0,  i+1)) - X;
          if ( fabs(step)*(1+READ_TABLE_STEPTOL) < fabs(diff) ||
                fabs(diff) < fabs(step)*(1-READ_TABLE_STEPTOL) )
          { constantstep = 0; break; }
        }
      }
    }
    Table->step_x= step;
    Table->max_x = max_x;
    Table->min_x = min_x;
    Table->monotonic = monotonic;
    Table->constantstep = constantstep;
  } /* end Table_Stat */

/******************************************************************************
* t_Table *Table_Read_Array(char *File, long *blocks)
*   ACTION: read as many data blocks as available, iteratively from file
*   return: initialized t_Table array, last element is an empty Table.
*           the number of extracted blocks in non NULL pointer *blocks
*******************************************************************************/
  t_Table *Table_Read_Array(char *File, long *blocks)
  {
    t_Table *Table_Array=NULL;
    long offset=0;
    long block_number=0;
    long allocated=256;
    long nelements=1;

    /* fisrt allocate an initial empty t_Table array */
    Table_Array = (t_Table *)malloc(allocated*sizeof(t_Table));
    if (!Table_Array) {
      fprintf(stderr, "Error: Can not allocate memory %li (Table_Read_Array).\n",
         allocated*sizeof(t_Table));
      *blocks = 0;
      return (NULL);
    }

    while (nelements > 0)
    {
      t_Table Table;

      /* access file at offset and get following block */
      nelements = Table_Read_Offset(&Table, File, 1, &offset,0);
      /* if ok, set t_Table block number else exit loop */
      block_number++;
      Table.block_number = block_number;
      /* if t_Table array is not long enough, expand and realocate */
      if (block_number >= allocated-1) {
        allocated += 256;
        Table_Array = (t_Table *)realloc(Table_Array,
           allocated*sizeof(t_Table));
        if (!Table_Array) {
          fprintf(stderr, "Error: Can not re-allocate memory %li (Table_Read_Array).\n",
              allocated*sizeof(t_Table));
          *blocks = 0;
          return (NULL);
        }
      }
      /* store it into t_Table array */
      sprintf(Table.filename, "%s#%li", File, block_number-1);
      Table_Array[block_number-1] = Table;
      /* continues until we find an empty block */
    }
    /* send back number of extracted blocks */
    if (blocks) *blocks = block_number-1;

    /* now store total number of elements in Table array */
    for (offset=0; offset < block_number;
      Table_Array[offset++].array_length = block_number-1);

    return(Table_Array);
  } /* end Table_Read_Array */
/*******************************************************************************
* void Table_Free_Array(t_Table *Table)
*   ACTION: free a Table array
*******************************************************************************/
  void Table_Free_Array(t_Table *Table)
  {
    long index=0;
    if (!Table) return;
    do {
        if (Table[index].data || Table[index].header)
          Table_Free(&Table[index]);
        else index=-1;
    } while (index>= 0);
    free(Table);
  } /* end Table_Free_Array */

/******************************************************************************
* long Table_Info_Array(t_Table *Table)
*    ACTION: print informations about a Table array
*    return: number of elements in the Table array
*******************************************************************************/
  long Table_Info_Array(t_Table *Table)
  {
    long index=0;

    if (!Table) return(-1);
    while (index < Table[index].array_length
       && (Table[index].data || Table[index].header)
       && (Table[index].rows*Table[index].columns) ) {
      Table_Info(Table[index]);
      index++;
    }
    printf("This Table array contains %li elements\n", index);
    return(index);
  } /* end Table_Info_Array */

/******************************************************************************
* char **Table_ParseHeader(char *header, symbol1, symbol2, ..., NULL)
*    ACTION: search for char* symbols in header and return their value or NULL
*            Last argument MUST be NULL
*    return: array of char* with line following each symbol, or NULL if not found
*******************************************************************************/
#ifndef MyNL_ARGMAX
#define MyNL_ARGMAX 50
#endif

char **Table_ParseHeader_backend(char *header, ...){
  va_list ap;
  char exit_flag=0;
  int counter   =0;
  char **ret    =NULL;
  if (!header || header[0]=='\0') return(NULL);

  ret = (char**)calloc(MyNL_ARGMAX, sizeof(char*));
  if (!ret) {
    printf("Table_ParseHeader: Cannot allocate %i values array for Parser (Table_ParseHeader).\n",
      MyNL_ARGMAX);
    return(NULL);
  }
  for (counter=0; counter < MyNL_ARGMAX; ret[counter++] = NULL);
  counter=0;

  va_start(ap, header);
  while(!exit_flag && counter < MyNL_ARGMAX-1)
  {
    char *arg_char=NULL;
    char *pos     =NULL;
    /* get variable argument value as a char */
    arg_char = va_arg(ap, char *);
    if (!arg_char || arg_char[0]=='\0'){
      exit_flag = 1; break;
    }
    /* search for the symbol in the header */
    pos = strstr(header, arg_char);
    if (pos) {
      char *eol_pos;
      eol_pos = strchr(pos+strlen(arg_char), '\n');
      if (!eol_pos)
        eol_pos = strchr(pos+strlen(arg_char), '\r');
      if (!eol_pos)
        eol_pos = pos+strlen(pos)-1;
      ret[counter] = (char*)malloc(eol_pos - pos);
      if (!ret[counter]) {
        printf("Table_ParseHeader: Cannot allocate value[%i] array for Parser searching for %s (Table_ParseHeader).\n",
          counter, arg_char);
        exit_flag = 1; break;
      }
      strncpy(ret[counter], pos+strlen(arg_char), eol_pos - pos - strlen(arg_char));
      ret[counter][eol_pos - pos - strlen(arg_char)]='\0';
    }
    counter++;
  }
  va_end(ap);
  return(ret);
} /* Table_ParseHeader */

/******************************************************************************
* double Table_Interp1d(x, x1, y1, x2, y2)
*    ACTION: interpolates linearly at x between y1=f(x1) and y2=f(x2)
*    return: y=f(x) value
*******************************************************************************/
double Table_Interp1d(double x,
  double x1, double y1,
  double x2, double y2)
{
  double slope;
  if (x2 == x1) return (y1+y2)/2;
  if (y1 == y2) return  y1;
  slope = (y2 - y1)/(x2 - x1);
  return y1+slope*(x - x1);
} /* Table_Interp1d */

/******************************************************************************
* double Table_Interp1d_nearest(x, x1, y1, x2, y2)
*    ACTION: table lookup with nearest method at x between y1=f(x1) and y2=f(x2)
*    return: y=f(x) value
*******************************************************************************/
double Table_Interp1d_nearest(double x,
  double x1, double y1,
  double x2, double y2)
{
  if (fabs(x-x1) < fabs(x-x2)) return (y1);
  else return(y2);
} /* Table_Interp1d_nearest */

/******************************************************************************
* double Table_Interp2d(x,y, x1,y1, x2,y2, z11,z12,z21,z22)
*    ACTION: interpolates bi-linearly at (x,y) between z1=f(x1,y1) and z2=f(x2,y2)
*    return: z=f(x,y) value
*    x,y |   x1   x2
*    ----------------
*     y1 |   z11  z21
*     y2 |   z12  z22
*******************************************************************************/
double Table_Interp2d(double x, double y,
  double x1, double y1,
  double x2, double y2,
  double z11, double z12, double z21, double z22)
{
  double ratio_x, ratio_y;
  if (x2 == x1) return Table_Interp1d(y, y1,z11, y2,z12);
  if (y1 == y2) return Table_Interp1d(x, x1,z11, x2,z21);

  ratio_y = (y - y1)/(y2 - y1);
  ratio_x = (x - x1)/(x2 - x1);
  return (1-ratio_x)*(1-ratio_y)*z11 + ratio_x*(1-ratio_y)*z21
    + ratio_x*ratio_y*z22         + (1-ratio_x)*ratio_y*z12;
} /* Table_Interp2d */

/* end of read_table-lib.c */

  double Gauss2D(double sigmaX, double sigmaY, double x, double y, double A){
    double F;
    F=A*exp(-(((x*x)/(2.0*sigmaX*sigmaX))+((y*y)/(2.0*sigmaY*sigmaY))));
    return F;
  }
#line 8012 "Test_src_gaussian.c"

/* Shared user declarations for all components 'Source_gaussian2'. */
#line 56 "Source_gaussian2.comp"

#line 8017 "Test_src_gaussian.c"

/* Instrument parameters. */
MCNUM mcipXW;
MCNUM mcipYH;
MCNUM mcipDIST;
MCNUM mcipSRC;
MCNUM mcipE0;

#define mcNUMIPAR 5
int mcnumipar = 5;
struct mcinputtable_struct mcinputtable[mcNUMIPAR+1] = {
  "XW", &mcipXW, instr_type_double, "1e-2", 
  "YH", &mcipYH, instr_type_double, "1e-2", 
  "DIST", &mcipDIST, instr_type_double, "10", 
  "SRC", &mcipSRC, instr_type_double, "0", 
  "E0", &mcipE0, instr_type_double, "9", 
  NULL, NULL, instr_type_double, ""
};

/* User declarations from instrument definition. */
#define mccompcurname  Test_src_gaussian
#define mccompcurtype  INSTRUMENT
#define mccompcurindex 0
#define mcposaTest_src_gaussian coords_set(0,0,0)
#define XW mcipXW
#define YH mcipYH
#define DIST mcipDIST
#define SRC mcipSRC
#define E0 mcipE0
#undef E0
#undef SRC
#undef DIST
#undef YH
#undef XW
#undef mcposaTest_src_gaussian
#undef mccompcurindex
#undef mccompcurtype
#undef mccompcurname

/* xray state table at each component input (local coords) */
/* [x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p] */
MCNUM mccomp_storein[12*20];
/* Components position table (absolute and relative coords) */
Coords mccomp_posa[20];
Coords mccomp_posr[20];
/* Counter for each comp to check for inactive ones */
MCNUM  mcNCounter[20];
MCNUM  mcPCounter[20];
MCNUM  mcP2Counter[20];
#define mcNUMCOMP 19 /* number of components */
/* Counter for PROP ABSORB */
MCNUM  mcAbsorbProp[20];
/* Flag true when previous component acted on the xray (SCATTER) */
MCNUM mcScattered=0;
/* Declarations of component definition and setting parameters. */

/* Definition parameters for component 'Origin' [1]. */
#define mccOrigin_profile 0 /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'Origin' [1]. */
MCNUM mccOrigin_percent;
MCNUM mccOrigin_flag_save;
MCNUM mccOrigin_minutes;

/* Definition parameters for component 'src' [2]. */
#define mccsrc_spectrum_file NULL /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'src' [2]. */
MCNUM mccsrc_sig_x;
MCNUM mccsrc_sig_y;
MCNUM mccsrc_sigPr_x;
MCNUM mccsrc_sigPr_y;
MCNUM mccsrc_flux;
MCNUM mccsrc_dist;
MCNUM mccsrc_gauss;
MCNUM mccsrc_gamma;
MCNUM mccsrc_E0;
MCNUM mccsrc_dE;
MCNUM mccsrc_lambda0;
MCNUM mccsrc_dlambda;
MCNUM mccsrc_phase;

/* Definition parameters for component 'src2' [3]. */
#define mccsrc2_spectrum_file NULL /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'src2' [3]. */
MCNUM mccsrc2_sig_x;
MCNUM mccsrc2_sig_y;
MCNUM mccsrc2_sigPr_x;
MCNUM mccsrc2_sigPr_y;
MCNUM mccsrc2_flux;
MCNUM mccsrc2_brilliance;
MCNUM mccsrc2_dist;
MCNUM mccsrc2_gauss;
MCNUM mccsrc2_focus_xw;
MCNUM mccsrc2_focus_yh;
MCNUM mccsrc2_E0;
MCNUM mccsrc2_dE;
MCNUM mccsrc2_lambda0;
MCNUM mccsrc2_dlambda;
MCNUM mccsrc2_phase;

/* Definition parameters for component 'src3' [4]. */
#define mccsrc3_spectrum_file "characteristics.dc0" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'src3' [4]. */
MCNUM mccsrc3_sig_x;
MCNUM mccsrc3_sig_y;
MCNUM mccsrc3_sigPr_x;
MCNUM mccsrc3_sigPr_y;
MCNUM mccsrc3_flux;
MCNUM mccsrc3_brilliance;
MCNUM mccsrc3_dist;
MCNUM mccsrc3_gauss;
MCNUM mccsrc3_focus_xw;
MCNUM mccsrc3_focus_yh;
MCNUM mccsrc3_E0;
MCNUM mccsrc3_dE;
MCNUM mccsrc3_lambda0;
MCNUM mccsrc3_dlambda;
MCNUM mccsrc3_phase;

/* Definition parameters for component 'src4' [5]. */
#define mccsrc4_spectrum_file "pmu18p5_brilliance_spec_narrow2.dc0" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'src4' [5]. */
MCNUM mccsrc4_sig_x;
MCNUM mccsrc4_sig_y;
MCNUM mccsrc4_sigPr_x;
MCNUM mccsrc4_sigPr_y;
MCNUM mccsrc4_flux;
MCNUM mccsrc4_brilliance;
MCNUM mccsrc4_dist;
MCNUM mccsrc4_gauss;
MCNUM mccsrc4_focus_xw;
MCNUM mccsrc4_focus_yh;
MCNUM mccsrc4_E0;
MCNUM mccsrc4_dE;
MCNUM mccsrc4_lambda0;
MCNUM mccsrc4_dlambda;
MCNUM mccsrc4_phase;

/* Definition parameters for component 'emon00' [6]. */
#define mccemon00_nE 66
#define mccemon00_filename "emon00.dat" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'emon00' [6]. */
MCNUM mccemon00_xmin;
MCNUM mccemon00_xmax;
MCNUM mccemon00_ymin;
MCNUM mccemon00_ymax;
MCNUM mccemon00_xwidth;
MCNUM mccemon00_yheight;
MCNUM mccemon00_Emin;
MCNUM mccemon00_Emax;
MCNUM mccemon00_restore_xray;

/* Definition parameters for component 'emon02' [7]. */
#define mccemon02_nE 100
#define mccemon02_filename "emon02.dat" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'emon02' [7]. */
MCNUM mccemon02_xmin;
MCNUM mccemon02_xmax;
MCNUM mccemon02_ymin;
MCNUM mccemon02_ymax;
MCNUM mccemon02_xwidth;
MCNUM mccemon02_yheight;
MCNUM mccemon02_Emin;
MCNUM mccemon02_Emax;
MCNUM mccemon02_restore_xray;

/* Definition parameters for component 'emon01' [8]. */
#define mccemon01_nE 100
#define mccemon01_filename "emon01.dat" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'emon01' [8]. */
MCNUM mccemon01_xmin;
MCNUM mccemon01_xmax;
MCNUM mccemon01_ymin;
MCNUM mccemon01_ymax;
MCNUM mccemon01_xwidth;
MCNUM mccemon01_yheight;
MCNUM mccemon01_Emin;
MCNUM mccemon01_Emax;
MCNUM mccemon01_restore_xray;

/* Definition parameters for component 'emon0' [9]. */
#define mccemon0_nE 100
#define mccemon0_filename "emon0.dat" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'emon0' [9]. */
MCNUM mccemon0_xmin;
MCNUM mccemon0_xmax;
MCNUM mccemon0_ymin;
MCNUM mccemon0_ymax;
MCNUM mccemon0_xwidth;
MCNUM mccemon0_yheight;
MCNUM mccemon0_Emin;
MCNUM mccemon0_Emax;
MCNUM mccemon0_restore_xray;

/* Definition parameters for component 'lmon0' [10]. */
#define mcclmon0_nL 100
#define mcclmon0_filename "lmon0.dat" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'lmon0' [10]. */
MCNUM mcclmon0_xmin;
MCNUM mcclmon0_xmax;
MCNUM mcclmon0_ymin;
MCNUM mcclmon0_ymax;
MCNUM mcclmon0_xwidth;
MCNUM mcclmon0_yheight;
MCNUM mcclmon0_Lmin;
MCNUM mcclmon0_Lmax;
MCNUM mcclmon0_restore_xray;

/* Definition parameters for component 'psd0' [11]. */
#define mccpsd0_nx 90
#define mccpsd0_ny 90
#define mccpsd0_nr 0
#define mccpsd0_filename "psd0.dat" /* declared as a string. May produce warnings at compile */
#define mccpsd0_restore_xray 0
/* Setting parameters for component 'psd0' [11]. */
MCNUM mccpsd0_xmin;
MCNUM mccpsd0_xmax;
MCNUM mccpsd0_ymin;
MCNUM mccpsd0_ymax;
MCNUM mccpsd0_xwidth;
MCNUM mccpsd0_yheight;
MCNUM mccpsd0_radius;

/* Definition parameters for component 'emon1' [12]. */
#define mccemon1_nE 100
#define mccemon1_filename "emon1.dat" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'emon1' [12]. */
MCNUM mccemon1_xmin;
MCNUM mccemon1_xmax;
MCNUM mccemon1_ymin;
MCNUM mccemon1_ymax;
MCNUM mccemon1_xwidth;
MCNUM mccemon1_yheight;
MCNUM mccemon1_Emin;
MCNUM mccemon1_Emax;
MCNUM mccemon1_restore_xray;

/* Definition parameters for component 'lmon1' [13]. */
#define mcclmon1_nL 100
#define mcclmon1_filename "lmon1.dat" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'lmon1' [13]. */
MCNUM mcclmon1_xmin;
MCNUM mcclmon1_xmax;
MCNUM mcclmon1_ymin;
MCNUM mcclmon1_ymax;
MCNUM mcclmon1_xwidth;
MCNUM mcclmon1_yheight;
MCNUM mcclmon1_Lmin;
MCNUM mcclmon1_Lmax;
MCNUM mcclmon1_restore_xray;

/* Definition parameters for component 'psd1' [14]. */
#define mccpsd1_nx 90
#define mccpsd1_ny 90
#define mccpsd1_nr 0
#define mccpsd1_filename "psd1.dat" /* declared as a string. May produce warnings at compile */
#define mccpsd1_restore_xray 0
/* Setting parameters for component 'psd1' [14]. */
MCNUM mccpsd1_xmin;
MCNUM mccpsd1_xmax;
MCNUM mccpsd1_ymin;
MCNUM mccpsd1_ymax;
MCNUM mccpsd1_xwidth;
MCNUM mccpsd1_yheight;
MCNUM mccpsd1_radius;

/* Setting parameters for component 'slit' [15]. */
MCNUM mccslit_xmin;
MCNUM mccslit_xmax;
MCNUM mccslit_ymin;
MCNUM mccslit_ymax;
MCNUM mccslit_radius;
MCNUM mccslit_cut;
MCNUM mccslit_xwidth;
MCNUM mccslit_yheight;
MCNUM mccslit_dist;
MCNUM mccslit_focus_xw;
MCNUM mccslit_focus_yh;
MCNUM mccslit_focus_x0;
MCNUM mccslit_focus_y0;

/* Definition parameters for component 'emon2' [16]. */
#define mccemon2_nE 100
#define mccemon2_filename "emon2.dat" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'emon2' [16]. */
MCNUM mccemon2_xmin;
MCNUM mccemon2_xmax;
MCNUM mccemon2_ymin;
MCNUM mccemon2_ymax;
MCNUM mccemon2_xwidth;
MCNUM mccemon2_yheight;
MCNUM mccemon2_Emin;
MCNUM mccemon2_Emax;
MCNUM mccemon2_restore_xray;

/* Definition parameters for component 'lmon2' [17]. */
#define mcclmon2_nL 100
#define mcclmon2_filename "lmon2.dat" /* declared as a string. May produce warnings at compile */
/* Setting parameters for component 'lmon2' [17]. */
MCNUM mcclmon2_xmin;
MCNUM mcclmon2_xmax;
MCNUM mcclmon2_ymin;
MCNUM mcclmon2_ymax;
MCNUM mcclmon2_xwidth;
MCNUM mcclmon2_yheight;
MCNUM mcclmon2_Lmin;
MCNUM mcclmon2_Lmax;
MCNUM mcclmon2_restore_xray;

/* Definition parameters for component 'psd2' [18]. */
#define mccpsd2_nx 90
#define mccpsd2_ny 90
#define mccpsd2_nr 0
#define mccpsd2_filename "psd2.dat" /* declared as a string. May produce warnings at compile */
#define mccpsd2_restore_xray 0
/* Setting parameters for component 'psd2' [18]. */
MCNUM mccpsd2_xmin;
MCNUM mccpsd2_xmax;
MCNUM mccpsd2_ymin;
MCNUM mccpsd2_ymax;
MCNUM mccpsd2_xwidth;
MCNUM mccpsd2_yheight;
MCNUM mccpsd2_radius;

/* User component declarations. */

/* User declarations for component 'Origin' [1]. */
#define mccompcurname  Origin
#define mccompcurtype  Progress_bar
#define mccompcurindex 1
#define profile mccOrigin_profile
#define IntermediateCnts mccOrigin_IntermediateCnts
#define StartTime mccOrigin_StartTime
#define EndTime mccOrigin_EndTime
#define percent mccOrigin_percent
#define flag_save mccOrigin_flag_save
#define minutes mccOrigin_minutes
#line 50 "/usr/local/lib/mcxtrace-x.y.z/misc/Progress_bar.comp"
#ifndef PROGRESS_BAR
#define PROGRESS_BAR
#else
#error Only one Progress_bar component may be used in an instrument definition.
#endif

  double IntermediateCnts=0;
  time_t StartTime       =0;
  time_t EndTime         =0;
  time_t CurrentTime     =0;
#line 8365 "Test_src_gaussian.c"
#undef minutes
#undef flag_save
#undef percent
#undef EndTime
#undef StartTime
#undef IntermediateCnts
#undef profile
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'src' [2]. */
#define mccompcurname  src
#define mccompcurtype  Source_gaussian
#define mccompcurindex 2
#define spectrum_file mccsrc_spectrum_file
#define prms mccsrc_prms
#define sig_x mccsrc_sig_x
#define sig_y mccsrc_sig_y
#define sigPr_x mccsrc_sigPr_x
#define sigPr_y mccsrc_sigPr_y
#define flux mccsrc_flux
#define dist mccsrc_dist
#define gauss mccsrc_gauss
#define gamma mccsrc_gamma
#define E0 mccsrc_E0
#define dE mccsrc_dE
#define lambda0 mccsrc_lambda0
#define dlambda mccsrc_dlambda
#define phase mccsrc_phase
#line 62 "/usr/local/lib/mcxtrace-x.y.z/sources/Source_gaussian.comp"
  struct {
    double l0,dl;
    double pmul,pint;
    t_Table T;
  } prms;
#line 8402 "Test_src_gaussian.c"
#undef phase
#undef dlambda
#undef lambda0
#undef dE
#undef E0
#undef gamma
#undef gauss
#undef dist
#undef flux
#undef sigPr_y
#undef sigPr_x
#undef sig_y
#undef sig_x
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'src2' [3]. */
#define mccompcurname  src2
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 3
#define spectrum_file mccsrc2_spectrum_file
#define prms mccsrc2_prms
#define spX mccsrc2_spX
#define spY mccsrc2_spY
#define sig_x mccsrc2_sig_x
#define sig_y mccsrc2_sig_y
#define sigPr_x mccsrc2_sigPr_x
#define sigPr_y mccsrc2_sigPr_y
#define flux mccsrc2_flux
#define brilliance mccsrc2_brilliance
#define dist mccsrc2_dist
#define gauss mccsrc2_gauss
#define focus_xw mccsrc2_focus_xw
#define focus_yh mccsrc2_focus_yh
#define E0 mccsrc2_E0
#define dE mccsrc2_dE
#define lambda0 mccsrc2_lambda0
#define dlambda mccsrc2_dlambda
#define phase mccsrc2_phase
#line 62 "Source_gaussian2.comp"
  struct {
    double l0,dl;
    double pmul,pint,pnum;
    t_Table T;
  } prms;

  double spX,spY;
  int uniform_sampling;
#line 8454 "Test_src_gaussian.c"
#undef phase
#undef dlambda
#undef lambda0
#undef dE
#undef E0
#undef focus_yh
#undef focus_xw
#undef gauss
#undef dist
#undef brilliance
#undef flux
#undef sigPr_y
#undef sigPr_x
#undef sig_y
#undef sig_x
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'src3' [4]. */
#define mccompcurname  src3
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 4
#define spectrum_file mccsrc3_spectrum_file
#define prms mccsrc3_prms
#define spX mccsrc3_spX
#define spY mccsrc3_spY
#define sig_x mccsrc3_sig_x
#define sig_y mccsrc3_sig_y
#define sigPr_x mccsrc3_sigPr_x
#define sigPr_y mccsrc3_sigPr_y
#define flux mccsrc3_flux
#define brilliance mccsrc3_brilliance
#define dist mccsrc3_dist
#define gauss mccsrc3_gauss
#define focus_xw mccsrc3_focus_xw
#define focus_yh mccsrc3_focus_yh
#define E0 mccsrc3_E0
#define dE mccsrc3_dE
#define lambda0 mccsrc3_lambda0
#define dlambda mccsrc3_dlambda
#define phase mccsrc3_phase
#line 62 "Source_gaussian2.comp"
  struct {
    double l0,dl;
    double pmul,pint,pnum;
    t_Table T;
  } prms;

  double spX,spY;
  int uniform_sampling;
#line 8510 "Test_src_gaussian.c"
#undef phase
#undef dlambda
#undef lambda0
#undef dE
#undef E0
#undef focus_yh
#undef focus_xw
#undef gauss
#undef dist
#undef brilliance
#undef flux
#undef sigPr_y
#undef sigPr_x
#undef sig_y
#undef sig_x
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'src4' [5]. */
#define mccompcurname  src4
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 5
#define spectrum_file mccsrc4_spectrum_file
#define prms mccsrc4_prms
#define spX mccsrc4_spX
#define spY mccsrc4_spY
#define sig_x mccsrc4_sig_x
#define sig_y mccsrc4_sig_y
#define sigPr_x mccsrc4_sigPr_x
#define sigPr_y mccsrc4_sigPr_y
#define flux mccsrc4_flux
#define brilliance mccsrc4_brilliance
#define dist mccsrc4_dist
#define gauss mccsrc4_gauss
#define focus_xw mccsrc4_focus_xw
#define focus_yh mccsrc4_focus_yh
#define E0 mccsrc4_E0
#define dE mccsrc4_dE
#define lambda0 mccsrc4_lambda0
#define dlambda mccsrc4_dlambda
#define phase mccsrc4_phase
#line 62 "Source_gaussian2.comp"
  struct {
    double l0,dl;
    double pmul,pint,pnum;
    t_Table T;
  } prms;

  double spX,spY;
  int uniform_sampling;
#line 8566 "Test_src_gaussian.c"
#undef phase
#undef dlambda
#undef lambda0
#undef dE
#undef E0
#undef focus_yh
#undef focus_xw
#undef gauss
#undef dist
#undef brilliance
#undef flux
#undef sigPr_y
#undef sigPr_x
#undef sig_y
#undef sig_x
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'emon00' [6]. */
#define mccompcurname  emon00
#define mccompcurtype  E_monitor
#define mccompcurindex 6
#define nE mccemon00_nE
#define filename mccemon00_filename
#define E_N mccemon00_E_N
#define E_p mccemon00_E_p
#define E_p2 mccemon00_E_p2
#define xmin mccemon00_xmin
#define xmax mccemon00_xmax
#define ymin mccemon00_ymin
#define ymax mccemon00_ymax
#define xwidth mccemon00_xwidth
#define yheight mccemon00_yheight
#define Emin mccemon00_Emin
#define Emax mccemon00_Emax
#define restore_xray mccemon00_restore_xray
#line 60 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
    double E_N[nE];
    double E_p[nE], E_p2[nE];
#line 8611 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'emon02' [7]. */
#define mccompcurname  emon02
#define mccompcurtype  E_monitor
#define mccompcurindex 7
#define nE mccemon02_nE
#define filename mccemon02_filename
#define E_N mccemon02_E_N
#define E_p mccemon02_E_p
#define E_p2 mccemon02_E_p2
#define xmin mccemon02_xmin
#define xmax mccemon02_xmax
#define ymin mccemon02_ymin
#define ymax mccemon02_ymax
#define xwidth mccemon02_xwidth
#define yheight mccemon02_yheight
#define Emin mccemon02_Emin
#define Emax mccemon02_Emax
#define restore_xray mccemon02_restore_xray
#line 60 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
    double E_N[nE];
    double E_p[nE], E_p2[nE];
#line 8651 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'emon01' [8]. */
#define mccompcurname  emon01
#define mccompcurtype  E_monitor
#define mccompcurindex 8
#define nE mccemon01_nE
#define filename mccemon01_filename
#define E_N mccemon01_E_N
#define E_p mccemon01_E_p
#define E_p2 mccemon01_E_p2
#define xmin mccemon01_xmin
#define xmax mccemon01_xmax
#define ymin mccemon01_ymin
#define ymax mccemon01_ymax
#define xwidth mccemon01_xwidth
#define yheight mccemon01_yheight
#define Emin mccemon01_Emin
#define Emax mccemon01_Emax
#define restore_xray mccemon01_restore_xray
#line 60 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
    double E_N[nE];
    double E_p[nE], E_p2[nE];
#line 8691 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'emon0' [9]. */
#define mccompcurname  emon0
#define mccompcurtype  E_monitor
#define mccompcurindex 9
#define nE mccemon0_nE
#define filename mccemon0_filename
#define E_N mccemon0_E_N
#define E_p mccemon0_E_p
#define E_p2 mccemon0_E_p2
#define xmin mccemon0_xmin
#define xmax mccemon0_xmax
#define ymin mccemon0_ymin
#define ymax mccemon0_ymax
#define xwidth mccemon0_xwidth
#define yheight mccemon0_yheight
#define Emin mccemon0_Emin
#define Emax mccemon0_Emax
#define restore_xray mccemon0_restore_xray
#line 60 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
    double E_N[nE];
    double E_p[nE], E_p2[nE];
#line 8731 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'lmon0' [10]. */
#define mccompcurname  lmon0
#define mccompcurtype  L_monitor
#define mccompcurindex 10
#define nL mcclmon0_nL
#define filename mcclmon0_filename
#define L_N mcclmon0_L_N
#define L_p mcclmon0_L_p
#define L_p2 mcclmon0_L_p2
#define xmin mcclmon0_xmin
#define xmax mcclmon0_xmax
#define ymin mcclmon0_ymin
#define ymax mcclmon0_ymax
#define xwidth mcclmon0_xwidth
#define yheight mcclmon0_yheight
#define Lmin mcclmon0_Lmin
#define Lmax mcclmon0_Lmax
#define restore_xray mcclmon0_restore_xray
#line 59 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
    double L_N[nL];
    double L_p[nL], L_p2[nL];
#line 8771 "Test_src_gaussian.c"
#undef restore_xray
#undef Lmax
#undef Lmin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'psd0' [11]. */
#define mccompcurname  psd0
#define mccompcurtype  PSD_monitor
#define mccompcurindex 11
#define nx mccpsd0_nx
#define ny mccpsd0_ny
#define nr mccpsd0_nr
#define filename mccpsd0_filename
#define restore_xray mccpsd0_restore_xray
#define PSD_N mccpsd0_PSD_N
#define PSD_p mccpsd0_PSD_p
#define PSD_p2 mccpsd0_PSD_p2
#define xmin mccpsd0_xmin
#define xmax mccpsd0_xmax
#define ymin mccpsd0_ymin
#define ymax mccpsd0_ymax
#define xwidth mccpsd0_xwidth
#define yheight mccpsd0_yheight
#define radius mccpsd0_radius
#line 62 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
    double **PSD_N;
    double **PSD_p;
    double **PSD_p2;
#line 8813 "Test_src_gaussian.c"
#undef radius
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'emon1' [12]. */
#define mccompcurname  emon1
#define mccompcurtype  E_monitor
#define mccompcurindex 12
#define nE mccemon1_nE
#define filename mccemon1_filename
#define E_N mccemon1_E_N
#define E_p mccemon1_E_p
#define E_p2 mccemon1_E_p2
#define xmin mccemon1_xmin
#define xmax mccemon1_xmax
#define ymin mccemon1_ymin
#define ymax mccemon1_ymax
#define xwidth mccemon1_xwidth
#define yheight mccemon1_yheight
#define Emin mccemon1_Emin
#define Emax mccemon1_Emax
#define restore_xray mccemon1_restore_xray
#line 60 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
    double E_N[nE];
    double E_p[nE], E_p2[nE];
#line 8854 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'lmon1' [13]. */
#define mccompcurname  lmon1
#define mccompcurtype  L_monitor
#define mccompcurindex 13
#define nL mcclmon1_nL
#define filename mcclmon1_filename
#define L_N mcclmon1_L_N
#define L_p mcclmon1_L_p
#define L_p2 mcclmon1_L_p2
#define xmin mcclmon1_xmin
#define xmax mcclmon1_xmax
#define ymin mcclmon1_ymin
#define ymax mcclmon1_ymax
#define xwidth mcclmon1_xwidth
#define yheight mcclmon1_yheight
#define Lmin mcclmon1_Lmin
#define Lmax mcclmon1_Lmax
#define restore_xray mcclmon1_restore_xray
#line 59 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
    double L_N[nL];
    double L_p[nL], L_p2[nL];
#line 8894 "Test_src_gaussian.c"
#undef restore_xray
#undef Lmax
#undef Lmin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'psd1' [14]. */
#define mccompcurname  psd1
#define mccompcurtype  PSD_monitor
#define mccompcurindex 14
#define nx mccpsd1_nx
#define ny mccpsd1_ny
#define nr mccpsd1_nr
#define filename mccpsd1_filename
#define restore_xray mccpsd1_restore_xray
#define PSD_N mccpsd1_PSD_N
#define PSD_p mccpsd1_PSD_p
#define PSD_p2 mccpsd1_PSD_p2
#define xmin mccpsd1_xmin
#define xmax mccpsd1_xmax
#define ymin mccpsd1_ymin
#define ymax mccpsd1_ymax
#define xwidth mccpsd1_xwidth
#define yheight mccpsd1_yheight
#define radius mccpsd1_radius
#line 62 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
    double **PSD_N;
    double **PSD_p;
    double **PSD_p2;
#line 8936 "Test_src_gaussian.c"
#undef radius
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'slit' [15]. */
#define mccompcurname  slit
#define mccompcurtype  Slit
#define mccompcurindex 15
#define xmin mccslit_xmin
#define xmax mccslit_xmax
#define ymin mccslit_ymin
#define ymax mccslit_ymax
#define radius mccslit_radius
#define cut mccslit_cut
#define xwidth mccslit_xwidth
#define yheight mccslit_yheight
#define dist mccslit_dist
#define focus_xw mccslit_focus_xw
#define focus_yh mccslit_focus_yh
#define focus_x0 mccslit_focus_x0
#define focus_y0 mccslit_focus_y0
#undef focus_y0
#undef focus_x0
#undef focus_yh
#undef focus_xw
#undef dist
#undef yheight
#undef xwidth
#undef cut
#undef radius
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'emon2' [16]. */
#define mccompcurname  emon2
#define mccompcurtype  E_monitor
#define mccompcurindex 16
#define nE mccemon2_nE
#define filename mccemon2_filename
#define E_N mccemon2_E_N
#define E_p mccemon2_E_p
#define E_p2 mccemon2_E_p2
#define xmin mccemon2_xmin
#define xmax mccemon2_xmax
#define ymin mccemon2_ymin
#define ymax mccemon2_ymax
#define xwidth mccemon2_xwidth
#define yheight mccemon2_yheight
#define Emin mccemon2_Emin
#define Emax mccemon2_Emax
#define restore_xray mccemon2_restore_xray
#line 60 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
    double E_N[nE];
    double E_p[nE], E_p2[nE];
#line 9011 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'lmon2' [17]. */
#define mccompcurname  lmon2
#define mccompcurtype  L_monitor
#define mccompcurindex 17
#define nL mcclmon2_nL
#define filename mcclmon2_filename
#define L_N mcclmon2_L_N
#define L_p mcclmon2_L_p
#define L_p2 mcclmon2_L_p2
#define xmin mcclmon2_xmin
#define xmax mcclmon2_xmax
#define ymin mcclmon2_ymin
#define ymax mcclmon2_ymax
#define xwidth mcclmon2_xwidth
#define yheight mcclmon2_yheight
#define Lmin mcclmon2_Lmin
#define Lmax mcclmon2_Lmax
#define restore_xray mcclmon2_restore_xray
#line 59 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
    double L_N[nL];
    double L_p[nL], L_p2[nL];
#line 9051 "Test_src_gaussian.c"
#undef restore_xray
#undef Lmax
#undef Lmin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

/* User declarations for component 'psd2' [18]. */
#define mccompcurname  psd2
#define mccompcurtype  PSD_monitor
#define mccompcurindex 18
#define nx mccpsd2_nx
#define ny mccpsd2_ny
#define nr mccpsd2_nr
#define filename mccpsd2_filename
#define restore_xray mccpsd2_restore_xray
#define PSD_N mccpsd2_PSD_N
#define PSD_p mccpsd2_PSD_p
#define PSD_p2 mccpsd2_PSD_p2
#define xmin mccpsd2_xmin
#define xmax mccpsd2_xmax
#define ymin mccpsd2_ymin
#define ymax mccpsd2_ymax
#define xwidth mccpsd2_xwidth
#define yheight mccpsd2_yheight
#define radius mccpsd2_radius
#line 62 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
    double **PSD_N;
    double **PSD_p;
    double **PSD_p2;
#line 9093 "Test_src_gaussian.c"
#undef radius
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

Coords mcposaOrigin, mcposrOrigin;
Rotation mcrotaOrigin, mcrotrOrigin;
Coords mcposasrc, mcposrsrc;
Rotation mcrotasrc, mcrotrsrc;
Coords mcposasrc2, mcposrsrc2;
Rotation mcrotasrc2, mcrotrsrc2;
Coords mcposasrc3, mcposrsrc3;
Rotation mcrotasrc3, mcrotrsrc3;
Coords mcposasrc4, mcposrsrc4;
Rotation mcrotasrc4, mcrotrsrc4;
Coords mcposaemon00, mcposremon00;
Rotation mcrotaemon00, mcrotremon00;
Coords mcposaemon02, mcposremon02;
Rotation mcrotaemon02, mcrotremon02;
Coords mcposaemon01, mcposremon01;
Rotation mcrotaemon01, mcrotremon01;
Coords mcposaemon0, mcposremon0;
Rotation mcrotaemon0, mcrotremon0;
Coords mcposalmon0, mcposrlmon0;
Rotation mcrotalmon0, mcrotrlmon0;
Coords mcposapsd0, mcposrpsd0;
Rotation mcrotapsd0, mcrotrpsd0;
Coords mcposaemon1, mcposremon1;
Rotation mcrotaemon1, mcrotremon1;
Coords mcposalmon1, mcposrlmon1;
Rotation mcrotalmon1, mcrotrlmon1;
Coords mcposapsd1, mcposrpsd1;
Rotation mcrotapsd1, mcrotrpsd1;
Coords mcposaslit, mcposrslit;
Rotation mcrotaslit, mcrotrslit;
Coords mcposaemon2, mcposremon2;
Rotation mcrotaemon2, mcrotremon2;
Coords mcposalmon2, mcposrlmon2;
Rotation mcrotalmon2, mcrotrlmon2;
Coords mcposapsd2, mcposrpsd2;
Rotation mcrotapsd2, mcrotrpsd2;

MCNUM mcnx, mcny, mcnz, mcnkx, mcnky, mcnkz, mcnphi, mcnt, mcnEx, mcnEy, mcnEz, mcnp;

/* end declare */

void mcinit(void) {
#define mccompcurname  Test_src_gaussian
#define mccompcurtype  INSTRUMENT
#define mccompcurindex 0
#define mcposaTest_src_gaussian coords_set(0,0,0)
#define XW mcipXW
#define YH mcipYH
#define DIST mcipDIST
#define SRC mcipSRC
#define E0 mcipE0
#undef E0
#undef SRC
#undef DIST
#undef YH
#undef XW
#undef mcposaTest_src_gaussian
#undef mccompcurindex
#undef mccompcurtype
#undef mccompcurname
  /* Computation of coordinate transformations. */
  {
    Coords mctc1, mctc2;
    Rotation mctr1;

    mcDEBUG_INSTR()
  /* Component initializations. */
    /* Component Origin. */
    SIG_MESSAGE("Origin (Init:Place/Rotate)");
    rot_set_rotation(mcrotaOrigin,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9186 "Test_src_gaussian.c"
    rot_copy(mcrotrOrigin, mcrotaOrigin);
    mcposaOrigin = coords_set(
#line 57 "Test_src_gaussian.instr"
      0,
#line 57 "Test_src_gaussian.instr"
      0,
#line 57 "Test_src_gaussian.instr"
      0);
#line 9195 "Test_src_gaussian.c"
    mctc1 = coords_neg(mcposaOrigin);
    mcposrOrigin = rot_apply(mcrotaOrigin, mctc1);
    mcDEBUG_COMPONENT("Origin", mcposaOrigin, mcrotaOrigin)
    mccomp_posa[1] = mcposaOrigin;
    mccomp_posr[1] = mcposrOrigin;
    mcNCounter[1]  = mcPCounter[1] = mcP2Counter[1] = 0;
    mcAbsorbProp[1]= 0;
  /* Setting parameters for component Origin. */
  SIG_MESSAGE("Origin (Init:SetPar)");
#line 44 "Test_src_gaussian.instr"
  mccOrigin_percent = 10;
#line 44 "Test_src_gaussian.instr"
  mccOrigin_flag_save = 0;
#line 44 "Test_src_gaussian.instr"
  mccOrigin_minutes = 0;
#line 9211 "Test_src_gaussian.c"

    /* Component src. */
    SIG_MESSAGE("src (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9219 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotaOrigin, mcrotasrc);
    rot_transpose(mcrotaOrigin, mctr1);
    rot_mul(mcrotasrc, mctr1, mcrotrsrc);
    mctc1 = coords_set(
#line 63 "Test_src_gaussian.instr"
      0,
#line 63 "Test_src_gaussian.instr"
      0,
#line 63 "Test_src_gaussian.instr"
      0);
#line 9230 "Test_src_gaussian.c"
    rot_transpose(mcrotaOrigin, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposasrc = coords_add(mcposaOrigin, mctc2);
    mctc1 = coords_sub(mcposaOrigin, mcposasrc);
    mcposrsrc = rot_apply(mcrotasrc, mctc1);
    mcDEBUG_COMPONENT("src", mcposasrc, mcrotasrc)
    mccomp_posa[2] = mcposasrc;
    mccomp_posr[2] = mcposrsrc;
    mcNCounter[2]  = mcPCounter[2] = mcP2Counter[2] = 0;
    mcAbsorbProp[2]= 0;
  /* Setting parameters for component src. */
  SIG_MESSAGE("src (Init:SetPar)");
#line 60 "Test_src_gaussian.instr"
  mccsrc_sig_x = 1e-3;
#line 60 "Test_src_gaussian.instr"
  mccsrc_sig_y = 0.5e-3;
#line 60 "Test_src_gaussian.instr"
  mccsrc_sigPr_x = 1e-3;
#line 61 "Test_src_gaussian.instr"
  mccsrc_sigPr_y = 0.5e-3;
#line 61 "Test_src_gaussian.instr"
  mccsrc_flux = 1;
#line 61 "Test_src_gaussian.instr"
  mccsrc_dist = mcipDIST;
#line 61 "Test_src_gaussian.instr"
  mccsrc_gauss = 1;
#line 46 "Test_src_gaussian.instr"
  mccsrc_gamma = 0;
#line 61 "Test_src_gaussian.instr"
  mccsrc_E0 = 9;
#line 62 "Test_src_gaussian.instr"
  mccsrc_dE = 1;
#line 46 "Test_src_gaussian.instr"
  mccsrc_lambda0 = 0;
#line 46 "Test_src_gaussian.instr"
  mccsrc_dlambda = -1;
#line 46 "Test_src_gaussian.instr"
  mccsrc_phase = -1;
#line 9269 "Test_src_gaussian.c"

    /* Component src2. */
    SIG_MESSAGE("src2 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9277 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotaOrigin, mcrotasrc2);
    rot_transpose(mcrotasrc, mctr1);
    rot_mul(mcrotasrc2, mctr1, mcrotrsrc2);
    mctc1 = coords_set(
#line 69 "Test_src_gaussian.instr"
      0,
#line 69 "Test_src_gaussian.instr"
      0,
#line 69 "Test_src_gaussian.instr"
      0);
#line 9288 "Test_src_gaussian.c"
    rot_transpose(mcrotaOrigin, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposasrc2 = coords_add(mcposaOrigin, mctc2);
    mctc1 = coords_sub(mcposasrc, mcposasrc2);
    mcposrsrc2 = rot_apply(mcrotasrc2, mctc1);
    mcDEBUG_COMPONENT("src2", mcposasrc2, mcrotasrc2)
    mccomp_posa[3] = mcposasrc2;
    mccomp_posr[3] = mcposrsrc2;
    mcNCounter[3]  = mcPCounter[3] = mcP2Counter[3] = 0;
    mcAbsorbProp[3]= 0;
  /* Setting parameters for component src2. */
  SIG_MESSAGE("src2 (Init:SetPar)");
#line 66 "Test_src_gaussian.instr"
  mccsrc2_sig_x = 1e-3;
#line 66 "Test_src_gaussian.instr"
  mccsrc2_sig_y = 0.5e-3;
#line 66 "Test_src_gaussian.instr"
  mccsrc2_sigPr_x = 1e-3;
#line 67 "Test_src_gaussian.instr"
  mccsrc2_sigPr_y = 0.5e-3;
#line 67 "Test_src_gaussian.instr"
  mccsrc2_flux = 1;
#line 50 "Test_src_gaussian.instr"
  mccsrc2_brilliance = 0;
#line 67 "Test_src_gaussian.instr"
  mccsrc2_dist = mcipDIST;
#line 67 "Test_src_gaussian.instr"
  mccsrc2_gauss = 1;
#line 68 "Test_src_gaussian.instr"
  mccsrc2_focus_xw = mcipXW;
#line 68 "Test_src_gaussian.instr"
  mccsrc2_focus_yh = mcipYH;
#line 67 "Test_src_gaussian.instr"
  mccsrc2_E0 = 9;
#line 68 "Test_src_gaussian.instr"
  mccsrc2_dE = 1;
#line 50 "Test_src_gaussian.instr"
  mccsrc2_lambda0 = 0;
#line 50 "Test_src_gaussian.instr"
  mccsrc2_dlambda = 0;
#line 50 "Test_src_gaussian.instr"
  mccsrc2_phase = -1;
#line 9331 "Test_src_gaussian.c"

    /* Component src3. */
    SIG_MESSAGE("src3 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9339 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotaOrigin, mcrotasrc3);
    rot_transpose(mcrotasrc2, mctr1);
    rot_mul(mcrotasrc3, mctr1, mcrotrsrc3);
    mctc1 = coords_set(
#line 76 "Test_src_gaussian.instr"
      0,
#line 76 "Test_src_gaussian.instr"
      0,
#line 76 "Test_src_gaussian.instr"
      0);
#line 9350 "Test_src_gaussian.c"
    rot_transpose(mcrotaOrigin, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposasrc3 = coords_add(mcposaOrigin, mctc2);
    mctc1 = coords_sub(mcposasrc2, mcposasrc3);
    mcposrsrc3 = rot_apply(mcrotasrc3, mctc1);
    mcDEBUG_COMPONENT("src3", mcposasrc3, mcrotasrc3)
    mccomp_posa[4] = mcposasrc3;
    mccomp_posr[4] = mcposrsrc3;
    mcNCounter[4]  = mcPCounter[4] = mcP2Counter[4] = 0;
    mcAbsorbProp[4]= 0;
  /* Setting parameters for component src3. */
  SIG_MESSAGE("src3 (Init:SetPar)");
#line 73 "Test_src_gaussian.instr"
  mccsrc3_sig_x = 1e-3;
#line 73 "Test_src_gaussian.instr"
  mccsrc3_sig_y = 0.5e-3;
#line 73 "Test_src_gaussian.instr"
  mccsrc3_sigPr_x = 1e-3;
#line 74 "Test_src_gaussian.instr"
  mccsrc3_sigPr_y = 0.5e-3;
#line 74 "Test_src_gaussian.instr"
  mccsrc3_flux = 1;
#line 75 "Test_src_gaussian.instr"
  mccsrc3_brilliance = 1;
#line 74 "Test_src_gaussian.instr"
  mccsrc3_dist = mcipDIST;
#line 74 "Test_src_gaussian.instr"
  mccsrc3_gauss = 1;
#line 75 "Test_src_gaussian.instr"
  mccsrc3_focus_xw = 0;
#line 75 "Test_src_gaussian.instr"
  mccsrc3_focus_yh = 0;
#line 74 "Test_src_gaussian.instr"
  mccsrc3_E0 = 9;
#line 75 "Test_src_gaussian.instr"
  mccsrc3_dE = 0;
#line 50 "Test_src_gaussian.instr"
  mccsrc3_lambda0 = 0;
#line 50 "Test_src_gaussian.instr"
  mccsrc3_dlambda = 0;
#line 50 "Test_src_gaussian.instr"
  mccsrc3_phase = -1;
#line 9393 "Test_src_gaussian.c"

    /* Component src4. */
    SIG_MESSAGE("src4 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9401 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotaOrigin, mcrotasrc4);
    rot_transpose(mcrotasrc3, mctr1);
    rot_mul(mcrotasrc4, mctr1, mcrotrsrc4);
    mctc1 = coords_set(
#line 83 "Test_src_gaussian.instr"
      0,
#line 83 "Test_src_gaussian.instr"
      0,
#line 83 "Test_src_gaussian.instr"
      0);
#line 9412 "Test_src_gaussian.c"
    rot_transpose(mcrotaOrigin, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposasrc4 = coords_add(mcposaOrigin, mctc2);
    mctc1 = coords_sub(mcposasrc3, mcposasrc4);
    mcposrsrc4 = rot_apply(mcrotasrc4, mctc1);
    mcDEBUG_COMPONENT("src4", mcposasrc4, mcrotasrc4)
    mccomp_posa[5] = mcposasrc4;
    mccomp_posr[5] = mcposrsrc4;
    mcNCounter[5]  = mcPCounter[5] = mcP2Counter[5] = 0;
    mcAbsorbProp[5]= 0;
  /* Setting parameters for component src4. */
  SIG_MESSAGE("src4 (Init:SetPar)");
#line 80 "Test_src_gaussian.instr"
  mccsrc4_sig_x = 1e-3;
#line 80 "Test_src_gaussian.instr"
  mccsrc4_sig_y = 0.5e-3;
#line 80 "Test_src_gaussian.instr"
  mccsrc4_sigPr_x = 1e-3;
#line 81 "Test_src_gaussian.instr"
  mccsrc4_sigPr_y = 0.5e-3;
#line 81 "Test_src_gaussian.instr"
  mccsrc4_flux = 1;
#line 82 "Test_src_gaussian.instr"
  mccsrc4_brilliance = 1;
#line 81 "Test_src_gaussian.instr"
  mccsrc4_dist = mcipDIST;
#line 81 "Test_src_gaussian.instr"
  mccsrc4_gauss = 1;
#line 82 "Test_src_gaussian.instr"
  mccsrc4_focus_xw = 0;
#line 82 "Test_src_gaussian.instr"
  mccsrc4_focus_yh = 0;
#line 81 "Test_src_gaussian.instr"
  mccsrc4_E0 = 9;
#line 82 "Test_src_gaussian.instr"
  mccsrc4_dE = 1;
#line 50 "Test_src_gaussian.instr"
  mccsrc4_lambda0 = 0;
#line 50 "Test_src_gaussian.instr"
  mccsrc4_dlambda = 0;
#line 50 "Test_src_gaussian.instr"
  mccsrc4_phase = -1;
#line 9455 "Test_src_gaussian.c"

    /* Component emon00. */
    SIG_MESSAGE("emon00 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9463 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotasrc, mcrotaemon00);
    rot_transpose(mcrotasrc4, mctr1);
    rot_mul(mcrotaemon00, mctr1, mcrotremon00);
    mctc1 = coords_set(
#line 88 "Test_src_gaussian.instr"
      0,
#line 88 "Test_src_gaussian.instr"
      0,
#line 88 "Test_src_gaussian.instr"
      0.09);
#line 9474 "Test_src_gaussian.c"
    rot_transpose(mcrotasrc, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposaemon00 = coords_add(mcposasrc, mctc2);
    mctc1 = coords_sub(mcposasrc4, mcposaemon00);
    mcposremon00 = rot_apply(mcrotaemon00, mctc1);
    mcDEBUG_COMPONENT("emon00", mcposaemon00, mcrotaemon00)
    mccomp_posa[6] = mcposaemon00;
    mccomp_posr[6] = mcposremon00;
    mcNCounter[6]  = mcPCounter[6] = mcP2Counter[6] = 0;
    mcAbsorbProp[6]= 0;
  /* Setting parameters for component emon00. */
  SIG_MESSAGE("emon00 (Init:SetPar)");
#line 54 "Test_src_gaussian.instr"
  mccemon00_xmin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon00_xmax = 0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon00_ymin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon00_ymax = 0.05;
#line 86 "Test_src_gaussian.instr"
  mccemon00_xwidth = 0.01;
#line 87 "Test_src_gaussian.instr"
  mccemon00_yheight = 0.01;
#line 87 "Test_src_gaussian.instr"
  mccemon00_Emin = 0.1;
#line 87 "Test_src_gaussian.instr"
  mccemon00_Emax = 13.1;
#line 86 "Test_src_gaussian.instr"
  mccemon00_restore_xray = 1;
#line 9505 "Test_src_gaussian.c"

    /* Component emon02. */
    SIG_MESSAGE("emon02 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9513 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotasrc, mcrotaemon02);
    rot_transpose(mcrotaemon00, mctr1);
    rot_mul(mcrotaemon02, mctr1, mcrotremon02);
    mctc1 = coords_set(
#line 94 "Test_src_gaussian.instr"
      0,
#line 94 "Test_src_gaussian.instr"
      0,
#line 94 "Test_src_gaussian.instr"
      0.09);
#line 9524 "Test_src_gaussian.c"
    rot_transpose(mcrotasrc, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposaemon02 = coords_add(mcposasrc, mctc2);
    mctc1 = coords_sub(mcposaemon00, mcposaemon02);
    mcposremon02 = rot_apply(mcrotaemon02, mctc1);
    mcDEBUG_COMPONENT("emon02", mcposaemon02, mcrotaemon02)
    mccomp_posa[7] = mcposaemon02;
    mccomp_posr[7] = mcposremon02;
    mcNCounter[7]  = mcPCounter[7] = mcP2Counter[7] = 0;
    mcAbsorbProp[7]= 0;
  /* Setting parameters for component emon02. */
  SIG_MESSAGE("emon02 (Init:SetPar)");
#line 54 "Test_src_gaussian.instr"
  mccemon02_xmin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon02_xmax = 0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon02_ymin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon02_ymax = 0.05;
#line 92 "Test_src_gaussian.instr"
  mccemon02_xwidth = 0.01;
#line 93 "Test_src_gaussian.instr"
  mccemon02_yheight = 0.01;
#line 93 "Test_src_gaussian.instr"
  mccemon02_Emin = 8;
#line 93 "Test_src_gaussian.instr"
  mccemon02_Emax = 10;
#line 92 "Test_src_gaussian.instr"
  mccemon02_restore_xray = 1;
#line 9555 "Test_src_gaussian.c"

    /* Component emon01. */
    SIG_MESSAGE("emon01 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9563 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotasrc, mcrotaemon01);
    rot_transpose(mcrotaemon02, mctr1);
    rot_mul(mcrotaemon01, mctr1, mcrotremon01);
    mctc1 = coords_set(
#line 99 "Test_src_gaussian.instr"
      0,
#line 99 "Test_src_gaussian.instr"
      0,
#line 99 "Test_src_gaussian.instr"
      0.09);
#line 9574 "Test_src_gaussian.c"
    rot_transpose(mcrotasrc, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposaemon01 = coords_add(mcposasrc, mctc2);
    mctc1 = coords_sub(mcposaemon02, mcposaemon01);
    mcposremon01 = rot_apply(mcrotaemon01, mctc1);
    mcDEBUG_COMPONENT("emon01", mcposaemon01, mcrotaemon01)
    mccomp_posa[8] = mcposaemon01;
    mccomp_posr[8] = mcposremon01;
    mcNCounter[8]  = mcPCounter[8] = mcP2Counter[8] = 0;
    mcAbsorbProp[8]= 0;
  /* Setting parameters for component emon01. */
  SIG_MESSAGE("emon01 (Init:SetPar)");
#line 54 "Test_src_gaussian.instr"
  mccemon01_xmin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon01_xmax = 0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon01_ymin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon01_ymax = 0.05;
#line 97 "Test_src_gaussian.instr"
  mccemon01_xwidth = 0.01;
#line 98 "Test_src_gaussian.instr"
  mccemon01_yheight = 0.01;
#line 98 "Test_src_gaussian.instr"
  mccemon01_Emin = 7;
#line 98 "Test_src_gaussian.instr"
  mccemon01_Emax = 11;
#line 97 "Test_src_gaussian.instr"
  mccemon01_restore_xray = 1;
#line 9605 "Test_src_gaussian.c"

    /* Component emon0. */
    SIG_MESSAGE("emon0 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9613 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotasrc, mcrotaemon0);
    rot_transpose(mcrotaemon01, mctr1);
    rot_mul(mcrotaemon0, mctr1, mcrotremon0);
    mctc1 = coords_set(
#line 104 "Test_src_gaussian.instr"
      0,
#line 104 "Test_src_gaussian.instr"
      0,
#line 104 "Test_src_gaussian.instr"
      0.1);
#line 9624 "Test_src_gaussian.c"
    rot_transpose(mcrotasrc, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposaemon0 = coords_add(mcposasrc, mctc2);
    mctc1 = coords_sub(mcposaemon01, mcposaemon0);
    mcposremon0 = rot_apply(mcrotaemon0, mctc1);
    mcDEBUG_COMPONENT("emon0", mcposaemon0, mcrotaemon0)
    mccomp_posa[9] = mcposaemon0;
    mccomp_posr[9] = mcposremon0;
    mcNCounter[9]  = mcPCounter[9] = mcP2Counter[9] = 0;
    mcAbsorbProp[9]= 0;
  /* Setting parameters for component emon0. */
  SIG_MESSAGE("emon0 (Init:SetPar)");
#line 54 "Test_src_gaussian.instr"
  mccemon0_xmin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon0_xmax = 0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon0_ymin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon0_ymax = 0.05;
#line 102 "Test_src_gaussian.instr"
  mccemon0_xwidth = 0.01;
#line 103 "Test_src_gaussian.instr"
  mccemon0_yheight = 0.01;
#line 103 "Test_src_gaussian.instr"
  mccemon0_Emin = ( 1 -0.001 ) * mcipE0;
#line 103 "Test_src_gaussian.instr"
  mccemon0_Emax = ( 1 + 0.001 ) * mcipE0;
#line 55 "Test_src_gaussian.instr"
  mccemon0_restore_xray = 0;
#line 9655 "Test_src_gaussian.c"

    /* Component lmon0. */
    SIG_MESSAGE("lmon0 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9663 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotaemon0, mcrotalmon0);
    rot_transpose(mcrotaemon0, mctr1);
    rot_mul(mcrotalmon0, mctr1, mcrotrlmon0);
    mctc1 = coords_set(
#line 109 "Test_src_gaussian.instr"
      0,
#line 109 "Test_src_gaussian.instr"
      0,
#line 109 "Test_src_gaussian.instr"
      1e-3);
#line 9674 "Test_src_gaussian.c"
    rot_transpose(mcrotaemon0, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposalmon0 = coords_add(mcposaemon0, mctc2);
    mctc1 = coords_sub(mcposaemon0, mcposalmon0);
    mcposrlmon0 = rot_apply(mcrotalmon0, mctc1);
    mcDEBUG_COMPONENT("lmon0", mcposalmon0, mcrotalmon0)
    mccomp_posa[10] = mcposalmon0;
    mccomp_posr[10] = mcposrlmon0;
    mcNCounter[10]  = mcPCounter[10] = mcP2Counter[10] = 0;
    mcAbsorbProp[10]= 0;
  /* Setting parameters for component lmon0. */
  SIG_MESSAGE("lmon0 (Init:SetPar)");
#line 53 "Test_src_gaussian.instr"
  mcclmon0_xmin = -0.05;
#line 53 "Test_src_gaussian.instr"
  mcclmon0_xmax = 0.05;
#line 53 "Test_src_gaussian.instr"
  mcclmon0_ymin = -0.05;
#line 53 "Test_src_gaussian.instr"
  mcclmon0_ymax = 0.05;
#line 107 "Test_src_gaussian.instr"
  mcclmon0_xwidth = 0.01;
#line 108 "Test_src_gaussian.instr"
  mcclmon0_yheight = 0.01;
#line 108 "Test_src_gaussian.instr"
  mcclmon0_Lmin = 0;
#line 108 "Test_src_gaussian.instr"
  mcclmon0_Lmax = 2;
#line 108 "Test_src_gaussian.instr"
  mcclmon0_restore_xray = 0;
#line 9705 "Test_src_gaussian.c"

    /* Component psd0. */
    SIG_MESSAGE("psd0 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9713 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotalmon0, mcrotapsd0);
    rot_transpose(mcrotalmon0, mctr1);
    rot_mul(mcrotapsd0, mctr1, mcrotrpsd0);
    mctc1 = coords_set(
#line 113 "Test_src_gaussian.instr"
      0,
#line 113 "Test_src_gaussian.instr"
      0,
#line 113 "Test_src_gaussian.instr"
      1e-3);
#line 9724 "Test_src_gaussian.c"
    rot_transpose(mcrotalmon0, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposapsd0 = coords_add(mcposalmon0, mctc2);
    mctc1 = coords_sub(mcposalmon0, mcposapsd0);
    mcposrpsd0 = rot_apply(mcrotapsd0, mctc1);
    mcDEBUG_COMPONENT("psd0", mcposapsd0, mcrotapsd0)
    mccomp_posa[11] = mcposapsd0;
    mccomp_posr[11] = mcposrpsd0;
    mcNCounter[11]  = mcPCounter[11] = mcP2Counter[11] = 0;
    mcAbsorbProp[11]= 0;
  /* Setting parameters for component psd0. */
  SIG_MESSAGE("psd0 (Init:SetPar)");
#line 56 "Test_src_gaussian.instr"
  mccpsd0_xmin = -0.05;
#line 56 "Test_src_gaussian.instr"
  mccpsd0_xmax = 0.05;
#line 56 "Test_src_gaussian.instr"
  mccpsd0_ymin = -0.05;
#line 56 "Test_src_gaussian.instr"
  mccpsd0_ymax = 0.05;
#line 112 "Test_src_gaussian.instr"
  mccpsd0_xwidth = 0.01;
#line 112 "Test_src_gaussian.instr"
  mccpsd0_yheight = 0.01;
#line 56 "Test_src_gaussian.instr"
  mccpsd0_radius = 0;
#line 9751 "Test_src_gaussian.c"

    /* Component emon1. */
    SIG_MESSAGE("emon1 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9759 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotasrc, mcrotaemon1);
    rot_transpose(mcrotapsd0, mctr1);
    rot_mul(mcrotaemon1, mctr1, mcrotremon1);
    mctc1 = coords_set(
#line 118 "Test_src_gaussian.instr"
      0,
#line 118 "Test_src_gaussian.instr"
      0,
#line 118 "Test_src_gaussian.instr"
      mcipDIST -3e-3);
#line 9770 "Test_src_gaussian.c"
    rot_transpose(mcrotasrc, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposaemon1 = coords_add(mcposasrc, mctc2);
    mctc1 = coords_sub(mcposapsd0, mcposaemon1);
    mcposremon1 = rot_apply(mcrotaemon1, mctc1);
    mcDEBUG_COMPONENT("emon1", mcposaemon1, mcrotaemon1)
    mccomp_posa[12] = mcposaemon1;
    mccomp_posr[12] = mcposremon1;
    mcNCounter[12]  = mcPCounter[12] = mcP2Counter[12] = 0;
    mcAbsorbProp[12]= 0;
  /* Setting parameters for component emon1. */
  SIG_MESSAGE("emon1 (Init:SetPar)");
#line 54 "Test_src_gaussian.instr"
  mccemon1_xmin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon1_xmax = 0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon1_ymin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon1_ymax = 0.05;
#line 116 "Test_src_gaussian.instr"
  mccemon1_xwidth = 0.1;
#line 117 "Test_src_gaussian.instr"
  mccemon1_yheight = 0.1;
#line 117 "Test_src_gaussian.instr"
  mccemon1_Emin = 0;
#line 117 "Test_src_gaussian.instr"
  mccemon1_Emax = 20;
#line 55 "Test_src_gaussian.instr"
  mccemon1_restore_xray = 0;
#line 9801 "Test_src_gaussian.c"

    /* Component lmon1. */
    SIG_MESSAGE("lmon1 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9809 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotaemon1, mcrotalmon1);
    rot_transpose(mcrotaemon1, mctr1);
    rot_mul(mcrotalmon1, mctr1, mcrotrlmon1);
    mctc1 = coords_set(
#line 122 "Test_src_gaussian.instr"
      0,
#line 122 "Test_src_gaussian.instr"
      0,
#line 122 "Test_src_gaussian.instr"
      1e-3);
#line 9820 "Test_src_gaussian.c"
    rot_transpose(mcrotaemon1, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposalmon1 = coords_add(mcposaemon1, mctc2);
    mctc1 = coords_sub(mcposaemon1, mcposalmon1);
    mcposrlmon1 = rot_apply(mcrotalmon1, mctc1);
    mcDEBUG_COMPONENT("lmon1", mcposalmon1, mcrotalmon1)
    mccomp_posa[13] = mcposalmon1;
    mccomp_posr[13] = mcposrlmon1;
    mcNCounter[13]  = mcPCounter[13] = mcP2Counter[13] = 0;
    mcAbsorbProp[13]= 0;
  /* Setting parameters for component lmon1. */
  SIG_MESSAGE("lmon1 (Init:SetPar)");
#line 53 "Test_src_gaussian.instr"
  mcclmon1_xmin = -0.05;
#line 53 "Test_src_gaussian.instr"
  mcclmon1_xmax = 0.05;
#line 53 "Test_src_gaussian.instr"
  mcclmon1_ymin = -0.05;
#line 53 "Test_src_gaussian.instr"
  mcclmon1_ymax = 0.05;
#line 120 "Test_src_gaussian.instr"
  mcclmon1_xwidth = 0.1;
#line 121 "Test_src_gaussian.instr"
  mcclmon1_yheight = 0.1;
#line 121 "Test_src_gaussian.instr"
  mcclmon1_Lmin = 0;
#line 121 "Test_src_gaussian.instr"
  mcclmon1_Lmax = 2;
#line 121 "Test_src_gaussian.instr"
  mcclmon1_restore_xray = 0;
#line 9851 "Test_src_gaussian.c"

    /* Component psd1. */
    SIG_MESSAGE("psd1 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9859 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotalmon1, mcrotapsd1);
    rot_transpose(mcrotalmon1, mctr1);
    rot_mul(mcrotapsd1, mctr1, mcrotrpsd1);
    mctc1 = coords_set(
#line 125 "Test_src_gaussian.instr"
      0,
#line 125 "Test_src_gaussian.instr"
      0,
#line 125 "Test_src_gaussian.instr"
      1e-3);
#line 9870 "Test_src_gaussian.c"
    rot_transpose(mcrotalmon1, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposapsd1 = coords_add(mcposalmon1, mctc2);
    mctc1 = coords_sub(mcposalmon1, mcposapsd1);
    mcposrpsd1 = rot_apply(mcrotapsd1, mctc1);
    mcDEBUG_COMPONENT("psd1", mcposapsd1, mcrotapsd1)
    mccomp_posa[14] = mcposapsd1;
    mccomp_posr[14] = mcposrpsd1;
    mcNCounter[14]  = mcPCounter[14] = mcP2Counter[14] = 0;
    mcAbsorbProp[14]= 0;
  /* Setting parameters for component psd1. */
  SIG_MESSAGE("psd1 (Init:SetPar)");
#line 56 "Test_src_gaussian.instr"
  mccpsd1_xmin = -0.05;
#line 56 "Test_src_gaussian.instr"
  mccpsd1_xmax = 0.05;
#line 56 "Test_src_gaussian.instr"
  mccpsd1_ymin = -0.05;
#line 56 "Test_src_gaussian.instr"
  mccpsd1_ymax = 0.05;
#line 124 "Test_src_gaussian.instr"
  mccpsd1_xwidth = 0.10;
#line 124 "Test_src_gaussian.instr"
  mccpsd1_yheight = 0.1;
#line 56 "Test_src_gaussian.instr"
  mccpsd1_radius = 0;
#line 9897 "Test_src_gaussian.c"

    /* Component slit. */
    SIG_MESSAGE("slit (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9905 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotasrc, mcrotaslit);
    rot_transpose(mcrotapsd1, mctr1);
    rot_mul(mcrotaslit, mctr1, mcrotrslit);
    mctc1 = coords_set(
#line 129 "Test_src_gaussian.instr"
      0,
#line 129 "Test_src_gaussian.instr"
      0,
#line 129 "Test_src_gaussian.instr"
      mcipDIST);
#line 9916 "Test_src_gaussian.c"
    rot_transpose(mcrotasrc, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposaslit = coords_add(mcposasrc, mctc2);
    mctc1 = coords_sub(mcposapsd1, mcposaslit);
    mcposrslit = rot_apply(mcrotaslit, mctc1);
    mcDEBUG_COMPONENT("slit", mcposaslit, mcrotaslit)
    mccomp_posa[15] = mcposaslit;
    mccomp_posr[15] = mcposrslit;
    mcNCounter[15]  = mcPCounter[15] = mcP2Counter[15] = 0;
    mcAbsorbProp[15]= 0;
  /* Setting parameters for component slit. */
  SIG_MESSAGE("slit (Init:SetPar)");
#line 50 "Test_src_gaussian.instr"
  mccslit_xmin = -0.01;
#line 50 "Test_src_gaussian.instr"
  mccslit_xmax = 0.01;
#line 50 "Test_src_gaussian.instr"
  mccslit_ymin = -0.01;
#line 50 "Test_src_gaussian.instr"
  mccslit_ymax = 0.01;
#line 50 "Test_src_gaussian.instr"
  mccslit_radius = 0;
#line 51 "Test_src_gaussian.instr"
  mccslit_cut = 0;
#line 128 "Test_src_gaussian.instr"
  mccslit_xwidth = 2e-3;
#line 128 "Test_src_gaussian.instr"
  mccslit_yheight = 2e-3;
#line 51 "Test_src_gaussian.instr"
  mccslit_dist = 0;
#line 51 "Test_src_gaussian.instr"
  mccslit_focus_xw = 0;
#line 51 "Test_src_gaussian.instr"
  mccslit_focus_yh = 0;
#line 51 "Test_src_gaussian.instr"
  mccslit_focus_x0 = 0;
#line 51 "Test_src_gaussian.instr"
  mccslit_focus_y0 = 0;
#line 9955 "Test_src_gaussian.c"

    /* Component emon2. */
    SIG_MESSAGE("emon2 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 9963 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotasrc, mcrotaemon2);
    rot_transpose(mcrotaslit, mctr1);
    rot_mul(mcrotaemon2, mctr1, mcrotremon2);
    mctc1 = coords_set(
#line 134 "Test_src_gaussian.instr"
      0,
#line 134 "Test_src_gaussian.instr"
      0,
#line 134 "Test_src_gaussian.instr"
      mcipDIST + 1e-3);
#line 9974 "Test_src_gaussian.c"
    rot_transpose(mcrotasrc, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposaemon2 = coords_add(mcposasrc, mctc2);
    mctc1 = coords_sub(mcposaslit, mcposaemon2);
    mcposremon2 = rot_apply(mcrotaemon2, mctc1);
    mcDEBUG_COMPONENT("emon2", mcposaemon2, mcrotaemon2)
    mccomp_posa[16] = mcposaemon2;
    mccomp_posr[16] = mcposremon2;
    mcNCounter[16]  = mcPCounter[16] = mcP2Counter[16] = 0;
    mcAbsorbProp[16]= 0;
  /* Setting parameters for component emon2. */
  SIG_MESSAGE("emon2 (Init:SetPar)");
#line 54 "Test_src_gaussian.instr"
  mccemon2_xmin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon2_xmax = 0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon2_ymin = -0.05;
#line 54 "Test_src_gaussian.instr"
  mccemon2_ymax = 0.05;
#line 132 "Test_src_gaussian.instr"
  mccemon2_xwidth = 0.01;
#line 133 "Test_src_gaussian.instr"
  mccemon2_yheight = 0.01;
#line 133 "Test_src_gaussian.instr"
  mccemon2_Emin = 0;
#line 133 "Test_src_gaussian.instr"
  mccemon2_Emax = 20;
#line 55 "Test_src_gaussian.instr"
  mccemon2_restore_xray = 0;
#line 10005 "Test_src_gaussian.c"

    /* Component lmon2. */
    SIG_MESSAGE("lmon2 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 10013 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotaemon2, mcrotalmon2);
    rot_transpose(mcrotaemon2, mctr1);
    rot_mul(mcrotalmon2, mctr1, mcrotrlmon2);
    mctc1 = coords_set(
#line 138 "Test_src_gaussian.instr"
      0,
#line 138 "Test_src_gaussian.instr"
      0,
#line 138 "Test_src_gaussian.instr"
      1e-3);
#line 10024 "Test_src_gaussian.c"
    rot_transpose(mcrotaemon2, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposalmon2 = coords_add(mcposaemon2, mctc2);
    mctc1 = coords_sub(mcposaemon2, mcposalmon2);
    mcposrlmon2 = rot_apply(mcrotalmon2, mctc1);
    mcDEBUG_COMPONENT("lmon2", mcposalmon2, mcrotalmon2)
    mccomp_posa[17] = mcposalmon2;
    mccomp_posr[17] = mcposrlmon2;
    mcNCounter[17]  = mcPCounter[17] = mcP2Counter[17] = 0;
    mcAbsorbProp[17]= 0;
  /* Setting parameters for component lmon2. */
  SIG_MESSAGE("lmon2 (Init:SetPar)");
#line 53 "Test_src_gaussian.instr"
  mcclmon2_xmin = -0.05;
#line 53 "Test_src_gaussian.instr"
  mcclmon2_xmax = 0.05;
#line 53 "Test_src_gaussian.instr"
  mcclmon2_ymin = -0.05;
#line 53 "Test_src_gaussian.instr"
  mcclmon2_ymax = 0.05;
#line 136 "Test_src_gaussian.instr"
  mcclmon2_xwidth = 0.01;
#line 137 "Test_src_gaussian.instr"
  mcclmon2_yheight = 0.01;
#line 137 "Test_src_gaussian.instr"
  mcclmon2_Lmin = 0;
#line 137 "Test_src_gaussian.instr"
  mcclmon2_Lmax = 2;
#line 137 "Test_src_gaussian.instr"
  mcclmon2_restore_xray = 0;
#line 10055 "Test_src_gaussian.c"

    /* Component psd2. */
    SIG_MESSAGE("psd2 (Init:Place/Rotate)");
    rot_set_rotation(mctr1,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD,
      (0.0)*DEG2RAD);
#line 10063 "Test_src_gaussian.c"
    rot_mul(mctr1, mcrotalmon2, mcrotapsd2);
    rot_transpose(mcrotalmon2, mctr1);
    rot_mul(mcrotapsd2, mctr1, mcrotrpsd2);
    mctc1 = coords_set(
#line 141 "Test_src_gaussian.instr"
      0,
#line 141 "Test_src_gaussian.instr"
      0,
#line 141 "Test_src_gaussian.instr"
      1e-3);
#line 10074 "Test_src_gaussian.c"
    rot_transpose(mcrotalmon2, mctr1);
    mctc2 = rot_apply(mctr1, mctc1);
    mcposapsd2 = coords_add(mcposalmon2, mctc2);
    mctc1 = coords_sub(mcposalmon2, mcposapsd2);
    mcposrpsd2 = rot_apply(mcrotapsd2, mctc1);
    mcDEBUG_COMPONENT("psd2", mcposapsd2, mcrotapsd2)
    mccomp_posa[18] = mcposapsd2;
    mccomp_posr[18] = mcposrpsd2;
    mcNCounter[18]  = mcPCounter[18] = mcP2Counter[18] = 0;
    mcAbsorbProp[18]= 0;
  /* Setting parameters for component psd2. */
  SIG_MESSAGE("psd2 (Init:SetPar)");
#line 56 "Test_src_gaussian.instr"
  mccpsd2_xmin = -0.05;
#line 56 "Test_src_gaussian.instr"
  mccpsd2_xmax = 0.05;
#line 56 "Test_src_gaussian.instr"
  mccpsd2_ymin = -0.05;
#line 56 "Test_src_gaussian.instr"
  mccpsd2_ymax = 0.05;
#line 140 "Test_src_gaussian.instr"
  mccpsd2_xwidth = 0.01;
#line 140 "Test_src_gaussian.instr"
  mccpsd2_yheight = 0.01;
#line 56 "Test_src_gaussian.instr"
  mccpsd2_radius = 0;
#line 10101 "Test_src_gaussian.c"

  /* Component initializations. */
  /* Initializations for component Origin. */
  SIG_MESSAGE("Origin (Init)");
#define mccompcurname  Origin
#define mccompcurtype  Progress_bar
#define mccompcurindex 1
#define profile mccOrigin_profile
#define IntermediateCnts mccOrigin_IntermediateCnts
#define StartTime mccOrigin_StartTime
#define EndTime mccOrigin_EndTime
#define percent mccOrigin_percent
#define flag_save mccOrigin_flag_save
#define minutes mccOrigin_minutes
#line 63 "/usr/local/lib/mcxtrace-x.y.z/misc/Progress_bar.comp"
{
  fprintf(stdout, "[%s] Initialize\n", mcinstrument_name);
  if (percent*mcget_ncount()/100 < 1e5) {
    percent=1e5*100.0/mcget_ncount();
  }
}
#line 10123 "Test_src_gaussian.c"
#undef minutes
#undef flag_save
#undef percent
#undef EndTime
#undef StartTime
#undef IntermediateCnts
#undef profile
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component src. */
  SIG_MESSAGE("src (Init)");
#define mccompcurname  src
#define mccompcurtype  Source_gaussian
#define mccompcurindex 2
#define spectrum_file mccsrc_spectrum_file
#define prms mccsrc_prms
#define sig_x mccsrc_sig_x
#define sig_y mccsrc_sig_y
#define sigPr_x mccsrc_sigPr_x
#define sigPr_y mccsrc_sigPr_y
#define flux mccsrc_flux
#define dist mccsrc_dist
#define gauss mccsrc_gauss
#define gamma mccsrc_gamma
#define E0 mccsrc_E0
#define dE mccsrc_dE
#define lambda0 mccsrc_lambda0
#define dlambda mccsrc_dlambda
#define phase mccsrc_phase
#line 71 "/usr/local/lib/mcxtrace-x.y.z/sources/Source_gaussian.comp"
{
  if (!sig_y) sig_y=sig_x;
  
  if (!sigPr_x || !sigPr_y){
    fprintf(stderr,"Source_gaussian (%s): Must define horizontal and vertical angular divergences \n",NAME_CURRENT_COMP);
    exit(0);
  }

 if (spectrum_file){
    /*read spectrum from file*/
    int status=0;
    if ( (status=Table_Read(&(prms.T),spectrum_file,0))==-1){
      fprintf(stderr,"Source_gaussian(%s) Error: Could not parse file \"%s\"\n",NAME_CURRENT_COMP,spectrum_file?spectrum_file:"");
      exit(-1);
    }
    /*data is now in table t*/
    /*integrate to get total flux, assuming numbers have been corrected for measuring aperture*/
    int i;
    prms.pint=0;
    t_Table *T=&(prms.T);
    for (i=0;i<prms.T.rows-1;i++){
      prms.pint+=((T->data[i*T->columns+1]+T->data[(i+1)*T->columns+1])/2.0)*(T->data[(i+1)*T->columns]-T->data[i*T->columns]); 
    }
    printf("Source_gaussian(%s) Integrated intensity radiated is %g pht/s\n",NAME_CURRENT_COMP,prms.pint);
    if(E0) printf("Source_gaussian(%s) E0!=0 -> assuming intensity spectrum is parametrized by energy [keV]\n",NAME_CURRENT_COMP);
  } else if (E0){
    lambda0=2*M_PI/E2K * (E0/(E0*E0-dE*dE));
    if (dE) {
      dlambda=2*M_PI/E2K * (dE/(E0*E0-dE*dE));
    } else {
      dlambda=0;
    }
  }else if (!lambda0) {
    fprintf(stderr,"Source_gaussian (%s): Must specify either wavelength or energy distribution\n",NAME_CURRENT_COMP);
    exit(0);
  }
  /*calculate the X-ray weight from the flux*/
  if (flux){//pmul=flux;
    prms.pmul=flux*1.0/(double)mcget_ncount(); 
  }else{
    prms.pmul=1.0/(double)mcget_ncount();
  }
}
#line 10199 "Test_src_gaussian.c"
#undef phase
#undef dlambda
#undef lambda0
#undef dE
#undef E0
#undef gamma
#undef gauss
#undef dist
#undef flux
#undef sigPr_y
#undef sigPr_x
#undef sig_y
#undef sig_x
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component src2. */
  SIG_MESSAGE("src2 (Init)");
#define mccompcurname  src2
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 3
#define spectrum_file mccsrc2_spectrum_file
#define prms mccsrc2_prms
#define spX mccsrc2_spX
#define spY mccsrc2_spY
#define sig_x mccsrc2_sig_x
#define sig_y mccsrc2_sig_y
#define sigPr_x mccsrc2_sigPr_x
#define sigPr_y mccsrc2_sigPr_y
#define flux mccsrc2_flux
#define brilliance mccsrc2_brilliance
#define dist mccsrc2_dist
#define gauss mccsrc2_gauss
#define focus_xw mccsrc2_focus_xw
#define focus_yh mccsrc2_focus_yh
#define E0 mccsrc2_E0
#define dE mccsrc2_dE
#define lambda0 mccsrc2_lambda0
#define dlambda mccsrc2_dlambda
#define phase mccsrc2_phase
#line 74 "Source_gaussian2.comp"
{
  if (!sig_y) sig_y=sig_x;
  
  if (!sigPr_x || !sigPr_y){
    fprintf(stderr,"Source_gaussian (%s): Must define horizontal and vertical angular divergences \n",NAME_CURRENT_COMP);
    exit(1);
  }
  
  if ( (focus_xw || focus_yh) && (dist*focus_xw*focus_yh == 0.0) ){
    fprintf(stderr,"Error (%s): Nonsensical definition of sampling window: (focus_xw,focus_yh,dist)=(%g %g %g). Aborting\n",NAME_CURRENT_COMP,focus_xw,focus_yh,dist);
    exit(1);
  }   

  if (spectrum_file){
    /*read spectrum from file*/
    int status=0;
    if ( (status=Table_Read(&(prms.T),spectrum_file,0))==-1){
      fprintf(stderr,"Source_gaussian(%s) Error: Could not parse file \"%s\"\n",NAME_CURRENT_COMP,spectrum_file?spectrum_file:"");
      exit(1);
    }
    /*data is now in table t*/
    /*integrate to get total flux, assuming numbers have been corrected for measuring aperture*/
    int i;
    prms.pint=0;
    prms.pnum=0;

    /*upper and lower bounds of bandwidth window*/
    double BWmin,BWmax;
    if (E0 && dE){
      BWmin=E0-dE;
      BWmax=E0+dE;
    }else if (lambda0 && dlambda){
      /*this means the file is paramaterized by wavelength*/
      BWmin=(lambda0-dlambda);
      BWmax=(lambda0+dlambda);
    }else{
      BWmin=-FLT_MAX;
      BWmax=FLT_MAX;
    }

    t_Table *T=&(prms.T);
    for (i=0;i<prms.T.rows-1;i++){
      double I,e;
      e=T->data[i*T->columns];
      if (brilliance){
        /*unit is brilliance - not raw flux per unit angle and source area*/
        /*correct for the non-uniform energy binning*/
        I=(1.0/(T->data[i*T->columns]*0.01))*((T->data[i*T->columns+2]+T->data[(i+1)*T->columns+2])/2.0)*(T->data[(i+1)*T->columns]-T->data[i*T->columns]);
        if (e<BWmax && e>BWmin){
          prms.pnum+=I;
        }
        prms.pint+=I;
      }else{
        I=((T->data[i*T->columns+2]+T->data[(i+1)*T->columns+2])/2.0)*(T->data[(i+1)*T->columns]-T->data[i*T->columns]);
        if (e<BWmax && e>BWmin){
          prms.pnum+=I;
        }
        prms.pint+=I;
      }
    }

    printf("%s: Integrated intensity radiated is %g pht/s\n",NAME_CURRENT_COMP,prms.pint);
    printf("%s: Intensity radiated in chosen Energy/wavelength window is %g pht/s\n",NAME_CURRENT_COMP,prms.pnum);
    
    if(E0) printf("%s: E0!=0 -> assuming intensity spectrum is parametrized by energy [keV]\n",NAME_CURRENT_COMP);
  } else if (!E0 && !lambda0){
    fprintf(stderr,"Error (%s): Must specify either wavelength or energy distribution\n",NAME_CURRENT_COMP);
    exit(1);
  }
  /*calculate the X-ray weight from the flux*/
  if (flux){//pmul=flux;
    prms.pmul=flux*1.0/(double)mcget_ncount(); 
  }else{
    prms.pmul=1.0/(double)mcget_ncount();
  }
  /*adjust for BW window*/
  prms.pmul*=prms.pnum/prms.pint;

  /*Beam's footprint at a dist calculation*/
  spX=sqrt(sig_x*sig_x+sigPr_x*sigPr_x*dist*dist);
  spY=sqrt(sig_y*sig_y+sigPr_y*sigPr_y*dist*dist);

  uniform_sampling=0;
  if (focus_xw && focus_yh){
    /*adjust for a focusing window*/
    prms.pmul*=erf(focus_xw*0.5*M_SQRT1_2/spX)*erf(focus_yh*0.5*M_SQRT1_2/spY);
    if ( focus_xw<spX || focus_yh<spY){
      /*use a uniform sampling scheme for more efficient sampling by adjusting weights*/
      uniform_sampling=1;
    }
  }


}
#line 10338 "Test_src_gaussian.c"
#undef phase
#undef dlambda
#undef lambda0
#undef dE
#undef E0
#undef focus_yh
#undef focus_xw
#undef gauss
#undef dist
#undef brilliance
#undef flux
#undef sigPr_y
#undef sigPr_x
#undef sig_y
#undef sig_x
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component src3. */
  SIG_MESSAGE("src3 (Init)");
#define mccompcurname  src3
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 4
#define spectrum_file mccsrc3_spectrum_file
#define prms mccsrc3_prms
#define spX mccsrc3_spX
#define spY mccsrc3_spY
#define sig_x mccsrc3_sig_x
#define sig_y mccsrc3_sig_y
#define sigPr_x mccsrc3_sigPr_x
#define sigPr_y mccsrc3_sigPr_y
#define flux mccsrc3_flux
#define brilliance mccsrc3_brilliance
#define dist mccsrc3_dist
#define gauss mccsrc3_gauss
#define focus_xw mccsrc3_focus_xw
#define focus_yh mccsrc3_focus_yh
#define E0 mccsrc3_E0
#define dE mccsrc3_dE
#define lambda0 mccsrc3_lambda0
#define dlambda mccsrc3_dlambda
#define phase mccsrc3_phase
#line 74 "Source_gaussian2.comp"
{
  if (!sig_y) sig_y=sig_x;
  
  if (!sigPr_x || !sigPr_y){
    fprintf(stderr,"Source_gaussian (%s): Must define horizontal and vertical angular divergences \n",NAME_CURRENT_COMP);
    exit(1);
  }
  
  if ( (focus_xw || focus_yh) && (dist*focus_xw*focus_yh == 0.0) ){
    fprintf(stderr,"Error (%s): Nonsensical definition of sampling window: (focus_xw,focus_yh,dist)=(%g %g %g). Aborting\n",NAME_CURRENT_COMP,focus_xw,focus_yh,dist);
    exit(1);
  }   

  if (spectrum_file){
    /*read spectrum from file*/
    int status=0;
    if ( (status=Table_Read(&(prms.T),spectrum_file,0))==-1){
      fprintf(stderr,"Source_gaussian(%s) Error: Could not parse file \"%s\"\n",NAME_CURRENT_COMP,spectrum_file?spectrum_file:"");
      exit(1);
    }
    /*data is now in table t*/
    /*integrate to get total flux, assuming numbers have been corrected for measuring aperture*/
    int i;
    prms.pint=0;
    prms.pnum=0;

    /*upper and lower bounds of bandwidth window*/
    double BWmin,BWmax;
    if (E0 && dE){
      BWmin=E0-dE;
      BWmax=E0+dE;
    }else if (lambda0 && dlambda){
      /*this means the file is paramaterized by wavelength*/
      BWmin=(lambda0-dlambda);
      BWmax=(lambda0+dlambda);
    }else{
      BWmin=-FLT_MAX;
      BWmax=FLT_MAX;
    }

    t_Table *T=&(prms.T);
    for (i=0;i<prms.T.rows-1;i++){
      double I,e;
      e=T->data[i*T->columns];
      if (brilliance){
        /*unit is brilliance - not raw flux per unit angle and source area*/
        /*correct for the non-uniform energy binning*/
        I=(1.0/(T->data[i*T->columns]*0.01))*((T->data[i*T->columns+2]+T->data[(i+1)*T->columns+2])/2.0)*(T->data[(i+1)*T->columns]-T->data[i*T->columns]);
        if (e<BWmax && e>BWmin){
          prms.pnum+=I;
        }
        prms.pint+=I;
      }else{
        I=((T->data[i*T->columns+2]+T->data[(i+1)*T->columns+2])/2.0)*(T->data[(i+1)*T->columns]-T->data[i*T->columns]);
        if (e<BWmax && e>BWmin){
          prms.pnum+=I;
        }
        prms.pint+=I;
      }
    }

    printf("%s: Integrated intensity radiated is %g pht/s\n",NAME_CURRENT_COMP,prms.pint);
    printf("%s: Intensity radiated in chosen Energy/wavelength window is %g pht/s\n",NAME_CURRENT_COMP,prms.pnum);
    
    if(E0) printf("%s: E0!=0 -> assuming intensity spectrum is parametrized by energy [keV]\n",NAME_CURRENT_COMP);
  } else if (!E0 && !lambda0){
    fprintf(stderr,"Error (%s): Must specify either wavelength or energy distribution\n",NAME_CURRENT_COMP);
    exit(1);
  }
  /*calculate the X-ray weight from the flux*/
  if (flux){//pmul=flux;
    prms.pmul=flux*1.0/(double)mcget_ncount(); 
  }else{
    prms.pmul=1.0/(double)mcget_ncount();
  }
  /*adjust for BW window*/
  prms.pmul*=prms.pnum/prms.pint;

  /*Beam's footprint at a dist calculation*/
  spX=sqrt(sig_x*sig_x+sigPr_x*sigPr_x*dist*dist);
  spY=sqrt(sig_y*sig_y+sigPr_y*sigPr_y*dist*dist);

  uniform_sampling=0;
  if (focus_xw && focus_yh){
    /*adjust for a focusing window*/
    prms.pmul*=erf(focus_xw*0.5*M_SQRT1_2/spX)*erf(focus_yh*0.5*M_SQRT1_2/spY);
    if ( focus_xw<spX || focus_yh<spY){
      /*use a uniform sampling scheme for more efficient sampling by adjusting weights*/
      uniform_sampling=1;
    }
  }


}
#line 10481 "Test_src_gaussian.c"
#undef phase
#undef dlambda
#undef lambda0
#undef dE
#undef E0
#undef focus_yh
#undef focus_xw
#undef gauss
#undef dist
#undef brilliance
#undef flux
#undef sigPr_y
#undef sigPr_x
#undef sig_y
#undef sig_x
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component src4. */
  SIG_MESSAGE("src4 (Init)");
#define mccompcurname  src4
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 5
#define spectrum_file mccsrc4_spectrum_file
#define prms mccsrc4_prms
#define spX mccsrc4_spX
#define spY mccsrc4_spY
#define sig_x mccsrc4_sig_x
#define sig_y mccsrc4_sig_y
#define sigPr_x mccsrc4_sigPr_x
#define sigPr_y mccsrc4_sigPr_y
#define flux mccsrc4_flux
#define brilliance mccsrc4_brilliance
#define dist mccsrc4_dist
#define gauss mccsrc4_gauss
#define focus_xw mccsrc4_focus_xw
#define focus_yh mccsrc4_focus_yh
#define E0 mccsrc4_E0
#define dE mccsrc4_dE
#define lambda0 mccsrc4_lambda0
#define dlambda mccsrc4_dlambda
#define phase mccsrc4_phase
#line 74 "Source_gaussian2.comp"
{
  if (!sig_y) sig_y=sig_x;
  
  if (!sigPr_x || !sigPr_y){
    fprintf(stderr,"Source_gaussian (%s): Must define horizontal and vertical angular divergences \n",NAME_CURRENT_COMP);
    exit(1);
  }
  
  if ( (focus_xw || focus_yh) && (dist*focus_xw*focus_yh == 0.0) ){
    fprintf(stderr,"Error (%s): Nonsensical definition of sampling window: (focus_xw,focus_yh,dist)=(%g %g %g). Aborting\n",NAME_CURRENT_COMP,focus_xw,focus_yh,dist);
    exit(1);
  }   

  if (spectrum_file){
    /*read spectrum from file*/
    int status=0;
    if ( (status=Table_Read(&(prms.T),spectrum_file,0))==-1){
      fprintf(stderr,"Source_gaussian(%s) Error: Could not parse file \"%s\"\n",NAME_CURRENT_COMP,spectrum_file?spectrum_file:"");
      exit(1);
    }
    /*data is now in table t*/
    /*integrate to get total flux, assuming numbers have been corrected for measuring aperture*/
    int i;
    prms.pint=0;
    prms.pnum=0;

    /*upper and lower bounds of bandwidth window*/
    double BWmin,BWmax;
    if (E0 && dE){
      BWmin=E0-dE;
      BWmax=E0+dE;
    }else if (lambda0 && dlambda){
      /*this means the file is paramaterized by wavelength*/
      BWmin=(lambda0-dlambda);
      BWmax=(lambda0+dlambda);
    }else{
      BWmin=-FLT_MAX;
      BWmax=FLT_MAX;
    }

    t_Table *T=&(prms.T);
    for (i=0;i<prms.T.rows-1;i++){
      double I,e;
      e=T->data[i*T->columns];
      if (brilliance){
        /*unit is brilliance - not raw flux per unit angle and source area*/
        /*correct for the non-uniform energy binning*/
        I=(1.0/(T->data[i*T->columns]*0.01))*((T->data[i*T->columns+2]+T->data[(i+1)*T->columns+2])/2.0)*(T->data[(i+1)*T->columns]-T->data[i*T->columns]);
        if (e<BWmax && e>BWmin){
          prms.pnum+=I;
        }
        prms.pint+=I;
      }else{
        I=((T->data[i*T->columns+2]+T->data[(i+1)*T->columns+2])/2.0)*(T->data[(i+1)*T->columns]-T->data[i*T->columns]);
        if (e<BWmax && e>BWmin){
          prms.pnum+=I;
        }
        prms.pint+=I;
      }
    }

    printf("%s: Integrated intensity radiated is %g pht/s\n",NAME_CURRENT_COMP,prms.pint);
    printf("%s: Intensity radiated in chosen Energy/wavelength window is %g pht/s\n",NAME_CURRENT_COMP,prms.pnum);
    
    if(E0) printf("%s: E0!=0 -> assuming intensity spectrum is parametrized by energy [keV]\n",NAME_CURRENT_COMP);
  } else if (!E0 && !lambda0){
    fprintf(stderr,"Error (%s): Must specify either wavelength or energy distribution\n",NAME_CURRENT_COMP);
    exit(1);
  }
  /*calculate the X-ray weight from the flux*/
  if (flux){//pmul=flux;
    prms.pmul=flux*1.0/(double)mcget_ncount(); 
  }else{
    prms.pmul=1.0/(double)mcget_ncount();
  }
  /*adjust for BW window*/
  prms.pmul*=prms.pnum/prms.pint;

  /*Beam's footprint at a dist calculation*/
  spX=sqrt(sig_x*sig_x+sigPr_x*sigPr_x*dist*dist);
  spY=sqrt(sig_y*sig_y+sigPr_y*sigPr_y*dist*dist);

  uniform_sampling=0;
  if (focus_xw && focus_yh){
    /*adjust for a focusing window*/
    prms.pmul*=erf(focus_xw*0.5*M_SQRT1_2/spX)*erf(focus_yh*0.5*M_SQRT1_2/spY);
    if ( focus_xw<spX || focus_yh<spY){
      /*use a uniform sampling scheme for more efficient sampling by adjusting weights*/
      uniform_sampling=1;
    }
  }


}
#line 10624 "Test_src_gaussian.c"
#undef phase
#undef dlambda
#undef lambda0
#undef dE
#undef E0
#undef focus_yh
#undef focus_xw
#undef gauss
#undef dist
#undef brilliance
#undef flux
#undef sigPr_y
#undef sigPr_x
#undef sig_y
#undef sig_x
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component emon00. */
  SIG_MESSAGE("emon00 (Init)");
#define mccompcurname  emon00
#define mccompcurtype  E_monitor
#define mccompcurindex 6
#define nE mccemon00_nE
#define filename mccemon00_filename
#define E_N mccemon00_E_N
#define E_p mccemon00_E_p
#define E_p2 mccemon00_E_p2
#define xmin mccemon00_xmin
#define xmax mccemon00_xmax
#define ymin mccemon00_ymin
#define ymax mccemon00_ymax
#define xwidth mccemon00_xwidth
#define yheight mccemon00_yheight
#define Emin mccemon00_Emin
#define Emax mccemon00_Emax
#define restore_xray mccemon00_restore_xray
#line 64 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ((xmin >= xmax) || (ymin >= ymax)) {
            printf("E_monitor: %s: Null detection area !\n"
                   "ERROR      (xwidth,yheight,xmin,xmax,ymin,ymax). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }

    for (i=0; i<nE; i++)
    {
      E_N[i] = 0;
      E_p[i] = 0;
      E_p2[i] = 0;
    }
}
#line 10688 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component emon02. */
  SIG_MESSAGE("emon02 (Init)");
#define mccompcurname  emon02
#define mccompcurtype  E_monitor
#define mccompcurindex 7
#define nE mccemon02_nE
#define filename mccemon02_filename
#define E_N mccemon02_E_N
#define E_p mccemon02_E_p
#define E_p2 mccemon02_E_p2
#define xmin mccemon02_xmin
#define xmax mccemon02_xmax
#define ymin mccemon02_ymin
#define ymax mccemon02_ymax
#define xwidth mccemon02_xwidth
#define yheight mccemon02_yheight
#define Emin mccemon02_Emin
#define Emax mccemon02_Emax
#define restore_xray mccemon02_restore_xray
#line 64 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ((xmin >= xmax) || (ymin >= ymax)) {
            printf("E_monitor: %s: Null detection area !\n"
                   "ERROR      (xwidth,yheight,xmin,xmax,ymin,ymax). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }

    for (i=0; i<nE; i++)
    {
      E_N[i] = 0;
      E_p[i] = 0;
      E_p2[i] = 0;
    }
}
#line 10747 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component emon01. */
  SIG_MESSAGE("emon01 (Init)");
#define mccompcurname  emon01
#define mccompcurtype  E_monitor
#define mccompcurindex 8
#define nE mccemon01_nE
#define filename mccemon01_filename
#define E_N mccemon01_E_N
#define E_p mccemon01_E_p
#define E_p2 mccemon01_E_p2
#define xmin mccemon01_xmin
#define xmax mccemon01_xmax
#define ymin mccemon01_ymin
#define ymax mccemon01_ymax
#define xwidth mccemon01_xwidth
#define yheight mccemon01_yheight
#define Emin mccemon01_Emin
#define Emax mccemon01_Emax
#define restore_xray mccemon01_restore_xray
#line 64 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ((xmin >= xmax) || (ymin >= ymax)) {
            printf("E_monitor: %s: Null detection area !\n"
                   "ERROR      (xwidth,yheight,xmin,xmax,ymin,ymax). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }

    for (i=0; i<nE; i++)
    {
      E_N[i] = 0;
      E_p[i] = 0;
      E_p2[i] = 0;
    }
}
#line 10806 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component emon0. */
  SIG_MESSAGE("emon0 (Init)");
#define mccompcurname  emon0
#define mccompcurtype  E_monitor
#define mccompcurindex 9
#define nE mccemon0_nE
#define filename mccemon0_filename
#define E_N mccemon0_E_N
#define E_p mccemon0_E_p
#define E_p2 mccemon0_E_p2
#define xmin mccemon0_xmin
#define xmax mccemon0_xmax
#define ymin mccemon0_ymin
#define ymax mccemon0_ymax
#define xwidth mccemon0_xwidth
#define yheight mccemon0_yheight
#define Emin mccemon0_Emin
#define Emax mccemon0_Emax
#define restore_xray mccemon0_restore_xray
#line 64 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ((xmin >= xmax) || (ymin >= ymax)) {
            printf("E_monitor: %s: Null detection area !\n"
                   "ERROR      (xwidth,yheight,xmin,xmax,ymin,ymax). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }

    for (i=0; i<nE; i++)
    {
      E_N[i] = 0;
      E_p[i] = 0;
      E_p2[i] = 0;
    }
}
#line 10865 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component lmon0. */
  SIG_MESSAGE("lmon0 (Init)");
#define mccompcurname  lmon0
#define mccompcurtype  L_monitor
#define mccompcurindex 10
#define nL mcclmon0_nL
#define filename mcclmon0_filename
#define L_N mcclmon0_L_N
#define L_p mcclmon0_L_p
#define L_p2 mcclmon0_L_p2
#define xmin mcclmon0_xmin
#define xmax mcclmon0_xmax
#define ymin mcclmon0_ymin
#define ymax mcclmon0_ymax
#define xwidth mcclmon0_xwidth
#define yheight mcclmon0_yheight
#define Lmin mcclmon0_Lmin
#define Lmax mcclmon0_Lmax
#define restore_xray mcclmon0_restore_xray
#line 63 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
    int i;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ((xmin >= xmax) || (ymin >= ymax)) {
            printf("L_monitor: %s: Null detection area !\n"
                   "ERROR      (xwidth,yheight,xmin,xmax,ymin,ymax). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }

    for (i=0; i<nL; i++)
    {
      L_N[i] = 0;
      L_p[i] = 0;
      L_p2[i] = 0;
    }
}
#line 10924 "Test_src_gaussian.c"
#undef restore_xray
#undef Lmax
#undef Lmin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component psd0. */
  SIG_MESSAGE("psd0 (Init)");
#define mccompcurname  psd0
#define mccompcurtype  PSD_monitor
#define mccompcurindex 11
#define nx mccpsd0_nx
#define ny mccpsd0_ny
#define nr mccpsd0_nr
#define filename mccpsd0_filename
#define restore_xray mccpsd0_restore_xray
#define PSD_N mccpsd0_PSD_N
#define PSD_p mccpsd0_PSD_p
#define PSD_p2 mccpsd0_PSD_p2
#define xmin mccpsd0_xmin
#define xmax mccpsd0_xmax
#define ymin mccpsd0_ymin
#define ymax mccpsd0_ymax
#define xwidth mccpsd0_xwidth
#define yheight mccpsd0_yheight
#define radius mccpsd0_radius
#line 67 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
    int i,j;
    double *p1,*p2,*p3;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ( ((xmin >= xmax) || (ymin >= ymax)) && !radius ) {
            printf("PSD_monitor: %s: Null detection area !\n"
                "ERROR        (xwidth,yheight,xmin,xmax,ymin,ymax,radius). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }
    if(!radius){
      p1=calloc(nx*ny,sizeof(double));
      p2=calloc(nx*ny,sizeof(double));
      p3=calloc(nx*ny,sizeof(double));
      
      PSD_N=calloc(nx,sizeof(double *));
      PSD_p=calloc(nx,sizeof(double *));
      PSD_p2=calloc(nx,sizeof(double *));

      for (i=0; i<nx; i++){
        PSD_N[i]=&(p1[i*ny]);//calloc(ny,sizeof(double));
        PSD_p[i]=&(p2[i*ny]);//calloc(ny,sizeof(double));
        PSD_p2[i]=&(p3[i*ny]);//calloc(ny,sizeof(double));
      }
    }else{
      PSD_N=calloc(1,sizeof(double *));
      PSD_p=calloc(1,sizeof(double *));
      PSD_p2=calloc(1,sizeof(double *));
      *PSD_N=calloc(nr,sizeof(double));
      *PSD_p=calloc(nr,sizeof(double));
      *PSD_p2=calloc(nr,sizeof(double));
    }

}
#line 11001 "Test_src_gaussian.c"
#undef radius
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component emon1. */
  SIG_MESSAGE("emon1 (Init)");
#define mccompcurname  emon1
#define mccompcurtype  E_monitor
#define mccompcurindex 12
#define nE mccemon1_nE
#define filename mccemon1_filename
#define E_N mccemon1_E_N
#define E_p mccemon1_E_p
#define E_p2 mccemon1_E_p2
#define xmin mccemon1_xmin
#define xmax mccemon1_xmax
#define ymin mccemon1_ymin
#define ymax mccemon1_ymax
#define xwidth mccemon1_xwidth
#define yheight mccemon1_yheight
#define Emin mccemon1_Emin
#define Emax mccemon1_Emax
#define restore_xray mccemon1_restore_xray
#line 64 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ((xmin >= xmax) || (ymin >= ymax)) {
            printf("E_monitor: %s: Null detection area !\n"
                   "ERROR      (xwidth,yheight,xmin,xmax,ymin,ymax). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }

    for (i=0; i<nE; i++)
    {
      E_N[i] = 0;
      E_p[i] = 0;
      E_p2[i] = 0;
    }
}
#line 11061 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component lmon1. */
  SIG_MESSAGE("lmon1 (Init)");
#define mccompcurname  lmon1
#define mccompcurtype  L_monitor
#define mccompcurindex 13
#define nL mcclmon1_nL
#define filename mcclmon1_filename
#define L_N mcclmon1_L_N
#define L_p mcclmon1_L_p
#define L_p2 mcclmon1_L_p2
#define xmin mcclmon1_xmin
#define xmax mcclmon1_xmax
#define ymin mcclmon1_ymin
#define ymax mcclmon1_ymax
#define xwidth mcclmon1_xwidth
#define yheight mcclmon1_yheight
#define Lmin mcclmon1_Lmin
#define Lmax mcclmon1_Lmax
#define restore_xray mcclmon1_restore_xray
#line 63 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
    int i;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ((xmin >= xmax) || (ymin >= ymax)) {
            printf("L_monitor: %s: Null detection area !\n"
                   "ERROR      (xwidth,yheight,xmin,xmax,ymin,ymax). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }

    for (i=0; i<nL; i++)
    {
      L_N[i] = 0;
      L_p[i] = 0;
      L_p2[i] = 0;
    }
}
#line 11120 "Test_src_gaussian.c"
#undef restore_xray
#undef Lmax
#undef Lmin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component psd1. */
  SIG_MESSAGE("psd1 (Init)");
#define mccompcurname  psd1
#define mccompcurtype  PSD_monitor
#define mccompcurindex 14
#define nx mccpsd1_nx
#define ny mccpsd1_ny
#define nr mccpsd1_nr
#define filename mccpsd1_filename
#define restore_xray mccpsd1_restore_xray
#define PSD_N mccpsd1_PSD_N
#define PSD_p mccpsd1_PSD_p
#define PSD_p2 mccpsd1_PSD_p2
#define xmin mccpsd1_xmin
#define xmax mccpsd1_xmax
#define ymin mccpsd1_ymin
#define ymax mccpsd1_ymax
#define xwidth mccpsd1_xwidth
#define yheight mccpsd1_yheight
#define radius mccpsd1_radius
#line 67 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
    int i,j;
    double *p1,*p2,*p3;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ( ((xmin >= xmax) || (ymin >= ymax)) && !radius ) {
            printf("PSD_monitor: %s: Null detection area !\n"
                "ERROR        (xwidth,yheight,xmin,xmax,ymin,ymax,radius). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }
    if(!radius){
      p1=calloc(nx*ny,sizeof(double));
      p2=calloc(nx*ny,sizeof(double));
      p3=calloc(nx*ny,sizeof(double));
      
      PSD_N=calloc(nx,sizeof(double *));
      PSD_p=calloc(nx,sizeof(double *));
      PSD_p2=calloc(nx,sizeof(double *));

      for (i=0; i<nx; i++){
        PSD_N[i]=&(p1[i*ny]);//calloc(ny,sizeof(double));
        PSD_p[i]=&(p2[i*ny]);//calloc(ny,sizeof(double));
        PSD_p2[i]=&(p3[i*ny]);//calloc(ny,sizeof(double));
      }
    }else{
      PSD_N=calloc(1,sizeof(double *));
      PSD_p=calloc(1,sizeof(double *));
      PSD_p2=calloc(1,sizeof(double *));
      *PSD_N=calloc(nr,sizeof(double));
      *PSD_p=calloc(nr,sizeof(double));
      *PSD_p2=calloc(nr,sizeof(double));
    }

}
#line 11197 "Test_src_gaussian.c"
#undef radius
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component slit. */
  SIG_MESSAGE("slit (Init)");
#define mccompcurname  slit
#define mccompcurtype  Slit
#define mccompcurindex 15
#define xmin mccslit_xmin
#define xmax mccslit_xmax
#define ymin mccslit_ymin
#define ymax mccslit_ymax
#define radius mccslit_radius
#define cut mccslit_cut
#define xwidth mccslit_xwidth
#define yheight mccslit_yheight
#define dist mccslit_dist
#define focus_xw mccslit_focus_xw
#define focus_yh mccslit_focus_yh
#define focus_x0 mccslit_focus_x0
#define focus_y0 mccslit_focus_y0
#line 57 "/usr/local/lib/mcxtrace-x.y.z/optics/Slit.comp"
{
  if (xwidth)  { xmax=xwidth/2;  xmin=-xmax; }
  if (yheight) { ymax=yheight/2; ymin=-ymax; }
  if (xmin == 0 && xmax == 0 && ymin == 0 && ymax == 0 && radius == 0)
    { fprintf(stderr,"Slit: %s: Error: give geometry\n", NAME_CURRENT_COMP); exit(-1); }

  if ( (focus_xw || focus_yh || dist) && !( focus_xw && focus_yh && dist) ){
    fprintf(stderr,"Error (%s): Inconsistent target definition\n",NAME_CURRENT_COMP);
    exit(-1);
  }

}
#line 11248 "Test_src_gaussian.c"
#undef focus_y0
#undef focus_x0
#undef focus_yh
#undef focus_xw
#undef dist
#undef yheight
#undef xwidth
#undef cut
#undef radius
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component emon2. */
  SIG_MESSAGE("emon2 (Init)");
#define mccompcurname  emon2
#define mccompcurtype  E_monitor
#define mccompcurindex 16
#define nE mccemon2_nE
#define filename mccemon2_filename
#define E_N mccemon2_E_N
#define E_p mccemon2_E_p
#define E_p2 mccemon2_E_p2
#define xmin mccemon2_xmin
#define xmax mccemon2_xmax
#define ymin mccemon2_ymin
#define ymax mccemon2_ymax
#define xwidth mccemon2_xwidth
#define yheight mccemon2_yheight
#define Emin mccemon2_Emin
#define Emax mccemon2_Emax
#define restore_xray mccemon2_restore_xray
#line 64 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ((xmin >= xmax) || (ymin >= ymax)) {
            printf("E_monitor: %s: Null detection area !\n"
                   "ERROR      (xwidth,yheight,xmin,xmax,ymin,ymax). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }

    for (i=0; i<nE; i++)
    {
      E_N[i] = 0;
      E_p[i] = 0;
      E_p2[i] = 0;
    }
}
#line 11306 "Test_src_gaussian.c"
#undef restore_xray
#undef Emax
#undef Emin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component lmon2. */
  SIG_MESSAGE("lmon2 (Init)");
#define mccompcurname  lmon2
#define mccompcurtype  L_monitor
#define mccompcurindex 17
#define nL mcclmon2_nL
#define filename mcclmon2_filename
#define L_N mcclmon2_L_N
#define L_p mcclmon2_L_p
#define L_p2 mcclmon2_L_p2
#define xmin mcclmon2_xmin
#define xmax mcclmon2_xmax
#define ymin mcclmon2_ymin
#define ymax mcclmon2_ymax
#define xwidth mcclmon2_xwidth
#define yheight mcclmon2_yheight
#define Lmin mcclmon2_Lmin
#define Lmax mcclmon2_Lmax
#define restore_xray mcclmon2_restore_xray
#line 63 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
    int i;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ((xmin >= xmax) || (ymin >= ymax)) {
            printf("L_monitor: %s: Null detection area !\n"
                   "ERROR      (xwidth,yheight,xmin,xmax,ymin,ymax). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }

    for (i=0; i<nL; i++)
    {
      L_N[i] = 0;
      L_p[i] = 0;
      L_p2[i] = 0;
    }
}
#line 11365 "Test_src_gaussian.c"
#undef restore_xray
#undef Lmax
#undef Lmin
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* Initializations for component psd2. */
  SIG_MESSAGE("psd2 (Init)");
#define mccompcurname  psd2
#define mccompcurtype  PSD_monitor
#define mccompcurindex 18
#define nx mccpsd2_nx
#define ny mccpsd2_ny
#define nr mccpsd2_nr
#define filename mccpsd2_filename
#define restore_xray mccpsd2_restore_xray
#define PSD_N mccpsd2_PSD_N
#define PSD_p mccpsd2_PSD_p
#define PSD_p2 mccpsd2_PSD_p2
#define xmin mccpsd2_xmin
#define xmax mccpsd2_xmax
#define ymin mccpsd2_ymin
#define ymax mccpsd2_ymax
#define xwidth mccpsd2_xwidth
#define yheight mccpsd2_yheight
#define radius mccpsd2_radius
#line 67 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
    int i,j;
    double *p1,*p2,*p3;

    if (xwidth  > 0) { xmax = xwidth/2;  xmin = -xmax; }
    if (yheight > 0) { ymax = yheight/2; ymin = -ymax; }

    if ( ((xmin >= xmax) || (ymin >= ymax)) && !radius ) {
            printf("PSD_monitor: %s: Null detection area !\n"
                "ERROR        (xwidth,yheight,xmin,xmax,ymin,ymax,radius). Exiting",
           NAME_CURRENT_COMP);
      exit(0);
    }
    if(!radius){
      p1=calloc(nx*ny,sizeof(double));
      p2=calloc(nx*ny,sizeof(double));
      p3=calloc(nx*ny,sizeof(double));
      
      PSD_N=calloc(nx,sizeof(double *));
      PSD_p=calloc(nx,sizeof(double *));
      PSD_p2=calloc(nx,sizeof(double *));

      for (i=0; i<nx; i++){
        PSD_N[i]=&(p1[i*ny]);//calloc(ny,sizeof(double));
        PSD_p[i]=&(p2[i*ny]);//calloc(ny,sizeof(double));
        PSD_p2[i]=&(p3[i*ny]);//calloc(ny,sizeof(double));
      }
    }else{
      PSD_N=calloc(1,sizeof(double *));
      PSD_p=calloc(1,sizeof(double *));
      PSD_p2=calloc(1,sizeof(double *));
      *PSD_N=calloc(nr,sizeof(double));
      *PSD_p=calloc(nr,sizeof(double));
      *PSD_p2=calloc(nr,sizeof(double));
    }

}
#line 11442 "Test_src_gaussian.c"
#undef radius
#undef yheight
#undef xwidth
#undef ymax
#undef ymin
#undef xmax
#undef xmin
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

    if(mcdotrace) mcdisplay();
    mcDEBUG_INSTR_END()
  }

/* NeXus support */

#ifdef USE_NEXUS

strncmp(mcnxversion,"5 zip",128);

#endif

} /* end init */

void mcraytrace(void) {
  /* Copy xray state to local variables. */
  MCNUM mcnlx = mcnx;
  MCNUM mcnly = mcny;
  MCNUM mcnlz = mcnz;
  MCNUM mcnlkx = mcnkx;
  MCNUM mcnlky = mcnky;
  MCNUM mcnlkz = mcnkz;
  MCNUM mcnlphi = mcnphi;
  MCNUM mcnlt = mcnt;
  MCNUM mcnlEx = mcnEx;
  MCNUM mcnlEy = mcnEy;
  MCNUM mcnlEz = mcnEz;
  MCNUM mcnlp = mcnp;

  mcDEBUG_ENTER()
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define mcabsorb mcabsorbAll
  /* TRACE Component Origin [1] */
  mccoordschange(mcposrOrigin, mcrotrOrigin,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component Origin (without coords transformations) */
  mcJumpTrace_Origin:
  SIG_MESSAGE("Origin (Trace)");
  mcDEBUG_COMP("Origin")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(1,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[1]++;
  mcPCounter[1] += p;
  mcP2Counter[1] += p*p;
#define mccompcurname  Origin
#define mccompcurtype  Progress_bar
#define mccompcurindex 1
#define profile mccOrigin_profile
#define IntermediateCnts mccOrigin_IntermediateCnts
#define StartTime mccOrigin_StartTime
#define EndTime mccOrigin_EndTime
{   /* Declarations of Origin=Progress_bar() SETTING parameters. */
MCNUM percent = mccOrigin_percent;
MCNUM flag_save = mccOrigin_flag_save;
MCNUM minutes = mccOrigin_minutes;
#line 71 "/usr/local/lib/mcxtrace-x.y.z/misc/Progress_bar.comp"
{
  double ncount;
  ncount = mcget_run_num();
  if (!StartTime) {
    time(&StartTime); /* compute starting time */
    IntermediateCnts = 1e3;
  }
  time_t NowTime;
  time(&NowTime);
  if (!EndTime && ncount >= IntermediateCnts) {
    CurrentTime = NowTime;
    if (difftime(NowTime,StartTime) > 10) { /* wait 10 sec before writing ETA */
      EndTime = StartTime + (time_t)(difftime(NowTime,StartTime)
				     *(double)mcget_ncount()/ncount);
      IntermediateCnts = 0;
      fprintf(stdout, "\nTrace ETA ");
      if (difftime(EndTime,StartTime) < 60.0)
        fprintf(stdout, "%g [s] %% ", difftime(EndTime,StartTime));
      else if (difftime(EndTime,StartTime) > 3600.0)
        fprintf(stdout, "%g [h] %% ", difftime(EndTime,StartTime)/3600.0);
      else
        fprintf(stdout, "%g [min] %% ", difftime(EndTime,StartTime)/60.0);
    } else IntermediateCnts += 1e3;
    fflush(stdout);
  }

  if (EndTime &&
    (    (minutes && difftime(NowTime,CurrentTime) > minutes*60)
      || (percent && !minutes && ncount >= IntermediateCnts))   )
  {
    fprintf(stdout, "%d ", (int)(ncount*100/mcget_ncount())); fflush(stdout);
    CurrentTime = NowTime;
    IntermediateCnts = ncount + percent*mcget_ncount()/100;
    if (IntermediateCnts >= mcget_ncount()) fprintf(stdout, "\n");
    if (flag_save) mcsave(NULL);
  }
}
#line 11608 "Test_src_gaussian.c"
}   /* End of Origin=Progress_bar() SETTING parameter declarations. */
#undef EndTime
#undef StartTime
#undef IntermediateCnts
#undef profile
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component src [2] */
  mccoordschange(mcposrsrc, mcrotrsrc,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component src (without coords transformations) */
  mcJumpTrace_src:
  SIG_MESSAGE("src (Trace)");
  mcDEBUG_COMP("src")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(2,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[2]++;
  mcPCounter[2] += p;
  mcP2Counter[2] += p*p;
#define mccompcurname  src
#define mccompcurtype  Source_gaussian
#define mccompcurindex 2
#define spectrum_file mccsrc_spectrum_file
#define prms mccsrc_prms
{   /* Declarations of src=Source_gaussian() SETTING parameters. */
MCNUM sig_x = mccsrc_sig_x;
MCNUM sig_y = mccsrc_sig_y;
MCNUM sigPr_x = mccsrc_sigPr_x;
MCNUM sigPr_y = mccsrc_sigPr_y;
MCNUM flux = mccsrc_flux;
MCNUM dist = mccsrc_dist;
MCNUM gauss = mccsrc_gauss;
MCNUM gamma = mccsrc_gamma;
MCNUM E0 = mccsrc_E0;
MCNUM dE = mccsrc_dE;
MCNUM lambda0 = mccsrc_lambda0;
MCNUM dlambda = mccsrc_dlambda;
MCNUM phase = mccsrc_phase;
/* 'src=Source_gaussian()' component instance has conditional execution */
if (( mcipSRC == 0 ))

#line 117 "/usr/local/lib/mcxtrace-x.y.z/sources/Source_gaussian.comp"
{
  double xx,yy,spX,spY,x1,y1,z1;
  double k,e,l;
  double F1,F2;
  double dx,dy,dz;
  
  // initial source area
  xx=randnorm();
  yy=randnorm();
  x=xx*sig_x;
  y=yy*sig_y;
  z=0;
 
  // Gaussian distribution at origin
  F1=Gauss2D(sig_x,sig_y,x,y,prms.pmul);

  if (spectrum_file){
    double pp=0;
    //while (pp<=0){ 
    l=prms.T.data[0]+ (prms.T.data[(prms.T.rows-1)*prms.T.columns] -prms.T.data[0])*rand01();
    pp=Table_Value(prms.T,l,2);
    //}
    p*=pp;
    /*if E0!=0 convert the tabled value to wavelength*/
    if (E0) {
      l=2*M_PI/(l*E2K);
    }
  }else if (dlambda){
    if (gauss){
      l=lambda0+dlambda*randnorm();
    }else{
      l=randpm1()*dlambda*0.5 + lambda0;
    }
  }else{
    l=lambda0;
  }

  k=(2*M_PI/l); 

  // Beam's footprint at a dist calculation
  spX=sqrt(sig_x*sig_x+sigPr_x*sigPr_x*dist*dist);
  spY=sqrt(sig_y*sig_y+sigPr_y*sigPr_y*dist*dist);
  
  // targeted area calculation
  x1=randnorm()*spX;
  y1=randnorm()*spY;
  z1=dist;
  
  dx=x1-x;
  dy=y1-y;
  dz=sqrt(dx*dx+dy*dy+dist*dist);
  
  kx=(k*dx)/dz;
  ky=(k*dy)/dz;
  kz=(k*dist)/dz;
  
  // Guassian distribution at a distance
  F2=Gauss2D(spX,spY,x1,y1,F1);
  
    /*randomly pick phase*/
  if (phase==-1){
    phi=rand01()*2*M_PI;
  }else{
    phi=phase;
  }

  /*set polarization vector*/
  Ex=0;Ey=0;Ez=0;
  
  p*=erf(F2)*prms.pmul;  
  
}
#line 11794 "Test_src_gaussian.c"
}   /* End of src=Source_gaussian() SETTING parameter declarations. */
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component src2 [3] */
  mccoordschange(mcposrsrc2, mcrotrsrc2,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component src2 (without coords transformations) */
  mcJumpTrace_src2:
  SIG_MESSAGE("src2 (Trace)");
  mcDEBUG_COMP("src2")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(3,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[3]++;
  mcPCounter[3] += p;
  mcP2Counter[3] += p*p;
#define mccompcurname  src2
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 3
#define spectrum_file mccsrc2_spectrum_file
#define prms mccsrc2_prms
#define spX mccsrc2_spX
#define spY mccsrc2_spY
{   /* Declarations of src2=Source_gaussian2() SETTING parameters. */
MCNUM sig_x = mccsrc2_sig_x;
MCNUM sig_y = mccsrc2_sig_y;
MCNUM sigPr_x = mccsrc2_sigPr_x;
MCNUM sigPr_y = mccsrc2_sigPr_y;
MCNUM flux = mccsrc2_flux;
MCNUM brilliance = mccsrc2_brilliance;
MCNUM dist = mccsrc2_dist;
MCNUM gauss = mccsrc2_gauss;
MCNUM focus_xw = mccsrc2_focus_xw;
MCNUM focus_yh = mccsrc2_focus_yh;
MCNUM E0 = mccsrc2_E0;
MCNUM dE = mccsrc2_dE;
MCNUM lambda0 = mccsrc2_lambda0;
MCNUM dlambda = mccsrc2_dlambda;
MCNUM phase = mccsrc2_phase;
/* 'src2=Source_gaussian2()' component instance has conditional execution */
if (( mcipSRC == 1 ))

#line 171 "Source_gaussian2.comp"
{
  double xx,yy,x1,y1,z1;
  double k,e,l;
  double F1=1.0;
  double dx,dy,dz;
  
  // initial source area
  xx=randnorm();
  yy=randnorm();
  x=xx*sig_x;
  y=yy*sig_y;
  z=0;
  p=1.0;
 
  // Gaussian distribution at origin
  //F1=Gauss2D(sig_x,sig_y,x,y,prms.pmul);

  if (spectrum_file){
    double pp=0;
    if (E0) {
      if (dE){
        e=randpm1()*dE+E0;
      }else{
        e=prms.T.data[0]+ (prms.T.data[(prms.T.rows-1)*prms.T.columns] -prms.T.data[0])*rand01();
      }
      //while (pp<=0){ 
      if(brilliance){
        /*correct for brilliance being defined in relative wavelength band to get raw flux*/
        //pp=Table_Value(prms.T,e,2)/(Table_Value(prms.T,e,0)*0.01);
        pp=Table_Value(prms.T,e,2)/(e*0.01);
        //printf("BRILL: %g %g %g\n",Table_Value(prms.T,e,2),Table_Value(prms.T,e,0)*0.01,pp);
      }else{
        pp=Table_Value(prms.T,e,2);
      }
    //}
      k=E2K*e;
    }else{
      if (dlambda){
        l=randpm1()*dlambda+lambda0;
      }else{
        l=prms.T.data[0]+ (prms.T.data[(prms.T.rows-1)*prms.T.columns] -prms.T.data[0])*rand01();
      }
      //while (pp<=0){ 
      if(brilliance){
        /*correct for brilliance being defined in relative wavelength band to get raw flux*/
        pp=Table_Value(prms.T,l,2)/(Table_Value(prms.T,l,0)*0.01);
      }else{
        pp=Table_Value(prms.T,l,2);
      }
      //}
      k=(2*M_PI/l);
    }
    p*=pp;
    //printf("%g\n",p);
    /*if E0!=0 convert the tabled value to */
  }else if (E0){
    if(!dE){
      e=E0;
    }else if (gauss){
      e=E0+dE*randnorm();
    }else{
      e=randpm1()*dE*0.5 + E0;
    }
    k=E2K*e;
  }else if (lambda0){
    if (!dlambda){
      l=lambda0;
    }else if (gauss){
      l=lambda0+dlambda*randnorm();
    }else{
      l=randpm1()*dlambda*0.5 + lambda0;
    }
    k=(2*M_PI/l); 
  }

  // targeted area calculation
  if (focus_xw){
    if (uniform_sampling){
      /*sample uniformly but adjust weight*/
      x1=randpm1()*focus_xw/2.0;
      p*=exp(-(x1*x1)/(2.0*spX*spX));
    }else {
      do {
        x1=randnorm()*spX;
      }while (fabs(x1)>focus_xw/2.0);
    }
  }else{
    x1=randnorm()*spX;
  }
  if (focus_yh){
    if (uniform_sampling){
      /*sample uniformly but adjust weight*/
      y1=randpm1()*focus_yh/2.0;
      p*=exp(-(y1*y1)/(2.0*spY*spY));
    }else {
      do {
        y1=randnorm()*spY;
      }while (fabs(y1)>focus_yh/2.0);
    }
  }else{
    y1=randnorm()*spY;
  }
  z1=dist;
  
  dx=x1-x;
  dy=y1-y;
  dz=sqrt(dx*dx+dy*dy+dist*dist);
  
  kx=(k*dx)/dz;
  ky=(k*dy)/dz;
  kz=(k*dist)/dz;
  
  // Guassian distribution at a distance
  //F2=Gauss2D(spX,spY,x1,y1,F1);
  
    /*randomly pick phase*/
  if (phase==-1){
    phi=rand01()*2*M_PI;
  }else{
    phi=phase;
  }

  /*set polarization vector*/
  Ex=0;Ey=0;Ez=0;
  p*=prms.pmul;
  
}
#line 12037 "Test_src_gaussian.c"
}   /* End of src2=Source_gaussian2() SETTING parameter declarations. */
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component src3 [4] */
  mccoordschange(mcposrsrc3, mcrotrsrc3,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component src3 (without coords transformations) */
  mcJumpTrace_src3:
  SIG_MESSAGE("src3 (Trace)");
  mcDEBUG_COMP("src3")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(4,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[4]++;
  mcPCounter[4] += p;
  mcP2Counter[4] += p*p;
#define mccompcurname  src3
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 4
#define spectrum_file mccsrc3_spectrum_file
#define prms mccsrc3_prms
#define spX mccsrc3_spX
#define spY mccsrc3_spY
{   /* Declarations of src3=Source_gaussian2() SETTING parameters. */
MCNUM sig_x = mccsrc3_sig_x;
MCNUM sig_y = mccsrc3_sig_y;
MCNUM sigPr_x = mccsrc3_sigPr_x;
MCNUM sigPr_y = mccsrc3_sigPr_y;
MCNUM flux = mccsrc3_flux;
MCNUM brilliance = mccsrc3_brilliance;
MCNUM dist = mccsrc3_dist;
MCNUM gauss = mccsrc3_gauss;
MCNUM focus_xw = mccsrc3_focus_xw;
MCNUM focus_yh = mccsrc3_focus_yh;
MCNUM E0 = mccsrc3_E0;
MCNUM dE = mccsrc3_dE;
MCNUM lambda0 = mccsrc3_lambda0;
MCNUM dlambda = mccsrc3_dlambda;
MCNUM phase = mccsrc3_phase;
/* 'src3=Source_gaussian2()' component instance has conditional execution */
if (( mcipSRC == 2 ))

#line 171 "Source_gaussian2.comp"
{
  double xx,yy,x1,y1,z1;
  double k,e,l;
  double F1=1.0;
  double dx,dy,dz;
  
  // initial source area
  xx=randnorm();
  yy=randnorm();
  x=xx*sig_x;
  y=yy*sig_y;
  z=0;
  p=1.0;
 
  // Gaussian distribution at origin
  //F1=Gauss2D(sig_x,sig_y,x,y,prms.pmul);

  if (spectrum_file){
    double pp=0;
    if (E0) {
      if (dE){
        e=randpm1()*dE+E0;
      }else{
        e=prms.T.data[0]+ (prms.T.data[(prms.T.rows-1)*prms.T.columns] -prms.T.data[0])*rand01();
      }
      //while (pp<=0){ 
      if(brilliance){
        /*correct for brilliance being defined in relative wavelength band to get raw flux*/
        //pp=Table_Value(prms.T,e,2)/(Table_Value(prms.T,e,0)*0.01);
        pp=Table_Value(prms.T,e,2)/(e*0.01);
        //printf("BRILL: %g %g %g\n",Table_Value(prms.T,e,2),Table_Value(prms.T,e,0)*0.01,pp);
      }else{
        pp=Table_Value(prms.T,e,2);
      }
    //}
      k=E2K*e;
    }else{
      if (dlambda){
        l=randpm1()*dlambda+lambda0;
      }else{
        l=prms.T.data[0]+ (prms.T.data[(prms.T.rows-1)*prms.T.columns] -prms.T.data[0])*rand01();
      }
      //while (pp<=0){ 
      if(brilliance){
        /*correct for brilliance being defined in relative wavelength band to get raw flux*/
        pp=Table_Value(prms.T,l,2)/(Table_Value(prms.T,l,0)*0.01);
      }else{
        pp=Table_Value(prms.T,l,2);
      }
      //}
      k=(2*M_PI/l);
    }
    p*=pp;
    //printf("%g\n",p);
    /*if E0!=0 convert the tabled value to */
  }else if (E0){
    if(!dE){
      e=E0;
    }else if (gauss){
      e=E0+dE*randnorm();
    }else{
      e=randpm1()*dE*0.5 + E0;
    }
    k=E2K*e;
  }else if (lambda0){
    if (!dlambda){
      l=lambda0;
    }else if (gauss){
      l=lambda0+dlambda*randnorm();
    }else{
      l=randpm1()*dlambda*0.5 + lambda0;
    }
    k=(2*M_PI/l); 
  }

  // targeted area calculation
  if (focus_xw){
    if (uniform_sampling){
      /*sample uniformly but adjust weight*/
      x1=randpm1()*focus_xw/2.0;
      p*=exp(-(x1*x1)/(2.0*spX*spX));
    }else {
      do {
        x1=randnorm()*spX;
      }while (fabs(x1)>focus_xw/2.0);
    }
  }else{
    x1=randnorm()*spX;
  }
  if (focus_yh){
    if (uniform_sampling){
      /*sample uniformly but adjust weight*/
      y1=randpm1()*focus_yh/2.0;
      p*=exp(-(y1*y1)/(2.0*spY*spY));
    }else {
      do {
        y1=randnorm()*spY;
      }while (fabs(y1)>focus_yh/2.0);
    }
  }else{
    y1=randnorm()*spY;
  }
  z1=dist;
  
  dx=x1-x;
  dy=y1-y;
  dz=sqrt(dx*dx+dy*dy+dist*dist);
  
  kx=(k*dx)/dz;
  ky=(k*dy)/dz;
  kz=(k*dist)/dz;
  
  // Guassian distribution at a distance
  //F2=Gauss2D(spX,spY,x1,y1,F1);
  
    /*randomly pick phase*/
  if (phase==-1){
    phi=rand01()*2*M_PI;
  }else{
    phi=phase;
  }

  /*set polarization vector*/
  Ex=0;Ey=0;Ez=0;
  p*=prms.pmul;
  
}
#line 12282 "Test_src_gaussian.c"
}   /* End of src3=Source_gaussian2() SETTING parameter declarations. */
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component src4 [5] */
  mccoordschange(mcposrsrc4, mcrotrsrc4,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component src4 (without coords transformations) */
  mcJumpTrace_src4:
  SIG_MESSAGE("src4 (Trace)");
  mcDEBUG_COMP("src4")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(5,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[5]++;
  mcPCounter[5] += p;
  mcP2Counter[5] += p*p;
#define mccompcurname  src4
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 5
#define spectrum_file mccsrc4_spectrum_file
#define prms mccsrc4_prms
#define spX mccsrc4_spX
#define spY mccsrc4_spY
{   /* Declarations of src4=Source_gaussian2() SETTING parameters. */
MCNUM sig_x = mccsrc4_sig_x;
MCNUM sig_y = mccsrc4_sig_y;
MCNUM sigPr_x = mccsrc4_sigPr_x;
MCNUM sigPr_y = mccsrc4_sigPr_y;
MCNUM flux = mccsrc4_flux;
MCNUM brilliance = mccsrc4_brilliance;
MCNUM dist = mccsrc4_dist;
MCNUM gauss = mccsrc4_gauss;
MCNUM focus_xw = mccsrc4_focus_xw;
MCNUM focus_yh = mccsrc4_focus_yh;
MCNUM E0 = mccsrc4_E0;
MCNUM dE = mccsrc4_dE;
MCNUM lambda0 = mccsrc4_lambda0;
MCNUM dlambda = mccsrc4_dlambda;
MCNUM phase = mccsrc4_phase;
/* 'src4=Source_gaussian2()' component instance has conditional execution */
if (( mcipSRC == 3 ))

#line 171 "Source_gaussian2.comp"
{
  double xx,yy,x1,y1,z1;
  double k,e,l;
  double F1=1.0;
  double dx,dy,dz;
  
  // initial source area
  xx=randnorm();
  yy=randnorm();
  x=xx*sig_x;
  y=yy*sig_y;
  z=0;
  p=1.0;
 
  // Gaussian distribution at origin
  //F1=Gauss2D(sig_x,sig_y,x,y,prms.pmul);

  if (spectrum_file){
    double pp=0;
    if (E0) {
      if (dE){
        e=randpm1()*dE+E0;
      }else{
        e=prms.T.data[0]+ (prms.T.data[(prms.T.rows-1)*prms.T.columns] -prms.T.data[0])*rand01();
      }
      //while (pp<=0){ 
      if(brilliance){
        /*correct for brilliance being defined in relative wavelength band to get raw flux*/
        //pp=Table_Value(prms.T,e,2)/(Table_Value(prms.T,e,0)*0.01);
        pp=Table_Value(prms.T,e,2)/(e*0.01);
        //printf("BRILL: %g %g %g\n",Table_Value(prms.T,e,2),Table_Value(prms.T,e,0)*0.01,pp);
      }else{
        pp=Table_Value(prms.T,e,2);
      }
    //}
      k=E2K*e;
    }else{
      if (dlambda){
        l=randpm1()*dlambda+lambda0;
      }else{
        l=prms.T.data[0]+ (prms.T.data[(prms.T.rows-1)*prms.T.columns] -prms.T.data[0])*rand01();
      }
      //while (pp<=0){ 
      if(brilliance){
        /*correct for brilliance being defined in relative wavelength band to get raw flux*/
        pp=Table_Value(prms.T,l,2)/(Table_Value(prms.T,l,0)*0.01);
      }else{
        pp=Table_Value(prms.T,l,2);
      }
      //}
      k=(2*M_PI/l);
    }
    p*=pp;
    //printf("%g\n",p);
    /*if E0!=0 convert the tabled value to */
  }else if (E0){
    if(!dE){
      e=E0;
    }else if (gauss){
      e=E0+dE*randnorm();
    }else{
      e=randpm1()*dE*0.5 + E0;
    }
    k=E2K*e;
  }else if (lambda0){
    if (!dlambda){
      l=lambda0;
    }else if (gauss){
      l=lambda0+dlambda*randnorm();
    }else{
      l=randpm1()*dlambda*0.5 + lambda0;
    }
    k=(2*M_PI/l); 
  }

  // targeted area calculation
  if (focus_xw){
    if (uniform_sampling){
      /*sample uniformly but adjust weight*/
      x1=randpm1()*focus_xw/2.0;
      p*=exp(-(x1*x1)/(2.0*spX*spX));
    }else {
      do {
        x1=randnorm()*spX;
      }while (fabs(x1)>focus_xw/2.0);
    }
  }else{
    x1=randnorm()*spX;
  }
  if (focus_yh){
    if (uniform_sampling){
      /*sample uniformly but adjust weight*/
      y1=randpm1()*focus_yh/2.0;
      p*=exp(-(y1*y1)/(2.0*spY*spY));
    }else {
      do {
        y1=randnorm()*spY;
      }while (fabs(y1)>focus_yh/2.0);
    }
  }else{
    y1=randnorm()*spY;
  }
  z1=dist;
  
  dx=x1-x;
  dy=y1-y;
  dz=sqrt(dx*dx+dy*dy+dist*dist);
  
  kx=(k*dx)/dz;
  ky=(k*dy)/dz;
  kz=(k*dist)/dz;
  
  // Guassian distribution at a distance
  //F2=Gauss2D(spX,spY,x1,y1,F1);
  
    /*randomly pick phase*/
  if (phase==-1){
    phi=rand01()*2*M_PI;
  }else{
    phi=phase;
  }

  /*set polarization vector*/
  Ex=0;Ey=0;Ez=0;
  p*=prms.pmul;
  
}
#line 12527 "Test_src_gaussian.c"
}   /* End of src4=Source_gaussian2() SETTING parameter declarations. */
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component emon00 [6] */
  mccoordschange(mcposremon00, mcrotremon00,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component emon00 (without coords transformations) */
  mcJumpTrace_emon00:
  SIG_MESSAGE("emon00 (Trace)");
  mcDEBUG_COMP("emon00")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(6,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[6]++;
  mcPCounter[6] += p;
  mcP2Counter[6] += p*p;
#define mccompcurname  emon00
#define mccompcurtype  E_monitor
#define mccompcurindex 6
#define nE mccemon00_nE
#define filename mccemon00_filename
#define E_N mccemon00_E_N
#define E_p mccemon00_E_p
#define E_p2 mccemon00_E_p2
{   /* Declarations of emon00=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon00_xmin;
MCNUM xmax = mccemon00_xmax;
MCNUM ymin = mccemon00_ymin;
MCNUM ymax = mccemon00_ymax;
MCNUM xwidth = mccemon00_xwidth;
MCNUM yheight = mccemon00_yheight;
MCNUM Emin = mccemon00_Emin;
MCNUM Emax = mccemon00_Emax;
MCNUM restore_xray = mccemon00_restore_xray;
#line 85 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;
    double E;

    PROP_Z0;
    if (x>xmin && x<xmax && y>ymin && y<ymax)
    {
      E = K2E*sqrt(kx*kx + ky*ky + kz*kz);

      i = floor((E-Emin)*nE/(Emax-Emin));
      if(i >= 0 && i < nE)
      {
        E_N[i]++;
        E_p[i] += p;
        E_p2[i] += p*p;
        SCATTER;
      }
    }
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 12660 "Test_src_gaussian.c"
}   /* End of emon00=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component emon02 [7] */
  mccoordschange(mcposremon02, mcrotremon02,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component emon02 (without coords transformations) */
  mcJumpTrace_emon02:
  SIG_MESSAGE("emon02 (Trace)");
  mcDEBUG_COMP("emon02")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(7,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[7]++;
  mcPCounter[7] += p;
  mcP2Counter[7] += p*p;
#define mccompcurname  emon02
#define mccompcurtype  E_monitor
#define mccompcurindex 7
#define nE mccemon02_nE
#define filename mccemon02_filename
#define E_N mccemon02_E_N
#define E_p mccemon02_E_p
#define E_p2 mccemon02_E_p2
{   /* Declarations of emon02=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon02_xmin;
MCNUM xmax = mccemon02_xmax;
MCNUM ymin = mccemon02_ymin;
MCNUM ymax = mccemon02_ymax;
MCNUM xwidth = mccemon02_xwidth;
MCNUM yheight = mccemon02_yheight;
MCNUM Emin = mccemon02_Emin;
MCNUM Emax = mccemon02_Emax;
MCNUM restore_xray = mccemon02_restore_xray;
#line 85 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;
    double E;

    PROP_Z0;
    if (x>xmin && x<xmax && y>ymin && y<ymax)
    {
      E = K2E*sqrt(kx*kx + ky*ky + kz*kz);

      i = floor((E-Emin)*nE/(Emax-Emin));
      if(i >= 0 && i < nE)
      {
        E_N[i]++;
        E_p[i] += p;
        E_p2[i] += p*p;
        SCATTER;
      }
    }
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 12794 "Test_src_gaussian.c"
}   /* End of emon02=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component emon01 [8] */
  mccoordschange(mcposremon01, mcrotremon01,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component emon01 (without coords transformations) */
  mcJumpTrace_emon01:
  SIG_MESSAGE("emon01 (Trace)");
  mcDEBUG_COMP("emon01")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(8,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[8]++;
  mcPCounter[8] += p;
  mcP2Counter[8] += p*p;
#define mccompcurname  emon01
#define mccompcurtype  E_monitor
#define mccompcurindex 8
#define nE mccemon01_nE
#define filename mccemon01_filename
#define E_N mccemon01_E_N
#define E_p mccemon01_E_p
#define E_p2 mccemon01_E_p2
{   /* Declarations of emon01=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon01_xmin;
MCNUM xmax = mccemon01_xmax;
MCNUM ymin = mccemon01_ymin;
MCNUM ymax = mccemon01_ymax;
MCNUM xwidth = mccemon01_xwidth;
MCNUM yheight = mccemon01_yheight;
MCNUM Emin = mccemon01_Emin;
MCNUM Emax = mccemon01_Emax;
MCNUM restore_xray = mccemon01_restore_xray;
#line 85 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;
    double E;

    PROP_Z0;
    if (x>xmin && x<xmax && y>ymin && y<ymax)
    {
      E = K2E*sqrt(kx*kx + ky*ky + kz*kz);

      i = floor((E-Emin)*nE/(Emax-Emin));
      if(i >= 0 && i < nE)
      {
        E_N[i]++;
        E_p[i] += p;
        E_p2[i] += p*p;
        SCATTER;
      }
    }
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 12928 "Test_src_gaussian.c"
}   /* End of emon01=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component emon0 [9] */
  mccoordschange(mcposremon0, mcrotremon0,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component emon0 (without coords transformations) */
  mcJumpTrace_emon0:
  SIG_MESSAGE("emon0 (Trace)");
  mcDEBUG_COMP("emon0")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(9,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[9]++;
  mcPCounter[9] += p;
  mcP2Counter[9] += p*p;
#define mccompcurname  emon0
#define mccompcurtype  E_monitor
#define mccompcurindex 9
#define nE mccemon0_nE
#define filename mccemon0_filename
#define E_N mccemon0_E_N
#define E_p mccemon0_E_p
#define E_p2 mccemon0_E_p2
{   /* Declarations of emon0=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon0_xmin;
MCNUM xmax = mccemon0_xmax;
MCNUM ymin = mccemon0_ymin;
MCNUM ymax = mccemon0_ymax;
MCNUM xwidth = mccemon0_xwidth;
MCNUM yheight = mccemon0_yheight;
MCNUM Emin = mccemon0_Emin;
MCNUM Emax = mccemon0_Emax;
MCNUM restore_xray = mccemon0_restore_xray;
#line 85 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;
    double E;

    PROP_Z0;
    if (x>xmin && x<xmax && y>ymin && y<ymax)
    {
      E = K2E*sqrt(kx*kx + ky*ky + kz*kz);

      i = floor((E-Emin)*nE/(Emax-Emin));
      if(i >= 0 && i < nE)
      {
        E_N[i]++;
        E_p[i] += p;
        E_p2[i] += p*p;
        SCATTER;
      }
    }
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 13062 "Test_src_gaussian.c"
}   /* End of emon0=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component lmon0 [10] */
  mccoordschange(mcposrlmon0, mcrotrlmon0,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component lmon0 (without coords transformations) */
  mcJumpTrace_lmon0:
  SIG_MESSAGE("lmon0 (Trace)");
  mcDEBUG_COMP("lmon0")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(10,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[10]++;
  mcPCounter[10] += p;
  mcP2Counter[10] += p*p;
#define mccompcurname  lmon0
#define mccompcurtype  L_monitor
#define mccompcurindex 10
#define nL mcclmon0_nL
#define filename mcclmon0_filename
#define L_N mcclmon0_L_N
#define L_p mcclmon0_L_p
#define L_p2 mcclmon0_L_p2
{   /* Declarations of lmon0=L_monitor() SETTING parameters. */
MCNUM xmin = mcclmon0_xmin;
MCNUM xmax = mcclmon0_xmax;
MCNUM ymin = mcclmon0_ymin;
MCNUM ymax = mcclmon0_ymax;
MCNUM xwidth = mcclmon0_xwidth;
MCNUM yheight = mcclmon0_yheight;
MCNUM Lmin = mcclmon0_Lmin;
MCNUM Lmax = mcclmon0_Lmax;
MCNUM restore_xray = mcclmon0_restore_xray;
#line 84 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
    int i;
    double L;

    PROP_Z0;
    if (x>xmin && x<xmax && y>ymin && y<ymax)
    {
      L = 2*PI/sqrt(kx*kx + ky*ky + kz*kz);
      i = floor((L-Lmin)*nL/(Lmax-Lmin));
      if(i >= 0 && i < nL)
      {
        L_N[i]++;
        L_p[i] += p;
        L_p2[i] += p*p;
        SCATTER;
      }
    } 
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 13195 "Test_src_gaussian.c"
}   /* End of lmon0=L_monitor() SETTING parameter declarations. */
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component psd0 [11] */
  mccoordschange(mcposrpsd0, mcrotrpsd0,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component psd0 (without coords transformations) */
  mcJumpTrace_psd0:
  SIG_MESSAGE("psd0 (Trace)");
  mcDEBUG_COMP("psd0")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(11,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[11]++;
  mcPCounter[11] += p;
  mcP2Counter[11] += p*p;
#define mccompcurname  psd0
#define mccompcurtype  PSD_monitor
#define mccompcurindex 11
#define nx mccpsd0_nx
#define ny mccpsd0_ny
#define nr mccpsd0_nr
#define filename mccpsd0_filename
#define restore_xray mccpsd0_restore_xray
#define PSD_N mccpsd0_PSD_N
#define PSD_p mccpsd0_PSD_p
#define PSD_p2 mccpsd0_PSD_p2
{   /* Declarations of psd0=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd0_xmin;
MCNUM xmax = mccpsd0_xmax;
MCNUM ymin = mccpsd0_ymin;
MCNUM ymax = mccpsd0_ymax;
MCNUM xwidth = mccpsd0_xwidth;
MCNUM yheight = mccpsd0_yheight;
MCNUM radius = mccpsd0_radius;
#line 105 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
    int i,j;

    PROP_Z0;
    if (!radius){
      if (x>xmin && x<xmax && y>ymin && y<ymax)
      {
        i = floor((x - xmin)*nx/(xmax - xmin));
        j = floor((y - ymin)*ny/(ymax - ymin));
        PSD_N[i][j]++;
        PSD_p[i][j] += p;
        PSD_p2[i][j] += p*p;
        SCATTER;
      }
    }else{
      double r=sqrt(x*x+y*y);
      if (r<radius){
        i = floor(r*nr/radius);
        PSD_N[0][i]++;
        PSD_p[0][i] += p;
        PSD_p2[0][i] += p*p;
        SCATTER;
      }
    }
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 13336 "Test_src_gaussian.c"
}   /* End of psd0=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component emon1 [12] */
  mccoordschange(mcposremon1, mcrotremon1,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component emon1 (without coords transformations) */
  mcJumpTrace_emon1:
  SIG_MESSAGE("emon1 (Trace)");
  mcDEBUG_COMP("emon1")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(12,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[12]++;
  mcPCounter[12] += p;
  mcP2Counter[12] += p*p;
#define mccompcurname  emon1
#define mccompcurtype  E_monitor
#define mccompcurindex 12
#define nE mccemon1_nE
#define filename mccemon1_filename
#define E_N mccemon1_E_N
#define E_p mccemon1_E_p
#define E_p2 mccemon1_E_p2
{   /* Declarations of emon1=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon1_xmin;
MCNUM xmax = mccemon1_xmax;
MCNUM ymin = mccemon1_ymin;
MCNUM ymax = mccemon1_ymax;
MCNUM xwidth = mccemon1_xwidth;
MCNUM yheight = mccemon1_yheight;
MCNUM Emin = mccemon1_Emin;
MCNUM Emax = mccemon1_Emax;
MCNUM restore_xray = mccemon1_restore_xray;
#line 85 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;
    double E;

    PROP_Z0;
    if (x>xmin && x<xmax && y>ymin && y<ymax)
    {
      E = K2E*sqrt(kx*kx + ky*ky + kz*kz);

      i = floor((E-Emin)*nE/(Emax-Emin));
      if(i >= 0 && i < nE)
      {
        E_N[i]++;
        E_p[i] += p;
        E_p2[i] += p*p;
        SCATTER;
      }
    }
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 13473 "Test_src_gaussian.c"
}   /* End of emon1=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component lmon1 [13] */
  mccoordschange(mcposrlmon1, mcrotrlmon1,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component lmon1 (without coords transformations) */
  mcJumpTrace_lmon1:
  SIG_MESSAGE("lmon1 (Trace)");
  mcDEBUG_COMP("lmon1")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(13,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[13]++;
  mcPCounter[13] += p;
  mcP2Counter[13] += p*p;
#define mccompcurname  lmon1
#define mccompcurtype  L_monitor
#define mccompcurindex 13
#define nL mcclmon1_nL
#define filename mcclmon1_filename
#define L_N mcclmon1_L_N
#define L_p mcclmon1_L_p
#define L_p2 mcclmon1_L_p2
{   /* Declarations of lmon1=L_monitor() SETTING parameters. */
MCNUM xmin = mcclmon1_xmin;
MCNUM xmax = mcclmon1_xmax;
MCNUM ymin = mcclmon1_ymin;
MCNUM ymax = mcclmon1_ymax;
MCNUM xwidth = mcclmon1_xwidth;
MCNUM yheight = mcclmon1_yheight;
MCNUM Lmin = mcclmon1_Lmin;
MCNUM Lmax = mcclmon1_Lmax;
MCNUM restore_xray = mcclmon1_restore_xray;
#line 84 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
    int i;
    double L;

    PROP_Z0;
    if (x>xmin && x<xmax && y>ymin && y<ymax)
    {
      L = 2*PI/sqrt(kx*kx + ky*ky + kz*kz);
      i = floor((L-Lmin)*nL/(Lmax-Lmin));
      if(i >= 0 && i < nL)
      {
        L_N[i]++;
        L_p[i] += p;
        L_p2[i] += p*p;
        SCATTER;
      }
    } 
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 13606 "Test_src_gaussian.c"
}   /* End of lmon1=L_monitor() SETTING parameter declarations. */
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component psd1 [14] */
  mccoordschange(mcposrpsd1, mcrotrpsd1,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component psd1 (without coords transformations) */
  mcJumpTrace_psd1:
  SIG_MESSAGE("psd1 (Trace)");
  mcDEBUG_COMP("psd1")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(14,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[14]++;
  mcPCounter[14] += p;
  mcP2Counter[14] += p*p;
#define mccompcurname  psd1
#define mccompcurtype  PSD_monitor
#define mccompcurindex 14
#define nx mccpsd1_nx
#define ny mccpsd1_ny
#define nr mccpsd1_nr
#define filename mccpsd1_filename
#define restore_xray mccpsd1_restore_xray
#define PSD_N mccpsd1_PSD_N
#define PSD_p mccpsd1_PSD_p
#define PSD_p2 mccpsd1_PSD_p2
{   /* Declarations of psd1=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd1_xmin;
MCNUM xmax = mccpsd1_xmax;
MCNUM ymin = mccpsd1_ymin;
MCNUM ymax = mccpsd1_ymax;
MCNUM xwidth = mccpsd1_xwidth;
MCNUM yheight = mccpsd1_yheight;
MCNUM radius = mccpsd1_radius;
#line 105 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
    int i,j;

    PROP_Z0;
    if (!radius){
      if (x>xmin && x<xmax && y>ymin && y<ymax)
      {
        i = floor((x - xmin)*nx/(xmax - xmin));
        j = floor((y - ymin)*ny/(ymax - ymin));
        PSD_N[i][j]++;
        PSD_p[i][j] += p;
        PSD_p2[i][j] += p*p;
        SCATTER;
      }
    }else{
      double r=sqrt(x*x+y*y);
      if (r<radius){
        i = floor(r*nr/radius);
        PSD_N[0][i]++;
        PSD_p[0][i] += p;
        PSD_p2[0][i] += p*p;
        SCATTER;
      }
    }
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 13747 "Test_src_gaussian.c"
}   /* End of psd1=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component slit [15] */
  mccoordschange(mcposrslit, mcrotrslit,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component slit (without coords transformations) */
  mcJumpTrace_slit:
  SIG_MESSAGE("slit (Trace)");
  mcDEBUG_COMP("slit")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(15,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[15]++;
  mcPCounter[15] += p;
  mcP2Counter[15] += p*p;
#define mccompcurname  slit
#define mccompcurtype  Slit
#define mccompcurindex 15
{   /* Declarations of slit=Slit() SETTING parameters. */
MCNUM xmin = mccslit_xmin;
MCNUM xmax = mccslit_xmax;
MCNUM ymin = mccslit_ymin;
MCNUM ymax = mccslit_ymax;
MCNUM radius = mccslit_radius;
MCNUM cut = mccslit_cut;
MCNUM xwidth = mccslit_xwidth;
MCNUM yheight = mccslit_yheight;
MCNUM dist = mccslit_dist;
MCNUM focus_xw = mccslit_focus_xw;
MCNUM focus_yh = mccslit_focus_yh;
MCNUM focus_x0 = mccslit_focus_x0;
MCNUM focus_y0 = mccslit_focus_y0;
#line 71 "/usr/local/lib/mcxtrace-x.y.z/optics/Slit.comp"
{
    PROP_Z0;
    if (((radius == 0) && (x<xmin || x>xmax || y<ymin || y>ymax))
    || ((radius != 0) && (x*x + y*y > radius*radius))){
      ABSORB;
    }else{
      if (p < cut)
        ABSORB;
      else{
        SCATTER;
        if ( focus_xw ){
          double posx,posy,posz,pdir,k;
          coords_get(POS_A_CURRENT_COMP,&posx,&posy,&posz);

          /*we have a target behind the slit - so we now consider the ray a Huygens wavelet.*/
          double xf,yf,zf;
          randvec_target_rect_real(&xf, &yf, &zf, &pdir,
              focus_x0-posx,focus_y0-posy,dist, focus_xw, focus_yh, ROT_A_CURRENT_COMP, x, y, z, 0);
          //printf("%g %g %g %g %g    %g %g %g   %g %g\n",xf,yf,zf,pdir,p,x,y,z,focus_x0,focus_y0);
          //p*=pdir;
          k=sqrt(scalar_prod(kx,ky,kz,kx,ky,kz));
          kx=(xf-x); ky=(yf-y); kz=(zf-z);
          NORM(kx,ky,kz);
          kx*=k;ky*=k;kz*=k;
        }
      }

    }
}
#line 13890 "Test_src_gaussian.c"
}   /* End of slit=Slit() SETTING parameter declarations. */
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component emon2 [16] */
  mccoordschange(mcposremon2, mcrotremon2,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component emon2 (without coords transformations) */
  mcJumpTrace_emon2:
  SIG_MESSAGE("emon2 (Trace)");
  mcDEBUG_COMP("emon2")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(16,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[16]++;
  mcPCounter[16] += p;
  mcP2Counter[16] += p*p;
#define mccompcurname  emon2
#define mccompcurtype  E_monitor
#define mccompcurindex 16
#define nE mccemon2_nE
#define filename mccemon2_filename
#define E_N mccemon2_E_N
#define E_p mccemon2_E_p
#define E_p2 mccemon2_E_p2
{   /* Declarations of emon2=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon2_xmin;
MCNUM xmax = mccemon2_xmax;
MCNUM ymin = mccemon2_ymin;
MCNUM ymax = mccemon2_ymax;
MCNUM xwidth = mccemon2_xwidth;
MCNUM yheight = mccemon2_yheight;
MCNUM Emin = mccemon2_Emin;
MCNUM Emax = mccemon2_Emax;
MCNUM restore_xray = mccemon2_restore_xray;
#line 85 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    int i;
    double E;

    PROP_Z0;
    if (x>xmin && x<xmax && y>ymin && y<ymax)
    {
      E = K2E*sqrt(kx*kx + ky*ky + kz*kz);

      i = floor((E-Emin)*nE/(Emax-Emin));
      if(i >= 0 && i < nE)
      {
        E_N[i]++;
        E_p[i] += p;
        E_p2[i] += p*p;
        SCATTER;
      }
    }
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 14019 "Test_src_gaussian.c"
}   /* End of emon2=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component lmon2 [17] */
  mccoordschange(mcposrlmon2, mcrotrlmon2,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component lmon2 (without coords transformations) */
  mcJumpTrace_lmon2:
  SIG_MESSAGE("lmon2 (Trace)");
  mcDEBUG_COMP("lmon2")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(17,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[17]++;
  mcPCounter[17] += p;
  mcP2Counter[17] += p*p;
#define mccompcurname  lmon2
#define mccompcurtype  L_monitor
#define mccompcurindex 17
#define nL mcclmon2_nL
#define filename mcclmon2_filename
#define L_N mcclmon2_L_N
#define L_p mcclmon2_L_p
#define L_p2 mcclmon2_L_p2
{   /* Declarations of lmon2=L_monitor() SETTING parameters. */
MCNUM xmin = mcclmon2_xmin;
MCNUM xmax = mcclmon2_xmax;
MCNUM ymin = mcclmon2_ymin;
MCNUM ymax = mcclmon2_ymax;
MCNUM xwidth = mcclmon2_xwidth;
MCNUM yheight = mcclmon2_yheight;
MCNUM Lmin = mcclmon2_Lmin;
MCNUM Lmax = mcclmon2_Lmax;
MCNUM restore_xray = mcclmon2_restore_xray;
#line 84 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
    int i;
    double L;

    PROP_Z0;
    if (x>xmin && x<xmax && y>ymin && y<ymax)
    {
      L = 2*PI/sqrt(kx*kx + ky*ky + kz*kz);
      i = floor((L-Lmin)*nL/(Lmax-Lmin));
      if(i >= 0 && i < nL)
      {
        L_N[i]++;
        L_p[i] += p;
        L_p2[i] += p*p;
        SCATTER;
      }
    } 
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 14152 "Test_src_gaussian.c"
}   /* End of lmon2=L_monitor() SETTING parameter declarations. */
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  /* TRACE Component psd2 [18] */
  mccoordschange(mcposrpsd2, mcrotrpsd2,
    &mcnlx,
    &mcnly,
    &mcnlz,
    &mcnlkx,
    &mcnlky,
    &mcnlkz,
    &mcnlEx,
    &mcnlEy,
    &mcnlEz);
  /* define label inside component psd2 (without coords transformations) */
  mcJumpTrace_psd2:
  SIG_MESSAGE("psd2 (Trace)");
  mcDEBUG_COMP("psd2")
  mcDEBUG_STATE(
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp)
#define x mcnlx
#define y mcnly
#define z mcnlz
#define kx mcnlkx
#define ky mcnlky
#define kz mcnlkz
#define phi mcnlphi
#define t mcnlt
#define Ex mcnlEx
#define Ey mcnlEy
#define Ez mcnlEz
#define p mcnlp
  STORE_XRAY(18,
    mcnlx,
    mcnly,
    mcnlz,
    mcnlkx,
    mcnlky,
    mcnlkz,
    mcnlphi,
    mcnlt,
    mcnlEx,
    mcnlEy,
    mcnlEz,
    mcnlp);
  mcScattered=0;
  mcNCounter[18]++;
  mcPCounter[18] += p;
  mcP2Counter[18] += p*p;
#define mccompcurname  psd2
#define mccompcurtype  PSD_monitor
#define mccompcurindex 18
#define nx mccpsd2_nx
#define ny mccpsd2_ny
#define nr mccpsd2_nr
#define filename mccpsd2_filename
#define restore_xray mccpsd2_restore_xray
#define PSD_N mccpsd2_PSD_N
#define PSD_p mccpsd2_PSD_p
#define PSD_p2 mccpsd2_PSD_p2
{   /* Declarations of psd2=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd2_xmin;
MCNUM xmax = mccpsd2_xmax;
MCNUM ymin = mccpsd2_ymin;
MCNUM ymax = mccpsd2_ymax;
MCNUM xwidth = mccpsd2_xwidth;
MCNUM yheight = mccpsd2_yheight;
MCNUM radius = mccpsd2_radius;
#line 105 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
    int i,j;

    PROP_Z0;
    if (!radius){
      if (x>xmin && x<xmax && y>ymin && y<ymax)
      {
        i = floor((x - xmin)*nx/(xmax - xmin));
        j = floor((y - ymin)*ny/(ymax - ymin));
        PSD_N[i][j]++;
        PSD_p[i][j] += p;
        PSD_p2[i][j] += p*p;
        SCATTER;
      }
    }else{
      double r=sqrt(x*x+y*y);
      if (r<radius){
        i = floor(r*nr/radius);
        PSD_N[0][i]++;
        PSD_p[0][i] += p;
        PSD_p2[0][i] += p*p;
        SCATTER;
      }
    }
    if (restore_xray) {
      RESTORE_XRAY(INDEX_CURRENT_COMP, x, y, z, kx, ky, kz, phi, t, Ex, Ey, Ez, p);
    }
}
#line 14293 "Test_src_gaussian.c"
}   /* End of psd2=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex
#undef p
#undef Ez
#undef Ey
#undef Ex
#undef t
#undef phi
#undef kz
#undef ky
#undef kx
#undef z
#undef y
#undef x
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)

  mcabsorbAll:
  mcDEBUG_LEAVE()
  mcDEBUG_STATE(
mcnlx,
mcnly,
mcnlz,
mcnlkx,
mcnlky,
mcnlkz,
mcnlphi,
mcnlt,
mcnlEx,
mcnlEy,
mcnlEz,
mcnlp)
  /* Copy xray state to global variables. */
  mcnx = mcnlx;
  mcny = mcnly;
  mcnz = mcnlz;
  mcnkx = mcnlkx;
  mcnky = mcnlky;
  mcnkz = mcnlkz;
  mcnphi = mcnlphi;
  mcnt = mcnlt;
  mcnEx = mcnlEx;
  mcnEy = mcnlEy;
  mcnEz = mcnlEz;
  mcnp = mcnlp;

} /* end trace */

void mcsave(FILE *handle) {
  if (!handle) mcsiminfo_init(NULL);
  /* User component SAVE code. */

  /* User SAVE code for component 'Origin'. */
  SIG_MESSAGE("Origin (Save)");
#define mccompcurname  Origin
#define mccompcurtype  Progress_bar
#define mccompcurindex 1
#define profile mccOrigin_profile
#define IntermediateCnts mccOrigin_IntermediateCnts
#define StartTime mccOrigin_StartTime
#define EndTime mccOrigin_EndTime
{   /* Declarations of Origin=Progress_bar() SETTING parameters. */
MCNUM percent = mccOrigin_percent;
MCNUM flag_save = mccOrigin_flag_save;
MCNUM minutes = mccOrigin_minutes;
#line 110 "/usr/local/lib/mcxtrace-x.y.z/misc/Progress_bar.comp"
{
  MPI_MASTER(fprintf(stdout, "\nSave [%s]\n", mcinstrument_name););
  if (profile && strlen(profile)) {
    char filename[256];
    if (!strlen(profile)) strcpy(filename, mcinstrument_name);
    else strcpy(filename, profile);
    DETECTOR_OUT_1D(
        "Intensity profiler",
        "Component index [1]",
        "Intensity",
        "prof", 1, mcNUMCOMP, mcNUMCOMP-1,
        &mcNCounter[1],&mcPCounter[1],&mcP2Counter[1],
        filename);

  }
}
#line 14397 "Test_src_gaussian.c"
}   /* End of Origin=Progress_bar() SETTING parameter declarations. */
#undef EndTime
#undef StartTime
#undef IntermediateCnts
#undef profile
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'emon00'. */
  SIG_MESSAGE("emon00 (Save)");
#define mccompcurname  emon00
#define mccompcurtype  E_monitor
#define mccompcurindex 6
#define nE mccemon00_nE
#define filename mccemon00_filename
#define E_N mccemon00_E_N
#define E_p mccemon00_E_p
#define E_p2 mccemon00_E_p2
{   /* Declarations of emon00=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon00_xmin;
MCNUM xmax = mccemon00_xmax;
MCNUM ymin = mccemon00_ymin;
MCNUM ymax = mccemon00_ymax;
MCNUM xwidth = mccemon00_xwidth;
MCNUM yheight = mccemon00_yheight;
MCNUM Emin = mccemon00_Emin;
MCNUM Emax = mccemon00_Emax;
MCNUM restore_xray = mccemon00_restore_xray;
#line 108 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    DETECTOR_OUT_1D(
        "Energy monitor",
        "Energy [keV]",
        "Intensity",
        "E", Emin, Emax, nE,
        &E_N[0],&E_p[0],&E_p2[0],
        filename);
}
#line 14437 "Test_src_gaussian.c"
}   /* End of emon00=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'emon02'. */
  SIG_MESSAGE("emon02 (Save)");
#define mccompcurname  emon02
#define mccompcurtype  E_monitor
#define mccompcurindex 7
#define nE mccemon02_nE
#define filename mccemon02_filename
#define E_N mccemon02_E_N
#define E_p mccemon02_E_p
#define E_p2 mccemon02_E_p2
{   /* Declarations of emon02=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon02_xmin;
MCNUM xmax = mccemon02_xmax;
MCNUM ymin = mccemon02_ymin;
MCNUM ymax = mccemon02_ymax;
MCNUM xwidth = mccemon02_xwidth;
MCNUM yheight = mccemon02_yheight;
MCNUM Emin = mccemon02_Emin;
MCNUM Emax = mccemon02_Emax;
MCNUM restore_xray = mccemon02_restore_xray;
#line 108 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    DETECTOR_OUT_1D(
        "Energy monitor",
        "Energy [keV]",
        "Intensity",
        "E", Emin, Emax, nE,
        &E_N[0],&E_p[0],&E_p2[0],
        filename);
}
#line 14478 "Test_src_gaussian.c"
}   /* End of emon02=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'emon01'. */
  SIG_MESSAGE("emon01 (Save)");
#define mccompcurname  emon01
#define mccompcurtype  E_monitor
#define mccompcurindex 8
#define nE mccemon01_nE
#define filename mccemon01_filename
#define E_N mccemon01_E_N
#define E_p mccemon01_E_p
#define E_p2 mccemon01_E_p2
{   /* Declarations of emon01=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon01_xmin;
MCNUM xmax = mccemon01_xmax;
MCNUM ymin = mccemon01_ymin;
MCNUM ymax = mccemon01_ymax;
MCNUM xwidth = mccemon01_xwidth;
MCNUM yheight = mccemon01_yheight;
MCNUM Emin = mccemon01_Emin;
MCNUM Emax = mccemon01_Emax;
MCNUM restore_xray = mccemon01_restore_xray;
#line 108 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    DETECTOR_OUT_1D(
        "Energy monitor",
        "Energy [keV]",
        "Intensity",
        "E", Emin, Emax, nE,
        &E_N[0],&E_p[0],&E_p2[0],
        filename);
}
#line 14519 "Test_src_gaussian.c"
}   /* End of emon01=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'emon0'. */
  SIG_MESSAGE("emon0 (Save)");
#define mccompcurname  emon0
#define mccompcurtype  E_monitor
#define mccompcurindex 9
#define nE mccemon0_nE
#define filename mccemon0_filename
#define E_N mccemon0_E_N
#define E_p mccemon0_E_p
#define E_p2 mccemon0_E_p2
{   /* Declarations of emon0=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon0_xmin;
MCNUM xmax = mccemon0_xmax;
MCNUM ymin = mccemon0_ymin;
MCNUM ymax = mccemon0_ymax;
MCNUM xwidth = mccemon0_xwidth;
MCNUM yheight = mccemon0_yheight;
MCNUM Emin = mccemon0_Emin;
MCNUM Emax = mccemon0_Emax;
MCNUM restore_xray = mccemon0_restore_xray;
#line 108 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    DETECTOR_OUT_1D(
        "Energy monitor",
        "Energy [keV]",
        "Intensity",
        "E", Emin, Emax, nE,
        &E_N[0],&E_p[0],&E_p2[0],
        filename);
}
#line 14560 "Test_src_gaussian.c"
}   /* End of emon0=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'lmon0'. */
  SIG_MESSAGE("lmon0 (Save)");
#define mccompcurname  lmon0
#define mccompcurtype  L_monitor
#define mccompcurindex 10
#define nL mcclmon0_nL
#define filename mcclmon0_filename
#define L_N mcclmon0_L_N
#define L_p mcclmon0_L_p
#define L_p2 mcclmon0_L_p2
{   /* Declarations of lmon0=L_monitor() SETTING parameters. */
MCNUM xmin = mcclmon0_xmin;
MCNUM xmax = mcclmon0_xmax;
MCNUM ymin = mcclmon0_ymin;
MCNUM ymax = mcclmon0_ymax;
MCNUM xwidth = mcclmon0_xwidth;
MCNUM yheight = mcclmon0_yheight;
MCNUM Lmin = mcclmon0_Lmin;
MCNUM Lmax = mcclmon0_Lmax;
MCNUM restore_xray = mcclmon0_restore_xray;
#line 106 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
    DETECTOR_OUT_1D(
        "Wavelength monitor",
        "Wavelength [AA]",
        "Intensity",
        "L", Lmin, Lmax, nL,
        &L_N[0],&L_p[0],&L_p2[0],
        filename);
}
#line 14601 "Test_src_gaussian.c"
}   /* End of lmon0=L_monitor() SETTING parameter declarations. */
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'psd0'. */
  SIG_MESSAGE("psd0 (Save)");
#define mccompcurname  psd0
#define mccompcurtype  PSD_monitor
#define mccompcurindex 11
#define nx mccpsd0_nx
#define ny mccpsd0_ny
#define nr mccpsd0_nr
#define filename mccpsd0_filename
#define restore_xray mccpsd0_restore_xray
#define PSD_N mccpsd0_PSD_N
#define PSD_p mccpsd0_PSD_p
#define PSD_p2 mccpsd0_PSD_p2
{   /* Declarations of psd0=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd0_xmin;
MCNUM xmax = mccpsd0_xmax;
MCNUM ymin = mccpsd0_ymin;
MCNUM ymax = mccpsd0_ymax;
MCNUM xwidth = mccpsd0_xwidth;
MCNUM yheight = mccpsd0_yheight;
MCNUM radius = mccpsd0_radius;
#line 134 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
    if(!radius){
      DETECTOR_OUT_2D(
          "PSD monitor",
          "X position [m]",
          "Y position [m]",
          xmin, xmax, ymin, ymax,
          nx, ny,
          *PSD_N,*PSD_p,*PSD_p2,
          filename);
    }else{
      DETECTOR_OUT_1D(
          "PSD_monitor","Radial Position[m]", "Intensity", "R",
          0,radius,nr,&PSD_N[0][0],&PSD_p[0][0],&PSD_p2[0][0],filename);
    }

}
#line 14651 "Test_src_gaussian.c"
}   /* End of psd0=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'emon1'. */
  SIG_MESSAGE("emon1 (Save)");
#define mccompcurname  emon1
#define mccompcurtype  E_monitor
#define mccompcurindex 12
#define nE mccemon1_nE
#define filename mccemon1_filename
#define E_N mccemon1_E_N
#define E_p mccemon1_E_p
#define E_p2 mccemon1_E_p2
{   /* Declarations of emon1=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon1_xmin;
MCNUM xmax = mccemon1_xmax;
MCNUM ymin = mccemon1_ymin;
MCNUM ymax = mccemon1_ymax;
MCNUM xwidth = mccemon1_xwidth;
MCNUM yheight = mccemon1_yheight;
MCNUM Emin = mccemon1_Emin;
MCNUM Emax = mccemon1_Emax;
MCNUM restore_xray = mccemon1_restore_xray;
#line 108 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    DETECTOR_OUT_1D(
        "Energy monitor",
        "Energy [keV]",
        "Intensity",
        "E", Emin, Emax, nE,
        &E_N[0],&E_p[0],&E_p2[0],
        filename);
}
#line 14695 "Test_src_gaussian.c"
}   /* End of emon1=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'lmon1'. */
  SIG_MESSAGE("lmon1 (Save)");
#define mccompcurname  lmon1
#define mccompcurtype  L_monitor
#define mccompcurindex 13
#define nL mcclmon1_nL
#define filename mcclmon1_filename
#define L_N mcclmon1_L_N
#define L_p mcclmon1_L_p
#define L_p2 mcclmon1_L_p2
{   /* Declarations of lmon1=L_monitor() SETTING parameters. */
MCNUM xmin = mcclmon1_xmin;
MCNUM xmax = mcclmon1_xmax;
MCNUM ymin = mcclmon1_ymin;
MCNUM ymax = mcclmon1_ymax;
MCNUM xwidth = mcclmon1_xwidth;
MCNUM yheight = mcclmon1_yheight;
MCNUM Lmin = mcclmon1_Lmin;
MCNUM Lmax = mcclmon1_Lmax;
MCNUM restore_xray = mcclmon1_restore_xray;
#line 106 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
    DETECTOR_OUT_1D(
        "Wavelength monitor",
        "Wavelength [AA]",
        "Intensity",
        "L", Lmin, Lmax, nL,
        &L_N[0],&L_p[0],&L_p2[0],
        filename);
}
#line 14736 "Test_src_gaussian.c"
}   /* End of lmon1=L_monitor() SETTING parameter declarations. */
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'psd1'. */
  SIG_MESSAGE("psd1 (Save)");
#define mccompcurname  psd1
#define mccompcurtype  PSD_monitor
#define mccompcurindex 14
#define nx mccpsd1_nx
#define ny mccpsd1_ny
#define nr mccpsd1_nr
#define filename mccpsd1_filename
#define restore_xray mccpsd1_restore_xray
#define PSD_N mccpsd1_PSD_N
#define PSD_p mccpsd1_PSD_p
#define PSD_p2 mccpsd1_PSD_p2
{   /* Declarations of psd1=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd1_xmin;
MCNUM xmax = mccpsd1_xmax;
MCNUM ymin = mccpsd1_ymin;
MCNUM ymax = mccpsd1_ymax;
MCNUM xwidth = mccpsd1_xwidth;
MCNUM yheight = mccpsd1_yheight;
MCNUM radius = mccpsd1_radius;
#line 134 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
    if(!radius){
      DETECTOR_OUT_2D(
          "PSD monitor",
          "X position [m]",
          "Y position [m]",
          xmin, xmax, ymin, ymax,
          nx, ny,
          *PSD_N,*PSD_p,*PSD_p2,
          filename);
    }else{
      DETECTOR_OUT_1D(
          "PSD_monitor","Radial Position[m]", "Intensity", "R",
          0,radius,nr,&PSD_N[0][0],&PSD_p[0][0],&PSD_p2[0][0],filename);
    }

}
#line 14786 "Test_src_gaussian.c"
}   /* End of psd1=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'emon2'. */
  SIG_MESSAGE("emon2 (Save)");
#define mccompcurname  emon2
#define mccompcurtype  E_monitor
#define mccompcurindex 16
#define nE mccemon2_nE
#define filename mccemon2_filename
#define E_N mccemon2_E_N
#define E_p mccemon2_E_p
#define E_p2 mccemon2_E_p2
{   /* Declarations of emon2=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon2_xmin;
MCNUM xmax = mccemon2_xmax;
MCNUM ymin = mccemon2_ymin;
MCNUM ymax = mccemon2_ymax;
MCNUM xwidth = mccemon2_xwidth;
MCNUM yheight = mccemon2_yheight;
MCNUM Emin = mccemon2_Emin;
MCNUM Emax = mccemon2_Emax;
MCNUM restore_xray = mccemon2_restore_xray;
#line 108 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
    DETECTOR_OUT_1D(
        "Energy monitor",
        "Energy [keV]",
        "Intensity",
        "E", Emin, Emax, nE,
        &E_N[0],&E_p[0],&E_p2[0],
        filename);
}
#line 14830 "Test_src_gaussian.c"
}   /* End of emon2=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'lmon2'. */
  SIG_MESSAGE("lmon2 (Save)");
#define mccompcurname  lmon2
#define mccompcurtype  L_monitor
#define mccompcurindex 17
#define nL mcclmon2_nL
#define filename mcclmon2_filename
#define L_N mcclmon2_L_N
#define L_p mcclmon2_L_p
#define L_p2 mcclmon2_L_p2
{   /* Declarations of lmon2=L_monitor() SETTING parameters. */
MCNUM xmin = mcclmon2_xmin;
MCNUM xmax = mcclmon2_xmax;
MCNUM ymin = mcclmon2_ymin;
MCNUM ymax = mcclmon2_ymax;
MCNUM xwidth = mcclmon2_xwidth;
MCNUM yheight = mcclmon2_yheight;
MCNUM Lmin = mcclmon2_Lmin;
MCNUM Lmax = mcclmon2_Lmax;
MCNUM restore_xray = mcclmon2_restore_xray;
#line 106 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
    DETECTOR_OUT_1D(
        "Wavelength monitor",
        "Wavelength [AA]",
        "Intensity",
        "L", Lmin, Lmax, nL,
        &L_N[0],&L_p[0],&L_p2[0],
        filename);
}
#line 14871 "Test_src_gaussian.c"
}   /* End of lmon2=L_monitor() SETTING parameter declarations. */
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* User SAVE code for component 'psd2'. */
  SIG_MESSAGE("psd2 (Save)");
#define mccompcurname  psd2
#define mccompcurtype  PSD_monitor
#define mccompcurindex 18
#define nx mccpsd2_nx
#define ny mccpsd2_ny
#define nr mccpsd2_nr
#define filename mccpsd2_filename
#define restore_xray mccpsd2_restore_xray
#define PSD_N mccpsd2_PSD_N
#define PSD_p mccpsd2_PSD_p
#define PSD_p2 mccpsd2_PSD_p2
{   /* Declarations of psd2=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd2_xmin;
MCNUM xmax = mccpsd2_xmax;
MCNUM ymin = mccpsd2_ymin;
MCNUM ymax = mccpsd2_ymax;
MCNUM xwidth = mccpsd2_xwidth;
MCNUM yheight = mccpsd2_yheight;
MCNUM radius = mccpsd2_radius;
#line 134 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
    if(!radius){
      DETECTOR_OUT_2D(
          "PSD monitor",
          "X position [m]",
          "Y position [m]",
          xmin, xmax, ymin, ymax,
          nx, ny,
          *PSD_N,*PSD_p,*PSD_p2,
          filename);
    }else{
      DETECTOR_OUT_1D(
          "PSD_monitor","Radial Position[m]", "Intensity", "R",
          0,radius,nr,&PSD_N[0][0],&PSD_p[0][0],&PSD_p2[0][0],filename);
    }

}
#line 14921 "Test_src_gaussian.c"
}   /* End of psd2=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  if (!handle) mcsiminfo_close(); 
} /* end save */
void mcfinally(void) {
  /* User component FINALLY code. */
  mcsiminfo_init(NULL);
  mcsave(mcsiminfo_file); /* save data when simulation ends */

  /* User FINALLY code for component 'Origin'. */
  SIG_MESSAGE("Origin (Finally)");
#define mccompcurname  Origin
#define mccompcurtype  Progress_bar
#define mccompcurindex 1
#define profile mccOrigin_profile
#define IntermediateCnts mccOrigin_IntermediateCnts
#define StartTime mccOrigin_StartTime
#define EndTime mccOrigin_EndTime
{   /* Declarations of Origin=Progress_bar() SETTING parameters. */
MCNUM percent = mccOrigin_percent;
MCNUM flag_save = mccOrigin_flag_save;
MCNUM minutes = mccOrigin_minutes;
#line 128 "/usr/local/lib/mcxtrace-x.y.z/misc/Progress_bar.comp"
{
  time_t NowTime;
  time(&NowTime);
  fprintf(stdout, "\nFinally [%s/%s]. Time: ", mcinstrument_name, mcdirname ? mcdirname : ".");
  if (difftime(NowTime,StartTime) < 60.0)
    fprintf(stdout, "%g [s] ", difftime(NowTime,StartTime));
  else if (difftime(NowTime,StartTime) > 3600.0)
    fprintf(stdout, "%g [h] ", difftime(NowTime,StartTime)/3660.0);
  else
    fprintf(stdout, "%g [min] ", difftime(NowTime,StartTime)/60.0);
  fprintf(stdout, "\n");
}
#line 14968 "Test_src_gaussian.c"
}   /* End of Origin=Progress_bar() SETTING parameter declarations. */
#undef EndTime
#undef StartTime
#undef IntermediateCnts
#undef profile
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

    if (!mcNCounter[1]) fprintf(stderr, "Warning: No xray could reach Component[1] Origin\n");
    if (mcAbsorbProp[1]) fprintf(stderr, "Warning: %g events were removed in Component[1] Origin=Progress_bar()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[1]);
    if (!mcNCounter[2]) fprintf(stderr, "Warning: No xray could reach Component[2] src\n");
    if (mcAbsorbProp[2]) fprintf(stderr, "Warning: %g events were removed in Component[2] src=Source_gaussian()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[2]);
    if (!mcNCounter[3]) fprintf(stderr, "Warning: No xray could reach Component[3] src2\n");
    if (mcAbsorbProp[3]) fprintf(stderr, "Warning: %g events were removed in Component[3] src2=Source_gaussian2()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[3]);
    if (!mcNCounter[4]) fprintf(stderr, "Warning: No xray could reach Component[4] src3\n");
    if (mcAbsorbProp[4]) fprintf(stderr, "Warning: %g events were removed in Component[4] src3=Source_gaussian2()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[4]);
    if (!mcNCounter[5]) fprintf(stderr, "Warning: No xray could reach Component[5] src4\n");
    if (mcAbsorbProp[5]) fprintf(stderr, "Warning: %g events were removed in Component[5] src4=Source_gaussian2()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[5]);
    if (!mcNCounter[6]) fprintf(stderr, "Warning: No xray could reach Component[6] emon00\n");
    if (mcAbsorbProp[6]) fprintf(stderr, "Warning: %g events were removed in Component[6] emon00=E_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[6]);
    if (!mcNCounter[7]) fprintf(stderr, "Warning: No xray could reach Component[7] emon02\n");
    if (mcAbsorbProp[7]) fprintf(stderr, "Warning: %g events were removed in Component[7] emon02=E_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[7]);
    if (!mcNCounter[8]) fprintf(stderr, "Warning: No xray could reach Component[8] emon01\n");
    if (mcAbsorbProp[8]) fprintf(stderr, "Warning: %g events were removed in Component[8] emon01=E_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[8]);
    if (!mcNCounter[9]) fprintf(stderr, "Warning: No xray could reach Component[9] emon0\n");
    if (mcAbsorbProp[9]) fprintf(stderr, "Warning: %g events were removed in Component[9] emon0=E_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[9]);
    if (!mcNCounter[10]) fprintf(stderr, "Warning: No xray could reach Component[10] lmon0\n");
    if (mcAbsorbProp[10]) fprintf(stderr, "Warning: %g events were removed in Component[10] lmon0=L_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[10]);
  /* User FINALLY code for component 'psd0'. */
  SIG_MESSAGE("psd0 (Finally)");
#define mccompcurname  psd0
#define mccompcurtype  PSD_monitor
#define mccompcurindex 11
#define nx mccpsd0_nx
#define ny mccpsd0_ny
#define nr mccpsd0_nr
#define filename mccpsd0_filename
#define restore_xray mccpsd0_restore_xray
#define PSD_N mccpsd0_PSD_N
#define PSD_p mccpsd0_PSD_p
#define PSD_p2 mccpsd0_PSD_p2
{   /* Declarations of psd0=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd0_xmin;
MCNUM xmax = mccpsd0_xmax;
MCNUM ymin = mccpsd0_ymin;
MCNUM ymax = mccpsd0_ymax;
MCNUM xwidth = mccpsd0_xwidth;
MCNUM yheight = mccpsd0_yheight;
MCNUM radius = mccpsd0_radius;
#line 153 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
   free(PSD_N[0]);
   free(PSD_N);
   free(PSD_p[0]);
   free(PSD_p);
   free(PSD_p2[0]);
   free(PSD_p2);
}
#line 15028 "Test_src_gaussian.c"
}   /* End of psd0=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

    if (!mcNCounter[11]) fprintf(stderr, "Warning: No xray could reach Component[11] psd0\n");
    if (mcAbsorbProp[11]) fprintf(stderr, "Warning: %g events were removed in Component[11] psd0=PSD_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[11]);
    if (!mcNCounter[12]) fprintf(stderr, "Warning: No xray could reach Component[12] emon1\n");
    if (mcAbsorbProp[12]) fprintf(stderr, "Warning: %g events were removed in Component[12] emon1=E_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[12]);
    if (!mcNCounter[13]) fprintf(stderr, "Warning: No xray could reach Component[13] lmon1\n");
    if (mcAbsorbProp[13]) fprintf(stderr, "Warning: %g events were removed in Component[13] lmon1=L_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[13]);
  /* User FINALLY code for component 'psd1'. */
  SIG_MESSAGE("psd1 (Finally)");
#define mccompcurname  psd1
#define mccompcurtype  PSD_monitor
#define mccompcurindex 14
#define nx mccpsd1_nx
#define ny mccpsd1_ny
#define nr mccpsd1_nr
#define filename mccpsd1_filename
#define restore_xray mccpsd1_restore_xray
#define PSD_N mccpsd1_PSD_N
#define PSD_p mccpsd1_PSD_p
#define PSD_p2 mccpsd1_PSD_p2
{   /* Declarations of psd1=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd1_xmin;
MCNUM xmax = mccpsd1_xmax;
MCNUM ymin = mccpsd1_ymin;
MCNUM ymax = mccpsd1_ymax;
MCNUM xwidth = mccpsd1_xwidth;
MCNUM yheight = mccpsd1_yheight;
MCNUM radius = mccpsd1_radius;
#line 153 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
   free(PSD_N[0]);
   free(PSD_N);
   free(PSD_p[0]);
   free(PSD_p);
   free(PSD_p2[0]);
   free(PSD_p2);
}
#line 15078 "Test_src_gaussian.c"
}   /* End of psd1=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

    if (!mcNCounter[14]) fprintf(stderr, "Warning: No xray could reach Component[14] psd1\n");
    if (mcAbsorbProp[14]) fprintf(stderr, "Warning: %g events were removed in Component[14] psd1=PSD_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[14]);
    if (!mcNCounter[15]) fprintf(stderr, "Warning: No xray could reach Component[15] slit\n");
    if (mcAbsorbProp[15]) fprintf(stderr, "Warning: %g events were removed in Component[15] slit=Slit()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[15]);
    if (!mcNCounter[16]) fprintf(stderr, "Warning: No xray could reach Component[16] emon2\n");
    if (mcAbsorbProp[16]) fprintf(stderr, "Warning: %g events were removed in Component[16] emon2=E_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[16]);
    if (!mcNCounter[17]) fprintf(stderr, "Warning: No xray could reach Component[17] lmon2\n");
    if (mcAbsorbProp[17]) fprintf(stderr, "Warning: %g events were removed in Component[17] lmon2=L_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[17]);
  /* User FINALLY code for component 'psd2'. */
  SIG_MESSAGE("psd2 (Finally)");
#define mccompcurname  psd2
#define mccompcurtype  PSD_monitor
#define mccompcurindex 18
#define nx mccpsd2_nx
#define ny mccpsd2_ny
#define nr mccpsd2_nr
#define filename mccpsd2_filename
#define restore_xray mccpsd2_restore_xray
#define PSD_N mccpsd2_PSD_N
#define PSD_p mccpsd2_PSD_p
#define PSD_p2 mccpsd2_PSD_p2
{   /* Declarations of psd2=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd2_xmin;
MCNUM xmax = mccpsd2_xmax;
MCNUM ymin = mccpsd2_ymin;
MCNUM ymax = mccpsd2_ymax;
MCNUM xwidth = mccpsd2_xwidth;
MCNUM yheight = mccpsd2_yheight;
MCNUM radius = mccpsd2_radius;
#line 153 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
   free(PSD_N[0]);
   free(PSD_N);
   free(PSD_p[0]);
   free(PSD_p);
   free(PSD_p2[0]);
   free(PSD_p2);
}
#line 15130 "Test_src_gaussian.c"
}   /* End of psd2=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

    if (!mcNCounter[18]) fprintf(stderr, "Warning: No xray could reach Component[18] psd2\n");
    if (mcAbsorbProp[18]) fprintf(stderr, "Warning: %g events were removed in Component[18] psd2=PSD_monitor()\n"
"         (negative time, miss next components, rounding errors, Nan, Inf).\n", mcAbsorbProp[18]);
  mcsiminfo_close(); 
} /* end finally */
#define magnify mcdis_magnify
#define line mcdis_line
#define dashed_line mcdis_dashed_line
#define multiline mcdis_multiline
#define rectangle mcdis_rectangle
#define box mcdis_box
#define circle mcdis_circle
void mcdisplay(void) {
  printf("MCDISPLAY: start\n");
  /* Components MCDISPLAY code. */

  /* MCDISPLAY code for component 'src'. */
  SIG_MESSAGE("src (McDisplay)");
  printf("MCDISPLAY: component %s\n", "src");
#define mccompcurname  src
#define mccompcurtype  Source_gaussian
#define mccompcurindex 2
#define spectrum_file mccsrc_spectrum_file
#define prms mccsrc_prms
{   /* Declarations of src=Source_gaussian() SETTING parameters. */
MCNUM sig_x = mccsrc_sig_x;
MCNUM sig_y = mccsrc_sig_y;
MCNUM sigPr_x = mccsrc_sigPr_x;
MCNUM sigPr_y = mccsrc_sigPr_y;
MCNUM flux = mccsrc_flux;
MCNUM dist = mccsrc_dist;
MCNUM gauss = mccsrc_gauss;
MCNUM gamma = mccsrc_gamma;
MCNUM E0 = mccsrc_E0;
MCNUM dE = mccsrc_dE;
MCNUM lambda0 = mccsrc_lambda0;
MCNUM dlambda = mccsrc_dlambda;
MCNUM phase = mccsrc_phase;
#line 191 "/usr/local/lib/mcxtrace-x.y.z/sources/Source_gaussian.comp"
{
  double radius;
  if (sig_x<sig_y) radius=sig_x;
  else radius=sig_y; 

  magnify("xy");
  circle("xy",0,0,0,radius);
}
#line 15190 "Test_src_gaussian.c"
}   /* End of src=Source_gaussian() SETTING parameter declarations. */
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'src2'. */
  SIG_MESSAGE("src2 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "src2");
#define mccompcurname  src2
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 3
#define spectrum_file mccsrc2_spectrum_file
#define prms mccsrc2_prms
#define spX mccsrc2_spX
#define spY mccsrc2_spY
{   /* Declarations of src2=Source_gaussian2() SETTING parameters. */
MCNUM sig_x = mccsrc2_sig_x;
MCNUM sig_y = mccsrc2_sig_y;
MCNUM sigPr_x = mccsrc2_sigPr_x;
MCNUM sigPr_y = mccsrc2_sigPr_y;
MCNUM flux = mccsrc2_flux;
MCNUM brilliance = mccsrc2_brilliance;
MCNUM dist = mccsrc2_dist;
MCNUM gauss = mccsrc2_gauss;
MCNUM focus_xw = mccsrc2_focus_xw;
MCNUM focus_yh = mccsrc2_focus_yh;
MCNUM E0 = mccsrc2_E0;
MCNUM dE = mccsrc2_dE;
MCNUM lambda0 = mccsrc2_lambda0;
MCNUM dlambda = mccsrc2_dlambda;
MCNUM phase = mccsrc2_phase;
#line 300 "Source_gaussian2.comp"
{
  double radius;
  if (sig_x<sig_y) radius=sig_x;
  else radius=sig_y; 

  magnify("xy");
  circle("xy",0,0,0,radius);
}
#line 15233 "Test_src_gaussian.c"
}   /* End of src2=Source_gaussian2() SETTING parameter declarations. */
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'src3'. */
  SIG_MESSAGE("src3 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "src3");
#define mccompcurname  src3
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 4
#define spectrum_file mccsrc3_spectrum_file
#define prms mccsrc3_prms
#define spX mccsrc3_spX
#define spY mccsrc3_spY
{   /* Declarations of src3=Source_gaussian2() SETTING parameters. */
MCNUM sig_x = mccsrc3_sig_x;
MCNUM sig_y = mccsrc3_sig_y;
MCNUM sigPr_x = mccsrc3_sigPr_x;
MCNUM sigPr_y = mccsrc3_sigPr_y;
MCNUM flux = mccsrc3_flux;
MCNUM brilliance = mccsrc3_brilliance;
MCNUM dist = mccsrc3_dist;
MCNUM gauss = mccsrc3_gauss;
MCNUM focus_xw = mccsrc3_focus_xw;
MCNUM focus_yh = mccsrc3_focus_yh;
MCNUM E0 = mccsrc3_E0;
MCNUM dE = mccsrc3_dE;
MCNUM lambda0 = mccsrc3_lambda0;
MCNUM dlambda = mccsrc3_dlambda;
MCNUM phase = mccsrc3_phase;
#line 300 "Source_gaussian2.comp"
{
  double radius;
  if (sig_x<sig_y) radius=sig_x;
  else radius=sig_y; 

  magnify("xy");
  circle("xy",0,0,0,radius);
}
#line 15278 "Test_src_gaussian.c"
}   /* End of src3=Source_gaussian2() SETTING parameter declarations. */
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'src4'. */
  SIG_MESSAGE("src4 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "src4");
#define mccompcurname  src4
#define mccompcurtype  Source_gaussian2
#define mccompcurindex 5
#define spectrum_file mccsrc4_spectrum_file
#define prms mccsrc4_prms
#define spX mccsrc4_spX
#define spY mccsrc4_spY
{   /* Declarations of src4=Source_gaussian2() SETTING parameters. */
MCNUM sig_x = mccsrc4_sig_x;
MCNUM sig_y = mccsrc4_sig_y;
MCNUM sigPr_x = mccsrc4_sigPr_x;
MCNUM sigPr_y = mccsrc4_sigPr_y;
MCNUM flux = mccsrc4_flux;
MCNUM brilliance = mccsrc4_brilliance;
MCNUM dist = mccsrc4_dist;
MCNUM gauss = mccsrc4_gauss;
MCNUM focus_xw = mccsrc4_focus_xw;
MCNUM focus_yh = mccsrc4_focus_yh;
MCNUM E0 = mccsrc4_E0;
MCNUM dE = mccsrc4_dE;
MCNUM lambda0 = mccsrc4_lambda0;
MCNUM dlambda = mccsrc4_dlambda;
MCNUM phase = mccsrc4_phase;
#line 300 "Source_gaussian2.comp"
{
  double radius;
  if (sig_x<sig_y) radius=sig_x;
  else radius=sig_y; 

  magnify("xy");
  circle("xy",0,0,0,radius);
}
#line 15323 "Test_src_gaussian.c"
}   /* End of src4=Source_gaussian2() SETTING parameter declarations. */
#undef spY
#undef spX
#undef prms
#undef spectrum_file
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'emon00'. */
  SIG_MESSAGE("emon00 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "emon00");
#define mccompcurname  emon00
#define mccompcurtype  E_monitor
#define mccompcurindex 6
#define nE mccemon00_nE
#define filename mccemon00_filename
#define E_N mccemon00_E_N
#define E_p mccemon00_E_p
#define E_p2 mccemon00_E_p2
{   /* Declarations of emon00=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon00_xmin;
MCNUM xmax = mccemon00_xmax;
MCNUM ymin = mccemon00_ymin;
MCNUM ymax = mccemon00_ymax;
MCNUM xwidth = mccemon00_xwidth;
MCNUM yheight = mccemon00_yheight;
MCNUM Emin = mccemon00_Emin;
MCNUM Emax = mccemon00_Emax;
MCNUM restore_xray = mccemon00_restore_xray;
#line 119 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15363 "Test_src_gaussian.c"
}   /* End of emon00=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'emon02'. */
  SIG_MESSAGE("emon02 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "emon02");
#define mccompcurname  emon02
#define mccompcurtype  E_monitor
#define mccompcurindex 7
#define nE mccemon02_nE
#define filename mccemon02_filename
#define E_N mccemon02_E_N
#define E_p mccemon02_E_p
#define E_p2 mccemon02_E_p2
{   /* Declarations of emon02=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon02_xmin;
MCNUM xmax = mccemon02_xmax;
MCNUM ymin = mccemon02_ymin;
MCNUM ymax = mccemon02_ymax;
MCNUM xwidth = mccemon02_xwidth;
MCNUM yheight = mccemon02_yheight;
MCNUM Emin = mccemon02_Emin;
MCNUM Emax = mccemon02_Emax;
MCNUM restore_xray = mccemon02_restore_xray;
#line 119 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15404 "Test_src_gaussian.c"
}   /* End of emon02=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'emon01'. */
  SIG_MESSAGE("emon01 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "emon01");
#define mccompcurname  emon01
#define mccompcurtype  E_monitor
#define mccompcurindex 8
#define nE mccemon01_nE
#define filename mccemon01_filename
#define E_N mccemon01_E_N
#define E_p mccemon01_E_p
#define E_p2 mccemon01_E_p2
{   /* Declarations of emon01=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon01_xmin;
MCNUM xmax = mccemon01_xmax;
MCNUM ymin = mccemon01_ymin;
MCNUM ymax = mccemon01_ymax;
MCNUM xwidth = mccemon01_xwidth;
MCNUM yheight = mccemon01_yheight;
MCNUM Emin = mccemon01_Emin;
MCNUM Emax = mccemon01_Emax;
MCNUM restore_xray = mccemon01_restore_xray;
#line 119 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15445 "Test_src_gaussian.c"
}   /* End of emon01=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'emon0'. */
  SIG_MESSAGE("emon0 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "emon0");
#define mccompcurname  emon0
#define mccompcurtype  E_monitor
#define mccompcurindex 9
#define nE mccemon0_nE
#define filename mccemon0_filename
#define E_N mccemon0_E_N
#define E_p mccemon0_E_p
#define E_p2 mccemon0_E_p2
{   /* Declarations of emon0=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon0_xmin;
MCNUM xmax = mccemon0_xmax;
MCNUM ymin = mccemon0_ymin;
MCNUM ymax = mccemon0_ymax;
MCNUM xwidth = mccemon0_xwidth;
MCNUM yheight = mccemon0_yheight;
MCNUM Emin = mccemon0_Emin;
MCNUM Emax = mccemon0_Emax;
MCNUM restore_xray = mccemon0_restore_xray;
#line 119 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15486 "Test_src_gaussian.c"
}   /* End of emon0=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'lmon0'. */
  SIG_MESSAGE("lmon0 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "lmon0");
#define mccompcurname  lmon0
#define mccompcurtype  L_monitor
#define mccompcurindex 10
#define nL mcclmon0_nL
#define filename mcclmon0_filename
#define L_N mcclmon0_L_N
#define L_p mcclmon0_L_p
#define L_p2 mcclmon0_L_p2
{   /* Declarations of lmon0=L_monitor() SETTING parameters. */
MCNUM xmin = mcclmon0_xmin;
MCNUM xmax = mcclmon0_xmax;
MCNUM ymin = mcclmon0_ymin;
MCNUM ymax = mcclmon0_ymax;
MCNUM xwidth = mcclmon0_xwidth;
MCNUM yheight = mcclmon0_yheight;
MCNUM Lmin = mcclmon0_Lmin;
MCNUM Lmax = mcclmon0_Lmax;
MCNUM restore_xray = mcclmon0_restore_xray;
#line 117 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15527 "Test_src_gaussian.c"
}   /* End of lmon0=L_monitor() SETTING parameter declarations. */
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'psd0'. */
  SIG_MESSAGE("psd0 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "psd0");
#define mccompcurname  psd0
#define mccompcurtype  PSD_monitor
#define mccompcurindex 11
#define nx mccpsd0_nx
#define ny mccpsd0_ny
#define nr mccpsd0_nr
#define filename mccpsd0_filename
#define restore_xray mccpsd0_restore_xray
#define PSD_N mccpsd0_PSD_N
#define PSD_p mccpsd0_PSD_p
#define PSD_p2 mccpsd0_PSD_p2
{   /* Declarations of psd0=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd0_xmin;
MCNUM xmax = mccpsd0_xmax;
MCNUM ymin = mccpsd0_ymin;
MCNUM ymax = mccpsd0_ymax;
MCNUM xwidth = mccpsd0_xwidth;
MCNUM yheight = mccpsd0_yheight;
MCNUM radius = mccpsd0_radius;
#line 162 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15569 "Test_src_gaussian.c"
}   /* End of psd0=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'emon1'. */
  SIG_MESSAGE("emon1 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "emon1");
#define mccompcurname  emon1
#define mccompcurtype  E_monitor
#define mccompcurindex 12
#define nE mccemon1_nE
#define filename mccemon1_filename
#define E_N mccemon1_E_N
#define E_p mccemon1_E_p
#define E_p2 mccemon1_E_p2
{   /* Declarations of emon1=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon1_xmin;
MCNUM xmax = mccemon1_xmax;
MCNUM ymin = mccemon1_ymin;
MCNUM ymax = mccemon1_ymax;
MCNUM xwidth = mccemon1_xwidth;
MCNUM yheight = mccemon1_yheight;
MCNUM Emin = mccemon1_Emin;
MCNUM Emax = mccemon1_Emax;
MCNUM restore_xray = mccemon1_restore_xray;
#line 119 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15613 "Test_src_gaussian.c"
}   /* End of emon1=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'lmon1'. */
  SIG_MESSAGE("lmon1 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "lmon1");
#define mccompcurname  lmon1
#define mccompcurtype  L_monitor
#define mccompcurindex 13
#define nL mcclmon1_nL
#define filename mcclmon1_filename
#define L_N mcclmon1_L_N
#define L_p mcclmon1_L_p
#define L_p2 mcclmon1_L_p2
{   /* Declarations of lmon1=L_monitor() SETTING parameters. */
MCNUM xmin = mcclmon1_xmin;
MCNUM xmax = mcclmon1_xmax;
MCNUM ymin = mcclmon1_ymin;
MCNUM ymax = mcclmon1_ymax;
MCNUM xwidth = mcclmon1_xwidth;
MCNUM yheight = mcclmon1_yheight;
MCNUM Lmin = mcclmon1_Lmin;
MCNUM Lmax = mcclmon1_Lmax;
MCNUM restore_xray = mcclmon1_restore_xray;
#line 117 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15654 "Test_src_gaussian.c"
}   /* End of lmon1=L_monitor() SETTING parameter declarations. */
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'psd1'. */
  SIG_MESSAGE("psd1 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "psd1");
#define mccompcurname  psd1
#define mccompcurtype  PSD_monitor
#define mccompcurindex 14
#define nx mccpsd1_nx
#define ny mccpsd1_ny
#define nr mccpsd1_nr
#define filename mccpsd1_filename
#define restore_xray mccpsd1_restore_xray
#define PSD_N mccpsd1_PSD_N
#define PSD_p mccpsd1_PSD_p
#define PSD_p2 mccpsd1_PSD_p2
{   /* Declarations of psd1=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd1_xmin;
MCNUM xmax = mccpsd1_xmax;
MCNUM ymin = mccpsd1_ymin;
MCNUM ymax = mccpsd1_ymax;
MCNUM xwidth = mccpsd1_xwidth;
MCNUM yheight = mccpsd1_yheight;
MCNUM radius = mccpsd1_radius;
#line 162 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15696 "Test_src_gaussian.c"
}   /* End of psd1=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'slit'. */
  SIG_MESSAGE("slit (McDisplay)");
  printf("MCDISPLAY: component %s\n", "slit");
#define mccompcurname  slit
#define mccompcurtype  Slit
#define mccompcurindex 15
{   /* Declarations of slit=Slit() SETTING parameters. */
MCNUM xmin = mccslit_xmin;
MCNUM xmax = mccslit_xmax;
MCNUM ymin = mccslit_ymin;
MCNUM ymax = mccslit_ymax;
MCNUM radius = mccslit_radius;
MCNUM cut = mccslit_cut;
MCNUM xwidth = mccslit_xwidth;
MCNUM yheight = mccslit_yheight;
MCNUM dist = mccslit_dist;
MCNUM focus_xw = mccslit_focus_xw;
MCNUM focus_yh = mccslit_focus_yh;
MCNUM focus_x0 = mccslit_focus_x0;
MCNUM focus_y0 = mccslit_focus_y0;
#line 103 "/usr/local/lib/mcxtrace-x.y.z/optics/Slit.comp"
{
  magnify("xy");
  if (radius == 0) {
    double xw, yh;
    xw = (xmax - xmin)/2.0;
    yh = (ymax - ymin)/2.0;
    multiline(3, xmin-xw, (double)ymax, 0.0,
              (double)xmin, (double)ymax, 0.0,
              (double)xmin, ymax+yh, 0.0);
    multiline(3, xmax+xw, (double)ymax, 0.0,
              (double)xmax, (double)ymax, 0.0,
              (double)xmax, ymax+yh, 0.0);
    multiline(3, xmin-xw, (double)ymin, 0.0,
              (double)xmin, (double)ymin, 0.0,
              (double)xmin, ymin-yh, 0.0);
    multiline(3, xmax+xw, (double)ymin, 0.0,
              (double)xmax, (double)ymin, 0.0,
              (double)xmax, ymin-yh, 0.0);
  } else {
    circle("xy",0,0,0,radius);
  }
}
#line 15753 "Test_src_gaussian.c"
}   /* End of slit=Slit() SETTING parameter declarations. */
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'emon2'. */
  SIG_MESSAGE("emon2 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "emon2");
#define mccompcurname  emon2
#define mccompcurtype  E_monitor
#define mccompcurindex 16
#define nE mccemon2_nE
#define filename mccemon2_filename
#define E_N mccemon2_E_N
#define E_p mccemon2_E_p
#define E_p2 mccemon2_E_p2
{   /* Declarations of emon2=E_monitor() SETTING parameters. */
MCNUM xmin = mccemon2_xmin;
MCNUM xmax = mccemon2_xmax;
MCNUM ymin = mccemon2_ymin;
MCNUM ymax = mccemon2_ymax;
MCNUM xwidth = mccemon2_xwidth;
MCNUM yheight = mccemon2_yheight;
MCNUM Emin = mccemon2_Emin;
MCNUM Emax = mccemon2_Emax;
MCNUM restore_xray = mccemon2_restore_xray;
#line 119 "/usr/local/lib/mcxtrace-x.y.z/monitors/E_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15789 "Test_src_gaussian.c"
}   /* End of emon2=E_monitor() SETTING parameter declarations. */
#undef E_p2
#undef E_p
#undef E_N
#undef filename
#undef nE
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'lmon2'. */
  SIG_MESSAGE("lmon2 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "lmon2");
#define mccompcurname  lmon2
#define mccompcurtype  L_monitor
#define mccompcurindex 17
#define nL mcclmon2_nL
#define filename mcclmon2_filename
#define L_N mcclmon2_L_N
#define L_p mcclmon2_L_p
#define L_p2 mcclmon2_L_p2
{   /* Declarations of lmon2=L_monitor() SETTING parameters. */
MCNUM xmin = mcclmon2_xmin;
MCNUM xmax = mcclmon2_xmax;
MCNUM ymin = mcclmon2_ymin;
MCNUM ymax = mcclmon2_ymax;
MCNUM xwidth = mcclmon2_xwidth;
MCNUM yheight = mcclmon2_yheight;
MCNUM Lmin = mcclmon2_Lmin;
MCNUM Lmax = mcclmon2_Lmax;
MCNUM restore_xray = mcclmon2_restore_xray;
#line 117 "/usr/local/lib/mcxtrace-x.y.z/monitors/L_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15830 "Test_src_gaussian.c"
}   /* End of lmon2=L_monitor() SETTING parameter declarations. */
#undef L_p2
#undef L_p
#undef L_N
#undef filename
#undef nL
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  /* MCDISPLAY code for component 'psd2'. */
  SIG_MESSAGE("psd2 (McDisplay)");
  printf("MCDISPLAY: component %s\n", "psd2");
#define mccompcurname  psd2
#define mccompcurtype  PSD_monitor
#define mccompcurindex 18
#define nx mccpsd2_nx
#define ny mccpsd2_ny
#define nr mccpsd2_nr
#define filename mccpsd2_filename
#define restore_xray mccpsd2_restore_xray
#define PSD_N mccpsd2_PSD_N
#define PSD_p mccpsd2_PSD_p
#define PSD_p2 mccpsd2_PSD_p2
{   /* Declarations of psd2=PSD_monitor() SETTING parameters. */
MCNUM xmin = mccpsd2_xmin;
MCNUM xmax = mccpsd2_xmax;
MCNUM ymin = mccpsd2_ymin;
MCNUM ymax = mccpsd2_ymax;
MCNUM xwidth = mccpsd2_xwidth;
MCNUM yheight = mccpsd2_yheight;
MCNUM radius = mccpsd2_radius;
#line 162 "/usr/local/lib/mcxtrace-x.y.z/monitors/PSD_monitor.comp"
{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
}
#line 15872 "Test_src_gaussian.c"
}   /* End of psd2=PSD_monitor() SETTING parameter declarations. */
#undef PSD_p2
#undef PSD_p
#undef PSD_N
#undef restore_xray
#undef filename
#undef nr
#undef ny
#undef nx
#undef mccompcurname
#undef mccompcurtype
#undef mccompcurindex

  printf("MCDISPLAY: end\n");
} /* end display */
#undef magnify
#undef line
#undef dashed_line
#undef multiline
#undef rectangle
#undef box
#undef circle
/* end of generated C code Test_src_gaussian.c */
