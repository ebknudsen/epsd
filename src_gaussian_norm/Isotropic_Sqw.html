<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML><HEAD>
<TITLE>mcstas-2.9999-svn: Isotropic_Sqw Component</TITLE>
<LINK REV="made" HREF="mailto:peter.willendrup@risoe.dk">
</HEAD>

<BODY>

<P ALIGN=CENTER>
 [ <A href="#id">Identification</A>
 | <A href="#desc">Description</A>
 | <A href="#ipar">Input parameters</A>
 | <A href="#opar">Output parameters</A>
 | <A href="#links">Links</A> ]
</P>

<H1>The <CODE>Isotropic_Sqw</CODE> Component</H1>

Isotropic sample handling multiple scattering and absorption for a general
S(q,w) (coherent and/or incoherent/self)


<H2><A NAME=id></A>Identification</H2>

<UL>
  <LI> <B>Author:</B> E. Farhi, V. Hugouvieux</B>
  <LI> <B>Origin:</B>  ILL</B>
  <LI> <B>Date:</B> August 2003</B>
  <LI> <B>Version:</B> 1.90 </B>
  <LI> <B>Modification history:</B> <UL>
    <LI>  E. Farhi, Jul 2005: made it work, concentric mode, multiple use
    <LI>  E. Farhi, Mar 2007: improved implementation, correct small bugs
    <LI>  E. Farhi, Oct 2008: added any shape sample geometry
    <LI>  E. Farhi, Oct 2012: improved sampling scheme, correct bug in powder S(q)
  </UL>
</UL>
<H2><A NAME=desc></A>Description</H2>

<PRE>
An isotropic sample handling multiple scattering and including as input the
dynamic structure factor of the chosen sample (e.g. from Molecular
Dynamics). Handles elastic/inelastic, coherent and incoherent scattering -
depending on the input S(q,w) - with multiple scattering and absorption.
Only the norm of q is handled (not the vector), and thus suitable for
liquids, gazes, amorphous and powder samples.

If incoherent/self S(q,w) file is specified as empty (0 or "") then the
scattering is constant isotropic (Vanadium like).
In case you only have one S(q,w) data containing both coherent and
incoherent contributions you should e.g. use 'Sqw_coh' and set 'sigma_coh'
to the total scattering cross section.
The implementation assumes that the S(q,w) data is normalized, i.e. S(q)
goes to 1 for large q. If this is not the case, the component can do that
when 'norm=-1'. Alternatively, the S(q,w) data will be multiplied by
'norm' for positive values. Both non symmetric and classical S(q,w)
data sets are handled by mean of the 'classical' parameter (see below).

Additionally, for single order scattering (order=1), you may restrict the
vertical spreading of the scattering area using d_phi parameter.

An important option to enhance statistics is to set 'p_interact' to, say,
30 percent (0.3) in order to force a fraction of the beam to scatter. This
will result on a larger number of scattered events, retaining intensity.

If you use this component and produce valuable scientific results, please
cite authors with references bellow (in <a href="#links">Links</a>).
    E. Farhi et al, J Comp Phys 228 (2009) 5251

<b>Sample shape:</b>
Sample shape may be a cylinder, a sphere, a box or any other shape
  box/plate:       xwidth x yheight x zdepth (thickness=0)
  hollow box/plate:xwidth x yheight x zdepth and thickness>0
  cylinder:        radius x yheight (thickness=0)
  hollow cylinder: radius x yheight and thickness>0
  sphere:          radius (yheight=0 thickness=0)
  hollow sphere:   radius and thickness>0 (yheight=0)
  any shape:       geometry=OFF file

  The complex geometry option handles any closed non-convex polyhedra.
  It computes the intersection points of the neutron ray with the object
  transparently, so that it can be used like a regular sample object.
  It supports the OFF, PLY and NOFF file format but not COFF (colored faces).
  Such files may be generated from XYZ data using:
    qhull < coordinates.xyz Qx Qv Tv o > geomview.off
  or
    powercrust coordinates.xyz
  and viewed with geomview or java -jar jroff.jar (see below).
  The default size of the object depends of the OFF file data, but its
  bounding box may be resized using xwidth,yheight and zdepth.


<b>Concentric components:</b>
This component has the ability to contain other components when used in
hollow cylinder geometry (namely sample environment, e.g. cryostat and
furnace structure). Such component 'shells' should be split into input and
output side surrounding the 'inside' components. First part must then use
'concentric=1' flag to enter the inside part. The component itself must be
repeated to mark the end of the concentric zone. The number of concentric
shells and number of components inside is not limited.

COMPONENT S_in = Isotropic_Sqw(Sqw_coh="Al.laz", concentric=1, ...)
AT (0,0,0) RELATIVE sample_position

COMPONENT something_inside ... // e.g. the sample itself or other materials

COMPONENT S_out = COPY(S_in)(concentric=0)
AT (0,0,0) RELATIVE sample_position

<b>Sqw file format:</b>
File format for S(Q,w) (coherent and incoherent) should contain 3 numerical
blocks, defining q axis values (vector), then energy axis values (vector),
then a matrix with one line per q axis value, containing Sqw values for
each energy axis value. Comments (starting with '#') and non numerical lines
are ignored and used to separate blocks. Sampling must be regular.

Example:
# q axis values
# vector of m values in Angstroem-1
0.001000 .... 3.591000
# w axis values
# vector of n values in meV
0.001391 ... 1.681391
# sqw values (one line per q axis value)
# matrix of S(q,w) values (m rows x n values), one line per q value,
9.721422  10.599145 ... 0.000000
10.054191 11.025244 ... 0.000000
...
0.000000            ... 3.860253

See for instance file He4_liq_coh.sqw. Such files may be obtained from e.g. INX,
Nathan, Lamp and IDA softwares, as well as Molecular Dynamics (nMoldyn).
When the provided S(q,w) data is obtained from the classical correlation function
G(r,t), which is real and symmetric in time, the 'classical=1' parameter
should be set in order to multiply the file data with exp(hw/2kT). Otherwise,
the S(q,w) is NOT symmetrised (classical). If the S(q,w) data set includes both
negative and positive energy values, setting 'classical=-1' will attempt to
guess what type of S(q,w) it is. The temperature can also be determined this way.
In case you do not know if the data is classical or quantum, assume it is classical
at high temperatures, and quantum otherwise (T  < typical mode excitations).
The positive energy values correspond to Stokes processes, i.e. material gains
energy, and neutrons loose energy. The energy range is symmetrized to allow up
and down scattering, taking into account detailed balance exp(-hw/2kT).

<b>Powder file format:</b>
Files for coherent elastic powder scattering may also be used.
Format specification follows the same principle as in the PowderN
component, with parameters:

    powder_format=Crystallographica
or  powder_format=Fullprof
or  powder_format=Lazy
or  powder_format={j,d,F2,DW,Delta_d/d,1/2d,q,F,strain} (column indexes 1:n)

or column indexes (starting from 1) given as comments in the file header
(e.g. '#column_j 4'). Refer to the PowderN component for more details.
Delta_d/d and Debye-Waller factor may be specified for all lines with the
'powder_Dd' and 'powder_DW' parameters.
The reflection list should be ordered by decreasing d-spacing values.

Additionally a special [q,Sq] format is also defined with:
  powder_format=qSq
for which column 1 is 'q' and column 2 is 'S(q)'.

<b>Examples:</b>
1- Vanadium-like incoherent elastic scattering
  Isotropic_Sqw(radius=0.005, yheight=0.01, V_rho=1/13.827,
    sigma_abs=5.08, sigma_inc=4.935, sigma_coh=0)

2- liq-4He parameters
  Isotropic_Sqw(..., Sqw_coh="He4_liq_coh.sqw", T=10, p_interact=0.3)

3- powder sample
 Isotropic_Sqw(..., Sqw_coh="Al.laz")

%BUGS:
When used in concentric mode, multiple bouncing scattering
(traversing the hollow part) is not taken into account.

%VALIDATION
For Vanadium incoherent scattering mode, V_sample, PowderN, Single_crystal
and Isotropic_Sqw produce equivalent results, eventhough the two later are
more accurate (geometry, multiple scattering). Isotropic_Sqw gives same
powder patterns as PowderN, with an intensity within 20 %.

</PRE>

<H2><A NAME=ipar></A>Input parameters</H2>
Parameters in <B>boldface</B> are required;
the others are optional.
<TABLE BORDER=1>
<TR><TH>Name</TH>  <TH>Unit</TH>  <TH>Description</TH> <TH>Default</TH></TR>
<TR> <TD>powder_format</TD>
     <TD>no quotes</TD>
     <TD>name or definition of column indexes in file
</TD>
<TD ALIGN=RIGHT>Undefined</TD> </TR>
<TR> <TD>Sqw_coh</TD>
     <TD>str</TD>
     <TD>Name of the file containing the values of Q, w and S(Q,w)
Coherent part; Q in Angs-1, E in meV, S(q,w) in meV-1.
Use 0, NULL or "" to disable.
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>Sqw_inc</TD>
     <TD>str</TD>
     <TD>Name of the file containing the values of Q, w and S(Q,w).
Incoherent (self) part.
Use 0, NULL or "" to scatter isotropically (V-like).
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>geometry</TD>
     <TD>str</TD>
     <TD>Name of an Object File Format (OFF) or PLY file for complex geometry.
The OFF/PLY file may be generated from XYZ coordinates using qhull/powercrust
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>radius</TD>
     <TD>m</TD>
     <TD>Outer radius of sample in (x,z) plane. cylinder/sphere.
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>thickness</TD>
     <TD>m</TD>
     <TD>Thickness of hollow sample
Negative value extends the hollow volume outside of the box/cylinder.
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>xwidth</TD>
     <TD>m</TD>
     <TD>width for a box sample shape
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>yheight</TD>
     <TD>m</TD>
     <TD>Height of sample in vertical direction for box/cylinder
shapes
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>zdepth</TD>
     <TD>m</TD>
     <TD>depth for a box sample shape
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>threshold</TD>
     <TD>1</TD>
     <TD>Value under which S(Q,w) is not accounted for.
to set according to the S(Q,w) values, i.e. not too low.
</TD>
<TD ALIGN=RIGHT>1e-10</TD> </TR>
<TR> <TD>order</TD>
     <TD>1</TD>
     <TD>Limit multiple scattering up to given order
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>T</TD>
     <TD>K</TD>
     <TD>Temperature of sample, detailed balance
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>verbose</TD>
     <TD>1</TD>
     <TD>Verbosity level (0:silent, 1:normal, 2:verbose, 3:debug).
A verbosity>0 also computes dispersions and S(q,w) analysis.
</TD>
<TD ALIGN=RIGHT>1</TD> </TR>
<TR> <TD>d_phi</TD>
     <TD>deg</TD>
     <TD>scattering vertical angular spreading (usually the height
of the next component/detector). Use 0 for full space.
This is only relevant for single scattering (order=1).
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>concentric</TD>
     <TD>1</TD>
     <TD>Indicate that this component has a hollow geometry and
may contain other components. It should then be duplicated
after the inside part (only for box, cylinder, sphere) [1]
See description for an example.
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>rho</TD>
     <TD>AA-3</TD>
     <TD>Density of scattering elements (nb atoms/unit cell V_0).
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>sigma_abs</TD>
     <TD>barns</TD>
     <TD>Absorption cross-section at 2200 m/s. Use -1 to unactivate.
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>sigma_coh</TD>
     <TD>barns</TD>
     <TD>Coherent Scattering cross-section. Use -1 to unactivate.
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>sigma_inc</TD>
     <TD>barns</TD>
     <TD>Incoherent Scattering cross-section. Use -1 to unactivate.
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>classical</TD>
     <TD>1</TD>
     <TD>Assumes the S(q,w) data from the files is a classical S(q,w),
and multiply that data by exp(hw/2kT) on up/down energy sides.
Use 0 when obtained from raw experiments, 1 from molecular dynamics.
Use -1 to guess from a data set including both energy sides.
</TD>
<TD ALIGN=RIGHT>-1</TD> </TR>
<TR> <TD>powder_Dd</TD>
     <TD>1</TD>
     <TD>global Delta_d/d spreading, or 0 if ideal.
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>powder_DW</TD>
     <TD>1</TD>
     <TD>global Debey-Waller factor, if not in |F2| or 1.
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>powder_Vc</TD>
     <TD>AA^3</TD>
     <TD>volume of the unit cell
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>density</TD>
     <TD>g/cm^3</TD>
     <TD>density of material. V_rho=density/weight/1e24*N_A
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>weight</TD>
     <TD>g/mol</TD>
     <TD>atomic/molecular weight of material
</TD>
<TD ALIGN=RIGHT>0</TD> </TR>
<TR> <TD>p_interact</TD>
     <TD>1</TD>
     <TD>Force a given fraction of the beam to scatter, keeping
intensity right, to enhance small signals (-1 unactivate).
</TD>
<TD ALIGN=RIGHT>-1</TD> </TR>
<TR> <TD>norm</TD>
     <TD>1</TD>
     <TD>Normalize S(q,w) when -1 (default). Use raw data when 0,
multiply S(q,w) when norm>0.
</TD>
<TD ALIGN=RIGHT>-1</TD> </TR>
<TR> <TD>powder_barns</TD>
     <TD>1</TD>
     <TD>0 when |F2| data in powder file are fm^2, 1 when in
barns (barns=1 for laz, barns=0 for lau type files).
</TD>
<TD ALIGN=RIGHT>1</TD> </TR>
</TABLE>


<H2><A NAME=opar></A>Output parameters</H2>
<TABLE BORDER=1>
<TR><TH>Name</TH>  <TH>Unit</TH>  <TH>Description</TH> <TH>Default</TH></TR>
<TR> <TD><B>VarSqw</B></TD>
     <TD>-</TD>
     <TD>.type interaction type of event
'c' (coherent),  't' (transmitted)
'i' (incoherent) 'v' (isotropic incoherent, Vanadium-like).</TD>
<TD ALIGN=RIGHT>&nbsp;</TD> </TR>
<TR> <TD><B>columns</B></TD>
     <TD></TD> <TD></TD>
<TD ALIGN=RIGHT>&nbsp;</TD> </TR>
<TR> <TD><B>offdata</B></TD>
     <TD></TD> <TD></TD>
<TD ALIGN=RIGHT>&nbsp;</TD> </TR>
</TABLE>


<H2><A NAME=links></A>Links</H2>

<UL>
  <LI> <A HREF="Isotropic_Sqw.comp">Source code</A> for <CODE>Isotropic_Sqw.comp</CODE>.
  <LI> E. Farhi, V. Hugouvieux, M.R. Johnson, and W. Kob, Journal of Computational Physics 228 (2009) 5251-5261 "Virtual experiments: Combining realistic neutron scattering instrument and sample simulations"
  <LI> Hugouvieux V, Farhi E, Johnson MR, Physica B, 350 (2004) 151 "Virtual neutron scattering experiments"
  <LI> Hugouvieux V, PhD, University of Montpellier II, France (2004).
  <LI> H. Schober, Collection SFN 10 (2010) 159-336
  <LI> H.E. Fischer, A.C. Barnes, and P.S. Salmon. Rep. Prog. Phys., 69 (2006) 233
  <LI> P.A. Egelstaff, An Introduction to the Liquid State, 2nd Ed., Oxford Science Pub., Clarendon Press (1992).
  <LI> S. W. Lovesey, Theory of Neutron Scattering from Condensed Matter, Vol1, Oxford Science Pub., Clarendon Press (1984).
  <LI> <a href="http://www.ncnr.nist.gov/resources/n-lengths/">Cross sections for single elements</a>
  <LI> <a href="http://www.ncnr.nist.gov/resources/sldcalc.html>Cross sections for compounds</a>
  <LI> <a href="http://www.webelements.com/">Web Elements</a>
  <LI> <a href="http://www.ill.eu/sites/fullprof/index.html">Fullprof</a> powder refinement
  <LI> <a href="http://www.crystallographica.com/">Crystallographica</a> software
  <LI> Example data file <a href="../data/He4_liq_coh.sqw">He4_liq_coh.sqw</a>
  <LI> The <a href="PowderN.html">PowderN</a> component.
  <LI> The test/example instrument <a href="../examples/Test_Isotropic_Sqw.instr">Test_Isotropic_Sqw.instr</a>.
  <LI> <a href="http://www.geomview.org">Geomview and Object File Format (OFF)</a>
  <LI> Java version of Geomview (display only) <a href="http://www.holmes3d.net/graphics/roffview/">jroff.jar</a>
  <LI> <a href="http://qhull.org">qhull</a>
  <LI> <a href="http://www.cs.ucdavis.edu/~amenta/powercrust.html">powercrust</a>
</UL>
<HR>
<P ALIGN=CENTER>
 [ <A href="#id">Identification</A>
 | <A href="#desc">Description</A>
 | <A href="#ipar">Input parameters</A>
 | <A href="#opar">Output parameters</A>
 | <A href="#links">Links</A> ]
</P>

<ADDRESS>
Generated automatically by McDoc, Peter Willendrup
&lt;<A HREF="mailto:peter.willendrup@risoe.dk">peter.willendrup@risoe.dk</A>&gt; /
Wed Feb  6 09:30:36 2013</ADDRESS>
</BODY></HTML>
