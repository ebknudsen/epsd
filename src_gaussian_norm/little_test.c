#include <stdlib.h>
#include <math.h>
#include <stdio.h>

main(){
  double I0=1.0;
  double Iwh;

  double sx=1, sy=1;
  double xw,yh;

  xw=0.1;yh=0.1;
  printf("sx=%g, sy=%g, xw=%g, yh=%g yields Iwh=Í0*%g\n",sx,sy,xw,yh,erf(yh/2.0/M_SQRT2/sy)*erf(xw/2.0/M_SQRT2/sx)); 
  xw=1.0;yh=1.0;
  printf("sx=%g, sy=%g, xw=%g, yh=%g yields Iwh=Í0*%g\n",sx,sy,xw,yh,erf(yh/2.0/M_SQRT2/sy)*erf(xw/2.0/M_SQRT2/sx)); 
  xw=2.0;yh=2.0;
  printf("sx=%g, sy=%g, xw=%g, yh=%g yields Iwh=Í0*%g\n",sx,sy,xw,yh,erf(yh/2.0/M_SQRT2/sy)*erf(xw/2.0/M_SQRT2/sx)); 
  xw=10;yh=10;
  printf("sx=%g, sy=%g, xw=%g, yh=%g yields Iwh=Í0*%g\n",sx,sy,xw,yh,erf(yh/2.0/M_SQRT2/sy)*erf(xw/2.0/M_SQRT2/sx)); 

}
